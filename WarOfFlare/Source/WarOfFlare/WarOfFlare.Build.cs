﻿// DemoDreams. All rights reserved.

using UnrealBuildTool;

public class WarOfFlare : ModuleRules
{
	public WarOfFlare(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject", 
            "Engine",
            "AIModule",
            "GameFeatures",
            "ModularGameplay",
            "InputCore", 
            "HeadMountedDisplay", 
            "EnhancedInput", 
            "OnlineSubsystem", 
            "OnlineSubsystemUtils",
            "OnlineSubsystemSteam",
            "Steamworks",
            "Networking", 
            "Sockets",
            
            // Custom Modules
            "SteamConnector",
            "ElementalDamage",
            "Health",
            "Money",
            "Match",
            "Rounds",
            "Grid",
            "Minions",
            "Spawn",
            "Towers",
            "MinionExchange",
            "TowerBuilding", 
            "PlayerAbility", 
            "ObjectSelection", 
            "Survey"
        });

        DynamicallyLoadedModuleNames.AddRange(
	        new string[]
	        {
		        
	        }
        );
        
        PrivateIncludePaths.Add(ModuleDirectory);

        // Add SpaceWar steam_appid.txt for running the game without steam deploying.
        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
	        RuntimeDependencies.Add(
		        "$(ProjectDir)/Binaries/Win64/steam_appid.txt", 
		        "$(ProjectDir)/Build/Steam/SpaceWar/steam_appid.txt");
        }
        /*else if (Target.Platform == UnrealTargetPlatform.Linux)
        {
	        //RuntimeDependencies.Add("$(ProjectDir)/Binaries/Linux/steamclient.so");
	        RuntimeDependencies.Add(
		        "$(ProjectDir)/Binaries/Linux/steam_appid.txt", 
		        "$(ProjectDir)/Build/Steam/SpaceWar/steam_appid.txt");
        }*/
        
        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
