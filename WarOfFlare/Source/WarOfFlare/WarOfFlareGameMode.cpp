﻿// DemoDreams. All rights reserved.

#include "WarOfFlareGameMode.h"

#include <GameFramework/PlayerStart.h>

#include "EngineUtils.h"
#include "WarOfFlarePlayerState.h"
#include "WarOfFlareGameState.h"
#include "WarOfFlarePlayerController.h"
#include "Core/Core.h"
#include "Kismet/GameplayStatics.h"
#include "LevelInstance/LevelInstanceActor.h"
#include "Player/PlayerScene.h"

DEFINE_LOG_CATEGORY(GameModeLog);

AWarOfFlareGameMode::AWarOfFlareGameMode()
{
	PrimaryActorTick.bCanEverTick = true;

	PlayerControllerClass = AWarOfFlarePlayerController::StaticClass();
	PlayerStateClass = AWarOfFlarePlayerState::StaticClass();
	GameStateClass = AWarOfFlareGameState::StaticClass();
	
	bDelayedStart = false;
	bUseSeamlessTravel = true;
}

void AWarOfFlareGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWarOfFlareGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void AWarOfFlareGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	for (TActorIterator<ACore> CoreItr(GetWorld()); CoreItr; ++CoreItr )
		CoreItr->OnCoreBroken.RemoveDynamic(this, &AWarOfFlareGameMode::OnCoreDestroyed);
}

void AWarOfFlareGameMode::Logout(AController* Exiting)
{
	APlayerController* PlayerController = Cast<APlayerController>(Exiting);
	if (IsValid(PlayerController))
	{
		SurrenderPlayer(PlayerController);
	}
	
	Super::Logout(Exiting);
}

AActor* AWarOfFlareGameMode::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName)
{
	const UWorld* World = GetWorld();

	TArray<AActor*> PlayerScenes;
	UGameplayStatics::GetAllActorsOfClass(World, APlayerScene::StaticClass(), PlayerScenes);

	if (PlayerScenes.Num() == 0)
	{
		UE_LOG(GameModeLog, Error, TEXT("No Player Scenes"));
	}
	for (const auto& PlayerScene : PlayerScenes)
	{
		if (Cast<APlayerScene>(PlayerScene)->IsOwnedBy(Player))
		{
			TArray<AActor*> Actors;
			PlayerScene->GetAttachedActors(Actors, true, true);
			for (const auto& Actor : Actors)
			{
				if (Actor->IsA<APlayerStart>())
				{
					UE_LOG(GameModeLog, Display, TEXT("The Player Start (%p)[%s] is assigned for player [%s]"), Actor, *Actor->GetName(), *Player->GetName());
					return Actor;
				}
			}
		}
	}

	UE_LOG(GameModeLog, Warning, TEXT("The Player Start is not found for player: %s"), *Player->GetName());
	return Super::FindPlayerStart_Implementation(Player, IncomingName);
}

bool AWarOfFlareGameMode::ReadyToStartMatch_Implementation()
{
	if (bDelayedStart)
		return false;
	
	if (GetMatchState() == MatchState::WaitingToStart)
	{
		int NumPlayerControllers = UGameplayStatics::GetNumPlayerControllers(GetWorld());
		bool IsAllPlayerControllersCorrect = true;
		for (int i = 0; i < NumPlayerControllers; ++i)
		{
			APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), i);
			if (!IsValid(PlayerController) || !PlayerController->IsA(PlayerControllerClass))
			{
				IsAllPlayerControllersCorrect = false;
				break;
			}
		}

		TArray<AActor*> PlayerScenes;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerScene::StaticClass(), PlayerScenes);
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerScene::StaticClass(), PlayerScenes);
		TArray<AActor*> LevelInstances;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ALevelInstance::StaticClass(), LevelInstances);
		
		bool IsAllLevelInstancesLoaded = true;
		for (auto& Actor : LevelInstances)
		{
			ALevelInstance* LevelInstance = Cast<ALevelInstance>(Actor);
			if (!IsValid(LevelInstance) || !LevelInstance->IsLoaded())
			{
				IsAllLevelInstancesLoaded = false;
				break;
			}
		}
		
		return NumPlayers == MaxPlayersCount
			&& NumTravellingPlayers == 0
			&& NumPlayerControllers == MaxPlayersCount
			&& IsAllPlayerControllersCorrect
			&& PlayerScenes.Num() == MaxPlayersCount
			&& IsAllLevelInstancesLoaded;
	}

	return false;
}

void AWarOfFlareGameMode::InitializeCores()
{
	//TArray<EElementalDamageType> ElementalTypes;
	//for (EElementalDamageType ElementalType : TEnumRange<EElementalDamageType>())
	//{
	//	ElementalTypes.Add(ElementalType);
	//}
	
	for (TActorIterator<ACore> CoreItr(GetWorld()); CoreItr; ++CoreItr )
	{
		// Set core element
		//const int32 Index = FMath::RandRange(0, ElementalTypes.Num() - 1);
		//CoreItr->SetElement(ElementalTypes[Index]);
		//ElementalTypes.RemoveAt(Index);
		
		// Set core broken event
		CoreItr->OnCoreBroken.AddUniqueDynamic(this, &AWarOfFlareGameMode::OnCoreDestroyed);
	}
}

void AWarOfFlareGameMode::DeinitializeCores()
{
	for (TActorIterator<ACore> CoreItr(GetWorld()); CoreItr; ++CoreItr )
	{
		CoreItr->OnCoreBroken.RemoveDynamic(this, &AWarOfFlareGameMode::OnCoreDestroyed);
	}
}

void AWarOfFlareGameMode::StartMatch()
{
	InitializeCores();

	for (auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		AssignPlayerSceneFor(It->Get());
		RestartPlayer(It->Get());
	}
	
	URoundControllerComponent* RoundController = this->GameState->GetComponentByClass<URoundControllerComponent>();
	if (IsValid(RoundController))
		RoundController->SetActive(true);

	Super::StartMatch();
}

void AWarOfFlareGameMode::EndMatch()
{
	URoundControllerComponent* RoundController = this->GameState->GetComponentByClass<URoundControllerComponent>();
	if (IsValid(RoundController))
		RoundController->SetActive(false);

	GetWorld()->GetTimerManager().SetTimer(
		EndGameTimer,
		this,
		&AWarOfFlareGameMode::MoveToMenu,
		TimeFromEndMatchToLeaveMap,
		false);
	
	Super::EndMatch();
}

APlayerScene* AWarOfFlareGameMode::FindSceneFor(APlayerController* NewPlayer) const
{
	TArray<AActor*> PlayerScenes;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerScene::StaticClass(), PlayerScenes);
	for (const auto& PlayerScene : PlayerScenes)
	{
		UE_LOG(GameModeLog, Display, TEXT("Player scene found (%s): %s"), *NewPlayer->GetName(), *PlayerScene->GetName());
		
		if (PlayerScene->GetOwner() == NewPlayer || PlayerScene->GetOwner() == nullptr)
			return Cast<APlayerScene>(PlayerScene);
	}
	
	return nullptr;
}

void AWarOfFlareGameMode::AssignPlayerSceneFor(APlayerController* NewPlayer) const
{
	APlayerScene* PlayerScene = FindSceneFor(NewPlayer);
	if (!IsValid(PlayerScene)) // ToDo: Assign Player as Spectetor?
	{
		UE_LOG(GameModeLog, Display, TEXT("Scene is invalid for player for player %s"), *NewPlayer->GetName());
		return;
	}
	
	PlayerScene->SetOwner(NewPlayer);
	UE_LOG(GameModeLog, Display, TEXT("Assigned %s for %p"), *NewPlayer->GetName(), PlayerScene);
	APlayerState* Player = NewPlayer->PlayerState;
	AWarOfFlarePlayerState* WarOfFlarePlayerState = Cast<AWarOfFlarePlayerState>(Player);
	if (!IsValid(WarOfFlarePlayerState))
		return;

	WarOfFlarePlayerState->PlayerScene = PlayerScene;
}

void AWarOfFlareGameMode::OnCoreDestroyed(const ACore* Core)
{
	const APlayerController* CoreOwner = Cast<APlayerController>(Core->GetNetOwner());
	
	if (!IsValid(CoreOwner))
		return;
	
	SurrenderPlayer(CoreOwner);
	
	EndMatch();
}

void AWarOfFlareGameMode::MoveToMenu()
{
	GetWorld()->ServerTravel("MainMenu?listen");
}

void AWarOfFlareGameMode::SurrenderPlayer(const APlayerController* Player)
{
	if (MatchState != MatchState::InProgress)
		return;
	
	DeinitializeCores();
	
	if (IsValid(GetWorld()))
	{
		for (auto PlayerState : GameState->PlayerArray)
		{
			UMatchResultComponent* MatchResult = PlayerState->GetComponentByClass<UMatchResultComponent>();
			if (!IsValid(MatchResult) || MatchResult->GetMatchResult() != EMatchResult::Unknown)
				continue;
			
			MatchResult->SetMatchResult(PlayerState == Player->PlayerState ? EMatchResult::Lose : EMatchResult::Win);
		}
	}
	
	EndMatch();
}
