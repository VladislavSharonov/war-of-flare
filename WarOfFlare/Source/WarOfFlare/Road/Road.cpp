// DemoDreams. All rights reserved.

#include "Road.h"

#include "Net/UnrealNetwork.h"
#include "Player/PlayerScene.h"

ARoad::ARoad()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
}

void ARoad::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ARoad, IsGridVisible);
}

void ARoad::SetGridVisible_Implementation(const bool IsVisible)
{
	IsGridVisible = IsVisible;
}

void ARoad::SetAllRoadGridVisible(UObject* Context, const bool bIsVisible)
{
	TArray<AActor*> Roads;
	UGameplayStatics::GetAllActorsOfClass(Context, ARoad::StaticClass(), Roads);

	for (const auto& Road : Roads)
		Cast<ARoad>(Road)->SetGridVisible(bIsVisible);
}

void ARoad::SetRoadGridVisibleForPlayer(APlayerController* Player, const bool bIsVisible)
{
	if (!IsValid(Player))
		return;

	const APlayerScene* PlayerScene = APlayerScene::GetPlayerSceneForPlayer(Player);
	if (!IsValid(PlayerScene))
	{
		UE_LOG(LogTemp, Warning, TEXT("SetRoadGridVisibleForPlayer: APlayerScene was not found for player %s."), *Player->GetName());
		return;
	}
	
	TArray<AActor*> Roads;
	PlayerScene->GetAllActorsOfClass(StaticClass(), Roads);

	for (const auto& Road : Roads)
		Cast<ARoad>(Road)->SetGridVisible(bIsVisible);
}

void ARoad::OnRep_IsGridVisible(bool OldVisible)
{
	SetGridVisible(IsGridVisible);
}
