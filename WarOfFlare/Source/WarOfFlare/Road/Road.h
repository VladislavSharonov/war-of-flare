﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"

#include "Road.generated.h"

UCLASS()
class WAROFFLARE_API ARoad : public AActor
{
	GENERATED_BODY()

public:
	ARoad();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Road")
	void SetGridVisible(bool IsVisible);

	UFUNCTION(BlueprintCallable, BlueprintCosmetic, meta = (WorldContext = "Context"), Category = "Road")
	static void SetAllRoadGridVisible(UObject* Context, bool bIsVisible);

	UFUNCTION(BlueprintCallable, BlueprintCosmetic)
	static void SetRoadGridVisibleForPlayer(APlayerController* Player, bool bIsVisible);

protected:
	UFUNCTION(BlueprintCallable)
	void OnRep_IsGridVisible(bool OldVisible);
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing=OnRep_IsGridVisible)
	bool IsGridVisible = false;
};
