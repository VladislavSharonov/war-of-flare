﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"

#include "Round/RoundControllerComponent.h"
#include "Grid/GridComponent.h"

#include "WarOfFlareGameState.generated.h"

class AWarOfFlarePlayerController;
class AWarOfFlarePlayerState;

UCLASS()
class WAROFFLARE_API AWarOfFlareGameState : public AGameState
{
	GENERATED_BODY()

public:
	AWarOfFlareGameState();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	UPROPERTY(BlueprintReadOnly, Category = "Components")
	TObjectPtr<USceneComponent> DefaultRootComponent;
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Replicated, Category = "Components")
	TObjectPtr<URoundControllerComponent> RoundController;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Replicated, Category = "Components")
	TObjectPtr<UGridComponent> Grid;
};
