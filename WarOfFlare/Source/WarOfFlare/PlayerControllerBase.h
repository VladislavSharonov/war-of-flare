﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerControllerBase.generated.h"

UCLASS()
class WAROFFLARE_API APlayerControllerBase : public APlayerController
{
	GENERATED_BODY()
public:
	virtual void ClientWasKicked_Implementation(const FText& KickReason) override;
};
