﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "ElementalDamage/ElementalDamageType.h"
#include "GameFramework/Actor.h"
#include "Health/HealthComponent.h"

#include "Core.generated.h"

class ACore;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCoreBrokenSignature, const ACore*, Core);

UCLASS()
class WAROFFLARE_API ACore : public AActor
{
	GENERATED_BODY()
	
public:
	ACore();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHealthSpent();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	void SetElement(const EElementalDamageType NewElement);
	
	UFUNCTION(BlueprintCallable)
	void OnRep_Element();
	
public:
	UPROPERTY(BlueprintAssignable)
	FCoreBrokenSignature OnCoreBroken;

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Core")
	TObjectPtr<USceneComponent> DefaultRootComponent;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Replicated, Category = "Core")
	TObjectPtr<UHealthComponent> Health;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Tower|Attack")
	void OnElementChanged();
	virtual void OnElementChanged_Implementation() {}
	
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_Element, Category = "Core")
	EElementalDamageType Element = EElementalDamageType::Base;
};
