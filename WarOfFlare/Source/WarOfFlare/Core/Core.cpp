﻿// DemoDreams. All rights reserved.

#include "Core/Core.h"

#include "Net/UnrealNetwork.h"

#include "Health/HealthComponent.h"

ACore::ACore()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);
	
	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
	Health->SetIsReplicated(true);

	Health->SetMaxHealth(1000);
	Health->SetHealth(Health->GetMaxHealth());

	bReplicates = true;
	bAlwaysRelevant = true;
}

void ACore::BeginPlay()
{
	Super::BeginPlay();
	
	Health->OnHealthSpent.AddDynamic(this, &ACore::OnHealthSpent);
}

void ACore::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Health->OnHealthSpent.RemoveDynamic(this, &ACore::OnHealthSpent);
	
	Super::EndPlay(EndPlayReason);
}

void ACore::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ACore, Health);
	DOREPLIFETIME(ACore, Element);
}

void ACore::SetElement(const EElementalDamageType NewElement)
{
	Element = NewElement;
	OnElementChanged();
}

void ACore::OnHealthSpent_Implementation()
{
	OnCoreBroken.Broadcast(this);
}

void ACore::OnRep_Element()
{
	OnElementChanged();
}
