// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CheatManager.h"
#include "GlobalCheatManager.generated.h"

UCLASS()
class WAROFFLARE_API UGlobalCheatManager : public UCheatManager
{
	GENERATED_BODY()

public:
	UFUNCTION( exec )
	void SkipCurrentStage() const;
};
