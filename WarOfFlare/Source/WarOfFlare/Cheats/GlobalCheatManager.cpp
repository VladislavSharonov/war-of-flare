// DemoDreams. All rights reserved.


#include "GlobalCheatManager.h"

#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "Round/RoundControllerComponent.h"

void UGlobalCheatManager::SkipCurrentStage() const
{
	const AGameStateBase* GameState = UGameplayStatics::GetGameState(GetWorld());
	URoundControllerComponent* RoundController = GameState->GetComponentByClass<URoundControllerComponent>();

	RoundController->MoveToNextStage();
}
