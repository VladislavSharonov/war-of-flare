﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Core.h"
#include "GameFramework/GameMode.h"
#include "Net/UnrealNetwork.h"

#include "MinionExchange/MinionExchangeComponent.h"

#include "WarOfFlareGameMode.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(GameModeLog, Log, All);

class AWarOfFlarePlayerState;
class AWarOfFlareGameState;
class AWarOfFlarePlayerController;

UCLASS()
class WAROFFLARE_API AWarOfFlareGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AWarOfFlareGameMode();

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Logout(AController* Exiting) override;
	
	UFUNCTION()
	APlayerScene* FindSceneFor(APlayerController* NewPlayer) const;

	UFUNCTION()
	void AssignPlayerSceneFor(APlayerController* NewPlayer) const;

	virtual AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName) override;

	virtual bool ReadyToStartMatch_Implementation() override;
	
	UFUNCTION()
	void InitializeCores();
	
	UFUNCTION()
	void DeinitializeCores();
	
	virtual void StartMatch() override;

	virtual void EndMatch() override;

	
public:
	UFUNCTION(BlueprintCallable)
	void SurrenderPlayer(const APlayerController* Player);
	
#pragma region Game Ending
protected:
	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void OnCoreDestroyed(const ACore* Core = nullptr);

	UPROPERTY(BlueprintReadOnly, EditInstanceOnly)
	FTimerHandle EndGameTimer;
	
	UPROPERTY(BlueprintReadOnly, EditInstanceOnly)
    float TimeFromEndMatchToLeaveMap = 4;
    	
	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void MoveToMenu();
#pragma endregion
	
protected:
	const static uint8 MaxPlayersCount = 2;
};
