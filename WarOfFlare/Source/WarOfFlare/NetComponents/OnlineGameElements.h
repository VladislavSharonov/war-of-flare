﻿//
//#pragma once
//
//#include "CoreMinimal.h"
//
//#include "Engine/GameInstance.h"
//#include "Interfaces/OnlineSessionInterface.h"
//#include "OnlineGameElements.generated.h"
//
//class IOnlineSubsystem;
//
//UCLASS()
//class WAROFFLARE_API UOnlineGameElements : public UGameInstance
//{
//	GENERATED_BODY()
//
//public:
//	UOnlineGameElements();
//
//	virtual void Init() override;
//
//	TSharedPtr<FOnlineSessionSearch> SearchSettings;
//	TArray<FOnlineSessionSearchResult> Sessions;
//	UFUNCTION(BlueprintCallable)
//	void Login();
//	void OnLoginComplete(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& UserId, const FString& Error);
//
//	UFUNCTION(BlueprintCallable)
//	void CreateSession(bool bIsDedicated, bool bShouldAdvertise, bool bIsLANMatch, int32 NumPublicConnections,
//		bool bAllowJoinInProgress, bool bAllowJoinViaPresence, bool bUsesPresence, bool bUseLobbiesIfAvailable);
//	void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);
//
//	UFUNCTION(BlueprintCallable)
//	void DestroySession();
//	void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);
//
//	UFUNCTION(BlueprintCallable)
//	void FindFirstSession();
//	void OnFindFirstSessionComplete(bool bWasSuccessful);
//
//	UFUNCTION(BlueprintCallable)
//	void FindAllSessions();
//	void OnFindAllSessionsComplete(bool bWasSuccessful);
//
//	UFUNCTION(BlueprintCallable)
//	void JoinSessionByIndex(int32 SessionIndex);
//	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
//
//	UFUNCTION(BlueprintCallable)
//	void GetAllFriends();
//	void OnGetAllFriendsComplete(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName, const FString& ErrorStr);
//
//	UFUNCTION(BlueprintCallable)
//	void ShowFriendsList();
//
//	UFUNCTION(BlueprintCallable)
//	void ShowInviteUI();
//
//
//	UFUNCTION(BlueprintCallable)
//	int32 GetNumOfSessions();
//
//	UFUNCTION(BlueprintCallable)
//	FString GetSessionID(int32 SessionIndex);
//
//	UFUNCTION(BlueprintCallable)
//	FString GetSessionOwnerName(int32 SessionIndex);
//
//	UFUNCTION(BlueprintCallable)
//	FString GetPlayerName();
//
//	UFUNCTION(BlueprintCallable)
//	int32 GetSessionPing(int32 SessionIndex);
//
//	UFUNCTION(BlueprintCallable)
//	int32 GetSessionMaxSlots(int32 SessionIndex);
//
//	
//protected:
//	IOnlineSubsystem* OnlineSubsystem;
//
//	bool bIsLoggedIn;
//	
//};
