// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "ThirdParty/Steamworks/Steamv153/sdk/public/steam/steam_api.h"
#include "Engine/GameInstance.h"
#include "Engine/Texture2D.h"
#include "Interfaces/OnlineSessionDelegates.h"
#include "WarOfFlareGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSessionParticipantsChangeSignature);
UCLASS()
class WAROFFLARE_API UWarOfFlareGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	
	UWarOfFlareGameInstance();
	FName DefaultSessionName = NAME_GameSession;

	bool SessionDetected = false;

	UPROPERTY(BlueprintAssignable)
	FOnSessionParticipantsChangeSignature OnSessionParticipantsChange;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int LobbySize = 2;
	
	virtual void Init() override;
	virtual void Shutdown() override;
	
	virtual void HandleCreateSessionComplete(FName Session, bool Succeeded);
	virtual void HandleDestroySessionComplete(FName Name, bool bArg);
	virtual void HandleJoinSessionComplete(FName Session, EOnJoinSessionCompleteResult::Type Result);
	virtual void HandleSessionUserInviteAccepted(const bool bWasSuccessful, const int32 ControllerId, FUniqueNetIdPtr UserId, const FOnlineSessionSearchResult& InviteResult);
	virtual void HandleSessionParticipantsChange(FName Session, const FUniqueNetId& UniqueId, bool bJoined);
	virtual void HandleRegisterPlayersComplete(FName Session, const TArray<FUniqueNetIdRef>& Players, bool bWasSuccessful);
	virtual void HandleUnregisterPlayersComplete(FName Name, const TArray<TSharedRef<const FUniqueNetId>>& Shareds, bool bArg);
	
	UFUNCTION(BlueprintCallable)
	void CreateOnlineSession();
	
	UFUNCTION(BlueprintCallable)
	void TransferServer(TSoftObjectPtr<UWorld> Level);
	
	UFUNCTION(BlueprintCallable)// Unused
	void TransferClientToMainMenu();
	
	void DestroyOnlineSession(FName SessionName, const FOnDestroySessionCompleteDelegate& CompletionDelegate = FOnDestroySessionCompleteDelegate());

	UFUNCTION(BlueprintCallable, BlueprintCosmetic)
	void LeaveLobby();
	
	////////////
	UFUNCTION(BlueprintPure)
	static bool IsOnlineSubsystemConnected();

	UFUNCTION(BlueprintPure)
	static bool IsSteamConnected();	
	
	UFUNCTION(BlueprintPure)
	bool CanStartGame() const;
	
	UFUNCTION(BlueprintPure)
	bool IsPartyFull() const;
	
	UFUNCTION(BlueprintPure)
	bool IsSessionOwner() const;
	
	UFUNCTION(BlueprintPure)
	int32 GetLobbySize() const;
	
	UFUNCTION(BlueprintPure)
	int32 GetMaxLobbySize() const { return LobbySize; }
	///////////
	
	UFUNCTION(BlueprintCallable)
	void StartGame(TSoftObjectPtr<UWorld> Level);
	
	UFUNCTION(BlueprintCallable)
	void OpenSteamOverlayInviteDialog();
	
	UFUNCTION(BlueprintCallable)
	UTexture2D* GetPlayerAvatar(int TargetPlayer);
	
#pragma region Steam
public:
	UTexture2D* GetSteamFriendAvatar(const uint64 UniqueNetId);
	uint64 CastToSteamID(FUniqueNetIdRef InUniqueNetId);
	
private:
	STEAM_CALLBACK(UWarOfFlareGameInstance, OnAvatarImageLoaded, AvatarImageLoaded_t);
#pragma endregion
};
