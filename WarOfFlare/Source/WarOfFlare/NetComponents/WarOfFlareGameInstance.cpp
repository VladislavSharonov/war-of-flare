// DemoDreams. All rights reserved.

#include "NetComponents/WarOfFlareGameInstance.h"
#include "Interfaces/OnlineExternalUIInterface.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "OnlineSubsystemSteam.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Kismet/GameplayStatics.h"
#include "ThirdParty/Steamworks/Steamv153/sdk/public/steam/steam_api.h"
#include "Engine/World.h"
#include "Engine/Texture2D.h"

UWarOfFlareGameInstance::UWarOfFlareGameInstance(){}

void UWarOfFlareGameInstance::Init()
{
	Super::Init();
	EnableListenServer(true);
	
	if (IOnlineSubsystem::Get())
	{
		IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleCreateSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleJoinSessionComplete);
			SessionInterface->OnSessionUserInviteAcceptedDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleSessionUserInviteAccepted);
			SessionInterface->OnSessionParticipantsChangeDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleSessionParticipantsChange);
			SessionInterface->OnRegisterPlayersCompleteDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleRegisterPlayersComplete);
			SessionInterface->OnUnregisterPlayersCompleteDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleUnregisterPlayersComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UWarOfFlareGameInstance::HandleDestroySessionComplete);
			CreateOnlineSession();
		}
	}
}

void UWarOfFlareGameInstance::Shutdown()
{
	LeaveLobby();
	Super::Shutdown();
}

void UWarOfFlareGameInstance::HandleCreateSessionComplete(FName Session, bool Succeeded)
{
	if (Succeeded)
	{
		IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();

		UE_LOG(LogTemp, Warning, TEXT("Session Created"));
		EnableListenServer(true);
		
		// Register Listen Server player.
		const UWorld* World = GetWorld();
		if (!World || !World->IsGameWorld())
			return;
		
		if (!IsDedicatedServerInstance())
		{
			const IOnlineIdentityPtr OnlineIdentity = IOnlineSubsystem::Get()->GetIdentityInterface();
			if (OnlineIdentity.Get() == nullptr)
				return;

			SessionInterface->RegisterPlayer(
				Session,
				*OnlineIdentity->GetUniquePlayerId(0),
				true);
		}
		else
		{
			UE_LOG(LogTemp, Display, TEXT("Listen Server player cannot be registered. It is dedicated server."));
		}
	}
	UE_LOG(LogTemp, Display, TEXT("Session created: \"%s\", Succeeded = %d"), *Session.ToString(), Succeeded);
}

void UWarOfFlareGameInstance::HandleDestroySessionComplete(FName Name, bool bArg)
{
	UE_LOG(LogTemp, Display, TEXT("Session destroyed: %s"), *Name.ToString());
}

void UWarOfFlareGameInstance::HandleJoinSessionComplete(FName Session, EOnJoinSessionCompleteResult::Type Result)
{
	FString ConnectionInfo = FString();
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	switch (Result)
	{
	case EOnJoinSessionCompleteResult::Success:
		if (!SessionInterface->GetResolvedConnectString(Session, ConnectionInfo))
		{
			UE_LOG(LogTemp, Log, TEXT("Couldn't get connect string"));
			return;
		}
		if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0))
		{
			PlayerController->ClientTravel(ConnectionInfo, ETravelType::TRAVEL_Absolute);
			UE_LOG(LogTemp, Log, TEXT("Joined lobby."));
		}
		break;
		
	default:
		UE_LOG(LogTemp, Log, TEXT("Joining session failed"));
		CreateOnlineSession();
		break;
	}
}

void UWarOfFlareGameInstance::HandleSessionUserInviteAccepted(const bool bWasSuccessful, const int32 ControllerId, FUniqueNetIdPtr UserId, const FOnlineSessionSearchResult& InviteResult)
{
	if (!bWasSuccessful)
		return;
	
	if (InviteResult.IsValid())
	{
		TDelegate<void(FName, bool)> OnDestroySessionComplete;
		OnDestroySessionComplete.BindLambda(
			[this, Result = FOnlineSessionSearchResult(InviteResult)](FName Name, bool bWasSuccessful)
			{
				IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
				SessionInterface->JoinSession(0, DefaultSessionName, Result);
			}
		);
		DestroyOnlineSession(DefaultSessionName, OnDestroySessionComplete);
	}
	else
	{
		UE_LOG(LogOnline, Warning, TEXT("Invite accept returned no search result."));
    }
}

void UWarOfFlareGameInstance::HandleSessionParticipantsChange(FName Session, const FUniqueNetId& UniqueId, bool bJoined)
{
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	if (bJoined)
		SessionInterface->RegisterPlayer( Session, UniqueId, true);
	else
		SessionInterface->UnregisterPlayer(Session, UniqueId);

	OnSessionParticipantsChange.Broadcast();
}

void UWarOfFlareGameInstance::HandleRegisterPlayersComplete(FName Session, const TArray<FUniqueNetIdRef>& Players,
	bool bWasSuccessful)
{
	for (const auto & Player : Players)
	{
		UE_LOG(LogTemp, Display, TEXT("Players registered: %s"), *Player->ToString());
	}
	OnSessionParticipantsChange.Broadcast();
}

void UWarOfFlareGameInstance::HandleUnregisterPlayersComplete(FName Name,
	const TArray<TSharedRef<const FUniqueNetId>>& Shareds, bool bArg)
{
	for (const auto & Player : Shareds)
	{
		UE_LOG(LogTemp, Display, TEXT("Player unregistered: %s"), *Player->ToString());
	}
	OnSessionParticipantsChange.Broadcast();
}

void UWarOfFlareGameInstance::CreateOnlineSession()
{
	UE_LOG(LogTemp, Warning, TEXT("Create Server"));
	FOnlineSessionSettings SessionSettings;
	SessionSettings.bAllowJoinInProgress = true;
	SessionSettings.bIsDedicated = false;
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.bUsesPresence = true;
	SessionSettings.NumPublicConnections = LobbySize;
	SessionSettings.bUseLobbiesIfAvailable = true;
	SessionSettings.bIsLANMatch = false;
	
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	SessionInterface->CreateSession(0, DefaultSessionName, SessionSettings);
}

void UWarOfFlareGameInstance::TransferServer(TSoftObjectPtr<UWorld> Level)
{
	GetWorld()->ServerTravel("WarOfFlare?listen");
}

void UWarOfFlareGameInstance::TransferClientToMainMenu()
{
	if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		PlayerController->ClientTravel("MainMenu?listen", ETravelType::TRAVEL_Absolute);
		UE_LOG(LogTemp, Log, TEXT("Client transfered"));
	}
}

void UWarOfFlareGameInstance::DestroyOnlineSession(FName SessionName, const FOnDestroySessionCompleteDelegate& CompletionDelegate)
{
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	FNamedOnlineSession* Session = SessionInterface->GetNamedSession(SessionName);
	if (Session == nullptr)
		return;
	
	SessionInterface->DestroySession(DefaultSessionName, CompletionDelegate);
	TransferClientToMainMenu();
}

void UWarOfFlareGameInstance::LeaveLobby()
{
	if (IsSessionOwner() && GetLobbySize() > 1)
	{
		int NumberPlayerControllers = UGameplayStatics::GetNumPlayerControllers(GetWorld());
		for (int i = 1; i < NumberPlayerControllers; ++i)
		{
			APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), i);
			PlayerController->ClientWasKicked(FText());
		}
	}
	else
	{
		TDelegate<void(FName, bool)> OnDestroySessionComplete;
		OnDestroySessionComplete.BindLambda(
			[this](FName Name, bool bWasSuccessful)
			{
				CreateOnlineSession();
			}
		);
	
		DestroyOnlineSession(DefaultSessionName, OnDestroySessionComplete);
	}
}

bool UWarOfFlareGameInstance::IsOnlineSubsystemConnected()
{
	return IOnlineSubsystem::Get() != nullptr
		&& IOnlineSubsystem::Get()->GetSubsystemName() != NULL_SUBSYSTEM;
}

bool UWarOfFlareGameInstance::IsSteamConnected()
{
	return IOnlineSubsystem::Get() != nullptr
		&& IOnlineSubsystem::Get()->GetSubsystemName() == STEAM_SUBSYSTEM;
}

bool UWarOfFlareGameInstance::CanStartGame() const
{
	return IsPartyFull() && IsSessionOwner();
}

bool UWarOfFlareGameInstance::IsPartyFull() const
{
	return GetLobbySize() == LobbySize;
}

bool UWarOfFlareGameInstance::IsSessionOwner() const
{
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	FNamedOnlineSession* Session = SessionInterface->GetNamedSession(DefaultSessionName);
	
	if (Session == nullptr)
	{
		return false;
	}
	IOnlineIdentityPtr OnlineIdentity = IOnlineSubsystem::Get()->GetIdentityInterface();
	if (OnlineIdentity == nullptr)
	{
		return false;
	}
	FUniqueNetIdPtr LocalNetId = OnlineIdentity->GetUniquePlayerId(0);
	return Session->OwningUserId.IsValid() && *Session->OwningUserId == *LocalNetId.Get();
}

int32 UWarOfFlareGameInstance::GetLobbySize() const
{
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	FNamedOnlineSession* Session = SessionInterface->GetNamedSession(DefaultSessionName);
	if (Session == NULL)
		return 0;
	
	return Session->RegisteredPlayers.Num();
}

void UWarOfFlareGameInstance::StartGame(TSoftObjectPtr<UWorld> Level)
{
	if (!CanStartGame())
		return;

	TransferServer(Level);
}

void UWarOfFlareGameInstance::OpenSteamOverlayInviteDialog()
{
	IOnlineExternalUIPtr UIInterface = IOnlineSubsystem::Get()->GetExternalUIInterface();
	if (UIInterface.Get() == nullptr)
	{
		UE_LOG(LogTemp,Error,TEXT("IOnlineExternalUI is undefined"));
		return;
	}
	UIInterface.Get()->ShowInviteUI(0, DefaultSessionName);
}

UTexture2D* UWarOfFlareGameInstance::GetPlayerAvatar(int TargetPlayer)
{
	if (!IsSteamConnected())
		return nullptr;
	
	IOnlineSessionPtr SessionInterface = IOnlineSubsystem::Get()->GetSessionInterface();
	FNamedOnlineSession* Session = SessionInterface->GetNamedSession(DefaultSessionName);
	if (Session == nullptr)
	{
		UE_LOG(LogTemp,Error,TEXT("GetPlayerAvatar: session is invalid"));
		return nullptr;
	}
	if (TargetPlayer >= Session->RegisteredPlayers.Num() || TargetPlayer < 0)
	{
		UE_LOG(
			LogTemp,
			Error,
			TEXT("GetPlayerAvatar: Player Index out of range: %d(index) out of %d(size)"),
			TargetPlayer,
			Session->RegisteredPlayers.Num());
		return nullptr;
	}
	const FUniqueNetIdRef& PlayerId = Session->RegisteredPlayers[TargetPlayer];
	return GetSteamFriendAvatar(CastToSteamID(PlayerId));
}

#pragma region Steam
void UWarOfFlareGameInstance::OnAvatarImageLoaded(AvatarImageLoaded_t* AvatarImageLoaded)
{
	OnSessionParticipantsChange.Broadcast();
}

UTexture2D* UWarOfFlareGameInstance::GetSteamFriendAvatar(const uint64 UniqueNetId)
{
	uint32 Width = 0;
	uint32 Height = 0;

	int Picture = 0;

	Picture = SteamFriends()->GetLargeFriendAvatar(UniqueNetId);
	
	if (Picture <= 0)
	{
		UE_LOG(LogTemp,Error,TEXT("Player avatar isn't loaded"));
		return nullptr;
	}
	if (Picture == 0)
	{
		UE_LOG(LogTemp,Error,TEXT("Player avatar isn't set"));
		return nullptr;
	}
	
	SteamUtils()->GetImageSize(Picture, &Width, &Height);

	if (Width > 0 && Height > 0)
	{
		uint8 *oAvatarRGBA = new uint8[Width * Height * 4];

		SteamUtils()->GetImageRGBA(Picture, (uint8*)oAvatarRGBA, 4 * Height * Width * sizeof(char));

		UTexture2D* Avatar = UTexture2D::CreateTransient(Width, Height, PF_R8G8B8A8);

		uint8* MipData = (uint8*)Avatar->GetPlatformData()->Mips[0].BulkData.Lock(LOCK_READ_WRITE);

		FMemory::Memcpy(MipData, (void*)oAvatarRGBA, Height * Width * 4);

		Avatar->GetPlatformData()->Mips[0].BulkData.Unlock();
		
		delete[] oAvatarRGBA;

		Avatar->GetPlatformData()->SetNumSlices(1);
		Avatar->NeverStream = true;

		Avatar->UpdateResource();

		return Avatar;
	}
	return NULL;
}

uint64 UWarOfFlareGameInstance::CastToSteamID(FUniqueNetIdRef InUniqueNetId)
{
	return FCString::Atoi64(*InUniqueNetId->ToString());
}
#pragma endregion