﻿//
//
//
//#include "OnlineGameElements.h"
//
//#include "Interfaces/OnlineSessionInterface.h"
//#include "Interfaces/OnlineIdentityInterface.h"
//#include "OnlineSubsystem.h"
//#include "OnlineSessionSettings.h"
//#include "Interfaces/OnlineExternalUIInterface.h"
//#include "Interfaces/OnlineFriendsInterface.h"
//#include "Kismet/GameplayStatics.h"
//#include "Templates/SharedPointer.h"
//#include "Online.h"
//
//
//const FName SteamSessionName = FName(TEXT("GameSession"));
//
//UOnlineGameElements::UOnlineGameElements()
//{
//	bIsLoggedIn = false;
//}
//
//void UOnlineGameElements::Init()
//{
//	Super::Init();
//
//	OnlineSubsystem = IOnlineSubsystem::Get();
//
//	Login();
//}
//
//void UOnlineGameElements::Login()
//{
//
//	if (OnlineSubsystem)
//	{
//		if (IOnlineIdentityPtr Identity = OnlineSubsystem->GetIdentityInterface())
//		{
//			FOnlineAccountCredentials Credentials;
//			Credentials.Id = FString("127.0.0.1:8081");
//			Credentials.Token = FString("Seb");
//			Credentials.Type = FString("developer");
//			/*Credentials.Id = FString();
//			Credentials.Token = FString();
//			Credentials.Type = FString("accountportal");*/
//
//			Identity->OnLoginCompleteDelegates->AddUObject(this, &UOnlineGameElements::OnLoginComplete);
//			Identity->Login(0, Credentials);
//		}
//	}
//}
//
//void UOnlineGameElements::OnLoginComplete(int32 LocalUserNum, bool bWasSuccessful, const FUniqueNetId& UserId,
//	const FString& Error)
//{
//	UE_LOG(LogTemp, Warning, TEXT("Login Success: %d"), bWasSuccessful);
//
//	bIsLoggedIn = bWasSuccessful;
//
//	if (OnlineSubsystem)
//	{
//		if (IOnlineIdentityPtr Identity = OnlineSubsystem->GetIdentityInterface())
//		{
//			Identity->ClearOnLoginCompleteDelegates(0, this);
//		}
//	}
//}
//
//void UOnlineGameElements::CreateSession(bool bIsDedicated, bool bShouldAdvertise, bool bIsLANMatch, int32 NumPublicConnections,
//	bool bAllowJoinInProgress, bool bAllowJoinViaPresence, bool bUsesPresence, bool bUseLobbiesIfAvailable)
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
//			{
//				FOnlineSessionSettings SessionSettings;
//
//				/*SessionSettings.bIsDedicated = bIsDedicated;
//				SessionSettings.bShouldAdvertise = bShouldAdvertise;
//				SessionSettings.bIsLANMatch = bIsLANMatch;
//				SessionSettings.NumPublicConnections = NumPublicConnections;
//				SessionSettings.bAllowJoinInProgress = bAllowJoinInProgress;
//				SessionSettings.bAllowJoinViaPresence = bAllowJoinViaPresence;
//				SessionSettings.bUsesPresence = bUsesPresence;
//				SessionSettings.bUseLobbiesIfAvailable = bUseLobbiesIfAvailable;*/
//
//				/*SessionSettings.bIsDedicated = false;
//				SessionSettings.bShouldAdvertise = true;
//				SessionSettings.bIsLANMatch = false;
//				SessionSettings.NumPublicConnections = 5;
//				SessionSettings.bAllowJoinInProgress = true;
//				SessionSettings.bAllowJoinViaPresence = true;
//				SessionSettings.bUsesPresence = true;
//				SessionSettings.bUseLobbiesIfAvailable = true;*/
//
//				SessionSettings.NumPublicConnections = 5; //We will test our sessions with 2 players to keep things simple
//				SessionSettings.bShouldAdvertise = true; //This creates a public match and will be searchable.
//				SessionSettings.bUsesPresence = true;   //No presence on dedicated server. This requires a local user.
//				SessionSettings.bAllowJoinViaPresence = true;
//				SessionSettings.bAllowJoinViaPresenceFriendsOnly = false;
//				SessionSettings.bAllowInvites = true;    //Allow inviting players into session. This requires presence and a local user. 
//				SessionSettings.bAllowJoinInProgress = true; //Once the session is started, no one can join.
//				SessionSettings.bIsDedicated = false; //Session created on dedicated server.
//				SessionSettings.bUseLobbiesIfAvailable = true;
//				SessionSettings.bIsLANMatch = false;
//
//				SessionSettings.Set(SEARCH_KEYWORDS, FString("Lobby_Gipfel"),
//					EOnlineDataAdvertisementType::ViaOnlineService);
//
//				SessionPtr->OnCreateSessionCompleteDelegates.AddUObject(
//					this, &UOnlineGameElements::OnCreateSessionComplete);
//				SessionPtr->CreateSession(0, SteamSessionName, SessionSettings);
//
//			}
//		}
//	}
//	else
//	{
//		UE_LOG(LogTemp, Error, TEXT("Connot create session, not logged in"));
//	}
//}
//
//void UOnlineGameElements::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
//{
//	UE_LOG(LogTemp, Warning, TEXT("Successfully created session: %d"), bWasSuccessful);
//
//	if (OnlineSubsystem)
//	{
//		IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//
//		SessionPtr->ClearOnCreateSessionCompleteDelegates(this);
//		// GameplayStatics::OpenLevel(GetWorld(), "TestMap", true, "listen");
//		GetWorld()->ServerTravel(FString("TestMap?listen"));
//	}
//}
////TODO:добавить перед созданием новой сесии и при подключении к др
//void UOnlineGameElements::DestroySession()
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
//			{
//				SessionPtr->OnDestroySessionCompleteDelegates.AddUObject(
//					this, &UOnlineGameElements::OnDestroySessionComplete);
//				SessionPtr->DestroySession(SteamSessionName);
//			}
//		}
//	}
//}
//
//void UOnlineGameElements::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
//{
//	UE_LOG(LogTemp, Warning, TEXT("Successfully destroyed session: %d"), bWasSuccessful);
//
//	if (OnlineSubsystem)
//	{
//		if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
//		{
//			SessionPtr->ClearOnDestroySessionCompleteDelegates(this);
//		}
//	}
//}
//
////поиск сесий+присоеденение к сесии
//void UOnlineGameElements::FindFirstSession()
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//
//			SearchSettings = MakeShareable(new FOnlineSessionSearch());
//
//			SearchSettings->QuerySettings.SearchParams.Empty();
//
//			SearchSettings->QuerySettings.Set(SEARCH_KEYWORDS, FString("Lobby_Gipfel"),
//				EOnlineComparisonOp::Equals);
//
//			SearchSettings->QuerySettings.Set(SEARCH_LOBBIES, true,
//				EOnlineComparisonOp::Equals);
//
//			SessionPtr->OnFindSessionsCompleteDelegates.AddUObject(
//				this, &UOnlineGameElements::OnFindFirstSessionComplete);
//			SessionPtr->FindSessions(0, SearchSettings.ToSharedRef());
//
//		}
//	}
//
//}
//
//void UOnlineGameElements::OnFindFirstSessionComplete(bool bWasSuccessful)
//{
//	if (bWasSuccessful)
//	{
//		UE_LOG(LogTemp, Warning, TEXT("Successfully found %d Sessions !!!"), SearchSettings->SearchResults.Num());
//	}
//
//	if (OnlineSubsystem)
//	{
//		IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//		if (SearchSettings->SearchResults.Num() > 0)
//		{
//			SessionPtr->OnJoinSessionCompleteDelegates.AddUObject(
//				this, &UOnlineGameElements::OnJoinSessionComplete);
//			SessionPtr->JoinSession(0, SteamSessionName, SearchSettings->SearchResults[0]);
//		}
//		else
//		{
//			UE_LOG(LogTemp, Warning, TEXT("No Sessions found !!! :c"));
//		}
//
//		SessionPtr->ClearOnFindSessionsCompleteDelegates(this);
//
//	}
//}
//
////поиск сесий
//void UOnlineGameElements::FindAllSessions()
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//
//			SearchSettings = MakeShareable(new FOnlineSessionSearch());
//			SearchSettings->QuerySettings.Set(SEARCH_KEYWORDS, FString("Lobby_Gipfel"),
//				EOnlineComparisonOp::Equals);
//			SearchSettings->QuerySettings.Set(SEARCH_LOBBIES, true,
//				EOnlineComparisonOp::Equals);
//			SessionPtr->OnFindSessionsCompleteDelegates.AddUObject(
//				this, &UOnlineGameElements::OnFindAllSessionsComplete);
//			SessionPtr->FindSessions(0, SearchSettings.ToSharedRef());
//
//		}
//	}
//}
//
//void UOnlineGameElements::OnFindAllSessionsComplete(bool bWasSuccessful)
//{
//	if (OnlineSubsystem)
//	{
//		IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//
//		if (SearchSettings->SearchResults.Num() > 0)
//		{
//			SessionPtr->OnJoinSessionCompleteDelegates.AddUObject(
//				this, &UOnlineGameElements::OnJoinSessionComplete);;
//			for (FOnlineSessionSearchResult Session : SearchSettings->SearchResults)
//			{
//				UE_LOG(LogTemp, Warning, TEXT("Found Session: %s"));
//			}
//		}
//		else
//		{
//			UE_LOG(LogTemp, Warning, TEXT("No Sessions found !!! :c"));
//		}
//		SessionPtr->ClearOnFindSessionsCompleteDelegates(this);
//
//	}
//}
//
////
//void UOnlineGameElements::JoinSessionByIndex(int32 SessionIndex)
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//
//			SessionPtr->OnJoinSessionCompleteDelegates.AddUObject(
//				this, &UOnlineGameElements::OnJoinSessionComplete);
//			SessionPtr->JoinSession(0, SteamSessionName, SearchSettings->SearchResults[SessionIndex]);
//
//		}
//	}
//}
//
//void UOnlineGameElements::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
//{
//	if (OnlineSubsystem)
//	{
//		IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface();
//
//		FString ConnectionInfo = FString();
//		SessionPtr->GetResolvedConnectString(SessionName, ConnectionInfo);
//		if (!ConnectionInfo.IsEmpty())
//		{
//			if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
//			{
//				PC->ClientTravel(ConnectionInfo, ETravelType::TRAVEL_Absolute);
//				UE_LOG(LogTemp, Log, TEXT("Joined lobby."));
//			}
//		}
//	}
//}
//
//
//
//
////друзьяшки 
//void UOnlineGameElements::GetAllFriends()
//{
//	if (bIsLoggedIn && OnlineSubsystem)
//	{
//		if (IOnlineFriendsPtr FriendsPtr = OnlineSubsystem->GetFriendsInterface())
//		{
//			FriendsPtr->ReadFriendsList(0, FString(""),
//				FOnReadFriendsListComplete::CreateUObject(
//					this, &UOnlineGameElements::OnGetAllFriendsComplete));
//		}
//	}
//}
//
//void UOnlineGameElements::OnGetAllFriendsComplete(int32 LocalUserNum, bool bWasSuccessful, const FString& ListName,
//	const FString& ErrorStr)
//{
//	if (OnlineSubsystem)
//	{
//		if (IOnlineFriendsPtr FriendsPtr = OnlineSubsystem->GetFriendsInterface())
//		{
//			TArray<TSharedRef<FOnlineFriend>> FriendsList;
//			if (FriendsPtr->GetFriendsList(0, ListName, FriendsList))
//			{
//				for (TSharedRef<FOnlineFriend> Friend : FriendsList)
//				{
//					FString FriendName = Friend.Get().GetRealName();
//					UE_LOG(LogTemp, Warning, TEXT("Friend: %s"), *FriendName);
//				}
//			}
//		}
//	}
//}
//
//void UOnlineGameElements::ShowFriendsList()
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			if (IOnlineExternalUIPtr UIPtr = OnlineSubsystem->GetExternalUIInterface())
//			{
//				UIPtr->ShowFriendsUI(0);
//			}
//		}
//	}
//}
//
//void UOnlineGameElements::ShowInviteUI()
//{
//	if (bIsLoggedIn)
//	{
//		if (OnlineSubsystem)
//		{
//			if (IOnlineExternalUIPtr UIPtr = OnlineSubsystem->GetExternalUIInterface())
//			{
//				UIPtr->ShowInviteUI(0, SteamSessionName);
//			}
//		}
//	}
//}
//
////
//int32 UOnlineGameElements::GetNumOfSessions()
//{
//	FindAllSessions();
//	return this->Sessions.Num();
//}
//
//FString UOnlineGameElements::GetSessionID(int32 SessionIndex)
//{
//	FindAllSessions();
//	return this->Sessions[SessionIndex].GetSessionIdStr();
//}
//
//FString UOnlineGameElements::GetSessionOwnerName(int32 SessionIndex)
//{
//	FindAllSessions();
//	return this->Sessions[SessionIndex].Session.OwningUserName;
//}
//
//int32 UOnlineGameElements::GetSessionPing(int32 SessionIndex)
//{
//	FindAllSessions();
//	return this->Sessions[SessionIndex].PingInMs;
//}
//
//int32 UOnlineGameElements::GetSessionMaxSlots(int32 SessionIndex)
//{
//	FindAllSessions();
//	return this->Sessions[SessionIndex].Session.NumOpenPublicConnections;
//}
//
//FString UOnlineGameElements::GetPlayerName()
//{
//	if (OnlineSubsystem)
//	{
//		if (IOnlineIdentityPtr Identity = OnlineSubsystem->GetIdentityInterface())
//		{
//			FOnlineAccountCredentials AccountCredentials;
//			AccountCredentials.Type = TEXT("accountportal");
//
//			return Identity->GetPlayerNickname(0);
//		}
//	}
//	return FString("");
//}