﻿// DemoDreams. All rights reserved.

#include "WarOfFlareGameState.h"

#include "Net/UnrealNetwork.h"

AWarOfFlareGameState::AWarOfFlareGameState()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);
	
	RoundController = CreateDefaultSubobject<URoundControllerComponent>(TEXT("WaveController"));
	RoundController->SetIsReplicated(true);

	Grid = CreateDefaultSubobject<UGridComponent>(TEXT("Grid"));
	Grid->SetIsReplicated(true);
}

void AWarOfFlareGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWarOfFlareGameState, RoundController);
	DOREPLIFETIME(AWarOfFlareGameState, Grid);
}