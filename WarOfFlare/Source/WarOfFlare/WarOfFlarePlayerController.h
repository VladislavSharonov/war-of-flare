﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "PlayerControllerBase.h"
#include "Engine/World.h"
#include "Income/IncomeController.h"

#include "WarOfFlarePlayerController.generated.h"

UCLASS()
class WAROFFLARE_API AWarOfFlarePlayerController : public APlayerControllerBase
{
	GENERATED_BODY()

public:
	AWarOfFlarePlayerController();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;
	
protected:
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_Surrender();
		
	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UIncomeController> IncomeController;
};
