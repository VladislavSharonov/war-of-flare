﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include <Kismet/GameplayStatics.h>

#include "EdgeScrollingMovementComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEdgeScrollTriggered, FVector, Direction, float, Acceleration);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WAROFFLARE_API UEdgeScrollingMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UEdgeScrollingMovementComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintPure)
	static void ComputeDirectionByAxis(float Position, float Size, float ScrollZoneSize, double& Movement);

	UFUNCTION(BlueprintPure)
	float ComputeAcceleration() const;

	UFUNCTION(BlueprintCallable)
	void CheckTrigger(bool& Hovered);

public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Movement")
	FEdgeScrollTriggered EdgeScrollTriggered;

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Owner's Components")
	TObjectPtr<APlayerController> PlayerController;

	UPROPERTY(BlueprintReadOnly, Category = "Acceleration")
	float HoveredTime = 0.0f;

public:
	/** Time during which they will accelerate */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Acceleration")
	float AccelerationTime = 4.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Acceleration")
	float MaxSpeedScale = 2.0f;
};
