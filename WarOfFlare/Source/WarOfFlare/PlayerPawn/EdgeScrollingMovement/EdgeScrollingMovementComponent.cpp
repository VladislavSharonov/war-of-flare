﻿// DemoDreams. All rights reserved.

#include "PlayerPawn/EdgeScrollingMovement/EdgeScrollingMovementComponent.h"

UEdgeScrollingMovementComponent::UEdgeScrollingMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UEdgeScrollingMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get owner's PlayerController
	const APawn* Parent = Cast<APawn>(GetOwner());
	if (Parent == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Edge Scrolling Component: Owner is not a Pawn"));
	}
	else
	{
		PlayerController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		if (!IsValid(PlayerController))
			UE_LOG(LogTemp, Error, TEXT("Edge Scrolling Component: Can't get owner's Player Controller"));
	}
}

void UEdgeScrollingMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool bHovered;
	CheckTrigger(bHovered);

	HoveredTime = bHovered 
		? FMath::Min(HoveredTime + DeltaTime, AccelerationTime) 
		: 0.0f;
}

void UEdgeScrollingMovementComponent::CheckTrigger(bool& Hovered)
{
	Hovered = false;
	if (!IsValid(PlayerController))
		return;

	FIntVector2 ViewportSize = {};
	PlayerController->GetViewportSize(ViewportSize.X, ViewportSize.Y);

	FVector2D MousePosition = {};
	if (!PlayerController->GetMousePosition(MousePosition.X, MousePosition.Y))
		return; // Mouse position was not found

	FVector Direction;
	// Compute movement direction if mouse in movement zone
	ComputeDirectionByAxis(MousePosition.X, ViewportSize.X, 1, Direction.X);
	ComputeDirectionByAxis(ViewportSize.Y - MousePosition.Y, ViewportSize.Y, 1, Direction.Y);

	if (Direction.IsZero())
		return;

	Hovered = true;

	EdgeScrollTriggered.Broadcast(Direction, ComputeAcceleration());
}

void UEdgeScrollingMovementComponent::ComputeDirectionByAxis(float Position, float Size, float ScrollZoneSize, double& Movement)
{
	Movement = Position >= 0 && Position <= ScrollZoneSize
		? -1 /*Backward*/
		: Position >= Size - ScrollZoneSize && Position <= Size  /* 0 — don't move, 1 — Forward */;
}

float UEdgeScrollingMovementComponent::ComputeAcceleration() const
{
	return (HoveredTime / AccelerationTime) * (MaxSpeedScale - 1);
}