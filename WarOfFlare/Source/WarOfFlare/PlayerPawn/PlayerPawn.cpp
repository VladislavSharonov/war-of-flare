﻿// DemoDreams. All rights reserved.

#include "PlayerPawn/PlayerPawn.h"

#include "Components/SceneComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Camera/CameraComponent.h"

APlayerPawn::APlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	
	PlayerCollider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("PlayerCollider"));
	SetRootComponent(PlayerCollider);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	FloatingPawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingPawnMovement"));
	
	SpringArm->SetupAttachment(PlayerCollider);
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	
	SpringArm->bDoCollisionTest = false;
	SpringArm->SetRelativeRotation(FRotator(-60.0f, 0.0f, 0.0f));
	
	Camera->FieldOfView = 90.0f;
	
	Zoom = CreateDefaultSubobject<UStepZoomComponent>(TEXT("Zoom"));
	Zoom->SetSpringArmComponent(SpringArm);
	
	
	EdgeScrollingMovement = CreateDefaultSubobject<UEdgeScrollingMovementComponent>(TEXT("EdgeScrollingMovement"));

	SpringArm->bUsePawnControlRotation = true;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;
	SpringArm->bInheritYaw = true;
	
	FloatingPawnMovement->TurningBoost = 5;
}

void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
}

void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerPawn::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);
}

void APlayerPawn::MoveRight(const float AxisValue)
{
	if ((Controller == NULL) || AxisValue == 0.0f)
		return;

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

	AddMovementInput(Direction, AxisValue);
}

void APlayerPawn::MoveForward(const float AxisValue)
{
	if ((Controller == NULL) || AxisValue == 0.0f)
		return;
	
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(Direction, AxisValue);
}
