﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "EdgeScrollingMovement/EdgeScrollingMovementComponent.h"
#include "GameFramework/Pawn.h"
#include "Zoom/StepZoomComponent.h"

#include "PlayerPawn.generated.h"

class USceneComponent;
class USpringArmComponent;
class UCameraComponent;
class UFloatingPawnMovement;

UCLASS()
class WAROFFLARE_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawn();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void MoveRight(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void MoveForward(float AxisValue);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	TObjectPtr<UCapsuleComponent> PlayerCollider;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|Camera")
	TObjectPtr<USpringArmComponent> SpringArm;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|Camera")
	TObjectPtr<UCameraComponent> Camera;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	TObjectPtr<UFloatingPawnMovement> FloatingPawnMovement;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	TObjectPtr<UStepZoomComponent> Zoom;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	TObjectPtr<UEdgeScrollingMovementComponent> EdgeScrollingMovement;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float MovementSpeed = 1000.0f;
};
