﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "BaseZoomComponent.h"

#include "StepZoomComponent.generated.h"

UCLASS( Blueprintable, ClassGroup = Camera, meta = (BlueprintSpawnableComponent))
class WAROFFLARE_API UStepZoomComponent : public UBaseZoomComponent
{
	GENERATED_BODY()

public:
	UStepZoomComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, Category = "Camera|Zoom")
	virtual void AddZoom(const float Value) override;

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float Damping = 0.4f;
};
