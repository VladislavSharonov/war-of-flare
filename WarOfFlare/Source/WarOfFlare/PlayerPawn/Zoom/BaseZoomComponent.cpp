﻿// DemoDreams. All rights reserved.

#include "PlayerPawn/Zoom/BaseZoomComponent.h"

UBaseZoomComponent::UBaseZoomComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UBaseZoomComponent::BeginPlay()
{
	Super::BeginPlay();

	USpringArmComponent* TargetSpringArm = GetOwner()->FindComponentByClass<USpringArmComponent>();
	if (!IsValid(TargetSpringArm))
		SetSpringArmComponent(TargetSpringArm);
}

void UBaseZoomComponent::SetSpringArmComponent(USpringArmComponent* TargetSpringArm)
{
	if (!IsValid(TargetSpringArm))
	{
			UE_LOG(LogTemp, Error, TEXT("Zoom Component: Initialization is not valid"));
			return;
	}

	this->SpringArm = TargetSpringArm;
	SetCurrentZoom(Zoom);
}

float UBaseZoomComponent::GetCurrentZoom() const
{
	if (!SpringArm.IsValid())
		return -1.0f;

	return SpringArm->TargetArmLength / DefaultCameraDistance;
}

void UBaseZoomComponent::SetCurrentZoom(const float Value)
{
	Zoom = Value;
	SpringArm->TargetArmLength = Zoom * DefaultCameraDistance;
}

float UBaseZoomComponent::GetZoomPercent() const
{
	if (MinZoom == MaxZoom)
		return 1;

	return (Zoom - MinZoom) / (MaxZoom - MinZoom);
}
