﻿// DemoDreams. All rights reserved.


#include "PlayerPawn/Zoom/StepZoomComponent.h"

UStepZoomComponent::UStepZoomComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UStepZoomComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (SpringArm->TargetArmLength != Zoom * DefaultCameraDistance)
		SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, Zoom * DefaultCameraDistance, DeltaTime, Damping < 1E-5f ? 0 : 1 / Damping);
}

void UStepZoomComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UStepZoomComponent::AddZoom(const float Value)
{
	if (MinZoom == MaxZoom || Value == 0)
		return;

	float ZoomDirection = Value < 0 ? -1 : 1;
	float CurrentZoomAcceleration = FMath::Lerp(1, ZoomAcceleration, GetZoomPercent());
	//UE_LOG(LogTemp, Display, TEXT("%f"), currentZoomAcceleration);

	const float ZoomDelta = ZoomSpeed * ZoomDirection * CurrentZoomAcceleration;
	Zoom = FMath::Clamp(Zoom + ZoomDelta, MinZoom, MaxZoom);
}
