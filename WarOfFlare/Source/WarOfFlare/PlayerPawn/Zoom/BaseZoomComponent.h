﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "BaseZoomComponent.generated.h"

UCLASS( ClassGroup=Camera )
class WAROFFLARE_API UBaseZoomComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UBaseZoomComponent();

protected:
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintCallable, Category = "Camera|Zoom")
	virtual void SetSpringArmComponent(USpringArmComponent* TargetSpringArm);

	virtual void AddZoom(const float Value) PURE_VIRTUAL(UZoomComponent::AddZoom, );

	UFUNCTION(BlueprintCallable, Category = "Camera|Zoom")
	float GetCurrentZoom() const;

	UFUNCTION(BlueprintCallable, Category = "Camera|Zoom")
	void SetCurrentZoom(const float Value);
	
	UFUNCTION(BlueprintCallable, Category = "Camera|Zoom")
	float GetZoomPercent() const;

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float MinZoom = 0.4f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float MaxZoom = 10.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float Zoom = 1.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float ZoomSpeed = 0.2f;

	/** Maximum speed multiplyer */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float ZoomAcceleration = 16.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera|Zoom")
	float DefaultCameraDistance = 600.0f;

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Camera")
	TSoftObjectPtr<USpringArmComponent> SpringArm;
};
