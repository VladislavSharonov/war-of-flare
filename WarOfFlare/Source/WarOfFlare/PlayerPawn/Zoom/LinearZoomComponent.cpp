﻿// DemoDreams. All rights reserved.


#include "PlayerPawn/Zoom/LinearZoomComponent.h"

ULinearZoomComponent::ULinearZoomComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void ULinearZoomComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void ULinearZoomComponent::BeginPlay()
{
	Super::BeginPlay();
}

void ULinearZoomComponent::AddZoom(float Value)
{
	if (MinZoom == MaxZoom || Value == 0)
		return;

	Zoom = FMath::Clamp(Zoom + ZoomSpeed * Value, MinZoom, MaxZoom);
	SpringArm->TargetArmLength = DefaultCameraDistance * Zoom;
}
