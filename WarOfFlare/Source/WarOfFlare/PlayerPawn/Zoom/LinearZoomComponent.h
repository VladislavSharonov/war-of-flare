﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "BaseZoomComponent.h"

#include "LinearZoomComponent.generated.h"

/**
 * 
 */
UCLASS( Blueprintable, ClassGroup = Camera, meta = (BlueprintSpawnableComponent) )
class WAROFFLARE_API ULinearZoomComponent : public UBaseZoomComponent
{
	GENERATED_BODY()
public:
	ULinearZoomComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, Category = "Camera|Zoom")
	virtual void AddZoom(float Value) override;
};
