﻿// DemoDreams. All rights reserved.


#include "Income/IncomeController.h"

#include "Money/WalletComponent.h"

void UIncomeController::SetMoneyComponent(UWalletComponent* InWallet)
{
	Wallet = InWallet;
	Pay(NotPaidMoney);
	NotPaidMoney = 0;
}

void UIncomeController::AddIncomeForOrderingMinions(const TArray<FMinionSpawnDataItem>& MinionSpawnData)
{
	for (auto const& MinionSpawnDataItem : MinionSpawnData)
	{
		FName Name(*StaticEnum<EMinionType>()->GetNameStringByValue(static_cast<int64>(MinionSpawnDataItem.Minion)));

		if (!IsValid(MinionOrderingCashback))
		{
			UE_LOG(LogTemp, Error, TEXT("UIncomeController: The MinionOrderingCashback table is not set."));
			return;
		}
		
		const FPrice* Price = MinionOrderingCashback->FindRow<FPrice>(Name, "");

		if (Price == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("UIncomeController: Minion with name \"%s\" was not found."), *Name.ToString());
			continue;
		}
		/*
		UE_LOG(LogTemp, Display,
			TEXT("%s [price: %d] x %d: %d"),
			*UEnum::GetValueAsString(MinionSpawnDataItem.Minion),
			Price->Amount,
			MinionSpawnDataItem.Count,
			Price->Amount * MinionSpawnDataItem.Count);
		*/
		Pay(Price->Amount * MinionSpawnDataItem.Count);
	}
}

void UIncomeController::PayForRound(const uint8 Round)
{
	Pay(PassiveIncome * Round + IncomeFromOrderingMinions);
}

void UIncomeController::PayForKillingMinions(EMinionType Minion)
{
	const FName Name(*StaticEnum<EMinionType>()->GetNameStringByValue(static_cast<int64>(Minion)));

	if (!IsValid(MinionKillingCashback))
	{
		UE_LOG(LogTemp, Error, TEXT("UIncomeController: The MinionKillingCashback table is not set."));
		return;
	}
	
	const FPrice* Price = MinionKillingCashback->FindRow<FPrice>(Name, "");

	if (Price == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UIncomeController: Minion with name \"%s\" was not found."), *Name.ToString());
		return;
	}
	
	Pay(Price->Amount);
}

void UIncomeController::Pay(const int32 Amount)
{
	if (Wallet == nullptr)
	{
		NotPaidMoney = Amount;
		UE_LOG(LogTemp, Display, TEXT("UIncomeController: Wallet is undefine."));
	}
	else
		Wallet->AddMoney(Amount);
}
