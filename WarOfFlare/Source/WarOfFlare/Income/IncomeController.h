﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MinionExchange/MinionSpawnData.h"
#include "Money/WalletComponent.h"
#include "Money/Price.h"

#include "IncomeController.generated.h"

UCLASS()
class WAROFFLARE_API UIncomeController : public UActorComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintAuthorityOnly)
	void SetMoneyComponent(UWalletComponent* InWallet);
	
	UFUNCTION(BlueprintAuthorityOnly)
	void AddIncomeForOrderingMinions(const TArray<FMinionSpawnDataItem>& MinionSpawnData);

	UFUNCTION(BlueprintAuthorityOnly)
	void PayForRound(uint8 Round);

	UFUNCTION(BlueprintAuthorityOnly)
	void PayForKillingMinions(EMinionType Minion);
	
protected:
	UFUNCTION(BlueprintAuthorityOnly)
	void Pay(int32 Amount);
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(RowType="Price"))
	UDataTable* MinionOrderingCashback = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(RowType="Price"))
	UDataTable* MinionKillingCashback = nullptr;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int32 PassiveIncome = 80;
	
	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly)
	int32 IncomeFromOrderingMinions = 0;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	int32 NotPaidMoney = 0;
	
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	UWalletComponent* Wallet = nullptr;
};
