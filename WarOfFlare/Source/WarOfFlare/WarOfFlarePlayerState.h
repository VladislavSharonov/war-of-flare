﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Match/MatchResultComponent.h"

#include "Player/PlayerScene.h"

#include "Money/WalletComponent.h"

#include "WarOfFlarePlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerReadyToStartDefense, AWarOfFlarePlayerController*, PlayerController);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWatchedTeamSwitched, uint8, TeamId);

UCLASS()
class WAROFFLARE_API AWarOfFlarePlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	AWarOfFlarePlayerState();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

public:
	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<USceneComponent> DefaultRootComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<UMatchResultComponent> MatchResultClass = UMatchResultComponent::StaticClass();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UMatchResultComponent> MatchResult;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Replicated, Category = "Player|Health")
	TObjectPtr<APlayerScene> PlayerScene;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Player|Money")
	TObjectPtr<UWalletComponent> Wallet;
};
