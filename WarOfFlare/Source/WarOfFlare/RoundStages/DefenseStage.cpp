﻿// DemoDreams. All rights reserved.

#include "DefenseStage.h"

#include "Income/IncomeController.h"
#include "Kismet/GameplayStatics.h"
#include "Minions/Minion.h"
#include "PlayerAbility/AbilityCastingComponent.h"

UDefenseStage::UDefenseStage()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UDefenseStage::OnActivated_Implementation(bool bReset)
{
	Super::OnActivated_Implementation(bReset);
	if (GetOwner()->HasAuthority())
	{
		// On Server Only
		
		SetActiveAbilityCasting(true);
		// Start Minions spawn
		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnerCluster::StaticClass(), Actors);

		for (int i = 0; i < Actors.Num(); ++i)
		{
			ASpawnerCluster* SpawnerCluster = Cast<ASpawnerCluster>(Actors[i]);
			if (IsValid(SpawnerCluster))
			{
				InitializeSpawnerCluster(SpawnerCluster);
			}
		}

		for (int i = SpawnerClusters.Num() - 1; i >= 0; --i)
		{
			// A Spawner Cluster can be removed from the array if it has nothing to spawn.
			SpawnerClusters[i]->Spawn();
		}
	}
}

void UDefenseStage::OnDeactivated_Implementation()
{
	if (GetOwner()->HasAuthority())
	{
		// On Server Only
		
		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnerCluster::StaticClass(), Actors);
		for (const auto& Actor : Actors)
		{
			ASpawnerCluster* SpawnerCluster = Cast<ASpawnerCluster>(Actor);
			if (IsValid(SpawnerCluster))
			{
				DeinitializeSpawnerCluster(SpawnerCluster);
			}
		}

		SetActiveAbilityCasting(false);
	}

	Super::OnDeactivated_Implementation();
}

void UDefenseStage::OnMinionSpawned(AActor* Actor, AActor* OwningPlayer)
{
	AMinion* Minion = Cast<AMinion>(Actor);
	if (Minion != nullptr)
	{
		++LivingMinionsNumber;
		Minion->SetEnemy(Cast<APlayerController>(OwningPlayer));
		Minion->OnMinionDied.AddUniqueDynamic(this, &UDefenseStage::OnMinionDied);
	}
}

void UDefenseStage::OnMinionDied(const EMinionType MinionType, AActor* Killer)
{
	if (!IsValid(Killer))
	{
		UE_LOG(LogTemp, Error, TEXT("OnMinionDied: Minion killer is invalid"));
		return;
	}
	
	--LivingMinionsNumber;
	/*Minion's owner is an owner of spawner cluster*/
	UIncomeController* IncomeController = Killer->GetComponentByClass<UIncomeController>();
	if (IsValid(IncomeController))
	{
		IncomeController->PayForKillingMinions(MinionType);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("OnMinionDied: IncomeController is invalid"));
	}
	
	bool _ = TryEndStage();
}

void UDefenseStage::OnSpawnCompleted(ASpawnerCluster* SpawnerCluster)
{
	DeinitializeSpawnerCluster(SpawnerCluster);
	bool _ = TryEndStage();
}

bool UDefenseStage::TryEndStage() const
{
	bool IsStageCompleted = LivingMinionsNumber == 0 && SpawnerClusters.Num() == 0;
	
	if (IsStageCompleted)
		EndStage();

	return IsStageCompleted;
}

void UDefenseStage::SetActiveAbilityCasting(bool InIsActive) const
{
	for (auto PlayerControllerIterator = GetWorld()->GetPlayerControllerIterator(); PlayerControllerIterator; ++PlayerControllerIterator)
	{
		UAbilityCastingComponent* AbilityCastingComponent = PlayerControllerIterator->Get()->GetComponentByClass<UAbilityCastingComponent>();
		if (AbilityCastingComponent == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerController has no AbilityCastingComponent"));
		}
		AbilityCastingComponent->SetActive(InIsActive);
	}
}

void UDefenseStage::InitializeSpawnerCluster(ASpawnerCluster* SpawnerCluster)
{
	if (!IsValid(SpawnerCluster))
		return;
	
	SpawnerClusters.Add(SpawnerCluster);
	SpawnerCluster->OnActorSpawned.AddUniqueDynamic(this, &UDefenseStage::OnMinionSpawned);
	SpawnerCluster->OnSpawnCompleted.AddUniqueDynamic(this, &UDefenseStage::OnSpawnCompleted);
}

void UDefenseStage::DeinitializeSpawnerCluster(ASpawnerCluster* SpawnerCluster)
{
	if (IsValid(SpawnerCluster))
	{
		SpawnerCluster->OnActorSpawned.RemoveDynamic(this, &UDefenseStage::OnMinionSpawned);
		SpawnerCluster->OnSpawnCompleted.RemoveDynamic(this, &UDefenseStage::OnSpawnCompleted);
	}
	
	SpawnerClusters.Remove(SpawnerCluster);
}
