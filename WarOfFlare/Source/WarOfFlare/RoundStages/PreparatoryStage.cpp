﻿// DemoDreams. All rights reserved.

#include "RoundStages/PreparatoryStage.h"

#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"
#include "Income/IncomeController.h"
#include "Net/UnrealNetwork.h"
#include "Road/Road.h"
#include "Round/RoundControllerComponent.h"
#include "TowerBuilding/TowerBuildingComponent.h"

UPreparatoryStage::UPreparatoryStage()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPreparatoryStage::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(UPreparatoryStage, PreparingStageDuration);
	DOREPLIFETIME(UPreparatoryStage, StartTimeSeconds);
}

void UPreparatoryStage::OnActivated_Implementation(bool bReset)
{
	Super::OnActivated_Implementation(bReset);

	if (GetOwner()->HasAuthority())
	{
		// On Server Only
		PayIncomeForRound();
	
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UPreparatoryStage::EndStage, PreparingStageDuration, false);

		SetActiveTowerBuilding(true);
		SetActiveMinionExchange(true);
		SetActiveReadinessManager(true);

		StartTimeSeconds = UGameplayStatics::GetGameState(GetWorld())->GetServerWorldTimeSeconds();
	}
}

void UPreparatoryStage::OnDeactivated_Implementation()
{
	Super::OnDeactivated_Implementation();
	
	if (GetOwner()->HasAuthority())
	{
		// On Server Only
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
		SetActiveTowerBuilding(false);
		SetActiveMinionExchange(false);
		SetActiveReadinessManager(false);
	}
}

void UPreparatoryStage::PayIncomeForRound() const
{
	for (auto PlayerControllerIterator = GetWorld()->GetPlayerControllerIterator(); PlayerControllerIterator; ++PlayerControllerIterator)
	{
		UIncomeController* IncomeController = PlayerControllerIterator->Get()->GetComponentByClass<UIncomeController>();
		if (!IsValid(IncomeController))
			continue;
		IncomeController->PayForRound(RoundController->GetCurrentRoundNumber());
	}
}

void UPreparatoryStage::AddPaymentForMinionOrder(const TArray<FMinionSpawnDataItem>& MinionSpawnData,
	APlayerController* Sender, APlayerController* Recipient)
{
	UIncomeController* IncomeController = Sender->GetComponentByClass<UIncomeController>();
	if (!IsValid(IncomeController))
	{
		UE_LOG(LogTemp, Error, TEXT("AddPaymentForMinionOrder: From %s to %s: %d. IncomeController is invalid"), *Sender->GetName(), *Recipient->GetName(), MinionSpawnData.Num());
		return;
	}
	//UE_LOG(LogTemp, Display, TEXT("From %s to %s: %d"), *Sender->GetName(), *Recipient->GetName(), MinionSpawnData.Num());
	IncomeController->AddIncomeForOrderingMinions(MinionSpawnData);
}

void UPreparatoryStage::SetActiveTowerBuilding(const bool InIsActive) const
{
	for (auto PlayerControllerIterator = GetWorld()->GetPlayerControllerIterator(); PlayerControllerIterator; ++PlayerControllerIterator)
	{
		UTowerBuildingComponent* TowerBuildingComponent = PlayerControllerIterator->Get()->GetComponentByClass<UTowerBuildingComponent>();
		if (TowerBuildingComponent == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerController has no TowerBuildingComponent"));
			return;
		}
		TowerBuildingComponent->SetActive(InIsActive);
	}
	
	ARoad::SetAllRoadGridVisible(GetWorld(), InIsActive);
}

void UPreparatoryStage::SetActiveMinionExchange(const bool InIsActive) const
{
	UMinionExchangeComponent* MinionExchange = GetOwner()->FindComponentByClass<UMinionExchangeComponent>();
	if (InIsActive)
	{
		if (!IsValid(MinionExchange))
		{
			MinionExchange = Cast<UMinionExchangeComponent>(GetOwner()->AddComponentByClass(
				MinionExchangeClass, false, FTransform::Identity, false));
		}
		MinionExchange->OnExchange.AddUniqueDynamic(this, &UPreparatoryStage::AddPaymentForMinionOrder);
		MinionExchange->Activate(false);
	}
	else
	{
		if (IsValid(MinionExchange))
		{
			MinionExchange->Deactivate();
			MinionExchange->DestroyComponent();
		}

		MinionExchange = nullptr;
	}
}

void UPreparatoryStage::SetActiveReadinessManager(const bool InIsActive)
{
	if (InIsActive)
	{
		AActor* Owner = GetOwner();
		if (!IsValid(Owner) || IsValid(ReadinessManager))
			return;

		UActorComponent* Component = Owner->AddComponentByClass(ReadinessManagerClass, false, FTransform::Identity,false);
		ReadinessManager = Cast<UReadinessManagerComponent>(Component);
		for (auto& Player : UGameplayStatics::GetGameState(GetWorld())->PlayerArray)
		{
			ReadinessManager->AddRespondent(Player);
		}
		ReadinessManager->OnAllReady.AddUniqueDynamic(this, &UPreparatoryStage::OnAllReady);
	}
	else
	{
		if (IsValid(ReadinessManager))
		{
			ReadinessManager->OnAllReady.RemoveDynamic(this, &UPreparatoryStage::OnAllReady);
			ReadinessManager->DestroyComponent();
		}
	}
}

void UPreparatoryStage::OnAllReady()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	EndStage();
}
