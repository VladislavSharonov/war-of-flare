﻿// DemoDreams. All rights reserved.

#pragma once

#include "Minions/Minion.h"
#include "Round/RoundStage.h"
#include "Spawn/SpawnerCluster.h"

#include "DefenseStage.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WAROFFLARE_API UDefenseStage : public URoundStage
{
	GENERATED_BODY()

public:
	UDefenseStage();
	
	virtual void OnActivated_Implementation(bool bReset = false) override;
	
	virtual void OnDeactivated_Implementation() override;
	
protected:
	UFUNCTION(BlueprintCallable)
	void OnMinionSpawned(AActor* Actor, AActor* OwningPlayer);
	
	UFUNCTION(BlueprintCallable)
	void OnMinionDied(EMinionType MinionType, AActor* Killer);

	UFUNCTION(BlueprintCallable)
	void OnSpawnCompleted(ASpawnerCluster* SpawnerCluster);

	UFUNCTION(BlueprintCallable)
	bool TryEndStage() const;

	UFUNCTION(BlueprintCallable)
	void SetActiveAbilityCasting(bool InIsActive = true) const;

	UFUNCTION(BlueprintCallable)
	void InitializeSpawnerCluster(ASpawnerCluster* SpawnerCluster);

	UFUNCTION(BlueprintCallable)
	void DeinitializeSpawnerCluster(ASpawnerCluster* SpawnerCluster);
	
	int32 LivingMinionsNumber = 0;

	UPROPERTY()
	TArray<ASpawnerCluster*> SpawnerClusters;
};
