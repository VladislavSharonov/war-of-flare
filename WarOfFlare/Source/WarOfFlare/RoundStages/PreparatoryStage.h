﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Round/RoundStage.h"

#include "MinionExchange/MinionExchangeComponent.h"
#include "Readiness/ReadinessManagerComponent.h"
#include "TowerBuilding/TowerBuildingComponent.h"

#include "PreparatoryStage.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class WAROFFLARE_API UPreparatoryStage : public URoundStage
{
	GENERATED_BODY()

public:
	UPreparatoryStage();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void OnActivated_Implementation(bool bReset = false) override;
	
	virtual void OnDeactivated_Implementation() override;
	
	UFUNCTION(BlueprintAuthorityOnly)
	void PayIncomeForRound() const;

	UFUNCTION()
	void AddPaymentForMinionOrder(
		const TArray<FMinionSpawnDataItem>& MinionSpawnData,
		APlayerController* Sender,
		APlayerController* Recipient);
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Replicated)
	int32 PreparingStageDuration = 120 /*sec*/;
	
	UFUNCTION()
	void SetActiveTowerBuilding(bool InIsActive = true) const;

	UFUNCTION()
	void SetActiveMinionExchange(bool InIsActive = true) const;

	UFUNCTION()
	void SetActiveReadinessManager(bool InIsActive = true);

	UFUNCTION()
	void OnAllReady();
	
#pragma region Classes
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="PreparatoryStage|Classes")
	TSubclassOf<UMinionExchangeComponent> MinionExchangeClass = UMinionExchangeComponent::StaticClass();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="PreparatoryStage|Classes")
	TSubclassOf<UReadinessManagerComponent> ReadinessManagerClass = UReadinessManagerComponent::StaticClass();
#pragma endregion
	
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UReadinessManagerComponent* ReadinessManager = nullptr;
	
	UPROPERTY(BlueprintReadOnly, Category="Gameplay")
	FTimerHandle TimerHandle;

	UPROPERTY(BlueprintReadOnly, Replicated)
	double StartTimeSeconds = 0;
};

