﻿// DemoDreams. All rights reserved.

#include "WarOfFlarePlayerController.h"

#include "Cheats/GlobalCheatManager.h"
#include "GameFramework/PlayerState.h"
#include "Components/GameFrameworkComponentManager.h"
#include "Kismet/GameplayStatics.h"
#include "WarOfFlareGameMode.h"
#include "NetComponents/WarOfFlareGameInstance.h"

AWarOfFlarePlayerController::AWarOfFlarePlayerController()
{
	IncomeController = CreateDefaultSubobject<UIncomeController>(TEXT("IncomeController"));
	
	CheatClass = UGlobalCheatManager::StaticClass();
	
	bShowMouseCursor = true;
}

void AWarOfFlarePlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AWarOfFlarePlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	if (UGameFrameworkComponentManager* ComponentManager = GetGameInstance()->GetSubsystem<UGameFrameworkComponentManager>())
	{
		ComponentManager->AddReceiver(this);
	}

	if (HasAuthority())
	{
		UWalletComponent* Wallet = PlayerState->GetComponentByClass<UWalletComponent>();
		if (IsValid(Wallet))
			IncomeController->SetMoneyComponent(Wallet);
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("FindMoneyComponent: PlayerState has no moneyComponent."));
		}
	}
}

void AWarOfFlarePlayerController::Server_Surrender_Implementation()
{
	AGameModeBase* GameModeBase = UGameplayStatics::GetGameMode(GetWorld());
	AWarOfFlareGameMode* GameMode = Cast<AWarOfFlareGameMode>(GameModeBase);
	if (!IsValid(GameMode))
		return;

	GameMode->SurrenderPlayer(this);
}
