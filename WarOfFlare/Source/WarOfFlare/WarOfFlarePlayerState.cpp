﻿// DemoDreams. All rights reserved.

#include "WarOfFlarePlayerState.h"
#include "Net/UnrealNetwork.h"

AWarOfFlarePlayerState::AWarOfFlarePlayerState()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);
	
	Wallet = CreateDefaultSubobject<UWalletComponent>(TEXT("WalletComponent"));
	Wallet->SetIsReplicated(true);
	PrimaryActorTick.bCanEverTick = true;
}

void AWarOfFlarePlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AWarOfFlarePlayerState, PlayerScene);
	DOREPLIFETIME(AWarOfFlarePlayerState, Wallet);
}

void AWarOfFlarePlayerState::BeginPlay()
{
	Super::BeginPlay();
	
	UActorComponent* MatchResultComponent = AddComponentByClass(MatchResultClass, false, FTransform::Identity, false);
	MatchResult = Cast<UMatchResultComponent>(MatchResultComponent);
}
