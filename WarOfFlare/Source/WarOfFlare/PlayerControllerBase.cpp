﻿// DemoDreams. All rights reserved.


#include "PlayerControllerBase.h"

#include "GameFramework/PlayerState.h"
#include "NetComponents/WarOfFlareGameInstance.h"

void APlayerControllerBase::ClientWasKicked_Implementation(const FText& KickReason)
{
	Super::ClientWasKicked_Implementation(KickReason);
	UWarOfFlareGameInstance* Instance = GetGameInstance<UWarOfFlareGameInstance>();
	if (IsValid(Instance))
		Instance->LeaveLobby();
}
