﻿// DemoDreams. All rights reserved.

using UnrealBuildTool;

public class WarOfFlareEditorTarget : TargetRules
{
	public WarOfFlareEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

        ExtraModuleNames.AddRange(new string[] {
			"BlueprintRetarget",
			
	        "Match",
	        "Money",
	        "WarOfFlare",
	        "Rounds",
	        "Grid",
	        "Minions",
	        "Towers",
	        "MinionExchange",
	        "TowerBuilding",
	        "PlayerAbility"
        });
	}
}
