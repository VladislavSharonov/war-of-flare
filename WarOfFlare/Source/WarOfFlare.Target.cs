﻿// DemoDreams. All rights reserved.

using UnrealBuildTool;

public class WarOfFlareTarget : TargetRules
{
	public WarOfFlareTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

        ExtraModuleNames.AddRange(new string[] {
            "Match",
            "Money",
            "WarOfFlare",
            "Rounds",
            "Grid",
            "Minions",
	        "Towers",
            "MinionExchange",
            "TowerBuilding",
            "PlayerAbility"
        });
	}
}
