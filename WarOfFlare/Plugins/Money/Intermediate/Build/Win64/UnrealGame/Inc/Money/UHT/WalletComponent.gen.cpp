// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Money/Public/Money/WalletComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWalletComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	MONEY_API UClass* Z_Construct_UClass_UWalletComponent();
	MONEY_API UClass* Z_Construct_UClass_UWalletComponent_NoRegister();
	MONEY_API UFunction* Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Money();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics
	{
		struct _Script_Money_eventPlayerMoneyChangedSignature_Parms
		{
			int32 Money;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_Money;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::NewProp_Money = { "Money", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Money_eventPlayerMoneyChangedSignature_Parms, Money), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::NewProp_Money,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Money, nullptr, "PlayerMoneyChangedSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::_Script_Money_eventPlayerMoneyChangedSignature_Parms), Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FPlayerMoneyChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& PlayerMoneyChangedSignature, int32 Money)
{
	struct _Script_Money_eventPlayerMoneyChangedSignature_Parms
	{
		int32 Money;
	};
	_Script_Money_eventPlayerMoneyChangedSignature_Parms Parms;
	Parms.Money=Money;
	PlayerMoneyChangedSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(UWalletComponent::execNotifyOwnerMoneyChanged)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_NewMoney);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NotifyOwnerMoneyChanged_Implementation(Z_Param_NewMoney);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWalletComponent::execOnRep_Money)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_Money();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWalletComponent::execTryBuy)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Cost);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryBuy(Z_Param_Cost);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWalletComponent::execAddMoney)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InMoney);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddMoney(Z_Param_InMoney);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWalletComponent::execCanBuy)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Cost);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanBuy(Z_Param_Cost);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWalletComponent::execGetMoney)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetMoney();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UWalletComponent::execSetMoney)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InMoney);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMoney(Z_Param_InMoney);
		P_NATIVE_END;
	}
	struct WalletComponent_eventNotifyOwnerMoneyChanged_Parms
	{
		int32 NewMoney;
	};
	static FName NAME_UWalletComponent_NotifyOwnerMoneyChanged = FName(TEXT("NotifyOwnerMoneyChanged"));
	void UWalletComponent::NotifyOwnerMoneyChanged(int32 NewMoney)
	{
		WalletComponent_eventNotifyOwnerMoneyChanged_Parms Parms;
		Parms.NewMoney=NewMoney;
		ProcessEvent(FindFunctionChecked(NAME_UWalletComponent_NotifyOwnerMoneyChanged),&Parms);
	}
	void UWalletComponent::StaticRegisterNativesUWalletComponent()
	{
		UClass* Class = UWalletComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddMoney", &UWalletComponent::execAddMoney },
			{ "CanBuy", &UWalletComponent::execCanBuy },
			{ "GetMoney", &UWalletComponent::execGetMoney },
			{ "NotifyOwnerMoneyChanged", &UWalletComponent::execNotifyOwnerMoneyChanged },
			{ "OnRep_Money", &UWalletComponent::execOnRep_Money },
			{ "SetMoney", &UWalletComponent::execSetMoney },
			{ "TryBuy", &UWalletComponent::execTryBuy },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWalletComponent_AddMoney_Statics
	{
		struct WalletComponent_eventAddMoney_Parms
		{
			int32 InMoney;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_InMoney;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::NewProp_InMoney = { "InMoney", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(WalletComponent_eventAddMoney_Parms, InMoney), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::NewProp_InMoney,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::Function_MetaDataParams[] = {
		{ "Category", "Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "AddMoney", nullptr, nullptr, sizeof(Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::WalletComponent_eventAddMoney_Parms), Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_AddMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_AddMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWalletComponent_CanBuy_Statics
	{
		struct WalletComponent_eventCanBuy_Parms
		{
			int32 Cost;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_Cost;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::NewProp_Cost = { "Cost", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(WalletComponent_eventCanBuy_Parms, Cost), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WalletComponent_eventCanBuy_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(WalletComponent_eventCanBuy_Parms), &Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::NewProp_Cost,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::Function_MetaDataParams[] = {
		{ "Category", "Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "CanBuy", nullptr, nullptr, sizeof(Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::WalletComponent_eventCanBuy_Parms), Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_CanBuy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_CanBuy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWalletComponent_GetMoney_Statics
	{
		struct WalletComponent_eventGetMoney_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(WalletComponent_eventGetMoney_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::Function_MetaDataParams[] = {
		{ "Category", "Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "GetMoney", nullptr, nullptr, sizeof(Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::WalletComponent_eventGetMoney_Parms), Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_GetMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_GetMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics
	{
		static const UECodeGen_Private::FIntPropertyParams NewProp_NewMoney;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::NewProp_NewMoney = { "NewMoney", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(WalletComponent_eventNotifyOwnerMoneyChanged_Parms, NewMoney), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::NewProp_NewMoney,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player|Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "NotifyOwnerMoneyChanged", nullptr, nullptr, sizeof(WalletComponent_eventNotifyOwnerMoneyChanged_Parms), Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x05080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWalletComponent_OnRep_Money_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_OnRep_Money_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player|Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_OnRep_Money_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "OnRep_Money", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_OnRep_Money_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_OnRep_Money_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_OnRep_Money()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_OnRep_Money_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWalletComponent_SetMoney_Statics
	{
		struct WalletComponent_eventSetMoney_Parms
		{
			int32 InMoney;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_InMoney;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::NewProp_InMoney = { "InMoney", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(WalletComponent_eventSetMoney_Parms, InMoney), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::NewProp_InMoney,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::Function_MetaDataParams[] = {
		{ "Category", "Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "SetMoney", nullptr, nullptr, sizeof(Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::WalletComponent_eventSetMoney_Parms), Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_SetMoney()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_SetMoney_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UWalletComponent_TryBuy_Statics
	{
		struct WalletComponent_eventTryBuy_Parms
		{
			int32 Cost;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_Cost;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::NewProp_Cost = { "Cost", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(WalletComponent_eventTryBuy_Parms, Cost), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WalletComponent_eventTryBuy_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(WalletComponent_eventTryBuy_Parms), &Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::NewProp_Cost,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::Function_MetaDataParams[] = {
		{ "Category", "Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWalletComponent, nullptr, "TryBuy", nullptr, nullptr, sizeof(Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::WalletComponent_eventTryBuy_Parms), Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWalletComponent_TryBuy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UWalletComponent_TryBuy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UWalletComponent);
	UClass* Z_Construct_UClass_UWalletComponent_NoRegister()
	{
		return UWalletComponent::StaticClass();
	}
	struct Z_Construct_UClass_UWalletComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnPlayerMoneyChanged_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPlayerMoneyChanged;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Money_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_Money;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWalletComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Money,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWalletComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWalletComponent_AddMoney, "AddMoney" }, // 671103076
		{ &Z_Construct_UFunction_UWalletComponent_CanBuy, "CanBuy" }, // 2256669284
		{ &Z_Construct_UFunction_UWalletComponent_GetMoney, "GetMoney" }, // 226627233
		{ &Z_Construct_UFunction_UWalletComponent_NotifyOwnerMoneyChanged, "NotifyOwnerMoneyChanged" }, // 3994045811
		{ &Z_Construct_UFunction_UWalletComponent_OnRep_Money, "OnRep_Money" }, // 3232737709
		{ &Z_Construct_UFunction_UWalletComponent_SetMoney, "SetMoney" }, // 723821671
		{ &Z_Construct_UFunction_UWalletComponent_TryBuy, "TryBuy" }, // 3858543795
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWalletComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Money/WalletComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWalletComponent_Statics::NewProp_OnPlayerMoneyChanged_MetaData[] = {
		{ "Category", "Player|Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UWalletComponent_Statics::NewProp_OnPlayerMoneyChanged = { "OnPlayerMoneyChanged", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UWalletComponent, OnPlayerMoneyChanged), Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UWalletComponent_Statics::NewProp_OnPlayerMoneyChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWalletComponent_Statics::NewProp_OnPlayerMoneyChanged_MetaData)) }; // 885908808
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWalletComponent_Statics::NewProp_Money_MetaData[] = {
		{ "Category", "Money" },
		{ "ModuleRelativePath", "Public/Money/WalletComponent.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWalletComponent_Statics::NewProp_Money = { "Money", "OnRep_Money", (EPropertyFlags)0x0020080100000025, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UWalletComponent, Money), METADATA_PARAMS(Z_Construct_UClass_UWalletComponent_Statics::NewProp_Money_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWalletComponent_Statics::NewProp_Money_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWalletComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWalletComponent_Statics::NewProp_OnPlayerMoneyChanged,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWalletComponent_Statics::NewProp_Money,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWalletComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWalletComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UWalletComponent_Statics::ClassParams = {
		&UWalletComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWalletComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UWalletComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWalletComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWalletComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWalletComponent()
	{
		if (!Z_Registration_Info_UClass_UWalletComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UWalletComponent.OuterSingleton, Z_Construct_UClass_UWalletComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UWalletComponent.OuterSingleton;
	}
	template<> MONEY_API UClass* StaticClass<UWalletComponent>()
	{
		return UWalletComponent::StaticClass();
	}

	void UWalletComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_Money(TEXT("Money"));

		const bool bIsValid = true
			&& Name_Money == ClassReps[(int32)ENetFields_Private::Money].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UWalletComponent"));
	}
	UWalletComponent::UWalletComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWalletComponent);
	UWalletComponent::~UWalletComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UWalletComponent, UWalletComponent::StaticClass, TEXT("UWalletComponent"), &Z_Registration_Info_UClass_UWalletComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UWalletComponent), 4179620634U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_2984123851(TEXT("/Script/Money"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
