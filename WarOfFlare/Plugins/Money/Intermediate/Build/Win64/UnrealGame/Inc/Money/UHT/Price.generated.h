// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Money/Price.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MONEY_Price_generated_h
#error "Price.generated.h already included, missing '#pragma once' in Price.h"
#endif
#define MONEY_Price_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_Price_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPrice_Statics; \
	MONEY_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> MONEY_API UScriptStruct* StaticStruct<struct FPrice>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_Price_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
