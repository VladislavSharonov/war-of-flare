// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoney_init() {}
	MONEY_API UFunction* Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_Money;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_Money()
	{
		if (!Z_Registration_Info_UPackage__Script_Money.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_Money_PlayerMoneyChangedSignature__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/Money",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xA66041CD,
				0x721FD188,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_Money.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_Money.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_Money(Z_Construct_UPackage__Script_Money, TEXT("/Script/Money"), Z_Registration_Info_UPackage__Script_Money, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xA66041CD, 0x721FD188));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
