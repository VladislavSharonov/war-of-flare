// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Money/WalletComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MONEY_WalletComponent_generated_h
#error "WalletComponent.generated.h already included, missing '#pragma once' in WalletComponent.h"
#endif
#define MONEY_WalletComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_9_DELEGATE \
MONEY_API void FPlayerMoneyChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& PlayerMoneyChangedSignature, int32 Money);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_RPC_WRAPPERS \
	virtual void NotifyOwnerMoneyChanged_Implementation(int32 NewMoney); \
 \
	DECLARE_FUNCTION(execNotifyOwnerMoneyChanged); \
	DECLARE_FUNCTION(execOnRep_Money); \
	DECLARE_FUNCTION(execTryBuy); \
	DECLARE_FUNCTION(execAddMoney); \
	DECLARE_FUNCTION(execCanBuy); \
	DECLARE_FUNCTION(execGetMoney); \
	DECLARE_FUNCTION(execSetMoney);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void NotifyOwnerMoneyChanged_Implementation(int32 NewMoney); \
 \
	DECLARE_FUNCTION(execNotifyOwnerMoneyChanged); \
	DECLARE_FUNCTION(execOnRep_Money); \
	DECLARE_FUNCTION(execTryBuy); \
	DECLARE_FUNCTION(execAddMoney); \
	DECLARE_FUNCTION(execCanBuy); \
	DECLARE_FUNCTION(execGetMoney); \
	DECLARE_FUNCTION(execSetMoney);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWalletComponent(); \
	friend struct Z_Construct_UClass_UWalletComponent_Statics; \
public: \
	DECLARE_CLASS(UWalletComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Money"), NO_API) \
	DECLARE_SERIALIZER(UWalletComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		Money=NETFIELD_REP_START, \
		NETFIELD_REP_END=Money	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUWalletComponent(); \
	friend struct Z_Construct_UClass_UWalletComponent_Statics; \
public: \
	DECLARE_CLASS(UWalletComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Money"), NO_API) \
	DECLARE_SERIALIZER(UWalletComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		Money=NETFIELD_REP_START, \
		NETFIELD_REP_END=Money	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWalletComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWalletComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWalletComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWalletComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWalletComponent(UWalletComponent&&); \
	NO_API UWalletComponent(const UWalletComponent&); \
public: \
	NO_API virtual ~UWalletComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWalletComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWalletComponent(UWalletComponent&&); \
	NO_API UWalletComponent(const UWalletComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWalletComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWalletComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWalletComponent) \
	NO_API virtual ~UWalletComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_11_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MONEY_API UClass* StaticClass<class UWalletComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Money_Source_Money_Public_Money_WalletComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
