// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "WalletComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerMoneyChangedSignature, int32, Money);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MONEY_API UWalletComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, Category = "Money")
	void SetMoney(int32 InMoney);

	UFUNCTION(BlueprintCallable, Category = "Money")
	int32 GetMoney() const { return Money; }

	UFUNCTION(BlueprintPure, Category = "Money")
	bool CanBuy(int32 Cost) const;

	UFUNCTION(BlueprintCallable, Category = "Money")
	void AddMoney(int32 InMoney);

	UFUNCTION(BlueprintCallable, Category = "Money")
	bool TryBuy(int32 Cost);

public:
	UPROPERTY(BlueprintAssignable, Category = "Player|Money")
	FPlayerMoneyChangedSignature OnPlayerMoneyChanged;

protected:
	UFUNCTION(BlueprintCallable, Category = "Player|Money")
	void OnRep_Money() { OnPlayerMoneyChanged.Broadcast(Money); }

	UFUNCTION(BlueprintCallable, Client, Reliable, Category = "Player|Money")
	void NotifyOwnerMoneyChanged(int32 NewMoney);

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing = OnRep_Money, Category = "Money")
	int32 Money = 120;
};
