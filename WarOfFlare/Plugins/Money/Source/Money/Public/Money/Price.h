﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "Price.generated.h"

USTRUCT(BlueprintType)
struct FPrice : public FTableRowBase
{
	GENERATED_BODY()

	FPrice(const int32 InAmount = 0) : Amount(InAmount) {}

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (MakeStructureDefaultValue = 0))
	int32 Amount = 0;
};
