﻿// DemoDreams. All rights reserved.

#include <Money/WalletComponent.h>
#include "Net/UnrealNetwork.h"

void UWalletComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UWalletComponent, Money, COND_SkipOwner);
}

void UWalletComponent::SetMoney(int32 InMoney)
{
	Money = InMoney;
	OnPlayerMoneyChanged.Broadcast(Money);
	NotifyOwnerMoneyChanged(Money);
}

bool UWalletComponent::CanBuy(const int32 Cost) const
{
	return Cost <= GetMoney();
}

bool UWalletComponent::TryBuy(const int32 Cost)
{
	const bool bCanBuy = CanBuy(Cost);
	if (bCanBuy)
		SetMoney(GetMoney() - Cost);

	return bCanBuy;
}

void UWalletComponent::NotifyOwnerMoneyChanged_Implementation(int32 NewMoney)
{
	Money = NewMoney;
	OnPlayerMoneyChanged.Broadcast(Money);
}

void UWalletComponent::AddMoney(const int32 InMoney)
{
	SetMoney(GetMoney() + InMoney);
}
