// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Grid/Public/Grid/GridComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGridComponent() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	GRID_API UClass* Z_Construct_UClass_UGridComponent();
	GRID_API UClass* Z_Construct_UClass_UGridComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Grid();
// End Cross Module References
	DEFINE_FUNCTION(UGridComponent::execOnActorDestroyed)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnActorDestroyed(Z_Param_Actor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execGetOccupiedCells)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FIntVector,AActor*>*)Z_Param__Result=P_THIS->GetOccupiedCells();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execDeoccupy)
	{
		P_GET_STRUCT(FIntVector,Z_Param_Cell);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Deoccupy(Z_Param_Cell);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execOccupy)
	{
		P_GET_STRUCT(FIntVector,Z_Param_Cell);
		P_GET_OBJECT(AActor,Z_Param_Block);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Occupy(Z_Param_Cell,Z_Param_Block);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execGetObject)
	{
		P_GET_STRUCT_REF(FIntVector,Z_Param_Out_Cell);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=P_THIS->GetObject(Z_Param_Out_Cell);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execIsOccupied)
	{
		P_GET_STRUCT_REF(FIntVector,Z_Param_Out_Cell);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsOccupied(Z_Param_Out_Cell);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execSetCellSize)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCellSize(Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execGetCellSize)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetCellSize();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execSnap)
	{
		P_GET_STRUCT(FVector,Z_Param_Location);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->Snap(Z_Param_Location);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execGetCell)
	{
		P_GET_STRUCT(FVector,Z_Param_Location);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FIntVector*)Z_Param__Result=P_THIS->GetCell(Z_Param_Location);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UGridComponent::execGetCenter)
	{
		P_GET_STRUCT(FIntVector,Z_Param_Cell);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetCenter(Z_Param_Cell);
		P_NATIVE_END;
	}
	void UGridComponent::StaticRegisterNativesUGridComponent()
	{
		UClass* Class = UGridComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Deoccupy", &UGridComponent::execDeoccupy },
			{ "GetCell", &UGridComponent::execGetCell },
			{ "GetCellSize", &UGridComponent::execGetCellSize },
			{ "GetCenter", &UGridComponent::execGetCenter },
			{ "GetObject", &UGridComponent::execGetObject },
			{ "GetOccupiedCells", &UGridComponent::execGetOccupiedCells },
			{ "IsOccupied", &UGridComponent::execIsOccupied },
			{ "Occupy", &UGridComponent::execOccupy },
			{ "OnActorDestroyed", &UGridComponent::execOnActorDestroyed },
			{ "SetCellSize", &UGridComponent::execSetCellSize },
			{ "Snap", &UGridComponent::execSnap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGridComponent_Deoccupy_Statics
	{
		struct GridComponent_eventDeoccupy_Parms
		{
			FIntVector Cell;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Cell;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::NewProp_Cell = { "Cell", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventDeoccupy_Parms, Cell), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::NewProp_Cell,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "Deoccupy", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::GridComponent_eventDeoccupy_Parms), Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_Deoccupy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_Deoccupy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_GetCell_Statics
	{
		struct GridComponent_eventGetCell_Parms
		{
			FVector Location;
			FIntVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_GetCell_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetCell_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_GetCell_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetCell_Parms, ReturnValue), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_GetCell_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetCell_Statics::NewProp_Location,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetCell_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_GetCell_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_GetCell_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "GetCell", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_GetCell_Statics::GridComponent_eventGetCell_Parms), Z_Construct_UFunction_UGridComponent_GetCell_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetCell_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_GetCell_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetCell_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_GetCell()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_GetCell_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_GetCellSize_Statics
	{
		struct GridComponent_eventGetCellSize_Parms
		{
			float ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetCellSize_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "GetCellSize", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::GridComponent_eventGetCellSize_Parms), Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_GetCellSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_GetCellSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_GetCenter_Statics
	{
		struct GridComponent_eventGetCenter_Parms
		{
			FIntVector Cell;
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Cell;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_GetCenter_Statics::NewProp_Cell = { "Cell", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetCenter_Parms, Cell), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_GetCenter_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetCenter_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_GetCenter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetCenter_Statics::NewProp_Cell,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetCenter_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_GetCenter_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_GetCenter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "GetCenter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_GetCenter_Statics::GridComponent_eventGetCenter_Parms), Z_Construct_UFunction_UGridComponent_GetCenter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetCenter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_GetCenter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetCenter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_GetCenter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_GetCenter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_GetObject_Statics
	{
		struct GridComponent_eventGetObject_Parms
		{
			FIntVector Cell;
			AActor* ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Cell_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Cell;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_Cell_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_Cell = { "Cell", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetObject_Parms, Cell), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_Cell_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_Cell_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetObject_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_GetObject_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_Cell,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetObject_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_GetObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_GetObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "GetObject", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_GetObject_Statics::GridComponent_eventGetObject_Parms), Z_Construct_UFunction_UGridComponent_GetObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_GetObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_GetObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_GetObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics
	{
		struct GridComponent_eventGetOccupiedCells_Parms
		{
			TMap<FIntVector,AActor*> ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_ValueProp;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UECodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventGetOccupiedCells_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::NewProp_ReturnValue_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "GetOccupiedCells", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::GridComponent_eventGetOccupiedCells_Parms), Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_GetOccupiedCells()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_GetOccupiedCells_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_IsOccupied_Statics
	{
		struct GridComponent_eventIsOccupied_Parms
		{
			FIntVector Cell;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Cell_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Cell;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_Cell_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_Cell = { "Cell", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventIsOccupied_Parms, Cell), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_Cell_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_Cell_MetaData)) };
	void Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GridComponent_eventIsOccupied_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(GridComponent_eventIsOccupied_Parms), &Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_Cell,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "IsOccupied", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::GridComponent_eventIsOccupied_Parms), Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_IsOccupied()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_IsOccupied_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_Occupy_Statics
	{
		struct GridComponent_eventOccupy_Parms
		{
			FIntVector Cell;
			AActor* Block;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Cell;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Block;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_Occupy_Statics::NewProp_Cell = { "Cell", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventOccupy_Parms, Cell), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGridComponent_Occupy_Statics::NewProp_Block = { "Block", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventOccupy_Parms, Block), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_Occupy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_Occupy_Statics::NewProp_Cell,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_Occupy_Statics::NewProp_Block,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_Occupy_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_Occupy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "Occupy", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_Occupy_Statics::GridComponent_eventOccupy_Parms), Z_Construct_UFunction_UGridComponent_Occupy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_Occupy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_Occupy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_Occupy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_Occupy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_Occupy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics
	{
		struct GridComponent_eventOnActorDestroyed_Parms
		{
			AActor* Actor;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventOnActorDestroyed_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "OnActorDestroyed", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::GridComponent_eventOnActorDestroyed_Parms), Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_OnActorDestroyed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_OnActorDestroyed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_SetCellSize_Statics
	{
		struct GridComponent_eventSetCellSize_Parms
		{
			float Value;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventSetCellSize_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "SetCellSize", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::GridComponent_eventSetCellSize_Parms), Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_SetCellSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_SetCellSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UGridComponent_Snap_Statics
	{
		struct GridComponent_eventSnap_Parms
		{
			FVector Location;
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_Snap_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventSnap_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGridComponent_Snap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(GridComponent_eventSnap_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGridComponent_Snap_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_Snap_Statics::NewProp_Location,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGridComponent_Snap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGridComponent_Snap_Statics::Function_MetaDataParams[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UGridComponent_Snap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGridComponent, nullptr, "Snap", nullptr, nullptr, sizeof(Z_Construct_UFunction_UGridComponent_Snap_Statics::GridComponent_eventSnap_Parms), Z_Construct_UFunction_UGridComponent_Snap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_Snap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGridComponent_Snap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGridComponent_Snap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGridComponent_Snap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UGridComponent_Snap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGridComponent);
	UClass* Z_Construct_UClass_UGridComponent_NoRegister()
	{
		return UGridComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGridComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CellSize_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_CellSize;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OccupiedCells_ValueProp;
		static const UECodeGen_Private::FStructPropertyParams NewProp_OccupiedCells_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OccupiedCells_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_OccupiedCells;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Grid,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGridComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGridComponent_Deoccupy, "Deoccupy" }, // 314489166
		{ &Z_Construct_UFunction_UGridComponent_GetCell, "GetCell" }, // 4292881791
		{ &Z_Construct_UFunction_UGridComponent_GetCellSize, "GetCellSize" }, // 2414279466
		{ &Z_Construct_UFunction_UGridComponent_GetCenter, "GetCenter" }, // 3498329616
		{ &Z_Construct_UFunction_UGridComponent_GetObject, "GetObject" }, // 1343434757
		{ &Z_Construct_UFunction_UGridComponent_GetOccupiedCells, "GetOccupiedCells" }, // 3002905810
		{ &Z_Construct_UFunction_UGridComponent_IsOccupied, "IsOccupied" }, // 3815964847
		{ &Z_Construct_UFunction_UGridComponent_Occupy, "Occupy" }, // 2333127969
		{ &Z_Construct_UFunction_UGridComponent_OnActorDestroyed, "OnActorDestroyed" }, // 6246567
		{ &Z_Construct_UFunction_UGridComponent_SetCellSize, "SetCellSize" }, // 2046792571
		{ &Z_Construct_UFunction_UGridComponent_Snap, "Snap" }, // 257642706
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Grid/GridComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridComponent_Statics::NewProp_CellSize_MetaData[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGridComponent_Statics::NewProp_CellSize = { "CellSize", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UGridComponent, CellSize), METADATA_PARAMS(Z_Construct_UClass_UGridComponent_Statics::NewProp_CellSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGridComponent_Statics::NewProp_CellSize_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_ValueProp = { "OccupiedCells", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_Key_KeyProp = { "OccupiedCells_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_MetaData[] = {
		{ "Category", "Grid" },
		{ "ModuleRelativePath", "Public/Grid/GridComponent.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells = { "OccupiedCells", nullptr, (EPropertyFlags)0x0020080000010005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UGridComponent, OccupiedCells), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGridComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridComponent_Statics::NewProp_CellSize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridComponent_Statics::NewProp_OccupiedCells,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGridComponent_Statics::ClassParams = {
		&UGridComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGridComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGridComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGridComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridComponent()
	{
		if (!Z_Registration_Info_UClass_UGridComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGridComponent.OuterSingleton, Z_Construct_UClass_UGridComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGridComponent.OuterSingleton;
	}
	template<> GRID_API UClass* StaticClass<UGridComponent>()
	{
		return UGridComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridComponent);
	UGridComponent::~UGridComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGridComponent, UGridComponent::StaticClass, TEXT("UGridComponent"), &Z_Registration_Info_UClass_UGridComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGridComponent), 21459164U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_2998994261(TEXT("/Script/Grid"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
