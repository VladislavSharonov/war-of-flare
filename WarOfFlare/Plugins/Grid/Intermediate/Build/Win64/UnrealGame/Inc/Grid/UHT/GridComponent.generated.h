// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Grid/GridComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
 class AActor;
class AActor;
#ifdef GRID_GridComponent_generated_h
#error "GridComponent.generated.h already included, missing '#pragma once' in GridComponent.h"
#endif
#define GRID_GridComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnActorDestroyed); \
	DECLARE_FUNCTION(execGetOccupiedCells); \
	DECLARE_FUNCTION(execDeoccupy); \
	DECLARE_FUNCTION(execOccupy); \
	DECLARE_FUNCTION(execGetObject); \
	DECLARE_FUNCTION(execIsOccupied); \
	DECLARE_FUNCTION(execSetCellSize); \
	DECLARE_FUNCTION(execGetCellSize); \
	DECLARE_FUNCTION(execSnap); \
	DECLARE_FUNCTION(execGetCell); \
	DECLARE_FUNCTION(execGetCenter);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnActorDestroyed); \
	DECLARE_FUNCTION(execGetOccupiedCells); \
	DECLARE_FUNCTION(execDeoccupy); \
	DECLARE_FUNCTION(execOccupy); \
	DECLARE_FUNCTION(execGetObject); \
	DECLARE_FUNCTION(execIsOccupied); \
	DECLARE_FUNCTION(execSetCellSize); \
	DECLARE_FUNCTION(execGetCellSize); \
	DECLARE_FUNCTION(execSnap); \
	DECLARE_FUNCTION(execGetCell); \
	DECLARE_FUNCTION(execGetCenter);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridComponent(); \
	friend struct Z_Construct_UClass_UGridComponent_Statics; \
public: \
	DECLARE_CLASS(UGridComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Grid"), NO_API) \
	DECLARE_SERIALIZER(UGridComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUGridComponent(); \
	friend struct Z_Construct_UClass_UGridComponent_Statics; \
public: \
	DECLARE_CLASS(UGridComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Grid"), NO_API) \
	DECLARE_SERIALIZER(UGridComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridComponent(UGridComponent&&); \
	NO_API UGridComponent(const UGridComponent&); \
public: \
	NO_API virtual ~UGridComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridComponent(UGridComponent&&); \
	NO_API UGridComponent(const UGridComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGridComponent) \
	NO_API virtual ~UGridComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_10_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GRID_API UClass* StaticClass<class UGridComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Grid_Source_Grid_Public_Grid_GridComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
