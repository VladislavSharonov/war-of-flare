﻿// DemoDreams. All rights reserved.

#include "Grid/GridComponent.h"

UGridComponent::UGridComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

FVector UGridComponent::GetCenter(const FIntVector Cell) const
{
    return FVector(Cell) * CellSize;
}

FIntVector UGridComponent::GetCell(const FVector Location) const
{
    return FIntVector(
        FMath::RoundToInt(Location.X / CellSize),
        FMath::RoundToInt(Location.Y / CellSize),
        FMath::RoundToInt(Location.Z / CellSize));
}

FVector UGridComponent::Snap(const FVector Location) const
{
    return Location.GridSnap(CellSize);
}

bool UGridComponent::IsOccupied(const FIntVector& Cell) const
{
    return OccupiedCells.Contains(Cell);
}

AActor* UGridComponent::GetObject(const FIntVector& Cell) const
{
    return OccupiedCells[Cell];
}

void UGridComponent::Occupy(const FIntVector Cell, AActor* Block)
{
    if (!IsValid(Block))
        return;
    
    Block->OnDestroyed.AddUniqueDynamic(this, &UGridComponent::OnActorDestroyed);
    OccupiedCells.Add(Cell, Block);
}

void UGridComponent::Deoccupy(const FIntVector Cell)
{
    if (!OccupiedCells.Contains(Cell))
            return;

    if (AActor* Block = OccupiedCells[Cell]; IsValid(Block))
    {
        Block->OnDestroyed.RemoveDynamic(this, &UGridComponent::OnActorDestroyed);
    }
    
    OccupiedCells.Remove(Cell);
}

void UGridComponent::OnActorDestroyed(AActor* Actor)
{
    if (const FIntVector* Cell = OccupiedCells.FindKey(Actor); Cell != nullptr)
        OccupiedCells.Remove(*Cell);
}
