﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "GridComponent.generated.h"

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GRID_API UGridComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UGridComponent();

#pragma region Cells
public:
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Grid")
    FVector GetCenter(FIntVector Cell) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Grid")
    FIntVector GetCell(FVector Location) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Grid")
    FVector Snap(FVector Location) const;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Grid")
    float GetCellSize() const { return CellSize; }

    UFUNCTION(BlueprintCallable, Category = "Grid")
    void SetCellSize(float Value) { CellSize = Value; }
protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grid")
    float CellSize = 100.0f;
#pragma endregion

#pragma region CellStates
public:
    UFUNCTION(BlueprintPure, Category = "Grid")
    bool IsOccupied(const FIntVector& Cell) const;

    UFUNCTION(BlueprintPure, Category = "Grid")
    AActor* GetObject(const FIntVector& Cell) const;

    UFUNCTION(BlueprintCallable, Category = "Grid")
    void Occupy(FIntVector Cell, AActor* Block);

    UFUNCTION(BlueprintCallable, Category = "Grid")
    void Deoccupy(FIntVector Cell);

    UFUNCTION(BlueprintCallable, Category = "Grid")
    TMap<FIntVector, AActor*>& GetOccupiedCells() { return OccupiedCells; }
    
protected:
    UFUNCTION(BlueprintCallable, Category = "Grid")
    void OnActorDestroyed(AActor* Actor);
    
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Grid")
    TMap<FIntVector, AActor*> OccupiedCells;
#pragma endregion
};
