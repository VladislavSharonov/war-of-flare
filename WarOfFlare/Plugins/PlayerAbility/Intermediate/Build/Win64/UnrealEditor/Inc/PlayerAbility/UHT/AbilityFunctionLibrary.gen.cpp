// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityFunctionLibrary.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityFunctionLibrary() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityEffect_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityFunctionLibrary();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityFunctionLibrary_NoRegister();
	PLAYERABILITY_API UEnum* Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityFunctionLibrary::execSwitchAbilityByTargetPolicy)
	{
		P_GET_OBJECT(UClass,Z_Param_Ability);
		P_GET_ENUM_REF(EAbilityTargetPolicy,Z_Param_Out_OutAbilityTargetPolicy);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAbilityFunctionLibrary::SwitchAbilityByTargetPolicy(Z_Param_Ability,(EAbilityTargetPolicy&)(Z_Param_Out_OutAbilityTargetPolicy));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityFunctionLibrary::execGetTargetPolicy)
	{
		P_GET_OBJECT(UClass,Z_Param_Ability);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EAbilityTargetPolicy*)Z_Param__Result=UAbilityFunctionLibrary::GetTargetPolicy(Z_Param_Ability);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityFunctionLibrary::execAddOrExtendEffect)
	{
		P_GET_OBJECT(AActor,Z_Param_Target);
		P_GET_OBJECT(UClass,Z_Param_EffectClass);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Lifetime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UAbilityEffect**)Z_Param__Result=UAbilityFunctionLibrary::AddOrExtendEffect(Z_Param_Target,Z_Param_EffectClass,Z_Param_Lifetime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityFunctionLibrary::execAddEffectWithLifetime)
	{
		P_GET_OBJECT(AActor,Z_Param_Target);
		P_GET_OBJECT(UClass,Z_Param_EffectClass);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Lifetime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UAbilityEffect**)Z_Param__Result=UAbilityFunctionLibrary::AddEffectWithLifetime(Z_Param_Target,Z_Param_EffectClass,Z_Param_Lifetime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityFunctionLibrary::execGetOrAddEffect)
	{
		P_GET_OBJECT(AActor,Z_Param_Target);
		P_GET_OBJECT(UClass,Z_Param_EffectClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UAbilityEffect**)Z_Param__Result=UAbilityFunctionLibrary::GetOrAddEffect(Z_Param_Target,Z_Param_EffectClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityFunctionLibrary::execAddEffect)
	{
		P_GET_OBJECT(AActor,Z_Param_Target);
		P_GET_OBJECT(UClass,Z_Param_EffectClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UAbilityEffect**)Z_Param__Result=UAbilityFunctionLibrary::AddEffect(Z_Param_Target,Z_Param_EffectClass);
		P_NATIVE_END;
	}
	void UAbilityFunctionLibrary::StaticRegisterNativesUAbilityFunctionLibrary()
	{
		UClass* Class = UAbilityFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddEffect", &UAbilityFunctionLibrary::execAddEffect },
			{ "AddEffectWithLifetime", &UAbilityFunctionLibrary::execAddEffectWithLifetime },
			{ "AddOrExtendEffect", &UAbilityFunctionLibrary::execAddOrExtendEffect },
			{ "GetOrAddEffect", &UAbilityFunctionLibrary::execGetOrAddEffect },
			{ "GetTargetPolicy", &UAbilityFunctionLibrary::execGetTargetPolicy },
			{ "SwitchAbilityByTargetPolicy", &UAbilityFunctionLibrary::execSwitchAbilityByTargetPolicy },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics
	{
		struct AbilityFunctionLibrary_eventAddEffect_Parms
		{
			AActor* Target;
			TSubclassOf<UAbilityEffect>  EffectClass;
			UAbilityEffect* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UECodeGen_Private::FClassPropertyParams NewProp_EffectClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffect_Parms, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_EffectClass = { "EffectClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffect_Parms, EffectClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffect_Parms, ReturnValue), Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_Target,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_EffectClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "Comment", "/**\n\x09 * Add new effect to the actor.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
		{ "ToolTip", "Add new effect to the actor." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityFunctionLibrary, nullptr, "AddEffect", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::AbilityFunctionLibrary_eventAddEffect_Parms), Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics
	{
		struct AbilityFunctionLibrary_eventAddEffectWithLifetime_Parms
		{
			AActor* Target;
			TSubclassOf<UAbilityEffect>  EffectClass;
			float Lifetime;
			UAbilityEffect* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UECodeGen_Private::FClassPropertyParams NewProp_EffectClass;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Lifetime;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffectWithLifetime_Parms, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_EffectClass = { "EffectClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffectWithLifetime_Parms, EffectClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_Lifetime = { "Lifetime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffectWithLifetime_Parms, Lifetime), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddEffectWithLifetime_Parms, ReturnValue), Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_Target,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_EffectClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_Lifetime,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityFunctionLibrary, nullptr, "AddEffectWithLifetime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::AbilityFunctionLibrary_eventAddEffectWithLifetime_Parms), Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics
	{
		struct AbilityFunctionLibrary_eventAddOrExtendEffect_Parms
		{
			AActor* Target;
			TSubclassOf<UAbilityEffect>  EffectClass;
			float Lifetime;
			UAbilityEffect* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UECodeGen_Private::FClassPropertyParams NewProp_EffectClass;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Lifetime;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddOrExtendEffect_Parms, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_EffectClass = { "EffectClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddOrExtendEffect_Parms, EffectClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_Lifetime = { "Lifetime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddOrExtendEffect_Parms, Lifetime), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventAddOrExtendEffect_Parms, ReturnValue), Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_Target,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_EffectClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_Lifetime,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "Comment", "/**\n\x09 * A new effect will be added if the actor does not have one. Otherwise effect time will be extended.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
		{ "ToolTip", "A new effect will be added if the actor does not have one. Otherwise effect time will be extended." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityFunctionLibrary, nullptr, "AddOrExtendEffect", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::AbilityFunctionLibrary_eventAddOrExtendEffect_Parms), Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics
	{
		struct AbilityFunctionLibrary_eventGetOrAddEffect_Parms
		{
			AActor* Target;
			TSubclassOf<UAbilityEffect>  EffectClass;
			UAbilityEffect* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UECodeGen_Private::FClassPropertyParams NewProp_EffectClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventGetOrAddEffect_Parms, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_EffectClass = { "EffectClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventGetOrAddEffect_Parms, EffectClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventGetOrAddEffect_Parms, ReturnValue), Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_Target,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_EffectClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "Comment", "/**\n\x09 * A new effect will be return if the actor have one. Otherwise the new effect will be added.\n\x09 */" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
		{ "ToolTip", "A new effect will be return if the actor have one. Otherwise the new effect will be added." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityFunctionLibrary, nullptr, "GetOrAddEffect", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::AbilityFunctionLibrary_eventGetOrAddEffect_Parms), Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics
	{
		struct AbilityFunctionLibrary_eventGetTargetPolicy_Parms
		{
			TSubclassOf<AAbility>  Ability;
			EAbilityTargetPolicy ReturnValue;
		};
		static const UECodeGen_Private::FClassPropertyParams NewProp_Ability;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::NewProp_Ability = { "Ability", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventGetTargetPolicy_Parms, Ability), Z_Construct_UClass_UClass, Z_Construct_UClass_AAbility_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventGetTargetPolicy_Parms, ReturnValue), Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy, METADATA_PARAMS(nullptr, 0) }; // 1400898670
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::NewProp_Ability,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityFunctionLibrary, nullptr, "GetTargetPolicy", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::AbilityFunctionLibrary_eventGetTargetPolicy_Parms), Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics
	{
		struct AbilityFunctionLibrary_eventSwitchAbilityByTargetPolicy_Parms
		{
			TSubclassOf<AAbility>  Ability;
			EAbilityTargetPolicy OutAbilityTargetPolicy;
		};
		static const UECodeGen_Private::FClassPropertyParams NewProp_Ability;
		static const UECodeGen_Private::FBytePropertyParams NewProp_OutAbilityTargetPolicy_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_OutAbilityTargetPolicy;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::NewProp_Ability = { "Ability", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventSwitchAbilityByTargetPolicy_Parms, Ability), Z_Construct_UClass_UClass, Z_Construct_UClass_AAbility_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::NewProp_OutAbilityTargetPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::NewProp_OutAbilityTargetPolicy = { "OutAbilityTargetPolicy", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityFunctionLibrary_eventSwitchAbilityByTargetPolicy_Parms, OutAbilityTargetPolicy), Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy, METADATA_PARAMS(nullptr, 0) }; // 1400898670
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::NewProp_Ability,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::NewProp_OutAbilityTargetPolicy_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::NewProp_OutAbilityTargetPolicy,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::Function_MetaDataParams[] = {
		{ "ExpandEnumAsExecs", "OutAbilityTargetPolicy" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityFunctionLibrary, nullptr, "SwitchAbilityByTargetPolicy", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::AbilityFunctionLibrary_eventSwitchAbilityByTargetPolicy_Parms), Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAbilityFunctionLibrary);
	UClass* Z_Construct_UClass_UAbilityFunctionLibrary_NoRegister()
	{
		return UAbilityFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffect, "AddEffect" }, // 1274619865
		{ &Z_Construct_UFunction_UAbilityFunctionLibrary_AddEffectWithLifetime, "AddEffectWithLifetime" }, // 2635691750
		{ &Z_Construct_UFunction_UAbilityFunctionLibrary_AddOrExtendEffect, "AddOrExtendEffect" }, // 3185196206
		{ &Z_Construct_UFunction_UAbilityFunctionLibrary_GetOrAddEffect, "GetOrAddEffect" }, // 2139937195
		{ &Z_Construct_UFunction_UAbilityFunctionLibrary_GetTargetPolicy, "GetTargetPolicy" }, // 341650582
		{ &Z_Construct_UFunction_UAbilityFunctionLibrary_SwitchAbilityByTargetPolicy, "SwitchAbilityByTargetPolicy" }, // 3329649374
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlayerAbility/AbilityFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityFunctionLibrary>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAbilityFunctionLibrary_Statics::ClassParams = {
		&UAbilityFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityFunctionLibrary()
	{
		if (!Z_Registration_Info_UClass_UAbilityFunctionLibrary.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAbilityFunctionLibrary.OuterSingleton, Z_Construct_UClass_UAbilityFunctionLibrary_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAbilityFunctionLibrary.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<UAbilityFunctionLibrary>()
	{
		return UAbilityFunctionLibrary::StaticClass();
	}
	UAbilityFunctionLibrary::UAbilityFunctionLibrary(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityFunctionLibrary);
	UAbilityFunctionLibrary::~UAbilityFunctionLibrary() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAbilityFunctionLibrary, UAbilityFunctionLibrary::StaticClass, TEXT("UAbilityFunctionLibrary"), &Z_Registration_Info_UClass_UAbilityFunctionLibrary, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAbilityFunctionLibrary), 978847968U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_3667285433(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
