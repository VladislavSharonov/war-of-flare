// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/AbilityEffect.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYERABILITY_AbilityEffect_generated_h
#error "AbilityEffect.generated.h already included, missing '#pragma once' in AbilityEffect.h"
#endif
#define PLAYERABILITY_AbilityEffect_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_RPC_WRAPPERS \
	virtual void Finish_Implementation(); \
	virtual void Cancel_Implementation(); \
	virtual void Start_Implementation(); \
 \
	DECLARE_FUNCTION(execExtendLifetime); \
	DECLARE_FUNCTION(execSetLifetime); \
	DECLARE_FUNCTION(execFinish); \
	DECLARE_FUNCTION(execCancel); \
	DECLARE_FUNCTION(execStart);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExtendLifetime); \
	DECLARE_FUNCTION(execSetLifetime); \
	DECLARE_FUNCTION(execFinish); \
	DECLARE_FUNCTION(execCancel); \
	DECLARE_FUNCTION(execStart);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAbilityEffect(); \
	friend struct Z_Construct_UClass_UAbilityEffect_Statics; \
public: \
	DECLARE_CLASS(UAbilityEffect, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(UAbilityEffect)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUAbilityEffect(); \
	friend struct Z_Construct_UClass_UAbilityEffect_Statics; \
public: \
	DECLARE_CLASS(UAbilityEffect, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(UAbilityEffect)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAbilityEffect(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAbilityEffect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbilityEffect); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbilityEffect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbilityEffect(UAbilityEffect&&); \
	NO_API UAbilityEffect(const UAbilityEffect&); \
public: \
	NO_API virtual ~UAbilityEffect();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbilityEffect(UAbilityEffect&&); \
	NO_API UAbilityEffect(const UAbilityEffect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbilityEffect); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbilityEffect); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAbilityEffect) \
	NO_API virtual ~UAbilityEffect();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_9_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYERABILITY_API UClass* StaticClass<class UAbilityEffect>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
