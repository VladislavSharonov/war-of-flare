// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityZone.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityZone() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreview_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbilityZone();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbilityZone_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(AAbilityZone::execSetPlacementPreview)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPlacementPreview_Implementation(Z_Param_InIsPreview);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAbilityZone::execGetOverlappedActors)
	{
		P_GET_OBJECT(UClass,Z_Param_ClassFilter);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Overlaped);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetOverlappedActors_Implementation(Z_Param_ClassFilter,Z_Param_Out_Overlaped);
		P_NATIVE_END;
	}
	struct AbilityZone_eventGetOverlappedActors_Parms
	{
		TSubclassOf<AActor>  ClassFilter;
		TArray<AActor*> Overlaped;
	};
	struct AbilityZone_eventSetPlacementPreview_Parms
	{
		bool InIsPreview;
	};
	static FName NAME_AAbilityZone_GetOverlappedActors = FName(TEXT("GetOverlappedActors"));
	void AAbilityZone::GetOverlappedActors(TSubclassOf<AActor>  ClassFilter, TArray<AActor*>& Overlaped)
	{
		AbilityZone_eventGetOverlappedActors_Parms Parms;
		Parms.ClassFilter=ClassFilter;
		Parms.Overlaped=Overlaped;
		ProcessEvent(FindFunctionChecked(NAME_AAbilityZone_GetOverlappedActors),&Parms);
		Overlaped=Parms.Overlaped;
	}
	static FName NAME_AAbilityZone_SetPlacementPreview = FName(TEXT("SetPlacementPreview"));
	void AAbilityZone::SetPlacementPreview(bool InIsPreview)
	{
		AbilityZone_eventSetPlacementPreview_Parms Parms;
		Parms.InIsPreview=InIsPreview ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AAbilityZone_SetPlacementPreview),&Parms);
	}
	void AAbilityZone::StaticRegisterNativesAAbilityZone()
	{
		UClass* Class = AAbilityZone::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetOverlappedActors", &AAbilityZone::execGetOverlappedActors },
			{ "SetPlacementPreview", &AAbilityZone::execSetPlacementPreview },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics
	{
		static const UECodeGen_Private::FClassPropertyParams NewProp_ClassFilter;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Overlaped_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Overlaped;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::NewProp_ClassFilter = { "ClassFilter", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityZone_eventGetOverlappedActors_Parms, ClassFilter), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::NewProp_Overlaped_Inner = { "Overlaped", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::NewProp_Overlaped = { "Overlaped", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityZone_eventGetOverlappedActors_Parms, Overlaped), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::NewProp_ClassFilter,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::NewProp_Overlaped_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::NewProp_Overlaped,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Zone" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityZone.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAbilityZone, nullptr, "GetOverlappedActors", nullptr, nullptr, sizeof(AbilityZone_eventGetOverlappedActors_Parms), Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAbilityZone_GetOverlappedActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAbilityZone_GetOverlappedActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics
	{
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((AbilityZone_eventSetPlacementPreview_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AbilityZone_eventSetPlacementPreview_Parms), &Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::NewProp_InIsPreview,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityZone.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAbilityZone, nullptr, "SetPlacementPreview", nullptr, nullptr, sizeof(AbilityZone_eventSetPlacementPreview_Parms), Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAbilityZone_SetPlacementPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAbilityZone_SetPlacementPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAbilityZone);
	UClass* Z_Construct_UClass_AAbilityZone_NoRegister()
	{
		return AAbilityZone::StaticClass();
	}
	struct Z_Construct_UClass_AAbilityZone_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAbilityZone_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AAbilityZone_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AAbilityZone_GetOverlappedActors, "GetOverlappedActors" }, // 3335667024
		{ &Z_Construct_UFunction_AAbilityZone_SetPlacementPreview, "SetPlacementPreview" }, // 1022821582
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAbilityZone_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlayerAbility/AbilityZone.h" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityZone.h" },
	};
#endif
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AAbilityZone_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UPlacementPreview_NoRegister, (int32)VTABLE_OFFSET(AAbilityZone, IPlacementPreview), false },  // 174827905
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAbilityZone_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAbilityZone>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAbilityZone_Statics::ClassParams = {
		&AAbilityZone::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAbilityZone_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAbilityZone_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAbilityZone()
	{
		if (!Z_Registration_Info_UClass_AAbilityZone.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAbilityZone.OuterSingleton, Z_Construct_UClass_AAbilityZone_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAbilityZone.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<AAbilityZone>()
	{
		return AAbilityZone::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAbilityZone);
	AAbilityZone::~AAbilityZone() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityZone_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityZone_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAbilityZone, AAbilityZone::StaticClass, TEXT("AAbilityZone"), &Z_Registration_Info_UClass_AAbilityZone, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAbilityZone), 4206758917U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityZone_h_597129971(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityZone_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityZone_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
