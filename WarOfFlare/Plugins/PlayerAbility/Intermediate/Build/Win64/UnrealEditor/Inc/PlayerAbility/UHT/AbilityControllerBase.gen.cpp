// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityControllers/AbilityControllerBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityControllerBase() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityControllerBase();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityControllerBase_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityControllerBase::execFinish)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Finish_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityControllerBase::execStart)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Start_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_UAbilityControllerBase_Finish = FName(TEXT("Finish"));
	void UAbilityControllerBase::Finish()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityControllerBase_Finish),NULL);
	}
	static FName NAME_UAbilityControllerBase_Start = FName(TEXT("Start"));
	void UAbilityControllerBase::Start()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityControllerBase_Start),NULL);
	}
	void UAbilityControllerBase::StaticRegisterNativesUAbilityControllerBase()
	{
		UClass* Class = UAbilityControllerBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Finish", &UAbilityControllerBase::execFinish },
			{ "Start", &UAbilityControllerBase::execStart },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityControllerBase_Finish_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityControllerBase_Finish_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Controller|Duration" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityControllerBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityControllerBase_Finish_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityControllerBase, nullptr, "Finish", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityControllerBase_Finish_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityControllerBase_Finish_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityControllerBase_Finish()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityControllerBase_Finish_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityControllerBase_Start_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityControllerBase_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Controller|Duration" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityControllerBase.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityControllerBase_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityControllerBase, nullptr, "Start", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityControllerBase_Start_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityControllerBase_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityControllerBase_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityControllerBase_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAbilityControllerBase);
	UClass* Z_Construct_UClass_UAbilityControllerBase_NoRegister()
	{
		return UAbilityControllerBase::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityControllerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityControllerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityControllerBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityControllerBase_Finish, "Finish" }, // 1876193780
		{ &Z_Construct_UFunction_UAbilityControllerBase_Start, "Start" }, // 1563824013
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityControllerBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "PlayerAbility/AbilityControllers/AbilityControllerBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityControllerBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityControllerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityControllerBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAbilityControllerBase_Statics::ClassParams = {
		&UAbilityControllerBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityControllerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityControllerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityControllerBase()
	{
		if (!Z_Registration_Info_UClass_UAbilityControllerBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAbilityControllerBase.OuterSingleton, Z_Construct_UClass_UAbilityControllerBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAbilityControllerBase.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<UAbilityControllerBase>()
	{
		return UAbilityControllerBase::StaticClass();
	}
	UAbilityControllerBase::UAbilityControllerBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityControllerBase);
	UAbilityControllerBase::~UAbilityControllerBase() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityControllerBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityControllerBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAbilityControllerBase, UAbilityControllerBase::StaticClass, TEXT("UAbilityControllerBase"), &Z_Registration_Info_UClass_UAbilityControllerBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAbilityControllerBase), 2673510923U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityControllerBase_h_4174191252(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityControllerBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityControllerBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
