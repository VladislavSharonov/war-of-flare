// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityRoundCountController() {}
// Cross Module References
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityControllerBase();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityRoundCountController();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityRoundCountController_NoRegister();
	ROUNDS_API UClass* Z_Construct_UClass_URoundControllerComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityRoundCountController::execOnRoundChanged)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRoundChanged();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityRoundCountController::execSetRoundCount)
	{
		P_GET_PROPERTY(FByteProperty,Z_Param_InRoundCount);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRoundCount(Z_Param_InRoundCount);
		P_NATIVE_END;
	}
	void UAbilityRoundCountController::StaticRegisterNativesUAbilityRoundCountController()
	{
		UClass* Class = UAbilityRoundCountController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnRoundChanged", &UAbilityRoundCountController::execOnRoundChanged },
			{ "SetRoundCount", &UAbilityRoundCountController::execSetRoundCount },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityRoundCountController, nullptr, "OnRoundChanged", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics
	{
		struct AbilityRoundCountController_eventSetRoundCount_Parms
		{
			uint8 InRoundCount;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InRoundCount_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_InRoundCount;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::NewProp_InRoundCount_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::NewProp_InRoundCount = { "InRoundCount", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityRoundCountController_eventSetRoundCount_Parms, InRoundCount), nullptr, METADATA_PARAMS(Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::NewProp_InRoundCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::NewProp_InRoundCount_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::NewProp_InRoundCount,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Controller|WaveCount" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityRoundCountController, nullptr, "SetRoundCount", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::AbilityRoundCountController_eventSetRoundCount_Parms), Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAbilityRoundCountController);
	UClass* Z_Construct_UClass_UAbilityRoundCountController_NoRegister()
	{
		return UAbilityRoundCountController::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityRoundCountController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RoundCount_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_RoundCount;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_StartRound_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_StartRound;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RoundController_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_RoundController;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityRoundCountController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAbilityControllerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityRoundCountController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityRoundCountController_OnRoundChanged, "OnRoundChanged" }, // 1936382742
		{ &Z_Construct_UFunction_UAbilityRoundCountController_SetRoundCount, "SetRoundCount" }, // 2279412389
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityRoundCountController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundCount_MetaData[] = {
		{ "Category", "Ability|Controller|WaveCount" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundCount = { "RoundCount", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityRoundCountController, RoundCount), nullptr, METADATA_PARAMS(Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundCount_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_StartRound_MetaData[] = {
		{ "Category", "Ability|Controller|WaveCount" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_StartRound = { "StartRound", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityRoundCountController, StartRound), nullptr, METADATA_PARAMS(Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_StartRound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_StartRound_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundController_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityRoundCountController.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundController = { "RoundController", nullptr, (EPropertyFlags)0x0024080000080008, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityRoundCountController, RoundController), Z_Construct_UClass_URoundControllerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundController_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAbilityRoundCountController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundCount,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_StartRound,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityRoundCountController_Statics::NewProp_RoundController,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityRoundCountController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityRoundCountController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAbilityRoundCountController_Statics::ClassParams = {
		&UAbilityRoundCountController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAbilityRoundCountController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityRoundCountController_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityRoundCountController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityRoundCountController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityRoundCountController()
	{
		if (!Z_Registration_Info_UClass_UAbilityRoundCountController.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAbilityRoundCountController.OuterSingleton, Z_Construct_UClass_UAbilityRoundCountController_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAbilityRoundCountController.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<UAbilityRoundCountController>()
	{
		return UAbilityRoundCountController::StaticClass();
	}
	UAbilityRoundCountController::UAbilityRoundCountController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityRoundCountController);
	UAbilityRoundCountController::~UAbilityRoundCountController() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityRoundCountController_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityRoundCountController_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAbilityRoundCountController, UAbilityRoundCountController::StaticClass, TEXT("UAbilityRoundCountController"), &Z_Registration_Info_UClass_UAbilityRoundCountController, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAbilityRoundCountController), 4147375187U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityRoundCountController_h_609882102(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityRoundCountController_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityRoundCountController_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
