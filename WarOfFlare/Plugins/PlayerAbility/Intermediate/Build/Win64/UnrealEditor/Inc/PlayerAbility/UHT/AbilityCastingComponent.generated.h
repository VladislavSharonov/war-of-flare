// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/AbilityCastingComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AAbility;
class AActor;
class APlayerController;
enum class EAbilities : uint8;
#ifdef PLAYERABILITY_AbilityCastingComponent_generated_h
#error "AbilityCastingComponent.generated.h already included, missing '#pragma once' in AbilityCastingComponent.h"
#endif
#define PLAYERABILITY_AbilityCastingComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_RPC_WRAPPERS \
	virtual void StartCasting_Implementation(EAbilities InAbility); \
	virtual void EndCasting_Implementation(); \
	virtual void Server_CastZoneAbility_Implementation(APlayerController* InAbilityOwner, EAbilities InAbility, FTransform const& InTransform); \
	virtual void Server_CastTargetAbility_Implementation(APlayerController* InAbilityOwner, EAbilities InAbility, AActor* InTarget); \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execTryBuyAbility); \
	DECLARE_FUNCTION(execGetAbilityClass); \
	DECLARE_FUNCTION(execCastZoneAbility); \
	DECLARE_FUNCTION(execCastTargetAbility); \
	DECLARE_FUNCTION(execCastAbility); \
	DECLARE_FUNCTION(execStartCasting); \
	DECLARE_FUNCTION(execEndCasting); \
	DECLARE_FUNCTION(execServer_CastZoneAbility); \
	DECLARE_FUNCTION(execServer_CastTargetAbility); \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execTryBuyAbility); \
	DECLARE_FUNCTION(execGetAbilityClass); \
	DECLARE_FUNCTION(execCastZoneAbility); \
	DECLARE_FUNCTION(execCastTargetAbility); \
	DECLARE_FUNCTION(execCastAbility); \
	DECLARE_FUNCTION(execStartCasting); \
	DECLARE_FUNCTION(execEndCasting); \
	DECLARE_FUNCTION(execServer_CastZoneAbility); \
	DECLARE_FUNCTION(execServer_CastTargetAbility); \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAbilityCastingComponent(); \
	friend struct Z_Construct_UClass_UAbilityCastingComponent_Statics; \
public: \
	DECLARE_CLASS(UAbilityCastingComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(UAbilityCastingComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUAbilityCastingComponent(); \
	friend struct Z_Construct_UClass_UAbilityCastingComponent_Statics; \
public: \
	DECLARE_CLASS(UAbilityCastingComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(UAbilityCastingComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAbilityCastingComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAbilityCastingComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbilityCastingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbilityCastingComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbilityCastingComponent(UAbilityCastingComponent&&); \
	NO_API UAbilityCastingComponent(const UAbilityCastingComponent&); \
public: \
	NO_API virtual ~UAbilityCastingComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbilityCastingComponent(UAbilityCastingComponent&&); \
	NO_API UAbilityCastingComponent(const UAbilityCastingComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbilityCastingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbilityCastingComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAbilityCastingComponent) \
	NO_API virtual ~UAbilityCastingComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_16_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYERABILITY_API UClass* StaticClass<class UAbilityCastingComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
