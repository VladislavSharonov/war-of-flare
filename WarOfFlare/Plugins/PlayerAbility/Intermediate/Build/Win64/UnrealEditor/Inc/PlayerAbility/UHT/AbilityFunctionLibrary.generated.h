// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/AbilityFunctionLibrary.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AAbility;
class AActor;
class UAbilityEffect;
enum class EAbilityTargetPolicy : uint8;
#ifdef PLAYERABILITY_AbilityFunctionLibrary_generated_h
#error "AbilityFunctionLibrary.generated.h already included, missing '#pragma once' in AbilityFunctionLibrary.h"
#endif
#define PLAYERABILITY_AbilityFunctionLibrary_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSwitchAbilityByTargetPolicy); \
	DECLARE_FUNCTION(execGetTargetPolicy); \
	DECLARE_FUNCTION(execAddOrExtendEffect); \
	DECLARE_FUNCTION(execAddEffectWithLifetime); \
	DECLARE_FUNCTION(execGetOrAddEffect); \
	DECLARE_FUNCTION(execAddEffect);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSwitchAbilityByTargetPolicy); \
	DECLARE_FUNCTION(execGetTargetPolicy); \
	DECLARE_FUNCTION(execAddOrExtendEffect); \
	DECLARE_FUNCTION(execAddEffectWithLifetime); \
	DECLARE_FUNCTION(execGetOrAddEffect); \
	DECLARE_FUNCTION(execAddEffect);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAbilityFunctionLibrary(); \
	friend struct Z_Construct_UClass_UAbilityFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UAbilityFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(UAbilityFunctionLibrary)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUAbilityFunctionLibrary(); \
	friend struct Z_Construct_UClass_UAbilityFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UAbilityFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(UAbilityFunctionLibrary)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAbilityFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAbilityFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbilityFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbilityFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbilityFunctionLibrary(UAbilityFunctionLibrary&&); \
	NO_API UAbilityFunctionLibrary(const UAbilityFunctionLibrary&); \
public: \
	NO_API virtual ~UAbilityFunctionLibrary();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAbilityFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAbilityFunctionLibrary(UAbilityFunctionLibrary&&); \
	NO_API UAbilityFunctionLibrary(const UAbilityFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAbilityFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAbilityFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAbilityFunctionLibrary) \
	NO_API virtual ~UAbilityFunctionLibrary();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_15_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYERABILITY_API UClass* StaticClass<class UAbilityFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
