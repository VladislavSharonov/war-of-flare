// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityClass.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityClass() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility_NoRegister();
	PLAYERABILITY_API UScriptStruct* Z_Construct_UScriptStruct_FAbilityClass();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References

static_assert(std::is_polymorphic<FAbilityClass>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FAbilityClass cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_AbilityClass;
class UScriptStruct* FAbilityClass::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_AbilityClass.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_AbilityClass.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FAbilityClass, (UObject*)Z_Construct_UPackage__Script_PlayerAbility(), TEXT("AbilityClass"));
	}
	return Z_Registration_Info_UScriptStruct_AbilityClass.OuterSingleton;
}
template<> PLAYERABILITY_API UScriptStruct* StaticStruct<FAbilityClass>()
{
	return FAbilityClass::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FAbilityClass_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAbilityClass_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityClass.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAbilityClass_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAbilityClass>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAbilityClass_Statics::NewProp_Class_MetaData[] = {
		{ "Category", "AbilityClass" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityClass.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FAbilityClass_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FAbilityClass, Class), Z_Construct_UClass_UClass, Z_Construct_UClass_AAbility_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAbilityClass_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAbilityClass_Statics::NewProp_Class_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAbilityClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAbilityClass_Statics::NewProp_Class,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAbilityClass_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"AbilityClass",
		sizeof(FAbilityClass),
		alignof(FAbilityClass),
		Z_Construct_UScriptStruct_FAbilityClass_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAbilityClass_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAbilityClass_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAbilityClass_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAbilityClass()
	{
		if (!Z_Registration_Info_UScriptStruct_AbilityClass.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_AbilityClass.InnerSingleton, Z_Construct_UScriptStruct_FAbilityClass_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_AbilityClass.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h_Statics::ScriptStructInfo[] = {
		{ FAbilityClass::StaticStruct, Z_Construct_UScriptStruct_FAbilityClass_Statics::NewStructOps, TEXT("AbilityClass"), &Z_Registration_Info_UScriptStruct_AbilityClass, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FAbilityClass), 3603859593U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h_953331320(TEXT("/Script/PlayerAbility"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
