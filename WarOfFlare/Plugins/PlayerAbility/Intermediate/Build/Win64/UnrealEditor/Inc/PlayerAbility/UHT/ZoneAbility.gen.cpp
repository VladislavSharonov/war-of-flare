// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/ZoneAbility.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeZoneAbility() {}
// Cross Module References
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AZoneAbility();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AZoneAbility_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(AZoneAbility::execIsPreview)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsPreview();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AZoneAbility::execStart)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Start_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_AZoneAbility_Start = FName(TEXT("Start"));
	void AZoneAbility::Start()
	{
		ProcessEvent(FindFunctionChecked(NAME_AZoneAbility_Start),NULL);
	}
	void AZoneAbility::StaticRegisterNativesAZoneAbility()
	{
		UClass* Class = AZoneAbility::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsPreview", &AZoneAbility::execIsPreview },
			{ "Start", &AZoneAbility::execStart },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AZoneAbility_IsPreview_Statics
	{
		struct ZoneAbility_eventIsPreview_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ZoneAbility_eventIsPreview_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ZoneAbility_eventIsPreview_Parms), &Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/ZoneAbility.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AZoneAbility, nullptr, "IsPreview", nullptr, nullptr, sizeof(Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::ZoneAbility_eventIsPreview_Parms), Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AZoneAbility_IsPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AZoneAbility_IsPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AZoneAbility_Start_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AZoneAbility_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/PlayerAbility/ZoneAbility.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AZoneAbility_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AZoneAbility, nullptr, "Start", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AZoneAbility_Start_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AZoneAbility_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AZoneAbility_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AZoneAbility_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AZoneAbility);
	UClass* Z_Construct_UClass_AZoneAbility_NoRegister()
	{
		return AZoneAbility::StaticClass();
	}
	struct Z_Construct_UClass_AZoneAbility_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bIsPreview_MetaData[];
#endif
		static void NewProp_bIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AZoneAbility_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAbility,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AZoneAbility_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AZoneAbility_IsPreview, "IsPreview" }, // 2745668062
		{ &Z_Construct_UFunction_AZoneAbility_Start, "Start" }, // 925398333
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AZoneAbility_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlayerAbility/ZoneAbility.h" },
		{ "ModuleRelativePath", "Public/PlayerAbility/ZoneAbility.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview_MetaData[] = {
		{ "Category", "ZoneAbility" },
		{ "ModuleRelativePath", "Public/PlayerAbility/ZoneAbility.h" },
	};
#endif
	void Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview_SetBit(void* Obj)
	{
		((AZoneAbility*)Obj)->bIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview = { "bIsPreview", nullptr, (EPropertyFlags)0x0020080000000014, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AZoneAbility), &Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AZoneAbility_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AZoneAbility_Statics::NewProp_bIsPreview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AZoneAbility_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AZoneAbility>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AZoneAbility_Statics::ClassParams = {
		&AZoneAbility::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AZoneAbility_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AZoneAbility_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AZoneAbility_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AZoneAbility_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AZoneAbility()
	{
		if (!Z_Registration_Info_UClass_AZoneAbility.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AZoneAbility.OuterSingleton, Z_Construct_UClass_AZoneAbility_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AZoneAbility.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<AZoneAbility>()
	{
		return AZoneAbility::StaticClass();
	}
	AZoneAbility::AZoneAbility() {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AZoneAbility);
	AZoneAbility::~AZoneAbility() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AZoneAbility, AZoneAbility::StaticClass, TEXT("AZoneAbility"), &Z_Registration_Info_UClass_AZoneAbility, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AZoneAbility), 136217902U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_3946749424(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
