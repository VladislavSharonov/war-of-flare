// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityTargetPolicy.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityTargetPolicy() {}
// Cross Module References
	PLAYERABILITY_API UEnum* Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EAbilityTargetPolicy;
	static UEnum* EAbilityTargetPolicy_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EAbilityTargetPolicy.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EAbilityTargetPolicy.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy, (UObject*)Z_Construct_UPackage__Script_PlayerAbility(), TEXT("EAbilityTargetPolicy"));
		}
		return Z_Registration_Info_UEnum_EAbilityTargetPolicy.OuterSingleton;
	}
	template<> PLAYERABILITY_API UEnum* StaticEnum<EAbilityTargetPolicy>()
	{
		return EAbilityTargetPolicy_StaticEnum();
	}
	struct Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::Enumerators[] = {
		{ "EAbilityTargetPolicy::Target", (int64)EAbilityTargetPolicy::Target },
		{ "EAbilityTargetPolicy::Zone", (int64)EAbilityTargetPolicy::Zone },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityTargetPolicy.h" },
		{ "Target.DisplayName", "Target" },
		{ "Target.Name", "EAbilityTargetPolicy::Target" },
		{ "Zone.DisplayName", "Zone" },
		{ "Zone.Name", "EAbilityTargetPolicy::Zone" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_PlayerAbility,
		nullptr,
		"EAbilityTargetPolicy",
		"EAbilityTargetPolicy",
		Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy()
	{
		if (!Z_Registration_Info_UEnum_EAbilityTargetPolicy.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EAbilityTargetPolicy.InnerSingleton, Z_Construct_UEnum_PlayerAbility_EAbilityTargetPolicy_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EAbilityTargetPolicy.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityTargetPolicy_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityTargetPolicy_h_Statics::EnumInfo[] = {
		{ EAbilityTargetPolicy_StaticEnum, TEXT("EAbilityTargetPolicy"), &Z_Registration_Info_UEnum_EAbilityTargetPolicy, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 1400898670U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityTargetPolicy_h_683060646(TEXT("/Script/PlayerAbility"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityTargetPolicy_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityTargetPolicy_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
