// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/ZoneAbility.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYERABILITY_ZoneAbility_generated_h
#error "ZoneAbility.generated.h already included, missing '#pragma once' in ZoneAbility.h"
#endif
#define PLAYERABILITY_ZoneAbility_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_RPC_WRAPPERS \
	virtual void Start_Implementation(); \
 \
	DECLARE_FUNCTION(execIsPreview); \
	DECLARE_FUNCTION(execStart);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsPreview); \
	DECLARE_FUNCTION(execStart);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAZoneAbility(); \
	friend struct Z_Construct_UClass_AZoneAbility_Statics; \
public: \
	DECLARE_CLASS(AZoneAbility, AAbility, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(AZoneAbility)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAZoneAbility(); \
	friend struct Z_Construct_UClass_AZoneAbility_Statics; \
public: \
	DECLARE_CLASS(AZoneAbility, AAbility, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(AZoneAbility)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZoneAbility(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AZoneAbility) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZoneAbility); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZoneAbility); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZoneAbility(AZoneAbility&&); \
	NO_API AZoneAbility(const AZoneAbility&); \
public: \
	NO_API virtual ~AZoneAbility();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AZoneAbility(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AZoneAbility(AZoneAbility&&); \
	NO_API AZoneAbility(const AZoneAbility&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AZoneAbility); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AZoneAbility); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AZoneAbility) \
	NO_API virtual ~AZoneAbility();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_12_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYERABILITY_API UClass* StaticClass<class AZoneAbility>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_ZoneAbility_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
