// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/TargetAbility.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef PLAYERABILITY_TargetAbility_generated_h
#error "TargetAbility.generated.h already included, missing '#pragma once' in TargetAbility.h"
#endif
#define PLAYERABILITY_TargetAbility_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_RPC_WRAPPERS \
	virtual void Apply_Implementation(AActor* Actor); \
 \
	DECLARE_FUNCTION(execApply);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execApply);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATargetAbility(); \
	friend struct Z_Construct_UClass_ATargetAbility_Statics; \
public: \
	DECLARE_CLASS(ATargetAbility, AAbility, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(ATargetAbility)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATargetAbility(); \
	friend struct Z_Construct_UClass_ATargetAbility_Statics; \
public: \
	DECLARE_CLASS(ATargetAbility, AAbility, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PlayerAbility"), NO_API) \
	DECLARE_SERIALIZER(ATargetAbility)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATargetAbility(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATargetAbility) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATargetAbility); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATargetAbility); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATargetAbility(ATargetAbility&&); \
	NO_API ATargetAbility(const ATargetAbility&); \
public: \
	NO_API virtual ~ATargetAbility();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATargetAbility(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATargetAbility(ATargetAbility&&); \
	NO_API ATargetAbility(const ATargetAbility&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATargetAbility); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATargetAbility); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATargetAbility) \
	NO_API virtual ~ATargetAbility();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_11_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYERABILITY_API UClass* StaticClass<class ATargetAbility>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
