// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/Abilities.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYERABILITY_Abilities_generated_h
#error "Abilities.generated.h already included, missing '#pragma once' in Abilities.h"
#endif
#define PLAYERABILITY_Abilities_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Abilities_h


#define FOREACH_ENUM_EABILITIES(op) \
	op(EAbilities::NoAbility) \
	op(EAbilities::OneTimeAreaDamage) \
	op(EAbilities::OneTimeMinionFixedDamage) \
	op(EAbilities::OneTimeMinionPercentageDamage) \
	op(EAbilities::PeriodicAreaDamageWithLifetime) \
	op(EAbilities::PeriodicAreaDamageDuringRounds) \
	op(EAbilities::DecelerationAreaWithLifetime) \
	op(EAbilities::DecelerationAreaDuringRounds) \
	op(EAbilities::ExplosiveArea) 

enum class EAbilities : uint8;
template<> struct TIsUEnumClass<EAbilities> { enum { Value = true }; };
template<> PLAYERABILITY_API UEnum* StaticEnum<EAbilities>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
