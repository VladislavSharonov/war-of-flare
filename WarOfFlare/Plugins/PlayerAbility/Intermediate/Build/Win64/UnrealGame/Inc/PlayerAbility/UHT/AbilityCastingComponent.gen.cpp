// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityCastingComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityCastingComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UCursorPositionSelectorComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreviewComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPositionSelectorComponent_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityCastingComponent();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityCastingComponent_NoRegister();
	PLAYERABILITY_API UEnum* Z_Construct_UEnum_PlayerAbility_EAbilities();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityCastingComponent::execTryBuyAbility)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InAbilityOwner);
		P_GET_ENUM(EAbilities,Z_Param_InAbility);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryBuyAbility(Z_Param_InAbilityOwner,EAbilities(Z_Param_InAbility));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execGetAbilityClass)
	{
		P_GET_ENUM(EAbilities,Z_Param_InAbility);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSubclassOf<AAbility> *)Z_Param__Result=P_THIS->GetAbilityClass(EAbilities(Z_Param_InAbility));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execCastZoneAbility)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CastZoneAbility();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execCastTargetAbility)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CastTargetAbility();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execCastAbility)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CastAbility();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execStartCasting)
	{
		P_GET_ENUM(EAbilities,Z_Param_InAbility);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartCasting_Implementation(EAbilities(Z_Param_InAbility));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execEndCasting)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndCasting_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execServer_CastZoneAbility)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InAbilityOwner);
		P_GET_ENUM(EAbilities,Z_Param_InAbility);
		P_GET_STRUCT(FTransform,Z_Param_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Server_CastZoneAbility_Implementation(Z_Param_InAbilityOwner,EAbilities(Z_Param_InAbility),Z_Param_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execServer_CastTargetAbility)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InAbilityOwner);
		P_GET_ENUM(EAbilities,Z_Param_InAbility);
		P_GET_OBJECT(AActor,Z_Param_InTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Server_CastTargetAbility_Implementation(Z_Param_InAbilityOwner,EAbilities(Z_Param_InAbility),Z_Param_InTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execOnDeactivated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDeactivated_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityCastingComponent::execOnActivated)
	{
		P_GET_UBOOL(Z_Param_bReset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnActivated_Implementation(Z_Param_bReset);
		P_NATIVE_END;
	}
	struct AbilityCastingComponent_eventOnActivated_Parms
	{
		bool bReset;
	};
	struct AbilityCastingComponent_eventServer_CastTargetAbility_Parms
	{
		APlayerController* InAbilityOwner;
		EAbilities InAbility;
		AActor* InTarget;
	};
	struct AbilityCastingComponent_eventServer_CastZoneAbility_Parms
	{
		APlayerController* InAbilityOwner;
		EAbilities InAbility;
		FTransform InTransform;
	};
	struct AbilityCastingComponent_eventStartCasting_Parms
	{
		EAbilities InAbility;
	};
	static FName NAME_UAbilityCastingComponent_EndCasting = FName(TEXT("EndCasting"));
	void UAbilityCastingComponent::EndCasting()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityCastingComponent_EndCasting),NULL);
	}
	static FName NAME_UAbilityCastingComponent_OnActivated = FName(TEXT("OnActivated"));
	void UAbilityCastingComponent::OnActivated(bool bReset)
	{
		AbilityCastingComponent_eventOnActivated_Parms Parms;
		Parms.bReset=bReset ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UAbilityCastingComponent_OnActivated),&Parms);
	}
	static FName NAME_UAbilityCastingComponent_OnDeactivated = FName(TEXT("OnDeactivated"));
	void UAbilityCastingComponent::OnDeactivated()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityCastingComponent_OnDeactivated),NULL);
	}
	static FName NAME_UAbilityCastingComponent_Server_CastTargetAbility = FName(TEXT("Server_CastTargetAbility"));
	void UAbilityCastingComponent::Server_CastTargetAbility(APlayerController* InAbilityOwner, EAbilities InAbility, AActor* InTarget)
	{
		AbilityCastingComponent_eventServer_CastTargetAbility_Parms Parms;
		Parms.InAbilityOwner=InAbilityOwner;
		Parms.InAbility=InAbility;
		Parms.InTarget=InTarget;
		ProcessEvent(FindFunctionChecked(NAME_UAbilityCastingComponent_Server_CastTargetAbility),&Parms);
	}
	static FName NAME_UAbilityCastingComponent_Server_CastZoneAbility = FName(TEXT("Server_CastZoneAbility"));
	void UAbilityCastingComponent::Server_CastZoneAbility(APlayerController* InAbilityOwner, EAbilities InAbility, FTransform const& InTransform)
	{
		AbilityCastingComponent_eventServer_CastZoneAbility_Parms Parms;
		Parms.InAbilityOwner=InAbilityOwner;
		Parms.InAbility=InAbility;
		Parms.InTransform=InTransform;
		ProcessEvent(FindFunctionChecked(NAME_UAbilityCastingComponent_Server_CastZoneAbility),&Parms);
	}
	static FName NAME_UAbilityCastingComponent_StartCasting = FName(TEXT("StartCasting"));
	void UAbilityCastingComponent::StartCasting(EAbilities InAbility)
	{
		AbilityCastingComponent_eventStartCasting_Parms Parms;
		Parms.InAbility=InAbility;
		ProcessEvent(FindFunctionChecked(NAME_UAbilityCastingComponent_StartCasting),&Parms);
	}
	void UAbilityCastingComponent::StaticRegisterNativesUAbilityCastingComponent()
	{
		UClass* Class = UAbilityCastingComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CastAbility", &UAbilityCastingComponent::execCastAbility },
			{ "CastTargetAbility", &UAbilityCastingComponent::execCastTargetAbility },
			{ "CastZoneAbility", &UAbilityCastingComponent::execCastZoneAbility },
			{ "EndCasting", &UAbilityCastingComponent::execEndCasting },
			{ "GetAbilityClass", &UAbilityCastingComponent::execGetAbilityClass },
			{ "OnActivated", &UAbilityCastingComponent::execOnActivated },
			{ "OnDeactivated", &UAbilityCastingComponent::execOnDeactivated },
			{ "Server_CastTargetAbility", &UAbilityCastingComponent::execServer_CastTargetAbility },
			{ "Server_CastZoneAbility", &UAbilityCastingComponent::execServer_CastZoneAbility },
			{ "StartCasting", &UAbilityCastingComponent::execStartCasting },
			{ "TryBuyAbility", &UAbilityCastingComponent::execTryBuyAbility },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_CastAbility_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_CastAbility_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Commit casting.\n" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
		{ "ToolTip", "Commit casting." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_CastAbility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "CastAbility", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_CastAbility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_CastAbility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_CastAbility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_CastAbility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "CastTargetAbility", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "CastZoneAbility", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_EndCasting_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_EndCasting_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Cancel casting\n" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
		{ "ToolTip", "Cancel casting" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_EndCasting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "EndCasting", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_EndCasting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_EndCasting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_EndCasting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_EndCasting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics
	{
		struct AbilityCastingComponent_eventGetAbilityClass_Parms
		{
			EAbilities InAbility;
			TSubclassOf<AAbility>  ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAbility_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAbility;
		static const UECodeGen_Private::FClassPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::NewProp_InAbility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::NewProp_InAbility = { "InAbility", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventGetAbilityClass_Parms, InAbility), Z_Construct_UEnum_PlayerAbility_EAbilities, METADATA_PARAMS(nullptr, 0) }; // 1299748093
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventGetAbilityClass_Parms, ReturnValue), Z_Construct_UClass_UClass, Z_Construct_UClass_AAbility_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::NewProp_InAbility_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::NewProp_InAbility,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "GetAbilityClass", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::AbilityCastingComponent_eventGetAbilityClass_Parms), Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics
	{
		static void NewProp_bReset_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bReset;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::NewProp_bReset_SetBit(void* Obj)
	{
		((AbilityCastingComponent_eventOnActivated_Parms*)Obj)->bReset = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::NewProp_bReset = { "bReset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AbilityCastingComponent_eventOnActivated_Parms), &Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::NewProp_bReset_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::NewProp_bReset,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::Function_MetaDataParams[] = {
		{ "CPP_Default_bReset", "false" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "OnActivated", nullptr, nullptr, sizeof(AbilityCastingComponent_eventOnActivated_Parms), Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_OnActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_OnActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "OnDeactivated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InAbilityOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAbility_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAbility;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTarget;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InAbilityOwner = { "InAbilityOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventServer_CastTargetAbility_Parms, InAbilityOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InAbility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InAbility = { "InAbility", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventServer_CastTargetAbility_Parms, InAbility), Z_Construct_UEnum_PlayerAbility_EAbilities, METADATA_PARAMS(nullptr, 0) }; // 1299748093
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InTarget = { "InTarget", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventServer_CastTargetAbility_Parms, InTarget), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InAbilityOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InAbility_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InAbility,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::NewProp_InTarget,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "Server_CastTargetAbility", nullptr, nullptr, sizeof(AbilityCastingComponent_eventServer_CastTargetAbility_Parms), Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04220C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InAbilityOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAbility_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAbility;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InAbilityOwner = { "InAbilityOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventServer_CastZoneAbility_Parms, InAbilityOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InAbility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InAbility = { "InAbility", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventServer_CastZoneAbility_Parms, InAbility), Z_Construct_UEnum_PlayerAbility_EAbilities, METADATA_PARAMS(nullptr, 0) }; // 1299748093
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000082, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventServer_CastZoneAbility_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InTransform_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InAbilityOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InAbility_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InAbility,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "Server_CastZoneAbility", nullptr, nullptr, sizeof(AbilityCastingComponent_eventServer_CastZoneAbility_Parms), Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04A20C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics
	{
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAbility_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAbility;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::NewProp_InAbility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::NewProp_InAbility = { "InAbility", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventStartCasting_Parms, InAbility), Z_Construct_UEnum_PlayerAbility_EAbilities, METADATA_PARAMS(nullptr, 0) }; // 1299748093
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::NewProp_InAbility_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::NewProp_InAbility,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Select target or position\n" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
		{ "ToolTip", "Select target or position" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "StartCasting", nullptr, nullptr, sizeof(AbilityCastingComponent_eventStartCasting_Parms), Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_StartCasting()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_StartCasting_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics
	{
		struct AbilityCastingComponent_eventTryBuyAbility_Parms
		{
			APlayerController* InAbilityOwner;
			EAbilities InAbility;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InAbilityOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAbility_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAbility;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_InAbilityOwner = { "InAbilityOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventTryBuyAbility_Parms, InAbilityOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_InAbility_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_InAbility = { "InAbility", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityCastingComponent_eventTryBuyAbility_Parms, InAbility), Z_Construct_UEnum_PlayerAbility_EAbilities, METADATA_PARAMS(nullptr, 0) }; // 1299748093
	void Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((AbilityCastingComponent_eventTryBuyAbility_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AbilityCastingComponent_eventTryBuyAbility_Parms), &Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_InAbilityOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_InAbility_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_InAbility,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityCastingComponent, nullptr, "TryBuyAbility", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::AbilityCastingComponent_eventTryBuyAbility_Parms), Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAbilityCastingComponent);
	UClass* Z_Construct_UClass_UAbilityCastingComponent_NoRegister()
	{
		return UAbilityCastingComponent::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityCastingComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AbilityPrices_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_AbilityPrices;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlacementPreviewClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_PlacementPreviewClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PositionSelectorClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_PositionSelectorClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ObjectHoverClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_ObjectHoverClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlacementPreview_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PlacementPreview;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PositionSelector_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PositionSelector;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ObjectHover_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ObjectHover;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AbilityClasses_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_AbilityClasses;
		static const UECodeGen_Private::FBytePropertyParams NewProp_Ability_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Ability_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_Ability;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityCastingComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityCastingComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityCastingComponent_CastAbility, "CastAbility" }, // 932485994
		{ &Z_Construct_UFunction_UAbilityCastingComponent_CastTargetAbility, "CastTargetAbility" }, // 1049823094
		{ &Z_Construct_UFunction_UAbilityCastingComponent_CastZoneAbility, "CastZoneAbility" }, // 1193922713
		{ &Z_Construct_UFunction_UAbilityCastingComponent_EndCasting, "EndCasting" }, // 1661036322
		{ &Z_Construct_UFunction_UAbilityCastingComponent_GetAbilityClass, "GetAbilityClass" }, // 2333654370
		{ &Z_Construct_UFunction_UAbilityCastingComponent_OnActivated, "OnActivated" }, // 2590903435
		{ &Z_Construct_UFunction_UAbilityCastingComponent_OnDeactivated, "OnDeactivated" }, // 2590344855
		{ &Z_Construct_UFunction_UAbilityCastingComponent_Server_CastTargetAbility, "Server_CastTargetAbility" }, // 2839622331
		{ &Z_Construct_UFunction_UAbilityCastingComponent_Server_CastZoneAbility, "Server_CastZoneAbility" }, // 600785163
		{ &Z_Construct_UFunction_UAbilityCastingComponent_StartCasting, "StartCasting" }, // 1470912838
		{ &Z_Construct_UFunction_UAbilityCastingComponent_TryBuyAbility, "TryBuyAbility" }, // 3210990444
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "PlayerAbility/AbilityCastingComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityPrices_MetaData[] = {
		{ "Category", "AbilityCastingComponent" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityPrices = { "AbilityPrices", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, AbilityPrices), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityPrices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityPrices_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreviewClass_MetaData[] = {
		{ "Category", "Components|ZoneAbility" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreviewClass = { "PlacementPreviewClass", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, PlacementPreviewClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UPlacementPreviewComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreviewClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreviewClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelectorClass_MetaData[] = {
		{ "Category", "Components|ZoneAbility" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelectorClass = { "PositionSelectorClass", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, PositionSelectorClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UPositionSelectorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelectorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelectorClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHoverClass_MetaData[] = {
		{ "Category", "Components|TargetAbility" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHoverClass = { "ObjectHoverClass", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, ObjectHoverClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UCursorHoverComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHoverClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHoverClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreview_MetaData[] = {
		{ "Category", "Components|ZoneAbility" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreview = { "PlacementPreview", nullptr, (EPropertyFlags)0x002008000009001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, PlacementPreview), Z_Construct_UClass_UPlacementPreviewComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreview_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelector_MetaData[] = {
		{ "Category", "Components|ZoneAbility" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelector = { "PositionSelector", nullptr, (EPropertyFlags)0x002008000009001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, PositionSelector), Z_Construct_UClass_UCursorPositionSelectorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelector_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelector_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHover_MetaData[] = {
		{ "Category", "Components|TargetAbility" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHover = { "ObjectHover", nullptr, (EPropertyFlags)0x002008000009001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, ObjectHover), Z_Construct_UClass_UCursorHoverComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHover_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHover_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityClasses_MetaData[] = {
		{ "Category", "AbilityCastingComponent" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityClasses = { "AbilityClasses", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, AbilityClasses), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityClasses_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability_MetaData[] = {
		{ "Category", "AbilityCastingComponent" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityCastingComponent.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability = { "Ability", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityCastingComponent, Ability), Z_Construct_UEnum_PlayerAbility_EAbilities, METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability_MetaData)) }; // 1299748093
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAbilityCastingComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityPrices,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreviewClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelectorClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHoverClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PlacementPreview,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_PositionSelector,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_ObjectHover,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_AbilityClasses,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityCastingComponent_Statics::NewProp_Ability,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityCastingComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityCastingComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAbilityCastingComponent_Statics::ClassParams = {
		&UAbilityCastingComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAbilityCastingComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityCastingComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityCastingComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityCastingComponent()
	{
		if (!Z_Registration_Info_UClass_UAbilityCastingComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAbilityCastingComponent.OuterSingleton, Z_Construct_UClass_UAbilityCastingComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAbilityCastingComponent.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<UAbilityCastingComponent>()
	{
		return UAbilityCastingComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityCastingComponent);
	UAbilityCastingComponent::~UAbilityCastingComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAbilityCastingComponent, UAbilityCastingComponent::StaticClass, TEXT("UAbilityCastingComponent"), &Z_Registration_Info_UClass_UAbilityCastingComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAbilityCastingComponent), 3280066200U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_3694859337(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityCastingComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
