// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/AbilityTargetPolicy.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYERABILITY_AbilityTargetPolicy_generated_h
#error "AbilityTargetPolicy.generated.h already included, missing '#pragma once' in AbilityTargetPolicy.h"
#endif
#define PLAYERABILITY_AbilityTargetPolicy_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityTargetPolicy_h


#define FOREACH_ENUM_EABILITYTARGETPOLICY(op) \
	op(EAbilityTargetPolicy::Target) \
	op(EAbilityTargetPolicy::Zone) 

enum class EAbilityTargetPolicy : uint8;
template<> struct TIsUEnumClass<EAbilityTargetPolicy> { enum { Value = true }; };
template<> PLAYERABILITY_API UEnum* StaticEnum<EAbilityTargetPolicy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
