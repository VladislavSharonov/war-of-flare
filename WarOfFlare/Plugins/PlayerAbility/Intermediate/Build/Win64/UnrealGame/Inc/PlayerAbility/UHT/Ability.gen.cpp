// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/Ability.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbility() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreview_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityEffect_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(AAbility::execSetPlacementPreview)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPlacementPreview_Implementation(Z_Param_InIsPreview);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAbility::execFinish)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Finish_Implementation();
		P_NATIVE_END;
	}
	struct Ability_eventSetPlacementPreview_Parms
	{
		bool InIsPreview;
	};
	static FName NAME_AAbility_Finish = FName(TEXT("Finish"));
	void AAbility::Finish()
	{
		ProcessEvent(FindFunctionChecked(NAME_AAbility_Finish),NULL);
	}
	static FName NAME_AAbility_SetPlacementPreview = FName(TEXT("SetPlacementPreview"));
	void AAbility::SetPlacementPreview(bool InIsPreview)
	{
		Ability_eventSetPlacementPreview_Parms Parms;
		Parms.InIsPreview=InIsPreview ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AAbility_SetPlacementPreview),&Parms);
	}
	void AAbility::StaticRegisterNativesAAbility()
	{
		UClass* Class = AAbility::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Finish", &AAbility::execFinish },
			{ "SetPlacementPreview", &AAbility::execSetPlacementPreview },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AAbility_Finish_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAbility_Finish_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/PlayerAbility/Ability.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAbility_Finish_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAbility, nullptr, "Finish", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAbility_Finish_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbility_Finish_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAbility_Finish()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAbility_Finish_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics
	{
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((Ability_eventSetPlacementPreview_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(Ability_eventSetPlacementPreview_Parms), &Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::NewProp_InIsPreview,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/PlayerAbility/Ability.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAbility, nullptr, "SetPlacementPreview", nullptr, nullptr, sizeof(Ability_eventSetPlacementPreview_Parms), Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAbility_SetPlacementPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAbility_SetPlacementPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAbility);
	UClass* Z_Construct_UClass_AAbility_NoRegister()
	{
		return AAbility::StaticClass();
	}
	struct Z_Construct_UClass_AAbility_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_EffectClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_EffectClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAbility_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AAbility_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AAbility_Finish, "Finish" }, // 4156443624
		{ &Z_Construct_UFunction_AAbility_SetPlacementPreview, "SetPlacementPreview" }, // 2911211087
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAbility_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlayerAbility/Ability.h" },
		{ "ModuleRelativePath", "Public/PlayerAbility/Ability.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAbility_Statics::NewProp_EffectClass_MetaData[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/PlayerAbility/Ability.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_AAbility_Statics::NewProp_EffectClass = { "EffectClass", nullptr, (EPropertyFlags)0x0014000000010005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AAbility, EffectClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UAbilityEffect_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAbility_Statics::NewProp_EffectClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAbility_Statics::NewProp_EffectClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAbility_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAbility_Statics::NewProp_EffectClass,
	};
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AAbility_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UPlacementPreview_NoRegister, (int32)VTABLE_OFFSET(AAbility, IPlacementPreview), false },  // 174827905
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAbility_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAbility>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAbility_Statics::ClassParams = {
		&AAbility::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AAbility_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AAbility_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAbility_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAbility_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAbility()
	{
		if (!Z_Registration_Info_UClass_AAbility.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAbility.OuterSingleton, Z_Construct_UClass_AAbility_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAbility.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<AAbility>()
	{
		return AAbility::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAbility);
	AAbility::~AAbility() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Ability_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Ability_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAbility, AAbility::StaticClass, TEXT("AAbility"), &Z_Registration_Info_UClass_AAbility, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAbility), 3724460524U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Ability_h_1999186604(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Ability_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Ability_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
