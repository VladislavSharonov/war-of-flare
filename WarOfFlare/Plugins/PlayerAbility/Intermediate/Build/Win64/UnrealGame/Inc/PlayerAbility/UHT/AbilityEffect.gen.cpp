// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityEffect.h"
#include "../../Source/Runtime/Engine/Classes/Engine/TimerHandle.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityEffect() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityEffect();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityEffect_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityEffect::execExtendLifetime)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InLifetime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ExtendLifetime(Z_Param_InLifetime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityEffect::execSetLifetime)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InLifetime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetLifetime(Z_Param_InLifetime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityEffect::execFinish)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Finish_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityEffect::execCancel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Cancel_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAbilityEffect::execStart)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Start_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_UAbilityEffect_Cancel = FName(TEXT("Cancel"));
	void UAbilityEffect::Cancel()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityEffect_Cancel),NULL);
	}
	static FName NAME_UAbilityEffect_Finish = FName(TEXT("Finish"));
	void UAbilityEffect::Finish()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityEffect_Finish),NULL);
	}
	static FName NAME_UAbilityEffect_Start = FName(TEXT("Start"));
	void UAbilityEffect::Start()
	{
		ProcessEvent(FindFunctionChecked(NAME_UAbilityEffect_Start),NULL);
	}
	void UAbilityEffect::StaticRegisterNativesUAbilityEffect()
	{
		UClass* Class = UAbilityEffect::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Cancel", &UAbilityEffect::execCancel },
			{ "ExtendLifetime", &UAbilityEffect::execExtendLifetime },
			{ "Finish", &UAbilityEffect::execFinish },
			{ "SetLifetime", &UAbilityEffect::execSetLifetime },
			{ "Start", &UAbilityEffect::execStart },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityEffect_Cancel_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityEffect_Cancel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Effect" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityEffect_Cancel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityEffect, nullptr, "Cancel", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityEffect_Cancel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_Cancel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityEffect_Cancel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityEffect_Cancel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics
	{
		struct AbilityEffect_eventExtendLifetime_Parms
		{
			float InLifetime;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_InLifetime;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::NewProp_InLifetime = { "InLifetime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityEffect_eventExtendLifetime_Parms, InLifetime), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::NewProp_InLifetime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Effect" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityEffect, nullptr, "ExtendLifetime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::AbilityEffect_eventExtendLifetime_Parms), Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityEffect_ExtendLifetime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityEffect_ExtendLifetime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityEffect_Finish_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityEffect_Finish_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Effect" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityEffect_Finish_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityEffect, nullptr, "Finish", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityEffect_Finish_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_Finish_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityEffect_Finish()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityEffect_Finish_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics
	{
		struct AbilityEffect_eventSetLifetime_Parms
		{
			float InLifetime;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_InLifetime;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::NewProp_InLifetime = { "InLifetime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityEffect_eventSetLifetime_Parms, InLifetime), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::NewProp_InLifetime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Effect" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityEffect, nullptr, "SetLifetime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::AbilityEffect_eventSetLifetime_Parms), Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityEffect_SetLifetime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityEffect_SetLifetime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAbilityEffect_Start_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityEffect_Start_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Effect" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityEffect_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityEffect, nullptr, "Start", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityEffect_Start_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityEffect_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityEffect_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityEffect_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAbilityEffect);
	UClass* Z_Construct_UClass_UAbilityEffect_NoRegister()
	{
		return UAbilityEffect::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityEffect_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_LifetimeTimerHandle_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_LifetimeTimerHandle;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityEffect_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityEffect_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityEffect_Cancel, "Cancel" }, // 775210214
		{ &Z_Construct_UFunction_UAbilityEffect_ExtendLifetime, "ExtendLifetime" }, // 3395298283
		{ &Z_Construct_UFunction_UAbilityEffect_Finish, "Finish" }, // 2261175140
		{ &Z_Construct_UFunction_UAbilityEffect_SetLifetime, "SetLifetime" }, // 1018828741
		{ &Z_Construct_UFunction_UAbilityEffect_Start, "Start" }, // 1800324007
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityEffect_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "PlayerAbility/AbilityEffect.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityEffect_Statics::NewProp_LifetimeTimerHandle_MetaData[] = {
		{ "Category", "Ability|Effect" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityEffect.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAbilityEffect_Statics::NewProp_LifetimeTimerHandle = { "LifetimeTimerHandle", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityEffect, LifetimeTimerHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_UAbilityEffect_Statics::NewProp_LifetimeTimerHandle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityEffect_Statics::NewProp_LifetimeTimerHandle_MetaData)) }; // 3633724737
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAbilityEffect_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityEffect_Statics::NewProp_LifetimeTimerHandle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityEffect_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityEffect>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAbilityEffect_Statics::ClassParams = {
		&UAbilityEffect::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAbilityEffect_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityEffect_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityEffect_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityEffect_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityEffect()
	{
		if (!Z_Registration_Info_UClass_UAbilityEffect.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAbilityEffect.OuterSingleton, Z_Construct_UClass_UAbilityEffect_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAbilityEffect.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<UAbilityEffect>()
	{
		return UAbilityEffect::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityEffect);
	UAbilityEffect::~UAbilityEffect() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAbilityEffect, UAbilityEffect::StaticClass, TEXT("UAbilityEffect"), &Z_Registration_Info_UClass_UAbilityEffect, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAbilityEffect), 2054650732U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_3140854535(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityEffect_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
