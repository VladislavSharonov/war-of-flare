// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/Abilities.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilities() {}
// Cross Module References
	PLAYERABILITY_API UEnum* Z_Construct_UEnum_PlayerAbility_EAbilities();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EAbilities;
	static UEnum* EAbilities_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EAbilities.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EAbilities.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_PlayerAbility_EAbilities, (UObject*)Z_Construct_UPackage__Script_PlayerAbility(), TEXT("EAbilities"));
		}
		return Z_Registration_Info_UEnum_EAbilities.OuterSingleton;
	}
	template<> PLAYERABILITY_API UEnum* StaticEnum<EAbilities>()
	{
		return EAbilities_StaticEnum();
	}
	struct Z_Construct_UEnum_PlayerAbility_EAbilities_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::Enumerators[] = {
		{ "EAbilities::NoAbility", (int64)EAbilities::NoAbility },
		{ "EAbilities::OneTimeAreaDamage", (int64)EAbilities::OneTimeAreaDamage },
		{ "EAbilities::OneTimeMinionFixedDamage", (int64)EAbilities::OneTimeMinionFixedDamage },
		{ "EAbilities::OneTimeMinionPercentageDamage", (int64)EAbilities::OneTimeMinionPercentageDamage },
		{ "EAbilities::PeriodicAreaDamageWithLifetime", (int64)EAbilities::PeriodicAreaDamageWithLifetime },
		{ "EAbilities::PeriodicAreaDamageDuringRounds", (int64)EAbilities::PeriodicAreaDamageDuringRounds },
		{ "EAbilities::DecelerationAreaWithLifetime", (int64)EAbilities::DecelerationAreaWithLifetime },
		{ "EAbilities::DecelerationAreaDuringRounds", (int64)EAbilities::DecelerationAreaDuringRounds },
		{ "EAbilities::ExplosiveArea", (int64)EAbilities::ExplosiveArea },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DecelerationAreaDuringRounds.DisplayName", "DecelerationAreaDuringRounds" },
		{ "DecelerationAreaDuringRounds.Name", "EAbilities::DecelerationAreaDuringRounds" },
		{ "DecelerationAreaWithLifetime.DisplayName", "DecelerationAreaWithLifetime" },
		{ "DecelerationAreaWithLifetime.Name", "EAbilities::DecelerationAreaWithLifetime" },
		{ "ExplosiveArea.Comment", "// IncreasedTowerDamage            UMETA(DisplayName = \"IncreasedTowerDamage\"),\n// IncreasedTowerDamageArea        UMETA(DisplayName = \"IncreasedTowerDamageArea\"),\n// IncreasedTowerDamageDuringRounds   UMETA(DisplayName = \"IncreasedTowerDuringRounds\"),\n" },
		{ "ExplosiveArea.DisplayName", "ExplosiveArea" },
		{ "ExplosiveArea.Name", "EAbilities::ExplosiveArea" },
		{ "ExplosiveArea.ToolTip", "IncreasedTowerDamage            UMETA(DisplayName = \"IncreasedTowerDamage\"),\nIncreasedTowerDamageArea        UMETA(DisplayName = \"IncreasedTowerDamageArea\"),\nIncreasedTowerDamageDuringRounds   UMETA(DisplayName = \"IncreasedTowerDuringRounds\")," },
		{ "ModuleRelativePath", "Public/PlayerAbility/Abilities.h" },
		{ "NoAbility.DisplayName", "NoAbility" },
		{ "NoAbility.Hidden", "" },
		{ "NoAbility.Name", "EAbilities::NoAbility" },
		{ "OneTimeAreaDamage.DisplayName", "OneTimeAreaDamage" },
		{ "OneTimeAreaDamage.Name", "EAbilities::OneTimeAreaDamage" },
		{ "OneTimeMinionFixedDamage.DisplayName", "OneTimeMinionFixedDamage" },
		{ "OneTimeMinionFixedDamage.Name", "EAbilities::OneTimeMinionFixedDamage" },
		{ "OneTimeMinionPercentageDamage.DisplayName", "OneTimeMinionPercentageDamage" },
		{ "OneTimeMinionPercentageDamage.Name", "EAbilities::OneTimeMinionPercentageDamage" },
		{ "PeriodicAreaDamageDuringRounds.DisplayName", "PeriodicAreaDamageDuringRounds" },
		{ "PeriodicAreaDamageDuringRounds.Name", "EAbilities::PeriodicAreaDamageDuringRounds" },
		{ "PeriodicAreaDamageWithLifetime.DisplayName", "PeriodicAreaDamageWithLifetime" },
		{ "PeriodicAreaDamageWithLifetime.Name", "EAbilities::PeriodicAreaDamageWithLifetime" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_PlayerAbility,
		nullptr,
		"EAbilities",
		"EAbilities",
		Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_PlayerAbility_EAbilities()
	{
		if (!Z_Registration_Info_UEnum_EAbilities.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EAbilities.InnerSingleton, Z_Construct_UEnum_PlayerAbility_EAbilities_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EAbilities.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Abilities_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Abilities_h_Statics::EnumInfo[] = {
		{ EAbilities_StaticEnum, TEXT("EAbilities"), &Z_Registration_Info_UEnum_EAbilities, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 1299748093U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Abilities_h_195745844(TEXT("/Script/PlayerAbility"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Abilities_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_Abilities_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
