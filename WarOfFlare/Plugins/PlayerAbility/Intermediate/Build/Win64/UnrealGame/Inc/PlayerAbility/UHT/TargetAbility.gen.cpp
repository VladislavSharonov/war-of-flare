// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/TargetAbility.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTargetAbility() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	PLAYERABILITY_API UClass* Z_Construct_UClass_AAbility();
	PLAYERABILITY_API UClass* Z_Construct_UClass_ATargetAbility();
	PLAYERABILITY_API UClass* Z_Construct_UClass_ATargetAbility_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(ATargetAbility::execApply)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Apply_Implementation(Z_Param_Actor);
		P_NATIVE_END;
	}
	struct TargetAbility_eventApply_Parms
	{
		AActor* Actor;
	};
	static FName NAME_ATargetAbility_Apply = FName(TEXT("Apply"));
	void ATargetAbility::Apply(AActor* Actor)
	{
		TargetAbility_eventApply_Parms Parms;
		Parms.Actor=Actor;
		ProcessEvent(FindFunctionChecked(NAME_ATargetAbility_Apply),&Parms);
	}
	void ATargetAbility::StaticRegisterNativesATargetAbility()
	{
		UClass* Class = ATargetAbility::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Apply", &ATargetAbility::execApply },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATargetAbility_Apply_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATargetAbility_Apply_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetAbility_eventApply_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATargetAbility_Apply_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATargetAbility_Apply_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATargetAbility_Apply_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability" },
		{ "ModuleRelativePath", "Public/PlayerAbility/TargetAbility.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATargetAbility_Apply_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATargetAbility, nullptr, "Apply", nullptr, nullptr, sizeof(TargetAbility_eventApply_Parms), Z_Construct_UFunction_ATargetAbility_Apply_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATargetAbility_Apply_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATargetAbility_Apply_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATargetAbility_Apply_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATargetAbility_Apply()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATargetAbility_Apply_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATargetAbility);
	UClass* Z_Construct_UClass_ATargetAbility_NoRegister()
	{
		return ATargetAbility::StaticClass();
	}
	struct Z_Construct_UClass_ATargetAbility_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATargetAbility_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAbility,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATargetAbility_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATargetAbility_Apply, "Apply" }, // 1186989835
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargetAbility_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlayerAbility/TargetAbility.h" },
		{ "ModuleRelativePath", "Public/PlayerAbility/TargetAbility.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATargetAbility_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATargetAbility>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATargetAbility_Statics::ClassParams = {
		&ATargetAbility::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATargetAbility_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATargetAbility_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATargetAbility()
	{
		if (!Z_Registration_Info_UClass_ATargetAbility.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATargetAbility.OuterSingleton, Z_Construct_UClass_ATargetAbility_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATargetAbility.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<ATargetAbility>()
	{
		return ATargetAbility::StaticClass();
	}
	ATargetAbility::ATargetAbility() {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATargetAbility);
	ATargetAbility::~ATargetAbility() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATargetAbility, ATargetAbility::StaticClass, TEXT("ATargetAbility"), &Z_Registration_Info_UClass_ATargetAbility, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATargetAbility), 246933832U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_2478968784(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_TargetAbility_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
