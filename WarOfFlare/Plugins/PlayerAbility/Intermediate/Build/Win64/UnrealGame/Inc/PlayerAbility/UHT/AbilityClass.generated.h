// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "PlayerAbility/AbilityClass.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYERABILITY_AbilityClass_generated_h
#error "AbilityClass.generated.h already included, missing '#pragma once' in AbilityClass.h"
#endif
#define PLAYERABILITY_AbilityClass_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAbilityClass_Statics; \
	PLAYERABILITY_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> PLAYERABILITY_API UScriptStruct* StaticStruct<struct FAbilityClass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityClass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
