// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PlayerAbility/Public/PlayerAbility/AbilityControllers/AbilityDurationController.h"
#include "../../Source/Runtime/Engine/Classes/Engine/TimerHandle.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityDurationController() {}
// Cross Module References
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityControllerBase();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityDurationController();
	PLAYERABILITY_API UClass* Z_Construct_UClass_UAbilityDurationController_NoRegister();
	UPackage* Z_Construct_UPackage__Script_PlayerAbility();
// End Cross Module References
	DEFINE_FUNCTION(UAbilityDurationController::execSetDuration)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InDuration);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDuration(Z_Param_InDuration);
		P_NATIVE_END;
	}
	void UAbilityDurationController::StaticRegisterNativesUAbilityDurationController()
	{
		UClass* Class = UAbilityDurationController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetDuration", &UAbilityDurationController::execSetDuration },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics
	{
		struct AbilityDurationController_eventSetDuration_Parms
		{
			float InDuration;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InDuration_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_InDuration;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::NewProp_InDuration_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::NewProp_InDuration = { "InDuration", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AbilityDurationController_eventSetDuration_Parms, InDuration), METADATA_PARAMS(Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::NewProp_InDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::NewProp_InDuration_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::NewProp_InDuration,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::Function_MetaDataParams[] = {
		{ "Category", "Ability|Controller|Duration" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityDurationController.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAbilityDurationController, nullptr, "SetDuration", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::AbilityDurationController_eventSetDuration_Parms), Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAbilityDurationController_SetDuration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAbilityDurationController_SetDuration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAbilityDurationController);
	UClass* Z_Construct_UClass_UAbilityDurationController_NoRegister()
	{
		return UAbilityDurationController::StaticClass();
	}
	struct Z_Construct_UClass_UAbilityDurationController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Duration_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Duration;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DurationTimerHandle_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_DurationTimerHandle;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAbilityDurationController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAbilityControllerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PlayerAbility,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAbilityDurationController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAbilityDurationController_SetDuration, "SetDuration" }, // 129790636
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityDurationController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "PlayerAbility/AbilityControllers/AbilityDurationController.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityDurationController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_Duration_MetaData[] = {
		{ "Category", "Ability|Controller|Duration" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityDurationController.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_Duration = { "Duration", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityDurationController, Duration), METADATA_PARAMS(Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_Duration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_Duration_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_DurationTimerHandle_MetaData[] = {
		{ "Category", "Ability|Controller|Duration" },
		{ "ModuleRelativePath", "Public/PlayerAbility/AbilityControllers/AbilityDurationController.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_DurationTimerHandle = { "DurationTimerHandle", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAbilityDurationController, DurationTimerHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_DurationTimerHandle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_DurationTimerHandle_MetaData)) }; // 3633724737
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAbilityDurationController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_Duration,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAbilityDurationController_Statics::NewProp_DurationTimerHandle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAbilityDurationController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAbilityDurationController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAbilityDurationController_Statics::ClassParams = {
		&UAbilityDurationController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAbilityDurationController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityDurationController_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAbilityDurationController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAbilityDurationController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAbilityDurationController()
	{
		if (!Z_Registration_Info_UClass_UAbilityDurationController.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAbilityDurationController.OuterSingleton, Z_Construct_UClass_UAbilityDurationController_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAbilityDurationController.OuterSingleton;
	}
	template<> PLAYERABILITY_API UClass* StaticClass<UAbilityDurationController>()
	{
		return UAbilityDurationController::StaticClass();
	}
	UAbilityDurationController::UAbilityDurationController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAbilityDurationController);
	UAbilityDurationController::~UAbilityDurationController() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityDurationController_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityDurationController_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAbilityDurationController, UAbilityDurationController::StaticClass, TEXT("UAbilityDurationController"), &Z_Registration_Info_UClass_UAbilityDurationController, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAbilityDurationController), 2000718803U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityDurationController_h_1214147246(TEXT("/Script/PlayerAbility"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityDurationController_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_PlayerAbility_Source_PlayerAbility_Public_PlayerAbility_AbilityControllers_AbilityDurationController_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
