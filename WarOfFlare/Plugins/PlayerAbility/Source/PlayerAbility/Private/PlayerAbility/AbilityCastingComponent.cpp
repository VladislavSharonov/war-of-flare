﻿// DemoDreams. All rights reserved.

#include "PlayerAbility/AbilityCastingComponent.h"

#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerState.h"
#include "Minions/Minion.h"

#include "Money/WalletComponent.h"
#include "PlayerAbility/AbilityClass.h"
#include "PlayerAbility/AbilityFunctionLibrary.h"
#include "PlayerAbility/ZoneAbility.h"
#include "Money/Price.h"

UAbilityCastingComponent::UAbilityCastingComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = false;
	SetIsReplicatedByDefault(true);
}

void UAbilityCastingComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UAbilityCastingComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void UAbilityCastingComponent::OnRep_IsActive()
{
	Super::OnRep_IsActive();
	
	if (IsActive())
		OnActivated(false);
	else
		OnDeactivated();
}

void UAbilityCastingComponent::Activate(bool bReset)
{
	Super::Activate(bReset);
	
	if (GetOwner()->HasLocalNetOwner())
		OnActivated(bReset);
}

void UAbilityCastingComponent::OnActivated_Implementation(bool bReset) { }

void UAbilityCastingComponent::Deactivate()
{
	Super::Deactivate();
	
	if (GetOwner()->HasLocalNetOwner())
		OnDeactivated();
}

void UAbilityCastingComponent::OnDeactivated_Implementation()
{
	EndCasting();
}

void UAbilityCastingComponent::Server_CastTargetAbility_Implementation(APlayerController* InAbilityOwner, EAbilities InAbility, AActor* InTarget)
{
	// Check ability class.
	const TSubclassOf<AAbility> ClassToSpawn = GetAbilityClass(InAbility);
	const EAbilityTargetPolicy TargetPolicy = UAbilityFunctionLibrary::GetTargetPolicy(ClassToSpawn);

	if (TargetPolicy != EAbilityTargetPolicy::Target || !IsValid(InTarget))
		return;

	Ability = InAbility;
	
	// Check if the player has money to buy.
	if (!TryBuyAbility(InAbilityOwner, InAbility))
		return;

	// Spawn
	UWorld* World = GetWorld();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = InAbilityOwner;

	ATargetAbility* AbilityInstance = World->SpawnActor<ATargetAbility>(ClassToSpawn, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParameters);
	AbilityInstance->Apply(InTarget);
}

void UAbilityCastingComponent::Server_CastZoneAbility_Implementation(APlayerController* InAbilityOwner, const EAbilities InAbility, const FTransform& InTransform)
{
	// Check ability class.
	const TSubclassOf<AAbility> ClassToSpawn = GetAbilityClass(InAbility);
	const EAbilityTargetPolicy TargetPolicy = UAbilityFunctionLibrary::GetTargetPolicy(ClassToSpawn);

	if (TargetPolicy != EAbilityTargetPolicy::Zone)
		return;

	Ability = InAbility;
	
	// Check if the player has money to buy.
	if (!TryBuyAbility(InAbilityOwner, InAbility))
		return;

	// Spawn
	UWorld* World = GetWorld();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = InAbilityOwner;

	AZoneAbility* AbilityInstance = World->SpawnActor<AZoneAbility>(ClassToSpawn, InTransform, SpawnParameters);
	AbilityInstance->Start();
}

void UAbilityCastingComponent::EndCasting_Implementation()
{
	Ability = EAbilities::NoAbility;

	if (IsValid(PositionSelector))
	{
		if (PositionSelector->OnTransformChanged.IsAlreadyBound(PlacementPreview, &UPlacementPreviewComponent::UpdatePreviewTransform))
			PositionSelector->OnTransformChanged.RemoveDynamic(PlacementPreview, &UPlacementPreviewComponent::UpdatePreviewTransform);

		PositionSelector->DestroyComponent();
	}

	PositionSelector = nullptr;

	if (IsValid(PlacementPreview))
		PlacementPreview->DestroyComponent();

	PlacementPreview = nullptr;

	if (IsValid(ObjectHover))
		ObjectHover->DestroyComponent();

	ObjectHover = nullptr;
}

void UAbilityCastingComponent::StartCasting_Implementation(const EAbilities InAbility)
{
	EndCasting(); // Cancel old casting

	if (InAbility == EAbilities::NoAbility)
		return;
	
	AActor* Owner = GetOwner();

	if (AbilityClasses == nullptr)
		UE_LOG(LogTemp, Error, TEXT("%s: Ability class data table was not set."), *GetName())

	Ability = InAbility;
	const FName AbilityName(*StaticEnum<EAbilities>()->GetNameStringByValue(static_cast<int64>(Ability)));
	const FAbilityClass* AbilityClassHolder = AbilityClasses->FindRow<FAbilityClass>(AbilityName, "");

	if (AbilityClassHolder->Class == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s: Ability with class \"%s\" was not found."), *GetName(), *AbilityName.ToString())
		return;
	}
	const EAbilityTargetPolicy TargetPolicy = UAbilityFunctionLibrary::GetTargetPolicy(AbilityClassHolder->Class);

	if (TargetPolicy == EAbilityTargetPolicy::Target)
	{
		ObjectHover = Cast<UCursorHoverComponent>(
			Owner->AddComponentByClass(ObjectHoverClass, false, FTransform::Identity, false));
		ObjectHover->AddFilter(AMinion::StaticClass());
		ObjectHover->SetActive(true);
	}
	else if (TargetPolicy == EAbilityTargetPolicy::Zone)
	{
		PlacementPreview = Cast<UPlacementPreviewComponent>(
			Owner->AddComponentByClass(PlacementPreviewClass, false, FTransform::Identity, false));
		
		PositionSelector = Cast<UCursorPositionSelectorComponent>(
			Owner->AddComponentByClass(PositionSelectorClass, false, FTransform::Identity, false));

		PlacementPreview->SetPreviewClass(AbilityClassHolder->Class);
		PositionSelector->OnTransformChanged.AddUniqueDynamic(PlacementPreview, &UPlacementPreviewComponent::UpdatePreviewTransform);
		
		PlacementPreview->SetActive(true);
		PositionSelector->SetActive(true);
	}
}

void UAbilityCastingComponent::CastAbility()
{
	if (Ability == EAbilities::NoAbility)
		return;

	const TSubclassOf<AAbility> ClassToSpawn = GetAbilityClass(Ability);
	const EAbilityTargetPolicy TargetPolicy = UAbilityFunctionLibrary::GetTargetPolicy(ClassToSpawn);
	
	if (TargetPolicy == EAbilityTargetPolicy::Target)
		CastTargetAbility();
	else if (TargetPolicy == EAbilityTargetPolicy::Zone)
		CastZoneAbility();
}

void UAbilityCastingComponent::CastTargetAbility()
{
	if (!IsValid(ObjectHover)
		|| !ObjectHover->IsActive()
		|| !IsValid(ObjectHover->GetHoveredActor())
		|| Ability == EAbilities::NoAbility)
	{
		return;
	}
	
	APlayerController* Caster = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	Server_CastTargetAbility(Caster, Ability, ObjectHover->GetHoveredActor());
}

void UAbilityCastingComponent::CastZoneAbility()
{
	if (!IsValid(PlacementPreview)
		|| !IsValid(PositionSelector)
		|| !PlacementPreview->IsActive()
		|| !PositionSelector->IsActive()
		|| !PositionSelector->IsDetected()
		|| Ability == EAbilities::NoAbility)
	{
		return;
	}
	
	APlayerController* Caster = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(PositionSelector->GetCurrentPosition());

	Server_CastZoneAbility(Caster, Ability, SpawnTransform);
}

TSubclassOf<AAbility> UAbilityCastingComponent::GetAbilityClass(EAbilities InAbility) const
{
	const FName AbilityName = static_cast<FName>(StaticEnum<EAbilities>()->GetNameStringByValue(static_cast<int64>(InAbility)));

	FAbilityClass* FoundRow = AbilityClasses->FindRow<FAbilityClass>(AbilityName, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("GetAbilityClass: A class for ability [\"%s\"] was not found."), *AbilityName.ToString());
		return AAbility::StaticClass();
	}

	return FoundRow->Class.Get();
}

bool UAbilityCastingComponent::TryBuyAbility(APlayerController* InAbilityOwner, EAbilities InAbility)
{
	UWalletComponent* Wallet = InAbilityOwner->PlayerState->GetComponentByClass<UWalletComponent>();
	if (!IsValid(Wallet))
	{
		UE_LOG(LogTemp, Warning, TEXT("FindMoneyComponent: PlayerState has no moneyComponent."));
		return nullptr;
	}
	
	if (Wallet == nullptr)
		return false;

	const FName Name = static_cast<FName>(StaticEnum<EAbilities>()->GetNameStringByValue(static_cast<int64>(InAbility)));

	FPrice* FoundRow = AbilityPrices->FindRow<FPrice>(Name, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("TryBuyAbility: A price for ability [\"%s\"] was not found."), *Name.ToString());
		return false;
	}

	return Wallet->TryBuy(FoundRow->Amount);
}
