﻿// DemoDreams. All rights reserved.

#include <PlayerAbility/AbilityControllers/AbilityRoundCountController.h>

#include <Kismet/GameplayStatics.h>
#include <GameFramework/GameState.h>

void UAbilityRoundCountController::BeginPlay()
{
    Super::BeginPlay();
}

void UAbilityRoundCountController::Start_Implementation()
{
    Super::Start_Implementation();
    
    RoundController = UGameplayStatics::GetGameState(GetWorld())->GetComponentByClass<URoundControllerComponent>();

    if (!IsValid(RoundController))
    {
        UE_LOG(LogTemp, Error, TEXT("AbilityWaveCountController: The WaveController is not available."));
        return;
    }

    StartRound = RoundController->GetCurrentRoundNumber();
    RoundController->OnRoundChanged.AddUniqueDynamic(this, &UAbilityRoundCountController::OnRoundChanged);
}

void UAbilityRoundCountController::Finish_Implementation()
{
    if (IsValid(RoundController))
    {
        RoundController->OnRoundChanged.RemoveDynamic(this, &UAbilityRoundCountController::OnRoundChanged);
    }
    
    Super::Finish_Implementation();
}

void UAbilityRoundCountController::SetRoundCount(const uint8 InRoundCount)
{
    RoundCount = InRoundCount;
}

void UAbilityRoundCountController::OnRoundChanged()
{
    const int CurrentWave = RoundController->GetCurrentRoundNumber() - StartRound;
    if (CurrentWave >= RoundCount)
        Finish();
}
