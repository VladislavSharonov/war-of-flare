﻿// DemoDreams. All rights reserved.

#include <PlayerAbility/AbilityControllers/AbilityDurationController.h>

void UAbilityDurationController::BeginPlay()
{
    Super::BeginPlay();
}

void UAbilityDurationController::SetDuration(const float InDuration)
{
    Duration = InDuration;
}

void UAbilityDurationController::Start_Implementation()
{
    Super::Start_Implementation();
    GetWorld()->GetTimerManager().SetTimer(
       DurationTimerHandle,
       this,
       &UAbilityDurationController::Finish,
       Duration,
       false);
}

void UAbilityDurationController::Finish_Implementation()
{
    GetWorld()->GetTimerManager().ClearTimer(DurationTimerHandle);
    
    Super::Finish_Implementation();
}
