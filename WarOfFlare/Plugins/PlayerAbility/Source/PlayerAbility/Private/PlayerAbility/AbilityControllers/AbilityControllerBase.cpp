﻿// DemoDreams. All rights reserved.

#include <PlayerAbility/AbilityControllers/AbilityControllerBase.h>

#include "PlayerAbility/Ability.h"

void UAbilityControllerBase::Finish_Implementation()
{
	AAbility* Ability = Cast<AAbility>(GetOwner());
	if (IsValid(Ability))
	{
		Ability->Finish();
	}
	else
		GetOwner()->Destroy();
}
