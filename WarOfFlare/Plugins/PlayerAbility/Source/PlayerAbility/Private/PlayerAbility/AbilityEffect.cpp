﻿// DemoDreams. All rights reserved.

#include "PlayerAbility/AbilityEffect.h"

UAbilityEffect::UAbilityEffect()
{
	SetIsReplicatedByDefault(true);
}

void UAbilityEffect::Cancel_Implementation()
{
	Finish();
}

void UAbilityEffect::Finish_Implementation()
{
	DestroyComponent();
}

void UAbilityEffect::SetLifetime(float InLifetime)
{
	GetWorld()->GetTimerManager().ClearTimer(LifetimeTimerHandle);

	GetWorld()->GetTimerManager().SetTimer(
		LifetimeTimerHandle,
		this,
		&UAbilityEffect::Finish,
		0.0f,
		true,
		InLifetime);
}

void UAbilityEffect::ExtendLifetime(float InLifetime)
{
	float remainingTime = GetWorld()->GetTimerManager().GetTimerRemaining(LifetimeTimerHandle);
	SetLifetime(InLifetime + remainingTime);
}
