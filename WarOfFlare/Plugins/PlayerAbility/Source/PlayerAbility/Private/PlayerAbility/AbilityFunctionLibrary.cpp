﻿// DemoDreams. All rights reserved.

#include "PlayerAbility/AbilityFunctionLibrary.h"

UAbilityEffect* UAbilityFunctionLibrary::AddEffect(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass)
{
	return Cast<UAbilityEffect>(Target->AddComponentByClass(EffectClass, false, FTransform::Identity, false));
}

UAbilityEffect* UAbilityFunctionLibrary::GetOrAddEffect(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass)
{
	UAbilityEffect* component = Cast<UAbilityEffect>(Target->FindComponentByClass(EffectClass));

	if (IsValid(component))
		return component;

	return AddEffect(Target, EffectClass);
}

UAbilityEffect* UAbilityFunctionLibrary::AddEffectWithLifetime(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass, float Lifetime)
{
	UAbilityEffect* component = AddEffect(Target, EffectClass);
	component->SetLifetime(Lifetime);
	return component;
}

UAbilityEffect* UAbilityFunctionLibrary::AddOrExtendEffect(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass, float Lifetime)
{
	UAbilityEffect* component = Cast<UAbilityEffect>(Target->FindComponentByClass(EffectClass));

	if (!IsValid(component))
		return AddEffectWithLifetime(Target, EffectClass, Lifetime);

	component->ExtendLifetime(Lifetime);
	return component;
}

EAbilityTargetPolicy UAbilityFunctionLibrary::GetTargetPolicy(TSubclassOf<AAbility> Ability)
{
	if (Ability->IsChildOf(AZoneAbility::StaticClass()))
		return EAbilityTargetPolicy::Zone;

	return EAbilityTargetPolicy::Target;
}

void UAbilityFunctionLibrary::SwitchAbilityByTargetPolicy(TSubclassOf<AAbility> Ability, EAbilityTargetPolicy& OutAbilityTargetPolicy)
{
	OutAbilityTargetPolicy = GetTargetPolicy(Ability);
}
