﻿// DemoDreams. All rights reserved.

#include "PlayerAbility/ZoneAbility.h"

void AZoneAbility::SetPlacementPreview_Implementation(bool InIsPreview)
{
	// Set Preview for all AAbilityZone.
	TArray<AActor*> AttachedActors;
	GetAttachedActors(AttachedActors);
	for (auto* attached : AttachedActors)
	{
		AAbilityZone* abilityZone = Cast<AAbilityZone>(attached);
		if (IsValid(abilityZone))
			abilityZone->SetPlacementPreview(InIsPreview);
	}
		
	Super::SetPlacementPreview_Implementation(InIsPreview);
}
