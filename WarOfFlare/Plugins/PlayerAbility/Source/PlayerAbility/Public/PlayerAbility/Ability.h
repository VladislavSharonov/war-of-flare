﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "ObjectPlacement/IPlacementPreview.h"
#include "AbilityEffect.h"

#include "Ability.generated.h"

UCLASS()
class PLAYERABILITY_API AAbility : public AActor, public IPlacementPreview
{
	GENERATED_BODY()

public:
	AAbility();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability")
	void Finish();
	virtual void Finish_Implementation() { Destroy(); };
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Preview")
	void SetPlacementPreview(bool InIsPreview);
	virtual void SetPlacementPreview_Implementation(bool InIsPreview) override {};

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<UAbilityEffect> EffectClass = UAbilityEffect::StaticClass();
};
