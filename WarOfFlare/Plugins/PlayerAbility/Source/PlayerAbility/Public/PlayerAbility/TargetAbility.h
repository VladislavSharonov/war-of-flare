﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Ability.h"

#include "TargetAbility.generated.h"

UCLASS()
class PLAYERABILITY_API ATargetAbility : public AAbility
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability")
	void Apply(AActor* Actor);
	virtual void Apply_Implementation(AActor* Actor) {};

	virtual void SetPlacementPreview_Implementation(bool InIsPreview) override;
};
