﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "AbilityControllerBase.h"

#include "AbilityDurationController.generated.h"

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PLAYERABILITY_API UAbilityDurationController : public UAbilityControllerBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable, Category = "Ability|Controller|Duration")
	void SetDuration(const float InDuration);
	
protected:
	virtual void Start_Implementation() override;
	
	virtual void Finish_Implementation() override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ability|Controller|Duration")
	float Duration = 0.1f;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ability|Controller|Duration")
	FTimerHandle DurationTimerHandle;
};
