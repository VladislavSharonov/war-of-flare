﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "AbilityControllerBase.generated.h"

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PLAYERABILITY_API UAbilityControllerBase : public UActorComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override { }
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override { }

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability|Controller|Duration")
	void Start();
	virtual void Start_Implementation() { }
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability|Controller|Duration")
	void Finish();
	virtual void Finish_Implementation();
};
