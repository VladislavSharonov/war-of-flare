﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Round/RoundControllerComponent.h"
#include "AbilityControllerBase.h"

#include "AbilityRoundCountController.generated.h"

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PLAYERABILITY_API UAbilityRoundCountController : public UAbilityControllerBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	virtual void Start_Implementation() override;
	
	virtual void Finish_Implementation() override;
	
	UFUNCTION(BlueprintCallable, Category = "Ability|Controller|WaveCount")
	void SetRoundCount(const uint8 InRoundCount);

protected:
	UFUNCTION()
	void OnRoundChanged();

protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ability|Controller|WaveCount")
	uint8 RoundCount = 1;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ability|Controller|WaveCount")
	uint8 StartRound = 1;

	UPROPERTY()
	TObjectPtr<URoundControllerComponent> RoundController;
};
