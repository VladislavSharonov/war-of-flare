﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "AbilityTargetPolicy.generated.h"

UENUM(BlueprintType)
enum class EAbilityTargetPolicy : uint8
{
    Target  UMETA(DisplayName = "Target"),
    Zone    UMETA(DisplayName = "Zone"),
};
