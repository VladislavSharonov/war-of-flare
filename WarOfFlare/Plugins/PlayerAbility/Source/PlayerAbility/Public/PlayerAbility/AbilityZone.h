﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include <ObjectPlacement/IPlacementPreview.h>

#include "AbilityZone.generated.h"

UCLASS()
class PLAYERABILITY_API AAbilityZone : public AActor, public IPlacementPreview
{
	GENERATED_BODY()

public:
	AAbilityZone();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability|Zone")
	void GetOverlappedActors(TSubclassOf<AActor> ClassFilter, TArray<AActor*>& Overlaped);
	virtual void GetOverlappedActors_Implementation(TSubclassOf<AActor> ClassFilter, TArray<AActor*>& Overlaped) {};

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Preview")
	void SetPlacementPreview(bool InIsPreview);
	virtual void SetPlacementPreview_Implementation(bool InIsPreview) override {};
};
