﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "AbilityEffect.generated.h"

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PLAYERABILITY_API UAbilityEffect : public USceneComponent
{
	GENERATED_BODY()

public:
	UAbilityEffect();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability|Effect")
	void Start();
	virtual void Start_Implementation() {};

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability|Effect")
	void Cancel();
	virtual void Cancel_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability|Effect")
	void Finish();
	virtual void Finish_Implementation();

	UFUNCTION(BlueprintCallable, Category = "Ability|Effect")
	void SetLifetime(float InLifetime);

	UFUNCTION(BlueprintCallable, Category = "Ability|Effect")
	void ExtendLifetime(float InLifetime);

protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Ability|Effect")
	FTimerHandle LifetimeTimerHandle;
};
