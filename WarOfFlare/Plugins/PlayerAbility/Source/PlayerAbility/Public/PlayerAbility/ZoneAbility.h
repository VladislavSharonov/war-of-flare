﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Ability.h"
#include "AbilityZone.h"

#include "ZoneAbility.generated.h"

UCLASS()
class PLAYERABILITY_API AZoneAbility : public AAbility
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ability")
	void Start();
	virtual void Start_Implementation() {};

	virtual void SetPlacementPreview_Implementation(bool InIsPreview) override;
	
	UFUNCTION(BlueprintCallable)
	bool IsPreview() const { return bIsPreview; }

protected:
	UPROPERTY(BlueprintReadOnly)
	bool bIsPreview = false;
};
