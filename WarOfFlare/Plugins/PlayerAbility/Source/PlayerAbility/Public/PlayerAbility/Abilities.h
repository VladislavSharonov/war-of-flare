﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Abilities.generated.h"

UENUM(BlueprintType)
enum class EAbilities : uint8 
{
    NoAbility                       UMETA(Hidden, DisplayName = "NoAbility"),

    OneTimeAreaDamage               UMETA(DisplayName = "OneTimeAreaDamage"),

    OneTimeMinionFixedDamage        UMETA(DisplayName = "OneTimeMinionFixedDamage"),
    OneTimeMinionPercentageDamage   UMETA(DisplayName = "OneTimeMinionPercentageDamage"),

    PeriodicAreaDamageWithLifetime  UMETA(DisplayName = "PeriodicAreaDamageWithLifetime"),
    PeriodicAreaDamageDuringRounds  UMETA(DisplayName = "PeriodicAreaDamageDuringRounds"),

    DecelerationAreaWithLifetime    UMETA(DisplayName = "DecelerationAreaWithLifetime"),
    DecelerationAreaDuringRounds    UMETA(DisplayName = "DecelerationAreaDuringRounds"),

    // MinionVulnerabilityArea         UMETA(DisplayName = "VulnerabilityArea"),
    // MinionVulnerability             UMETA(DisplayName = "Vulnerability"),

    // IncreasedTowerDamage            UMETA(DisplayName = "IncreasedTowerDamage"),
    // IncreasedTowerDamageArea        UMETA(DisplayName = "IncreasedTowerDamageArea"),
    // IncreasedTowerDamageDuringRounds   UMETA(DisplayName = "IncreasedTowerDuringRounds"),

    ExplosiveArea                   UMETA(DisplayName = "ExplosiveArea"),
};
