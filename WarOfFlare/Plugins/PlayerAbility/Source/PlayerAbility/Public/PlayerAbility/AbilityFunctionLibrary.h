﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include <PlayerAbility/AbilityEffect.h>
#include <PlayerAbility/AbilityTargetPolicy.h>
#include <PlayerAbility/Ability.h>
#include <PlayerAbility/TargetAbility.h>
#include <PlayerAbility/ZoneAbility.h>

#include "AbilityFunctionLibrary.generated.h"

UCLASS()
class PLAYERABILITY_API UAbilityFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/**
	 * Add new effect to the actor.
	 */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static UAbilityEffect* AddEffect(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass);

	/**
	 * A new effect will be return if the actor have one. Otherwise the new effect will be added.
	 */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static UAbilityEffect* GetOrAddEffect(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass);

	UFUNCTION(BlueprintCallable, Category = "Ability")
	static UAbilityEffect* AddEffectWithLifetime(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass, float Lifetime);

	/**
	 * A new effect will be added if the actor does not have one. Otherwise effect time will be extended.
	 */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static UAbilityEffect* AddOrExtendEffect(AActor* Target, TSubclassOf<UAbilityEffect> EffectClass, float Lifetime);

	UFUNCTION(BlueprintCallable, Category = "Ability")
	static EAbilityTargetPolicy GetTargetPolicy(TSubclassOf<AAbility> Ability);

	UFUNCTION(BlueprintCallable, meta = (ExpandEnumAsExecs = "OutAbilityTargetPolicy"))
	static void SwitchAbilityByTargetPolicy(TSubclassOf<AAbility> Ability, EAbilityTargetPolicy& OutAbilityTargetPolicy);
};
