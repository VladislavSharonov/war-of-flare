﻿// DemoDreams. All rights reserved.

#pragma once

#include "Engine/DataTable.h"

#include <PlayerAbility/Abilities.h>
#include <PlayerAbility/Ability.h>

#include "ObjectHovering/CursorHoverComponent.h"
#include "ObjectPlacement/PlacementPreviewComponent.h"
#include "ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h"

#include "AbilityCastingComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class PLAYERABILITY_API UAbilityCastingComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UAbilityCastingComponent();

	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	virtual void OnRep_IsActive() override;
	
	virtual void Activate(bool bReset = false) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnActivated(bool bReset = false);
	
	virtual void Deactivate() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnDeactivated();
	
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_CastTargetAbility(APlayerController* InAbilityOwner, EAbilities InAbility, AActor* InTarget);
	virtual void Server_CastTargetAbility_Implementation(APlayerController* InAbilityOwner, EAbilities InAbility, AActor* InTarget);

	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_CastZoneAbility(APlayerController* InAbilityOwner, EAbilities InAbility, const FTransform& InTransform);
	virtual void Server_CastZoneAbility_Implementation(APlayerController* InAbilityOwner, EAbilities InAbility, const FTransform& InTransform);

	// Cancel casting
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EndCasting();
	virtual void EndCasting_Implementation();

	// Select target or position
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void StartCasting(EAbilities InAbility);
	virtual void StartCasting_Implementation(EAbilities InAbility);

	// Commit casting.
	UFUNCTION(BlueprintCallable)
	void CastAbility();

	UFUNCTION(BlueprintCallable)
	void CastTargetAbility();
	
	UFUNCTION(BlueprintCallable)
	void CastZoneAbility();
	
protected:
	UFUNCTION(BlueprintCallable)
	TSubclassOf<AAbility> GetAbilityClass(EAbilities InAbility) const;

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool TryBuyAbility(APlayerController* InAbilityOwner, EAbilities InAbility);

protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UDataTable> AbilityPrices = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|ZoneAbility")
	TSubclassOf<UPlacementPreviewComponent> PlacementPreviewClass = UPlacementPreviewComponent::StaticClass();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|ZoneAbility")
	TSubclassOf<UPositionSelectorComponent> PositionSelectorClass = UCursorPositionSelectorComponent::StaticClass();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|TargetAbility")
	TSubclassOf<UCursorHoverComponent> ObjectHoverClass = UCursorHoverComponent::StaticClass();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|ZoneAbility")
	UPlacementPreviewComponent* PlacementPreview = nullptr;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|ZoneAbility")
	UCursorPositionSelectorComponent* PositionSelector = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components|TargetAbility")
	UCursorHoverComponent* ObjectHover = nullptr;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UDataTable> AbilityClasses = nullptr;

	UPROPERTY(BlueprintReadwrite, EditAnywhere)
	EAbilities Ability = EAbilities::NoAbility;
};
