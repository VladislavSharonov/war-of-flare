﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"

#include <PlayerAbility/Ability.h>

#include "AbilityClass.generated.h"

USTRUCT(BlueprintType)
struct FAbilityClass : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AAbility> Class = AAbility::StaticClass();
};
