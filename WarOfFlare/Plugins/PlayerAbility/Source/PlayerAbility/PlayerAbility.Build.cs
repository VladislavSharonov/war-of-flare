﻿// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PlayerAbility : ModuleRules
{
	public PlayerAbility(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"Rounds",
				"ObjectPlacement", 
				"ObjectHovering",
				"Money",
				"Minions"
			}
			);
			
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine"
			}
			);
	}
}
