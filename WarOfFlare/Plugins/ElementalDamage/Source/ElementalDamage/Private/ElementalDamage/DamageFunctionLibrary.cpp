﻿// DemoDreams. All rights reserved.

#include <ElementalDamage/DamageFunctionLibrary.h>

#include "ElementalDamage/DamageTypes.h"
#include "Kismet/GameplayStatics.h"

EElementalDamageType UDamageFunctionLibrary::GetDamageElement(TSubclassOf<UDamageType> DamageTypeClass)
{
    if (UElectricDamageType::StaticClass() == DamageTypeClass)
        return EElementalDamageType::Electric;

    if (UKineticDamageType::StaticClass() == DamageTypeClass)
        return EElementalDamageType::Kinetic;

    if (UFireDamageType::StaticClass() == DamageTypeClass)
        return EElementalDamageType::Fire;

    if (ULaserDamageType::StaticClass() == DamageTypeClass)
        return EElementalDamageType::Laser;

    if (UAcidDamageType::StaticClass() == DamageTypeClass)
        return EElementalDamageType::Acid;

    if (UEmpDamageType::StaticClass() == DamageTypeClass)
        return EElementalDamageType::Emp;

	return EElementalDamageType::Base;
}

TSubclassOf<UDamageType> UDamageFunctionLibrary::GetDamageTypeClass(EElementalDamageType ElementalDamageType)
{
    switch (ElementalDamageType)
    {
    case EElementalDamageType::Base:
        return TSubclassOf<UDamageType>();

    case EElementalDamageType::Electric:
        return TSubclassOf<UElectricDamageType>();

    case EElementalDamageType::Kinetic:
        return TSubclassOf<UKineticDamageType>();

    case EElementalDamageType::Fire:
        return TSubclassOf<UFireDamageType>();

    case EElementalDamageType::Laser:
        return TSubclassOf<ULaserDamageType>();

    case EElementalDamageType::Acid:
        return TSubclassOf<UAcidDamageType>();

    case EElementalDamageType::Emp:
        return TSubclassOf<UEmpDamageType>();

    default:
        return TSubclassOf<UDamageType>();
    }
}

bool UDamageFunctionLibrary::ApplyRadialDamage(
    const UObject* WorldContextObject, 
    float BaseDamage, 
    const FVector& Origin, 
    float DamageRadius, 
    EElementalDamageType ElementalDamageType,
    const TArray<AActor*>& IgnoreActors, 
    AActor* DamageCauser, 
    AController* InstigatedByController, 
    bool bDoFullDamage, 
    ECollisionChannel DamagePreventionChannel)
{
    return UGameplayStatics::ApplyRadialDamage(
        WorldContextObject,
        BaseDamage,
        Origin,
        DamageRadius,
        GetDamageTypeClass(ElementalDamageType),
        IgnoreActors,
        DamageCauser,
        InstigatedByController,
        bDoFullDamage,
        DamagePreventionChannel);
}

bool UDamageFunctionLibrary::ApplyRadialDamageWithFalloff(
    const UObject* WorldContextObject, 
    float BaseDamage, 
    float MinimumDamage,
    const FVector& Origin,
    float DamageInnerRadius,
    float DamageOuterRadius,
    float DamageFalloff,
    EElementalDamageType ElementalDamageType, 
    const TArray<AActor*>& IgnoreActors, 
    AActor* DamageCauser,
    AController* InstigatedByController,
    ECollisionChannel DamagePreventionChannel)
{
    return UGameplayStatics::ApplyRadialDamageWithFalloff(
        WorldContextObject,
        BaseDamage,
        MinimumDamage,
        Origin,
        DamageInnerRadius,
        DamageOuterRadius,
        DamageFalloff,
        GetDamageTypeClass(ElementalDamageType),
        IgnoreActors,
        DamageCauser,
        InstigatedByController,
        DamagePreventionChannel);
}

float UDamageFunctionLibrary::ApplyPointDamage(
    AActor* DamagedActor, 
    float BaseDamage, 
    const FVector& HitFromDirection, 
    const FHitResult& HitInfo, 
    AController* EventInstigator, 
    AActor* DamageCauser, 
    EElementalDamageType ElementalDamageType)
{
    return UGameplayStatics::ApplyPointDamage(
        DamagedActor,
        BaseDamage,
        HitFromDirection,
        HitInfo,
        EventInstigator,
        DamageCauser,
        GetDamageTypeClass(ElementalDamageType));
}

float UDamageFunctionLibrary::ApplyDamage(
    AActor* DamagedActor, 
    float BaseDamage, 
    AController* EventInstigator, 
    AActor* DamageCauser, 
    EElementalDamageType ElementalDamageType)
{
    return UGameplayStatics::ApplyDamage(
        DamagedActor,
        BaseDamage,
        EventInstigator,
        DamageCauser,
        GetDamageTypeClass(ElementalDamageType));
}
