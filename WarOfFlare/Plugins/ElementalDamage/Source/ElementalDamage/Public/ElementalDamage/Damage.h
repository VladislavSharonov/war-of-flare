﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "ElementalDamageType.h"

#include "Damage.generated.h"

USTRUCT(BlueprintType)
struct FDamage : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	FDamage() {}

	explicit FDamage(const float InDamage, const EElementalDamageType InElementalType = EElementalDamageType::Base) 
		: Damage(InDamage), ElementalType(InElementalType) { }

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EElementalDamageType ElementalType = EElementalDamageType::Base;
};
