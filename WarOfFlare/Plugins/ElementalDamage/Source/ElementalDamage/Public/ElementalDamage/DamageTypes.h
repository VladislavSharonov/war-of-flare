// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/DamageType.h"

#include "DamageTypes.generated.h"

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType, HideDropdown)
class UElementalDamageType : public UDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class UBaseDamageType : public UElementalDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class UElectricDamageType : public UElementalDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class UKineticDamageType : public UElementalDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class UFireDamageType : public UElementalDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class ULaserDamageType : public UElementalDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class UAcidDamageType : public UElementalDamageType { GENERATED_BODY() };

UCLASS(MinimalAPI, Const, Blueprintable, BlueprintType)
class UEmpDamageType : public UElementalDamageType { GENERATED_BODY() };
