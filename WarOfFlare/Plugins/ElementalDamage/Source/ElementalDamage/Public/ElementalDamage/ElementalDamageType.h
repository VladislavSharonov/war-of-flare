﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "ElementalDamageType.generated.h"

UENUM(BlueprintType)
enum class EElementalDamageType : uint8 
{
    Base        UMETA(DisplayName = "Base"),
    Electric    UMETA(DisplayName = "Electric"),
    Kinetic     UMETA(DisplayName = "Kinetic"),
    Fire        UMETA(DisplayName = "Fire"),
    Laser       UMETA(DisplayName = "Laser"),
    Acid        UMETA(DisplayName = "Acid"),
    Emp         UMETA(DisplayName = "Emp"), // Electromagnetic pulse
    Count       UMETA(Hidden)
};

ENUM_RANGE_BY_COUNT(EElementalDamageType, EElementalDamageType::Count);
