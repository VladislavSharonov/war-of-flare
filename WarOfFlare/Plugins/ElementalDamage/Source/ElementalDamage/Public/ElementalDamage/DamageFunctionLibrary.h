﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "ElementalDamageType.h"

#include "DamageFunctionLibrary.generated.h"

UCLASS()
class ELEMENTALDAMAGE_API UDamageFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "Damage")
	static EElementalDamageType GetDamageElement(TSubclassOf<UDamageType> DamageTypeClass);

	UFUNCTION(BlueprintPure, Category = "Damage")
	static TSubclassOf<UDamageType> GetDamageTypeClass(EElementalDamageType ElementalDamageType);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game|Damage", meta = (WorldContext = "WorldContextObject", AutoCreateRefTerm = "IgnoreActors"))
	static bool ApplyRadialDamage(
		const UObject* WorldContextObject, 
		float BaseDamage, 
		const FVector& Origin, 
		float DamageRadius, 
		EElementalDamageType ElementalDamageType,
		const TArray<AActor*>& IgnoreActors, 
		AActor* DamageCauser = nullptr, 
		AController* InstigatedByController = nullptr, 
		bool bDoFullDamage = false, 
		ECollisionChannel DamagePreventionChannel = ECC_Visibility);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game|Damage", meta = (WorldContext = "WorldContextObject", AutoCreateRefTerm = "IgnoreActors"))
	static bool ApplyRadialDamageWithFalloff(
		const UObject* WorldContextObject, 
		float BaseDamage, 
		float MinimumDamage, 
		const FVector& Origin, 
		float DamageInnerRadius, 
		float DamageOuterRadius, 
		float DamageFalloff, 
		EElementalDamageType ElementalDamageType,
		const TArray<AActor*>& IgnoreActors, 
		AActor* DamageCauser = nullptr, 
		AController* InstigatedByController = nullptr, 
		ECollisionChannel DamagePreventionChannel = ECC_Visibility);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game|Damage")
	static float ApplyPointDamage(
		AActor* DamagedActor, 
		float BaseDamage, 
		const FVector& HitFromDirection, 
		const FHitResult& HitInfo, 
		AController* EventInstigator, 
		AActor* DamageCauser, 
		EElementalDamageType ElementalDamageType);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Game|Damage")
	static float ApplyDamage(
		AActor* DamagedActor, 
		float BaseDamage, 
		AController* EventInstigator, 
		AActor* DamageCauser,
		EElementalDamageType ElementalDamageType);
};
