﻿// DemoDreams. All rights reserved.

#include "Health/HealthComponent.h"

#include "Net/UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, MaxHealth);
	DOREPLIFETIME(UHealthComponent, Health);
}

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	
	MaxHealth = 100;
	Health = MaxHealth;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;
}

void UHealthComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

float UHealthComponent::GetFraction() const
{
	if (UKismetMathLibrary::Abs(GetMaxHealth()) < 1e-6)
		return 1;

	return GetHealth() / GetMaxHealth();
}

void UHealthComponent::SetHealth(const float Value)
{ 
	Health = FMath::Clamp(Value, 0.0f, GetMaxHealth());
	OnHealthChanged.Broadcast(this);

	if (Health < 1E-6)
		OnHealthSpent.Broadcast();
}

void UHealthComponent::SetMaxHealth(const float Value)
{
	MaxHealth = Value;
	OnHealthChanged.Broadcast(this);
}

void UHealthComponent::AddHealth(const float Value)
{
	SetHealth(GetHealth() + Value);
}

void UHealthComponent::SubtractHealth(const float Value)
{
	SetHealth(GetHealth() - Value);
}

void UHealthComponent::OnRep_MaxHealth()
{
	OnHealthChanged.Broadcast(this);
}

void UHealthComponent::OnRep_Health()
{
	OnHealthChanged.Broadcast(this);

	if (Health < 1E-6)
		OnHealthSpent.Broadcast();
}
