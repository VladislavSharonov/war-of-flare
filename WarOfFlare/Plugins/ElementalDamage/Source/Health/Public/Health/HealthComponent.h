﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "HealthComponent.generated.h"

class UHealthComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthSpent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthChanged, UHealthComponent*, Health);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class HEALTH_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHealthComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
public:
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealth() const { return Health; }

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetMaxHealth() const { return MaxHealth; }

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetPercent() const { return GetFraction() * 100; }

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetFraction() const;
	
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetHealth(float Value);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetMaxHealth(float Value);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void AddHealth(float Value);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void SubtractHealth(float Value);
	
public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable, VisibleAnywhere, Category = "Health")
	FHealthSpent OnHealthSpent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, VisibleAnywhere, Category = "Health")
	FHealthChanged OnHealthChanged;

protected:
	UFUNCTION(BlueprintCallable, Category = "Health")
	void OnRep_MaxHealth();
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, ReplicatedUsing=OnRep_MaxHealth, Category = "Health")
	float MaxHealth = 0;

	UFUNCTION(BlueprintCallable, Category = "Health")
	void OnRep_Health();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, ReplicatedUsing=OnRep_Health, Category = "Health")
	float Health = 0;
};
