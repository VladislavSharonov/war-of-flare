// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ElementalDamage/Public/ElementalDamage/DamageTypes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDamageTypes() {}
// Cross Module References
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UAcidDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UAcidDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UBaseDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UBaseDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UElectricDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UElectricDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UElementalDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UElementalDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UEmpDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UEmpDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UFireDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UFireDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UKineticDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UKineticDamageType_NoRegister();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_ULaserDamageType();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_ULaserDamageType_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDamageType();
	UPackage* Z_Construct_UPackage__Script_ElementalDamage();
// End Cross Module References
	void UElementalDamageType::StaticRegisterNativesUElementalDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UElementalDamageType);
	UClass* Z_Construct_UClass_UElementalDamageType_NoRegister()
	{
		return UElementalDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UElementalDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UElementalDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UElementalDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UElementalDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UElementalDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UElementalDamageType_Statics::ClassParams = {
		&UElementalDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x040900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UElementalDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UElementalDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UElementalDamageType()
	{
		if (!Z_Registration_Info_UClass_UElementalDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UElementalDamageType.OuterSingleton, Z_Construct_UClass_UElementalDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UElementalDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UElementalDamageType>()
	{
		return UElementalDamageType::StaticClass();
	}
	UElementalDamageType::UElementalDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UElementalDamageType);
	UElementalDamageType::~UElementalDamageType() {}
	void UBaseDamageType::StaticRegisterNativesUBaseDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UBaseDamageType);
	UClass* Z_Construct_UClass_UBaseDamageType_NoRegister()
	{
		return UBaseDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UBaseDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBaseDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBaseDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBaseDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBaseDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBaseDamageType_Statics::ClassParams = {
		&UBaseDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBaseDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBaseDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBaseDamageType()
	{
		if (!Z_Registration_Info_UClass_UBaseDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UBaseDamageType.OuterSingleton, Z_Construct_UClass_UBaseDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UBaseDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UBaseDamageType>()
	{
		return UBaseDamageType::StaticClass();
	}
	UBaseDamageType::UBaseDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBaseDamageType);
	UBaseDamageType::~UBaseDamageType() {}
	void UElectricDamageType::StaticRegisterNativesUElectricDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UElectricDamageType);
	UClass* Z_Construct_UClass_UElectricDamageType_NoRegister()
	{
		return UElectricDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UElectricDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UElectricDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UElectricDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UElectricDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UElectricDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UElectricDamageType_Statics::ClassParams = {
		&UElectricDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UElectricDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UElectricDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UElectricDamageType()
	{
		if (!Z_Registration_Info_UClass_UElectricDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UElectricDamageType.OuterSingleton, Z_Construct_UClass_UElectricDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UElectricDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UElectricDamageType>()
	{
		return UElectricDamageType::StaticClass();
	}
	UElectricDamageType::UElectricDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UElectricDamageType);
	UElectricDamageType::~UElectricDamageType() {}
	void UKineticDamageType::StaticRegisterNativesUKineticDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UKineticDamageType);
	UClass* Z_Construct_UClass_UKineticDamageType_NoRegister()
	{
		return UKineticDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UKineticDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UKineticDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UKineticDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UKineticDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UKineticDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UKineticDamageType_Statics::ClassParams = {
		&UKineticDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UKineticDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UKineticDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UKineticDamageType()
	{
		if (!Z_Registration_Info_UClass_UKineticDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UKineticDamageType.OuterSingleton, Z_Construct_UClass_UKineticDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UKineticDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UKineticDamageType>()
	{
		return UKineticDamageType::StaticClass();
	}
	UKineticDamageType::UKineticDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UKineticDamageType);
	UKineticDamageType::~UKineticDamageType() {}
	void UFireDamageType::StaticRegisterNativesUFireDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UFireDamageType);
	UClass* Z_Construct_UClass_UFireDamageType_NoRegister()
	{
		return UFireDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UFireDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFireDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFireDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFireDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFireDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UFireDamageType_Statics::ClassParams = {
		&UFireDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFireDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFireDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFireDamageType()
	{
		if (!Z_Registration_Info_UClass_UFireDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UFireDamageType.OuterSingleton, Z_Construct_UClass_UFireDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UFireDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UFireDamageType>()
	{
		return UFireDamageType::StaticClass();
	}
	UFireDamageType::UFireDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFireDamageType);
	UFireDamageType::~UFireDamageType() {}
	void ULaserDamageType::StaticRegisterNativesULaserDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ULaserDamageType);
	UClass* Z_Construct_UClass_ULaserDamageType_NoRegister()
	{
		return ULaserDamageType::StaticClass();
	}
	struct Z_Construct_UClass_ULaserDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULaserDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULaserDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULaserDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULaserDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ULaserDamageType_Statics::ClassParams = {
		&ULaserDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULaserDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULaserDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULaserDamageType()
	{
		if (!Z_Registration_Info_UClass_ULaserDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ULaserDamageType.OuterSingleton, Z_Construct_UClass_ULaserDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ULaserDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<ULaserDamageType>()
	{
		return ULaserDamageType::StaticClass();
	}
	ULaserDamageType::ULaserDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULaserDamageType);
	ULaserDamageType::~ULaserDamageType() {}
	void UAcidDamageType::StaticRegisterNativesUAcidDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAcidDamageType);
	UClass* Z_Construct_UClass_UAcidDamageType_NoRegister()
	{
		return UAcidDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UAcidDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAcidDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAcidDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAcidDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAcidDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAcidDamageType_Statics::ClassParams = {
		&UAcidDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAcidDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAcidDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAcidDamageType()
	{
		if (!Z_Registration_Info_UClass_UAcidDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAcidDamageType.OuterSingleton, Z_Construct_UClass_UAcidDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAcidDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UAcidDamageType>()
	{
		return UAcidDamageType::StaticClass();
	}
	UAcidDamageType::UAcidDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAcidDamageType);
	UAcidDamageType::~UAcidDamageType() {}
	void UEmpDamageType::StaticRegisterNativesUEmpDamageType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UEmpDamageType);
	UClass* Z_Construct_UClass_UEmpDamageType_NoRegister()
	{
		return UEmpDamageType::StaticClass();
	}
	struct Z_Construct_UClass_UEmpDamageType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEmpDamageType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UElementalDamageType,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEmpDamageType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "ElementalDamage/DamageTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEmpDamageType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEmpDamageType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UEmpDamageType_Statics::ClassParams = {
		&UEmpDamageType::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000900A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEmpDamageType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEmpDamageType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEmpDamageType()
	{
		if (!Z_Registration_Info_UClass_UEmpDamageType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UEmpDamageType.OuterSingleton, Z_Construct_UClass_UEmpDamageType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UEmpDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UEmpDamageType>()
	{
		return UEmpDamageType::StaticClass();
	}
	UEmpDamageType::UEmpDamageType(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEmpDamageType);
	UEmpDamageType::~UEmpDamageType() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageTypes_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageTypes_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UElementalDamageType, UElementalDamageType::StaticClass, TEXT("UElementalDamageType"), &Z_Registration_Info_UClass_UElementalDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UElementalDamageType), 3308150728U) },
		{ Z_Construct_UClass_UBaseDamageType, UBaseDamageType::StaticClass, TEXT("UBaseDamageType"), &Z_Registration_Info_UClass_UBaseDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UBaseDamageType), 2243213626U) },
		{ Z_Construct_UClass_UElectricDamageType, UElectricDamageType::StaticClass, TEXT("UElectricDamageType"), &Z_Registration_Info_UClass_UElectricDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UElectricDamageType), 2198737555U) },
		{ Z_Construct_UClass_UKineticDamageType, UKineticDamageType::StaticClass, TEXT("UKineticDamageType"), &Z_Registration_Info_UClass_UKineticDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UKineticDamageType), 2481186969U) },
		{ Z_Construct_UClass_UFireDamageType, UFireDamageType::StaticClass, TEXT("UFireDamageType"), &Z_Registration_Info_UClass_UFireDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UFireDamageType), 3968427661U) },
		{ Z_Construct_UClass_ULaserDamageType, ULaserDamageType::StaticClass, TEXT("ULaserDamageType"), &Z_Registration_Info_UClass_ULaserDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ULaserDamageType), 2487813107U) },
		{ Z_Construct_UClass_UAcidDamageType, UAcidDamageType::StaticClass, TEXT("UAcidDamageType"), &Z_Registration_Info_UClass_UAcidDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAcidDamageType), 3475577613U) },
		{ Z_Construct_UClass_UEmpDamageType, UEmpDamageType::StaticClass, TEXT("UEmpDamageType"), &Z_Registration_Info_UClass_UEmpDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UEmpDamageType), 2611062106U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageTypes_h_1056338959(TEXT("/Script/ElementalDamage"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageTypes_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageTypes_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
