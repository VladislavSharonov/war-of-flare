// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ElementalDamage/ElementalDamageType.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ELEMENTALDAMAGE_ElementalDamageType_generated_h
#error "ElementalDamageType.generated.h already included, missing '#pragma once' in ElementalDamageType.h"
#endif
#define ELEMENTALDAMAGE_ElementalDamageType_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_ElementalDamageType_h


#define FOREACH_ENUM_EELEMENTALDAMAGETYPE(op) \
	op(EElementalDamageType::Base) \
	op(EElementalDamageType::Electric) \
	op(EElementalDamageType::Kinetic) \
	op(EElementalDamageType::Fire) \
	op(EElementalDamageType::Laser) \
	op(EElementalDamageType::Acid) \
	op(EElementalDamageType::Emp) \
	op(EElementalDamageType::Count) 

enum class EElementalDamageType : uint8;
template<> struct TIsUEnumClass<EElementalDamageType> { enum { Value = true }; };
template<> ELEMENTALDAMAGE_API UEnum* StaticEnum<EElementalDamageType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
