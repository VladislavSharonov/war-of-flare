// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Health/HealthComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UHealthComponent;
#ifdef HEALTH_HealthComponent_generated_h
#error "HealthComponent.generated.h already included, missing '#pragma once' in HealthComponent.h"
#endif
#define HEALTH_HealthComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_12_DELEGATE \
HEALTH_API void FHealthSpent_DelegateWrapper(const FMulticastScriptDelegate& HealthSpent);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_13_DELEGATE \
HEALTH_API void FHealthChanged_DelegateWrapper(const FMulticastScriptDelegate& HealthChanged, UHealthComponent* Health);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnRep_Health); \
	DECLARE_FUNCTION(execOnRep_MaxHealth); \
	DECLARE_FUNCTION(execSubtractHealth); \
	DECLARE_FUNCTION(execAddHealth); \
	DECLARE_FUNCTION(execSetMaxHealth); \
	DECLARE_FUNCTION(execSetHealth); \
	DECLARE_FUNCTION(execGetFraction); \
	DECLARE_FUNCTION(execGetPercent); \
	DECLARE_FUNCTION(execGetMaxHealth); \
	DECLARE_FUNCTION(execGetHealth);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_Health); \
	DECLARE_FUNCTION(execOnRep_MaxHealth); \
	DECLARE_FUNCTION(execSubtractHealth); \
	DECLARE_FUNCTION(execAddHealth); \
	DECLARE_FUNCTION(execSetMaxHealth); \
	DECLARE_FUNCTION(execSetHealth); \
	DECLARE_FUNCTION(execGetFraction); \
	DECLARE_FUNCTION(execGetPercent); \
	DECLARE_FUNCTION(execGetMaxHealth); \
	DECLARE_FUNCTION(execGetHealth);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealthComponent(); \
	friend struct Z_Construct_UClass_UHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Health"), NO_API) \
	DECLARE_SERIALIZER(UHealthComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		MaxHealth=NETFIELD_REP_START, \
		Health, \
		NETFIELD_REP_END=Health	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUHealthComponent(); \
	friend struct Z_Construct_UClass_UHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Health"), NO_API) \
	DECLARE_SERIALIZER(UHealthComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		MaxHealth=NETFIELD_REP_START, \
		Health, \
		NETFIELD_REP_END=Health	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealthComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealthComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthComponent(UHealthComponent&&); \
	NO_API UHealthComponent(const UHealthComponent&); \
public: \
	NO_API virtual ~UHealthComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthComponent(UHealthComponent&&); \
	NO_API UHealthComponent(const UHealthComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHealthComponent) \
	NO_API virtual ~UHealthComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_15_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HEALTH_API UClass* StaticClass<class UHealthComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_Health_Public_Health_HealthComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
