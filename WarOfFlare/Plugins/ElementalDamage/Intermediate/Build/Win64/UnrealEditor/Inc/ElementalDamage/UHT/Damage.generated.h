// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ElementalDamage/Damage.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ELEMENTALDAMAGE_Damage_generated_h
#error "Damage.generated.h already included, missing '#pragma once' in Damage.h"
#endif
#define ELEMENTALDAMAGE_Damage_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDamage_Statics; \
	ELEMENTALDAMAGE_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> ELEMENTALDAMAGE_API UScriptStruct* StaticStruct<struct FDamage>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
