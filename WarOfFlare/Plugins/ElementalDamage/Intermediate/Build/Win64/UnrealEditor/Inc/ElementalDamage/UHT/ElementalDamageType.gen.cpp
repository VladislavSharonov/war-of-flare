// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ElementalDamage/Public/ElementalDamage/ElementalDamageType.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeElementalDamageType() {}
// Cross Module References
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	UPackage* Z_Construct_UPackage__Script_ElementalDamage();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EElementalDamageType;
	static UEnum* EElementalDamageType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EElementalDamageType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EElementalDamageType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_ElementalDamage_EElementalDamageType, (UObject*)Z_Construct_UPackage__Script_ElementalDamage(), TEXT("EElementalDamageType"));
		}
		return Z_Registration_Info_UEnum_EElementalDamageType.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UEnum* StaticEnum<EElementalDamageType>()
	{
		return EElementalDamageType_StaticEnum();
	}
	struct Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::Enumerators[] = {
		{ "EElementalDamageType::Base", (int64)EElementalDamageType::Base },
		{ "EElementalDamageType::Electric", (int64)EElementalDamageType::Electric },
		{ "EElementalDamageType::Kinetic", (int64)EElementalDamageType::Kinetic },
		{ "EElementalDamageType::Fire", (int64)EElementalDamageType::Fire },
		{ "EElementalDamageType::Laser", (int64)EElementalDamageType::Laser },
		{ "EElementalDamageType::Acid", (int64)EElementalDamageType::Acid },
		{ "EElementalDamageType::Emp", (int64)EElementalDamageType::Emp },
		{ "EElementalDamageType::Count", (int64)EElementalDamageType::Count },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::Enum_MetaDataParams[] = {
		{ "Acid.DisplayName", "Acid" },
		{ "Acid.Name", "EElementalDamageType::Acid" },
		{ "Base.DisplayName", "Base" },
		{ "Base.Name", "EElementalDamageType::Base" },
		{ "BlueprintType", "true" },
		{ "Count.Comment", "// Electromagnetic pulse\n" },
		{ "Count.Hidden", "" },
		{ "Count.Name", "EElementalDamageType::Count" },
		{ "Count.ToolTip", "Electromagnetic pulse" },
		{ "Electric.DisplayName", "Electric" },
		{ "Electric.Name", "EElementalDamageType::Electric" },
		{ "Emp.DisplayName", "Emp" },
		{ "Emp.Name", "EElementalDamageType::Emp" },
		{ "Fire.DisplayName", "Fire" },
		{ "Fire.Name", "EElementalDamageType::Fire" },
		{ "Kinetic.DisplayName", "Kinetic" },
		{ "Kinetic.Name", "EElementalDamageType::Kinetic" },
		{ "Laser.DisplayName", "Laser" },
		{ "Laser.Name", "EElementalDamageType::Laser" },
		{ "ModuleRelativePath", "Public/ElementalDamage/ElementalDamageType.h" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_ElementalDamage,
		nullptr,
		"EElementalDamageType",
		"EElementalDamageType",
		Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType()
	{
		if (!Z_Registration_Info_UEnum_EElementalDamageType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EElementalDamageType.InnerSingleton, Z_Construct_UEnum_ElementalDamage_EElementalDamageType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EElementalDamageType.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_ElementalDamageType_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_ElementalDamageType_h_Statics::EnumInfo[] = {
		{ EElementalDamageType_StaticEnum, TEXT("EElementalDamageType"), &Z_Registration_Info_UEnum_EElementalDamageType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 1545068981U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_ElementalDamageType_h_1818401570(TEXT("/Script/ElementalDamage"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_ElementalDamageType_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_ElementalDamageType_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
