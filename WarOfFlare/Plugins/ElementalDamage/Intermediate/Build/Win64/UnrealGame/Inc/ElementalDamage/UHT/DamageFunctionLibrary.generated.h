// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ElementalDamage/DamageFunctionLibrary.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class AController;
class UDamageType;
class UObject;
enum class EElementalDamageType : uint8;
struct FHitResult;
#ifdef ELEMENTALDAMAGE_DamageFunctionLibrary_generated_h
#error "DamageFunctionLibrary.generated.h already included, missing '#pragma once' in DamageFunctionLibrary.h"
#endif
#define ELEMENTALDAMAGE_DamageFunctionLibrary_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execApplyDamage); \
	DECLARE_FUNCTION(execApplyPointDamage); \
	DECLARE_FUNCTION(execApplyRadialDamageWithFalloff); \
	DECLARE_FUNCTION(execApplyRadialDamage); \
	DECLARE_FUNCTION(execGetDamageTypeClass); \
	DECLARE_FUNCTION(execGetDamageElement);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execApplyDamage); \
	DECLARE_FUNCTION(execApplyPointDamage); \
	DECLARE_FUNCTION(execApplyRadialDamageWithFalloff); \
	DECLARE_FUNCTION(execApplyRadialDamage); \
	DECLARE_FUNCTION(execGetDamageTypeClass); \
	DECLARE_FUNCTION(execGetDamageElement);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDamageFunctionLibrary(); \
	friend struct Z_Construct_UClass_UDamageFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UDamageFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ElementalDamage"), NO_API) \
	DECLARE_SERIALIZER(UDamageFunctionLibrary)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDamageFunctionLibrary(); \
	friend struct Z_Construct_UClass_UDamageFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UDamageFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ElementalDamage"), NO_API) \
	DECLARE_SERIALIZER(UDamageFunctionLibrary)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDamageFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDamageFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDamageFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDamageFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDamageFunctionLibrary(UDamageFunctionLibrary&&); \
	NO_API UDamageFunctionLibrary(const UDamageFunctionLibrary&); \
public: \
	NO_API virtual ~UDamageFunctionLibrary();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDamageFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDamageFunctionLibrary(UDamageFunctionLibrary&&); \
	NO_API UDamageFunctionLibrary(const UDamageFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDamageFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDamageFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDamageFunctionLibrary) \
	NO_API virtual ~UDamageFunctionLibrary();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_11_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ELEMENTALDAMAGE_API UClass* StaticClass<class UDamageFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
