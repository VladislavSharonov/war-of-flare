// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ElementalDamage/Public/ElementalDamage/DamageFunctionLibrary.h"
#include "../../Source/Runtime/Engine/Classes/Engine/HitResult.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDamageFunctionLibrary() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UDamageFunctionLibrary();
	ELEMENTALDAMAGE_API UClass* Z_Construct_UClass_UDamageFunctionLibrary_NoRegister();
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UDamageType_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	UPackage* Z_Construct_UPackage__Script_ElementalDamage();
// End Cross Module References
	DEFINE_FUNCTION(UDamageFunctionLibrary::execApplyDamage)
	{
		P_GET_OBJECT(AActor,Z_Param_DamagedActor);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BaseDamage);
		P_GET_OBJECT(AController,Z_Param_EventInstigator);
		P_GET_OBJECT(AActor,Z_Param_DamageCauser);
		P_GET_ENUM(EElementalDamageType,Z_Param_ElementalDamageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UDamageFunctionLibrary::ApplyDamage(Z_Param_DamagedActor,Z_Param_BaseDamage,Z_Param_EventInstigator,Z_Param_DamageCauser,EElementalDamageType(Z_Param_ElementalDamageType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDamageFunctionLibrary::execApplyPointDamage)
	{
		P_GET_OBJECT(AActor,Z_Param_DamagedActor);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BaseDamage);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_HitFromDirection);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitInfo);
		P_GET_OBJECT(AController,Z_Param_EventInstigator);
		P_GET_OBJECT(AActor,Z_Param_DamageCauser);
		P_GET_ENUM(EElementalDamageType,Z_Param_ElementalDamageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UDamageFunctionLibrary::ApplyPointDamage(Z_Param_DamagedActor,Z_Param_BaseDamage,Z_Param_Out_HitFromDirection,Z_Param_Out_HitInfo,Z_Param_EventInstigator,Z_Param_DamageCauser,EElementalDamageType(Z_Param_ElementalDamageType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDamageFunctionLibrary::execApplyRadialDamageWithFalloff)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BaseDamage);
		P_GET_PROPERTY(FFloatProperty,Z_Param_MinimumDamage);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Origin);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DamageInnerRadius);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DamageOuterRadius);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DamageFalloff);
		P_GET_ENUM(EElementalDamageType,Z_Param_ElementalDamageType);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_IgnoreActors);
		P_GET_OBJECT(AActor,Z_Param_DamageCauser);
		P_GET_OBJECT(AController,Z_Param_InstigatedByController);
		P_GET_PROPERTY(FByteProperty,Z_Param_DamagePreventionChannel);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UDamageFunctionLibrary::ApplyRadialDamageWithFalloff(Z_Param_WorldContextObject,Z_Param_BaseDamage,Z_Param_MinimumDamage,Z_Param_Out_Origin,Z_Param_DamageInnerRadius,Z_Param_DamageOuterRadius,Z_Param_DamageFalloff,EElementalDamageType(Z_Param_ElementalDamageType),Z_Param_Out_IgnoreActors,Z_Param_DamageCauser,Z_Param_InstigatedByController,ECollisionChannel(Z_Param_DamagePreventionChannel));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDamageFunctionLibrary::execApplyRadialDamage)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BaseDamage);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Origin);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DamageRadius);
		P_GET_ENUM(EElementalDamageType,Z_Param_ElementalDamageType);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_IgnoreActors);
		P_GET_OBJECT(AActor,Z_Param_DamageCauser);
		P_GET_OBJECT(AController,Z_Param_InstigatedByController);
		P_GET_UBOOL(Z_Param_bDoFullDamage);
		P_GET_PROPERTY(FByteProperty,Z_Param_DamagePreventionChannel);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UDamageFunctionLibrary::ApplyRadialDamage(Z_Param_WorldContextObject,Z_Param_BaseDamage,Z_Param_Out_Origin,Z_Param_DamageRadius,EElementalDamageType(Z_Param_ElementalDamageType),Z_Param_Out_IgnoreActors,Z_Param_DamageCauser,Z_Param_InstigatedByController,Z_Param_bDoFullDamage,ECollisionChannel(Z_Param_DamagePreventionChannel));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDamageFunctionLibrary::execGetDamageTypeClass)
	{
		P_GET_ENUM(EElementalDamageType,Z_Param_ElementalDamageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSubclassOf<UDamageType> *)Z_Param__Result=UDamageFunctionLibrary::GetDamageTypeClass(EElementalDamageType(Z_Param_ElementalDamageType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDamageFunctionLibrary::execGetDamageElement)
	{
		P_GET_OBJECT(UClass,Z_Param_DamageTypeClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EElementalDamageType*)Z_Param__Result=UDamageFunctionLibrary::GetDamageElement(Z_Param_DamageTypeClass);
		P_NATIVE_END;
	}
	void UDamageFunctionLibrary::StaticRegisterNativesUDamageFunctionLibrary()
	{
		UClass* Class = UDamageFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyDamage", &UDamageFunctionLibrary::execApplyDamage },
			{ "ApplyPointDamage", &UDamageFunctionLibrary::execApplyPointDamage },
			{ "ApplyRadialDamage", &UDamageFunctionLibrary::execApplyRadialDamage },
			{ "ApplyRadialDamageWithFalloff", &UDamageFunctionLibrary::execApplyRadialDamageWithFalloff },
			{ "GetDamageElement", &UDamageFunctionLibrary::execGetDamageElement },
			{ "GetDamageTypeClass", &UDamageFunctionLibrary::execGetDamageTypeClass },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics
	{
		struct DamageFunctionLibrary_eventApplyDamage_Parms
		{
			AActor* DamagedActor;
			float BaseDamage;
			AController* EventInstigator;
			AActor* DamageCauser;
			EElementalDamageType ElementalDamageType;
			float ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DamagedActor;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_BaseDamage;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_EventInstigator;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ElementalDamageType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ElementalDamageType;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_DamagedActor = { "DamagedActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyDamage_Parms, DamagedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_BaseDamage = { "BaseDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyDamage_Parms, BaseDamage), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_EventInstigator = { "EventInstigator", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyDamage_Parms, EventInstigator), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_DamageCauser = { "DamageCauser", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyDamage_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_ElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_ElementalDamageType = { "ElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyDamage_Parms, ElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyDamage_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_DamagedActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_BaseDamage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_EventInstigator,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_DamageCauser,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_ElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_ElementalDamageType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game|Damage" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDamageFunctionLibrary, nullptr, "ApplyDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::DamageFunctionLibrary_eventApplyDamage_Parms), Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics
	{
		struct DamageFunctionLibrary_eventApplyPointDamage_Parms
		{
			AActor* DamagedActor;
			float BaseDamage;
			FVector HitFromDirection;
			FHitResult HitInfo;
			AController* EventInstigator;
			AActor* DamageCauser;
			EElementalDamageType ElementalDamageType;
			float ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DamagedActor;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_BaseDamage;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HitFromDirection_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_HitFromDirection;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HitInfo_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_HitInfo;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_EventInstigator;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ElementalDamageType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ElementalDamageType;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_DamagedActor = { "DamagedActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, DamagedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_BaseDamage = { "BaseDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, BaseDamage), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitFromDirection_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitFromDirection = { "HitFromDirection", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, HitFromDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitFromDirection_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitFromDirection_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitInfo_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitInfo = { "HitInfo", nullptr, (EPropertyFlags)0x0010008008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, HitInfo), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitInfo_MetaData)) }; // 1287526515
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_EventInstigator = { "EventInstigator", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, EventInstigator), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_DamageCauser = { "DamageCauser", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_ElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_ElementalDamageType = { "ElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, ElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyPointDamage_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_DamagedActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_BaseDamage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitFromDirection,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_HitInfo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_EventInstigator,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_DamageCauser,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_ElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_ElementalDamageType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game|Damage" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDamageFunctionLibrary, nullptr, "ApplyPointDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::DamageFunctionLibrary_eventApplyPointDamage_Parms), Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics
	{
		struct DamageFunctionLibrary_eventApplyRadialDamage_Parms
		{
			const UObject* WorldContextObject;
			float BaseDamage;
			FVector Origin;
			float DamageRadius;
			EElementalDamageType ElementalDamageType;
			TArray<AActor*> IgnoreActors;
			AActor* DamageCauser;
			AController* InstigatedByController;
			bool bDoFullDamage;
			TEnumAsByte<ECollisionChannel> DamagePreventionChannel;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_BaseDamage;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Origin;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_DamageRadius;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ElementalDamageType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ElementalDamageType;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_IgnoreActors_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IgnoreActors_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_IgnoreActors;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InstigatedByController;
		static void NewProp_bDoFullDamage_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bDoFullDamage;
		static const UECodeGen_Private::FBytePropertyParams NewProp_DamagePreventionChannel;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_WorldContextObject_MetaData)) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_BaseDamage = { "BaseDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, BaseDamage), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_Origin_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_Origin = { "Origin", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, Origin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_Origin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_Origin_MetaData)) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_DamageRadius = { "DamageRadius", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, DamageRadius), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ElementalDamageType = { "ElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, ElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors_Inner = { "IgnoreActors", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors = { "IgnoreActors", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, IgnoreActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_DamageCauser = { "DamageCauser", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_InstigatedByController = { "InstigatedByController", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, InstigatedByController), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_bDoFullDamage_SetBit(void* Obj)
	{
		((DamageFunctionLibrary_eventApplyRadialDamage_Parms*)Obj)->bDoFullDamage = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_bDoFullDamage = { "bDoFullDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(DamageFunctionLibrary_eventApplyRadialDamage_Parms), &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_bDoFullDamage_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_DamagePreventionChannel = { "DamagePreventionChannel", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamage_Parms, DamagePreventionChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(nullptr, 0) }; // 727872708
	void Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DamageFunctionLibrary_eventApplyRadialDamage_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(DamageFunctionLibrary_eventApplyRadialDamage_Parms), &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_BaseDamage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_Origin,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_DamageRadius,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ElementalDamageType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_IgnoreActors,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_DamageCauser,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_InstigatedByController,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_bDoFullDamage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_DamagePreventionChannel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::Function_MetaDataParams[] = {
		{ "AutoCreateRefTerm", "IgnoreActors" },
		{ "Category", "Game|Damage" },
		{ "CPP_Default_bDoFullDamage", "false" },
		{ "CPP_Default_DamageCauser", "None" },
		{ "CPP_Default_DamagePreventionChannel", "ECC_Visibility" },
		{ "CPP_Default_InstigatedByController", "None" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDamageFunctionLibrary, nullptr, "ApplyRadialDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::DamageFunctionLibrary_eventApplyRadialDamage_Parms), Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics
	{
		struct DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms
		{
			const UObject* WorldContextObject;
			float BaseDamage;
			float MinimumDamage;
			FVector Origin;
			float DamageInnerRadius;
			float DamageOuterRadius;
			float DamageFalloff;
			EElementalDamageType ElementalDamageType;
			TArray<AActor*> IgnoreActors;
			AActor* DamageCauser;
			AController* InstigatedByController;
			TEnumAsByte<ECollisionChannel> DamagePreventionChannel;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_BaseDamage;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_MinimumDamage;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Origin;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_DamageInnerRadius;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_DamageOuterRadius;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_DamageFalloff;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ElementalDamageType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ElementalDamageType;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_IgnoreActors_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IgnoreActors_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_IgnoreActors;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DamageCauser;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InstigatedByController;
		static const UECodeGen_Private::FBytePropertyParams NewProp_DamagePreventionChannel;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_WorldContextObject_MetaData)) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_BaseDamage = { "BaseDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, BaseDamage), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_MinimumDamage = { "MinimumDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, MinimumDamage), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_Origin_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_Origin = { "Origin", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, Origin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_Origin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_Origin_MetaData)) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageInnerRadius = { "DamageInnerRadius", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, DamageInnerRadius), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageOuterRadius = { "DamageOuterRadius", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, DamageOuterRadius), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageFalloff = { "DamageFalloff", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, DamageFalloff), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ElementalDamageType = { "ElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, ElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors_Inner = { "IgnoreActors", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors = { "IgnoreActors", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, IgnoreActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageCauser = { "DamageCauser", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, DamageCauser), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_InstigatedByController = { "InstigatedByController", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, InstigatedByController), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamagePreventionChannel = { "DamagePreventionChannel", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms, DamagePreventionChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(nullptr, 0) }; // 727872708
	void Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms), &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_WorldContextObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_BaseDamage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_MinimumDamage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_Origin,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageInnerRadius,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageOuterRadius,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageFalloff,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ElementalDamageType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_IgnoreActors,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamageCauser,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_InstigatedByController,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_DamagePreventionChannel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::Function_MetaDataParams[] = {
		{ "AutoCreateRefTerm", "IgnoreActors" },
		{ "Category", "Game|Damage" },
		{ "CPP_Default_DamageCauser", "None" },
		{ "CPP_Default_DamagePreventionChannel", "ECC_Visibility" },
		{ "CPP_Default_InstigatedByController", "None" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDamageFunctionLibrary, nullptr, "ApplyRadialDamageWithFalloff", nullptr, nullptr, sizeof(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::DamageFunctionLibrary_eventApplyRadialDamageWithFalloff_Parms), Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics
	{
		struct DamageFunctionLibrary_eventGetDamageElement_Parms
		{
			TSubclassOf<UDamageType>  DamageTypeClass;
			EElementalDamageType ReturnValue;
		};
		static const UECodeGen_Private::FClassPropertyParams NewProp_DamageTypeClass;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::NewProp_DamageTypeClass = { "DamageTypeClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventGetDamageElement_Parms, DamageTypeClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UDamageType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventGetDamageElement_Parms, ReturnValue), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::NewProp_DamageTypeClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::Function_MetaDataParams[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDamageFunctionLibrary, nullptr, "GetDamageElement", nullptr, nullptr, sizeof(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::DamageFunctionLibrary_eventGetDamageElement_Parms), Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics
	{
		struct DamageFunctionLibrary_eventGetDamageTypeClass_Parms
		{
			EElementalDamageType ElementalDamageType;
			TSubclassOf<UDamageType>  ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ElementalDamageType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ElementalDamageType;
		static const UECodeGen_Private::FClassPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::NewProp_ElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::NewProp_ElementalDamageType = { "ElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventGetDamageTypeClass_Parms, ElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(DamageFunctionLibrary_eventGetDamageTypeClass_Parms, ReturnValue), Z_Construct_UClass_UClass, Z_Construct_UClass_UDamageType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::NewProp_ElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::NewProp_ElementalDamageType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDamageFunctionLibrary, nullptr, "GetDamageTypeClass", nullptr, nullptr, sizeof(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::DamageFunctionLibrary_eventGetDamageTypeClass_Parms), Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UDamageFunctionLibrary);
	UClass* Z_Construct_UClass_UDamageFunctionLibrary_NoRegister()
	{
		return UDamageFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UDamageFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDamageFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDamageFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyDamage, "ApplyDamage" }, // 538814659
		{ &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyPointDamage, "ApplyPointDamage" }, // 364661375
		{ &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamage, "ApplyRadialDamage" }, // 1435293979
		{ &Z_Construct_UFunction_UDamageFunctionLibrary_ApplyRadialDamageWithFalloff, "ApplyRadialDamageWithFalloff" }, // 3494001770
		{ &Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageElement, "GetDamageElement" }, // 2176643453
		{ &Z_Construct_UFunction_UDamageFunctionLibrary_GetDamageTypeClass, "GetDamageTypeClass" }, // 3189162905
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDamageFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ElementalDamage/DamageFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/ElementalDamage/DamageFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDamageFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDamageFunctionLibrary>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UDamageFunctionLibrary_Statics::ClassParams = {
		&UDamageFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDamageFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDamageFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDamageFunctionLibrary()
	{
		if (!Z_Registration_Info_UClass_UDamageFunctionLibrary.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UDamageFunctionLibrary.OuterSingleton, Z_Construct_UClass_UDamageFunctionLibrary_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UDamageFunctionLibrary.OuterSingleton;
	}
	template<> ELEMENTALDAMAGE_API UClass* StaticClass<UDamageFunctionLibrary>()
	{
		return UDamageFunctionLibrary::StaticClass();
	}
	UDamageFunctionLibrary::UDamageFunctionLibrary(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDamageFunctionLibrary);
	UDamageFunctionLibrary::~UDamageFunctionLibrary() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UDamageFunctionLibrary, UDamageFunctionLibrary::StaticClass, TEXT("UDamageFunctionLibrary"), &Z_Registration_Info_UClass_UDamageFunctionLibrary, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UDamageFunctionLibrary), 2914849349U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_3344504712(TEXT("/Script/ElementalDamage"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_DamageFunctionLibrary_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
