// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ElementalDamage/Public/ElementalDamage/Damage.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDamage() {}
// Cross Module References
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	ELEMENTALDAMAGE_API UScriptStruct* Z_Construct_UScriptStruct_FDamage();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	UPackage* Z_Construct_UPackage__Script_ElementalDamage();
// End Cross Module References

static_assert(std::is_polymorphic<FDamage>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FDamage cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_Damage;
class UScriptStruct* FDamage::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_Damage.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_Damage.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FDamage, (UObject*)Z_Construct_UPackage__Script_ElementalDamage(), TEXT("Damage"));
	}
	return Z_Registration_Info_UScriptStruct_Damage.OuterSingleton;
}
template<> ELEMENTALDAMAGE_API UScriptStruct* StaticStruct<FDamage>()
{
	return FDamage::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FDamage_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Damage;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ElementalType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ElementalType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ElementalType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDamage_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/ElementalDamage/Damage.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDamage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDamage>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDamage_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Public/ElementalDamage/Damage.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDamage_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDamage, Damage), METADATA_PARAMS(Z_Construct_UScriptStruct_FDamage_Statics::NewProp_Damage_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDamage_Statics::NewProp_Damage_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType_MetaData[] = {
		{ "Category", "Damage" },
		{ "ModuleRelativePath", "Public/ElementalDamage/Damage.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType = { "ElementalType", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FDamage, ElementalType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType_MetaData)) }; // 1545068981
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDamage_Statics::NewProp_Damage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDamage_Statics::NewProp_ElementalType,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDamage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ElementalDamage,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"Damage",
		sizeof(FDamage),
		alignof(FDamage),
		Z_Construct_UScriptStruct_FDamage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDamage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDamage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDamage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDamage()
	{
		if (!Z_Registration_Info_UScriptStruct_Damage.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_Damage.InnerSingleton, Z_Construct_UScriptStruct_FDamage_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_Damage.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h_Statics::ScriptStructInfo[] = {
		{ FDamage::StaticStruct, Z_Construct_UScriptStruct_FDamage_Statics::NewStructOps, TEXT("Damage"), &Z_Registration_Info_UScriptStruct_Damage, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FDamage), 550170420U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h_1603482361(TEXT("/Script/ElementalDamage"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ElementalDamage_Source_ElementalDamage_Public_ElementalDamage_Damage_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
