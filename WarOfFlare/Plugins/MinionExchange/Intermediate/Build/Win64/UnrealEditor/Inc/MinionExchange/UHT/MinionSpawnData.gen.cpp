// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MinionExchange/Public/MinionExchange/MinionSpawnData.h"
#include "Net/Serialization/FastArraySerializerImplementation.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionSpawnData() {}
// Cross Module References
	MINIONEXCHANGE_API UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataArray();
	MINIONEXCHANGE_API UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataItem();
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EMinionType();
	NETCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFastArraySerializer();
	NETCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFastArraySerializerItem();
	UPackage* Z_Construct_UPackage__Script_MinionExchange();
// End Cross Module References

static_assert(std::is_polymorphic<FMinionSpawnDataItem>() == std::is_polymorphic<FFastArraySerializerItem>(), "USTRUCT FMinionSpawnDataItem cannot be polymorphic unless super FFastArraySerializerItem is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_MinionSpawnDataItem;
class UScriptStruct* FMinionSpawnDataItem::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_MinionSpawnDataItem.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_MinionSpawnDataItem.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FMinionSpawnDataItem, (UObject*)Z_Construct_UPackage__Script_MinionExchange(), TEXT("MinionSpawnDataItem"));
	}
	return Z_Registration_Info_UScriptStruct_MinionSpawnDataItem.OuterSingleton;
}
template<> MINIONEXCHANGE_API UScriptStruct* StaticStruct<FMinionSpawnDataItem>()
{
	return FMinionSpawnDataItem::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FBytePropertyParams NewProp_Minion_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Minion_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_Minion;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_Count;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionSpawnData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMinionSpawnDataItem>();
	}
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion_MetaData[] = {
		{ "Category", "MinionSpawnDataItem" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionSpawnData.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion = { "Minion", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FMinionSpawnDataItem, Minion), Z_Construct_UEnum_Minions_EMinionType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion_MetaData)) }; // 3979814491
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Count_MetaData[] = {
		{ "Category", "MinionSpawnDataItem" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionSpawnData.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FMinionSpawnDataItem, Count), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Count_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Minion,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewProp_Count,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MinionExchange,
		Z_Construct_UScriptStruct_FFastArraySerializerItem,
		&NewStructOps,
		"MinionSpawnDataItem",
		sizeof(FMinionSpawnDataItem),
		alignof(FMinionSpawnDataItem),
		Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataItem()
	{
		if (!Z_Registration_Info_UScriptStruct_MinionSpawnDataItem.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_MinionSpawnDataItem.InnerSingleton, Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_MinionSpawnDataItem.InnerSingleton;
	}

static_assert(std::is_polymorphic<FMinionSpawnDataArray>() == std::is_polymorphic<FFastArraySerializer>(), "USTRUCT FMinionSpawnDataArray cannot be polymorphic unless super FFastArraySerializer is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_MinionSpawnDataArray;
class UScriptStruct* FMinionSpawnDataArray::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_MinionSpawnDataArray.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_MinionSpawnDataArray.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FMinionSpawnDataArray, (UObject*)Z_Construct_UPackage__Script_MinionExchange(), TEXT("MinionSpawnDataArray"));
	}
	return Z_Registration_Info_UScriptStruct_MinionSpawnDataArray.OuterSingleton;
}
template<> MINIONEXCHANGE_API UScriptStruct* StaticStruct<FMinionSpawnDataArray>()
{
	return FMinionSpawnDataArray::StaticStruct();
}
#if defined(UE_NET_HAS_IRIS_FASTARRAY_BINDING) && UE_NET_HAS_IRIS_FASTARRAY_BINDING
	UE_NET_IMPLEMENT_FASTARRAY(FMinionSpawnDataArray);
#else
	UE_NET_IMPLEMENT_FASTARRAY_STUB(FMinionSpawnDataArray);
#endif
	struct Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FStructPropertyParams NewProp_Items_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Items;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionSpawnData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMinionSpawnDataArray>();
	}
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items_Inner = { "Items", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FMinionSpawnDataItem, METADATA_PARAMS(nullptr, 0) }; // 1444665368
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items_MetaData[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionSpawnData.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FMinionSpawnDataArray, Items), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items_MetaData)) }; // 1444665368
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewProp_Items,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MinionExchange,
		Z_Construct_UScriptStruct_FFastArraySerializer,
		&NewStructOps,
		"MinionSpawnDataArray",
		sizeof(FMinionSpawnDataArray),
		alignof(FMinionSpawnDataArray),
		Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataArray()
	{
		if (!Z_Registration_Info_UScriptStruct_MinionSpawnDataArray.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_MinionSpawnDataArray.InnerSingleton, Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_MinionSpawnDataArray.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_Statics::ScriptStructInfo[] = {
		{ FMinionSpawnDataItem::StaticStruct, Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics::NewStructOps, TEXT("MinionSpawnDataItem"), &Z_Registration_Info_UScriptStruct_MinionSpawnDataItem, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FMinionSpawnDataItem), 1444665368U) },
		{ FMinionSpawnDataArray::StaticStruct, Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics::NewStructOps, TEXT("MinionSpawnDataArray"), &Z_Registration_Info_UScriptStruct_MinionSpawnDataArray, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FMinionSpawnDataArray), 1338440401U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_4192504377(TEXT("/Script/MinionExchange"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
