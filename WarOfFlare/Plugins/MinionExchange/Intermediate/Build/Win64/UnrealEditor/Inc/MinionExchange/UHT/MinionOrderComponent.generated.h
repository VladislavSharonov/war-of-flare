// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MinionExchange/MinionOrderComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWalletComponent;
enum class EMinionType : uint8;
struct FMinionSpawnDataItem;
#ifdef MINIONEXCHANGE_MinionOrderComponent_generated_h
#error "MinionOrderComponent.generated.h already included, missing '#pragma once' in MinionOrderComponent.h"
#endif
#define MINIONEXCHANGE_MinionOrderComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_RPC_WRAPPERS \
	virtual bool Server_OrderMinion_Validate(EMinionType ); \
	virtual void Server_OrderMinion_Implementation(EMinionType Minion); \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execTryBuyMinion); \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execGetSpawnData); \
	DECLARE_FUNCTION(execServer_OrderMinion); \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void Server_OrderMinion_Implementation(EMinionType Minion); \
 \
	DECLARE_FUNCTION(execTryBuyMinion); \
	DECLARE_FUNCTION(execClear); \
	DECLARE_FUNCTION(execGetSpawnData); \
	DECLARE_FUNCTION(execServer_OrderMinion); \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMinionOrderComponent(); \
	friend struct Z_Construct_UClass_UMinionOrderComponent_Statics; \
public: \
	DECLARE_CLASS(UMinionOrderComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MinionExchange"), NO_API) \
	DECLARE_SERIALIZER(UMinionOrderComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		MinionSpawnData=NETFIELD_REP_START, \
		NETFIELD_REP_END=MinionSpawnData	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMinionOrderComponent(); \
	friend struct Z_Construct_UClass_UMinionOrderComponent_Statics; \
public: \
	DECLARE_CLASS(UMinionOrderComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MinionExchange"), NO_API) \
	DECLARE_SERIALIZER(UMinionOrderComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		MinionSpawnData=NETFIELD_REP_START, \
		NETFIELD_REP_END=MinionSpawnData	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMinionOrderComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMinionOrderComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinionOrderComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinionOrderComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinionOrderComponent(UMinionOrderComponent&&); \
	NO_API UMinionOrderComponent(const UMinionOrderComponent&); \
public: \
	NO_API virtual ~UMinionOrderComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinionOrderComponent(UMinionOrderComponent&&); \
	NO_API UMinionOrderComponent(const UMinionOrderComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinionOrderComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinionOrderComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMinionOrderComponent) \
	NO_API virtual ~UMinionOrderComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MINIONEXCHANGE_API UClass* StaticClass<class UMinionOrderComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
