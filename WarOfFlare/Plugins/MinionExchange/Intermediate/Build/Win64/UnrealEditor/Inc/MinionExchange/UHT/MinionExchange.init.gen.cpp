// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionExchange_init() {}
	MINIONEXCHANGE_API UFunction* Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_MinionExchange;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_MinionExchange()
	{
		if (!Z_Registration_Info_UPackage__Script_MinionExchange.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/MinionExchange",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x375E60F6,
				0x57B39C9F,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_MinionExchange.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_MinionExchange.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_MinionExchange(Z_Construct_UPackage__Script_MinionExchange, TEXT("/Script/MinionExchange"), Z_Registration_Info_UPackage__Script_MinionExchange, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x375E60F6, 0x57B39C9F));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
