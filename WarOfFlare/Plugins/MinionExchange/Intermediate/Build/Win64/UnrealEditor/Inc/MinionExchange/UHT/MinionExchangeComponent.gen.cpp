// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MinionExchange/Public/MinionExchange/MinionExchangeComponent.h"
#include "MinionExchange/Public/MinionExchange/MinionSpawnData.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionExchangeComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	MINIONEXCHANGE_API UClass* Z_Construct_UClass_UMinionExchangeComponent();
	MINIONEXCHANGE_API UClass* Z_Construct_UClass_UMinionExchangeComponent_NoRegister();
	MINIONEXCHANGE_API UClass* Z_Construct_UClass_UMinionOrderComponent_NoRegister();
	MINIONEXCHANGE_API UFunction* Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature();
	MINIONEXCHANGE_API UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataItem();
	UPackage* Z_Construct_UPackage__Script_MinionExchange();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics
	{
		struct _Script_MinionExchange_eventOnExchangeSignature_Parms
		{
			TArray<FMinionSpawnDataItem> MinionSpawnData;
			APlayerController* Sender;
			APlayerController* Recipient;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_MinionSpawnData_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionSpawnData_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_MinionSpawnData;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Sender;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Recipient;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData_Inner = { "MinionSpawnData", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FMinionSpawnDataItem, METADATA_PARAMS(nullptr, 0) }; // 1444665368
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData = { "MinionSpawnData", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_MinionExchange_eventOnExchangeSignature_Parms, MinionSpawnData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData_MetaData)) }; // 1444665368
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_Sender = { "Sender", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_MinionExchange_eventOnExchangeSignature_Parms, Sender), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_Recipient = { "Recipient", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_MinionExchange_eventOnExchangeSignature_Parms, Recipient), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_MinionSpawnData,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_Sender,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::NewProp_Recipient,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MinionExchange, nullptr, "OnExchangeSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::_Script_MinionExchange_eventOnExchangeSignature_Parms), Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FOnExchangeSignature_DelegateWrapper(const FMulticastScriptDelegate& OnExchangeSignature, TArray<FMinionSpawnDataItem> const& MinionSpawnData, APlayerController* Sender, APlayerController* Recipient)
{
	struct _Script_MinionExchange_eventOnExchangeSignature_Parms
	{
		TArray<FMinionSpawnDataItem> MinionSpawnData;
		APlayerController* Sender;
		APlayerController* Recipient;
	};
	_Script_MinionExchange_eventOnExchangeSignature_Parms Parms;
	Parms.MinionSpawnData=MinionSpawnData;
	Parms.Sender=Sender;
	Parms.Recipient=Recipient;
	OnExchangeSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(UMinionExchangeComponent::execExchange)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Exchange();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionExchangeComponent::execEndOrdering)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndOrdering();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionExchangeComponent::execStartOrdering)
	{
		P_GET_UBOOL(Z_Param_bResetOld);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartOrdering(Z_Param_bResetOld);
		P_NATIVE_END;
	}
	void UMinionExchangeComponent::StaticRegisterNativesUMinionExchangeComponent()
	{
		UClass* Class = UMinionExchangeComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EndOrdering", &UMinionExchangeComponent::execEndOrdering },
			{ "Exchange", &UMinionExchangeComponent::execExchange },
			{ "StartOrdering", &UMinionExchangeComponent::execStartOrdering },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionExchangeComponent, nullptr, "EndOrdering", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionExchangeComponent_Exchange_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionExchangeComponent_Exchange_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionExchangeComponent_Exchange_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionExchangeComponent, nullptr, "Exchange", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionExchangeComponent_Exchange_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionExchangeComponent_Exchange_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionExchangeComponent_Exchange()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionExchangeComponent_Exchange_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics
	{
		struct MinionExchangeComponent_eventStartOrdering_Parms
		{
			bool bResetOld;
		};
		static void NewProp_bResetOld_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bResetOld;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::NewProp_bResetOld_SetBit(void* Obj)
	{
		((MinionExchangeComponent_eventStartOrdering_Parms*)Obj)->bResetOld = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::NewProp_bResetOld = { "bResetOld", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(MinionExchangeComponent_eventStartOrdering_Parms), &Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::NewProp_bResetOld_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::NewProp_bResetOld,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::Function_MetaDataParams[] = {
		{ "CPP_Default_bResetOld", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionExchangeComponent, nullptr, "StartOrdering", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::MinionExchangeComponent_eventStartOrdering_Parms), Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMinionExchangeComponent);
	UClass* Z_Construct_UClass_UMinionExchangeComponent_NoRegister()
	{
		return UMinionExchangeComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMinionExchangeComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnExchange_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnExchange;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DefaultRootComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_DefaultRootComponent;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PlayersOrders_ValueProp;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PlayersOrders_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlayersOrders_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_PlayersOrders;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionBlueprints_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_MinionBlueprints;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMinionExchangeComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MinionExchange,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMinionExchangeComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMinionExchangeComponent_EndOrdering, "EndOrdering" }, // 234114264
		{ &Z_Construct_UFunction_UMinionExchangeComponent_Exchange, "Exchange" }, // 1101029709
		{ &Z_Construct_UFunction_UMinionExchangeComponent_StartOrdering, "StartOrdering" }, // 2483416629
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionExchangeComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "Comment", "// Start ordering in BeginPlay and end in EndPlay.\n// At EndPlay, it sends the collected data to the Spawner clusters.\n" },
		{ "IncludePath", "MinionExchange/MinionExchangeComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
		{ "ToolTip", "Start ordering in BeginPlay and end in EndPlay.\nAt EndPlay, it sends the collected data to the Spawner clusters." },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_OnExchange_MetaData[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_OnExchange = { "OnExchange", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionExchangeComponent, OnExchange), Z_Construct_UDelegateFunction_MinionExchange_OnExchangeSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_OnExchange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_OnExchange_MetaData)) }; // 3988955941
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_DefaultRootComponent_MetaData[] = {
		{ "Category", "MinionExchange" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_DefaultRootComponent = { "DefaultRootComponent", nullptr, (EPropertyFlags)0x002408000008001c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionExchangeComponent, DefaultRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_DefaultRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_DefaultRootComponent_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_ValueProp = { "PlayersOrders", nullptr, (EPropertyFlags)0x00000000000a0009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UClass_UMinionOrderComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_Key_KeyProp = { "PlayersOrders_Key", nullptr, (EPropertyFlags)0x00000000000a0009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_MetaData[] = {
		{ "Category", "MinionExchange" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders = { "PlayersOrders", nullptr, (EPropertyFlags)0x002008800002081d, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionExchangeComponent, PlayersOrders), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_MinionBlueprints_MetaData[] = {
		{ "Category", "MinionExchangeComponent" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionExchangeComponent.h" },
		{ "RowType", "MinionBlueprint" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_MinionBlueprints = { "MinionBlueprints", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionExchangeComponent, MinionBlueprints), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_MinionBlueprints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_MinionBlueprints_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMinionExchangeComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_OnExchange,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_DefaultRootComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_PlayersOrders,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionExchangeComponent_Statics::NewProp_MinionBlueprints,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMinionExchangeComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMinionExchangeComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMinionExchangeComponent_Statics::ClassParams = {
		&UMinionExchangeComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMinionExchangeComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMinionExchangeComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMinionExchangeComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionExchangeComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMinionExchangeComponent()
	{
		if (!Z_Registration_Info_UClass_UMinionExchangeComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMinionExchangeComponent.OuterSingleton, Z_Construct_UClass_UMinionExchangeComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMinionExchangeComponent.OuterSingleton;
	}
	template<> MINIONEXCHANGE_API UClass* StaticClass<UMinionExchangeComponent>()
	{
		return UMinionExchangeComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMinionExchangeComponent);
	UMinionExchangeComponent::~UMinionExchangeComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMinionExchangeComponent, UMinionExchangeComponent::StaticClass, TEXT("UMinionExchangeComponent"), &Z_Registration_Info_UClass_UMinionExchangeComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMinionExchangeComponent), 1303291577U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_2720613323(TEXT("/Script/MinionExchange"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
