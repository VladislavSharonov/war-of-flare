// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MinionExchange/MinionExchangeComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APlayerController;
struct FMinionSpawnDataItem;
#ifdef MINIONEXCHANGE_MinionExchangeComponent_generated_h
#error "MinionExchangeComponent.generated.h already included, missing '#pragma once' in MinionExchangeComponent.h"
#endif
#define MINIONEXCHANGE_MinionExchangeComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_16_DELEGATE \
MINIONEXCHANGE_API void FOnExchangeSignature_DelegateWrapper(const FMulticastScriptDelegate& OnExchangeSignature, TArray<FMinionSpawnDataItem> const& MinionSpawnData, APlayerController* Sender, APlayerController* Recipient);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExchange); \
	DECLARE_FUNCTION(execEndOrdering); \
	DECLARE_FUNCTION(execStartOrdering);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExchange); \
	DECLARE_FUNCTION(execEndOrdering); \
	DECLARE_FUNCTION(execStartOrdering);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMinionExchangeComponent(); \
	friend struct Z_Construct_UClass_UMinionExchangeComponent_Statics; \
public: \
	DECLARE_CLASS(UMinionExchangeComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MinionExchange"), NO_API) \
	DECLARE_SERIALIZER(UMinionExchangeComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUMinionExchangeComponent(); \
	friend struct Z_Construct_UClass_UMinionExchangeComponent_Statics; \
public: \
	DECLARE_CLASS(UMinionExchangeComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MinionExchange"), NO_API) \
	DECLARE_SERIALIZER(UMinionExchangeComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMinionExchangeComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMinionExchangeComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinionExchangeComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinionExchangeComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinionExchangeComponent(UMinionExchangeComponent&&); \
	NO_API UMinionExchangeComponent(const UMinionExchangeComponent&); \
public: \
	NO_API virtual ~UMinionExchangeComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinionExchangeComponent(UMinionExchangeComponent&&); \
	NO_API UMinionExchangeComponent(const UMinionExchangeComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinionExchangeComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinionExchangeComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMinionExchangeComponent) \
	NO_API virtual ~UMinionExchangeComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_20_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MINIONEXCHANGE_API UClass* StaticClass<class UMinionExchangeComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionExchangeComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
