// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MinionExchange/Public/MinionExchange/MinionBlueprint.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionBlueprint() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	MINIONEXCHANGE_API UScriptStruct* Z_Construct_UScriptStruct_FMinionBlueprint();
	MINIONS_API UClass* Z_Construct_UClass_AMinion_NoRegister();
	UPackage* Z_Construct_UPackage__Script_MinionExchange();
// End Cross Module References

static_assert(std::is_polymorphic<FMinionBlueprint>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FMinionBlueprint cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_MinionBlueprint;
class UScriptStruct* FMinionBlueprint::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_MinionBlueprint.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_MinionBlueprint.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FMinionBlueprint, (UObject*)Z_Construct_UPackage__Script_MinionExchange(), TEXT("MinionBlueprint"));
	}
	return Z_Registration_Info_UScriptStruct_MinionBlueprint.OuterSingleton;
}
template<> MINIONEXCHANGE_API UScriptStruct* StaticStruct<FMinionBlueprint>()
{
	return FMinionBlueprint::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FMinionBlueprint_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_BlueprintClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_BlueprintClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionBlueprint_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionBlueprint.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMinionBlueprint>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewProp_BlueprintClass_MetaData[] = {
		{ "Category", "MinionBlueprint" },
		{ "DisplayName", "BlueprintClass" },
		{ "MakeStructureDefaultValue", "None" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionBlueprint.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewProp_BlueprintClass = { "BlueprintClass", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FMinionBlueprint, BlueprintClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AMinion_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewProp_BlueprintClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewProp_BlueprintClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMinionBlueprint_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewProp_BlueprintClass,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMinionBlueprint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MinionExchange,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"MinionBlueprint",
		sizeof(FMinionBlueprint),
		alignof(FMinionBlueprint),
		Z_Construct_UScriptStruct_FMinionBlueprint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionBlueprint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMinionBlueprint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMinionBlueprint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMinionBlueprint()
	{
		if (!Z_Registration_Info_UScriptStruct_MinionBlueprint.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_MinionBlueprint.InnerSingleton, Z_Construct_UScriptStruct_FMinionBlueprint_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_MinionBlueprint.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h_Statics::ScriptStructInfo[] = {
		{ FMinionBlueprint::StaticStruct, Z_Construct_UScriptStruct_FMinionBlueprint_Statics::NewStructOps, TEXT("MinionBlueprint"), &Z_Registration_Info_UScriptStruct_MinionBlueprint, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FMinionBlueprint), 2412214588U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h_4102692679(TEXT("/Script/MinionExchange"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
