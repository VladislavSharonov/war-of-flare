// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MinionExchange/MinionSpawnData.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONEXCHANGE_MinionSpawnData_generated_h
#error "MinionSpawnData.generated.h already included, missing '#pragma once' in MinionSpawnData.h"
#endif
#define MINIONEXCHANGE_MinionSpawnData_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMinionSpawnDataItem_Statics; \
	MINIONEXCHANGE_API static class UScriptStruct* StaticStruct(); \
	typedef FFastArraySerializerItem Super;


template<> MINIONEXCHANGE_API UScriptStruct* StaticStruct<struct FMinionSpawnDataItem>();

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h_28_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMinionSpawnDataArray_Statics; \
	MINIONEXCHANGE_API static class UScriptStruct* StaticStruct(); \
	typedef FFastArraySerializer Super; \
	UE_NET_DECLARE_FASTARRAY(FMinionSpawnDataArray, Items, MINIONEXCHANGE_API );


template<> MINIONEXCHANGE_API UScriptStruct* StaticStruct<struct FMinionSpawnDataArray>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionSpawnData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
