// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MinionExchange/Public/MinionExchange/MinionOrderComponent.h"
#include "MinionExchange/Public/MinionExchange/MinionSpawnData.h"
#include "UObject/CoreNet.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionOrderComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	MINIONEXCHANGE_API UClass* Z_Construct_UClass_UMinionOrderComponent();
	MINIONEXCHANGE_API UClass* Z_Construct_UClass_UMinionOrderComponent_NoRegister();
	MINIONEXCHANGE_API UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataArray();
	MINIONEXCHANGE_API UScriptStruct* Z_Construct_UScriptStruct_FMinionSpawnDataItem();
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EMinionType();
	MONEY_API UClass* Z_Construct_UClass_UWalletComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_MinionExchange();
// End Cross Module References
	DEFINE_FUNCTION(UMinionOrderComponent::execTryBuyMinion)
	{
		P_GET_OBJECT(UWalletComponent,Z_Param_MoneyComponent);
		P_GET_ENUM(EMinionType,Z_Param_Minion);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryBuyMinion(Z_Param_MoneyComponent,EMinionType(Z_Param_Minion));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionOrderComponent::execClear)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Clear();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionOrderComponent::execGetSpawnData)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FMinionSpawnDataItem>*)Z_Param__Result=P_THIS->GetSpawnData();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionOrderComponent::execServer_OrderMinion)
	{
		P_GET_ENUM(EMinionType,Z_Param_Minion);
		P_FINISH;
		P_NATIVE_BEGIN;
		if (!P_THIS->Server_OrderMinion_Validate(EMinionType(Z_Param_Minion)))
		{
			RPC_ValidateFailed(TEXT("Server_OrderMinion_Validate"));
			return;
		}
		P_THIS->Server_OrderMinion_Implementation(EMinionType(Z_Param_Minion));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionOrderComponent::execOnDeactivated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDeactivated_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMinionOrderComponent::execOnActivated)
	{
		P_GET_UBOOL(Z_Param_bReset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnActivated_Implementation(Z_Param_bReset);
		P_NATIVE_END;
	}
	struct MinionOrderComponent_eventOnActivated_Parms
	{
		bool bReset;
	};
	struct MinionOrderComponent_eventServer_OrderMinion_Parms
	{
		EMinionType Minion;
	};
	static FName NAME_UMinionOrderComponent_OnActivated = FName(TEXT("OnActivated"));
	void UMinionOrderComponent::OnActivated(bool bReset)
	{
		MinionOrderComponent_eventOnActivated_Parms Parms;
		Parms.bReset=bReset ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UMinionOrderComponent_OnActivated),&Parms);
	}
	static FName NAME_UMinionOrderComponent_OnDeactivated = FName(TEXT("OnDeactivated"));
	void UMinionOrderComponent::OnDeactivated()
	{
		ProcessEvent(FindFunctionChecked(NAME_UMinionOrderComponent_OnDeactivated),NULL);
	}
	static FName NAME_UMinionOrderComponent_Server_OrderMinion = FName(TEXT("Server_OrderMinion"));
	void UMinionOrderComponent::Server_OrderMinion(EMinionType Minion)
	{
		MinionOrderComponent_eventServer_OrderMinion_Parms Parms;
		Parms.Minion=Minion;
		ProcessEvent(FindFunctionChecked(NAME_UMinionOrderComponent_Server_OrderMinion),&Parms);
	}
	void UMinionOrderComponent::StaticRegisterNativesUMinionOrderComponent()
	{
		UClass* Class = UMinionOrderComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Clear", &UMinionOrderComponent::execClear },
			{ "GetSpawnData", &UMinionOrderComponent::execGetSpawnData },
			{ "OnActivated", &UMinionOrderComponent::execOnActivated },
			{ "OnDeactivated", &UMinionOrderComponent::execOnDeactivated },
			{ "Server_OrderMinion", &UMinionOrderComponent::execServer_OrderMinion },
			{ "TryBuyMinion", &UMinionOrderComponent::execTryBuyMinion },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMinionOrderComponent_Clear_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_Clear_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionOrderComponent_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionOrderComponent, nullptr, "Clear", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_Clear_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionOrderComponent_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionOrderComponent_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics
	{
		struct MinionOrderComponent_eventGetSpawnData_Parms
		{
			TArray<FMinionSpawnDataItem> ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FMinionSpawnDataItem, METADATA_PARAMS(nullptr, 0) }; // 1444665368
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MinionOrderComponent_eventGetSpawnData_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) }; // 1444665368
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionOrderComponent, nullptr, "GetSpawnData", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::MinionOrderComponent_eventGetSpawnData_Parms), Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics
	{
		static void NewProp_bReset_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bReset;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::NewProp_bReset_SetBit(void* Obj)
	{
		((MinionOrderComponent_eventOnActivated_Parms*)Obj)->bReset = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::NewProp_bReset = { "bReset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(MinionOrderComponent_eventOnActivated_Parms), &Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::NewProp_bReset_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::NewProp_bReset,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::Function_MetaDataParams[] = {
		{ "CPP_Default_bReset", "false" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionOrderComponent, nullptr, "OnActivated", nullptr, nullptr, sizeof(MinionOrderComponent_eventOnActivated_Parms), Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionOrderComponent_OnActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionOrderComponent_OnActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionOrderComponent, nullptr, "OnDeactivated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics
	{
		static const UECodeGen_Private::FBytePropertyParams NewProp_Minion_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_Minion;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::NewProp_Minion_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::NewProp_Minion = { "Minion", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MinionOrderComponent_eventServer_OrderMinion_Parms, Minion), Z_Construct_UEnum_Minions_EMinionType, METADATA_PARAMS(nullptr, 0) }; // 3979814491
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::NewProp_Minion_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::NewProp_Minion,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionOrderComponent, nullptr, "Server_OrderMinion", nullptr, nullptr, sizeof(MinionOrderComponent_eventServer_OrderMinion_Parms), Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x84220C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics
	{
		struct MinionOrderComponent_eventTryBuyMinion_Parms
		{
			UWalletComponent* MoneyComponent;
			EMinionType Minion;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MoneyComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_MoneyComponent;
		static const UECodeGen_Private::FBytePropertyParams NewProp_Minion_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_Minion;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_MoneyComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_MoneyComponent = { "MoneyComponent", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MinionOrderComponent_eventTryBuyMinion_Parms, MoneyComponent), Z_Construct_UClass_UWalletComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_MoneyComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_MoneyComponent_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_Minion_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_Minion = { "Minion", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MinionOrderComponent_eventTryBuyMinion_Parms, Minion), Z_Construct_UEnum_Minions_EMinionType, METADATA_PARAMS(nullptr, 0) }; // 3979814491
	void Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MinionOrderComponent_eventTryBuyMinion_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(MinionOrderComponent_eventTryBuyMinion_Parms), &Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_MoneyComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_Minion_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_Minion,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMinionOrderComponent, nullptr, "TryBuyMinion", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::MinionOrderComponent_eventTryBuyMinion_Parms), Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMinionOrderComponent);
	UClass* Z_Construct_UClass_UMinionOrderComponent_NoRegister()
	{
		return UMinionOrderComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMinionOrderComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OwningPlayer_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OwningPlayer;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionPrices_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_MinionPrices;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionSpawnData_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_MinionSpawnData;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMinionOrderComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MinionExchange,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMinionOrderComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMinionOrderComponent_Clear, "Clear" }, // 3383641441
		{ &Z_Construct_UFunction_UMinionOrderComponent_GetSpawnData, "GetSpawnData" }, // 817049374
		{ &Z_Construct_UFunction_UMinionOrderComponent_OnActivated, "OnActivated" }, // 1433665520
		{ &Z_Construct_UFunction_UMinionOrderComponent_OnDeactivated, "OnDeactivated" }, // 155287774
		{ &Z_Construct_UFunction_UMinionOrderComponent_Server_OrderMinion, "Server_OrderMinion" }, // 3465065915
		{ &Z_Construct_UFunction_UMinionOrderComponent_TryBuyMinion, "TryBuyMinion" }, // 995789938
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionOrderComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "MinionExchange/MinionOrderComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_OwningPlayer_MetaData[] = {
		{ "Category", "MinionOrderComponent" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_OwningPlayer = { "OwningPlayer", nullptr, (EPropertyFlags)0x0020080000000014, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionOrderComponent, OwningPlayer), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_OwningPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_OwningPlayer_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionPrices_MetaData[] = {
		{ "Category", "MinionOrderComponent" },
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionPrices = { "MinionPrices", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionOrderComponent, MinionPrices), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionPrices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionPrices_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionSpawnData_MetaData[] = {
		{ "ModuleRelativePath", "Public/MinionExchange/MinionOrderComponent.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionSpawnData = { "MinionSpawnData", nullptr, (EPropertyFlags)0x0020080000000020, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMinionOrderComponent, MinionSpawnData), Z_Construct_UScriptStruct_FMinionSpawnDataArray, METADATA_PARAMS(Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionSpawnData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionSpawnData_MetaData)) }; // 1338440401
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMinionOrderComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_OwningPlayer,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionPrices,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMinionOrderComponent_Statics::NewProp_MinionSpawnData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMinionOrderComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMinionOrderComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMinionOrderComponent_Statics::ClassParams = {
		&UMinionOrderComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMinionOrderComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMinionOrderComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMinionOrderComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMinionOrderComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMinionOrderComponent()
	{
		if (!Z_Registration_Info_UClass_UMinionOrderComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMinionOrderComponent.OuterSingleton, Z_Construct_UClass_UMinionOrderComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMinionOrderComponent.OuterSingleton;
	}
	template<> MINIONEXCHANGE_API UClass* StaticClass<UMinionOrderComponent>()
	{
		return UMinionOrderComponent::StaticClass();
	}

	void UMinionOrderComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_MinionSpawnData(TEXT("MinionSpawnData"));

		const bool bIsValid = true
			&& Name_MinionSpawnData == ClassReps[(int32)ENetFields_Private::MinionSpawnData].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UMinionOrderComponent"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMinionOrderComponent);
	UMinionOrderComponent::~UMinionOrderComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMinionOrderComponent, UMinionOrderComponent::StaticClass, TEXT("UMinionOrderComponent"), &Z_Registration_Info_UClass_UMinionOrderComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMinionOrderComponent), 1693866235U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_2027967439(TEXT("/Script/MinionExchange"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionOrderComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
