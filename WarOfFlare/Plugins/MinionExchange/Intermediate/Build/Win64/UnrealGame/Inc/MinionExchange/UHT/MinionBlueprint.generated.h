// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "MinionExchange/MinionBlueprint.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONEXCHANGE_MinionBlueprint_generated_h
#error "MinionBlueprint.generated.h already included, missing '#pragma once' in MinionBlueprint.h"
#endif
#define MINIONEXCHANGE_MinionBlueprint_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMinionBlueprint_Statics; \
	MINIONEXCHANGE_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> MINIONEXCHANGE_API UScriptStruct* StaticStruct<struct FMinionBlueprint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_MinionExchange_Source_MinionExchange_Public_MinionExchange_MinionBlueprint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
