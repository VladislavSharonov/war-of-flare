﻿// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MinionExchange : ModuleRules
{
	public MinionExchange(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"NetCore",
				"Minions",
				"Spawn",
				"Money"
			}
			);
			
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine"
			}
			);
	}
}
