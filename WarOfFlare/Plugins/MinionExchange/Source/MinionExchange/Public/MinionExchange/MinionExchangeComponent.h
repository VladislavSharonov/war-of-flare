﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "MinionOrderComponent.h"
#include "Spawn/SpawnerCluster.h"

#include "MinionExchangeComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnExchangeSignature,
	const TArray<FMinionSpawnDataItem>&, MinionSpawnData,
	APlayerController*, Sender,
	APlayerController*, Recipient);

// Start ordering in BeginPlay and end in EndPlay.
// At EndPlay, it sends the collected data to the Spawner clusters.
UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class MINIONEXCHANGE_API UMinionExchangeComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UMinionExchangeComponent();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Activate(bool bReset) override;

	virtual void Deactivate() override;
	
protected:
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void StartOrdering(bool bResetOld = true);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void EndOrdering();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void Exchange();

	ASpawnerCluster* GetSpawnerCluster(const APlayerController* InPlayer) const;

	void ConvertSpawnData(UPARAM(ref) TArray<FMinionSpawnDataItem>& MinionSpawnData, TArray<FSpawnDataItem>& SpawnData) const;

public:
	UPROPERTY(BlueprintAssignable)
	FOnExchangeSignature OnExchange;
	
protected:
	UPROPERTY(BlueprintReadOnly, Category = "MinionExchange")
	TObjectPtr<USceneComponent> DefaultRootComponent;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "MinionExchange")
	TMap<APlayerController*, UMinionOrderComponent*> PlayersOrders;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(RowType="MinionBlueprint"))
	TObjectPtr<UDataTable> MinionBlueprints;
};
