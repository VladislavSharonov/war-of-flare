﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/NetSerialization.h"
#include "Net/Serialization/FastArraySerializer.h"

#include "Minions/MinionType.h"

#include "MinionSpawnData.generated.h"

USTRUCT(BlueprintType)
struct FMinionSpawnDataItem : public FFastArraySerializerItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EMinionType Minion = EMinionType::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Count = 0;
};

USTRUCT()
struct FMinionSpawnDataArray : public FFastArraySerializer
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<FMinionSpawnDataItem> Items;

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FMinionSpawnDataItem, FMinionSpawnDataArray>(Items, DeltaParms, *this);
	}

	void Add(EMinionType Minion);

	void RemoveLast();
	
	void Empty();
};

template<>
struct TStructOpsTypeTraits<FMinionSpawnDataArray> : public TStructOpsTypeTraitsBase2<FMinionSpawnDataArray>
{
	enum
	{
		WithNetDeltaSerializer = true,
	};
};
