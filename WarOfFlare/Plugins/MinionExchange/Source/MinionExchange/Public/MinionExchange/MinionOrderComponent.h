﻿// DemoDreams. All rights reserved.

#pragma once

#include "Components/ActorComponent.h"
#include <Engine/DataTable.h>

#include <Minions/MinionType.h>
#include "MinionSpawnData.h"
#include "Money/WalletComponent.h"

#include "MinionOrderComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class MINIONEXCHANGE_API UMinionOrderComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UMinionOrderComponent();

	virtual void BeginPlay() override;
	
	virtual void OnRep_IsActive() override;
	
	virtual void Activate(bool bReset) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnActivated(bool bReset = false);
	void OnActivated_Implementation(bool bReset) { }
	
	virtual void Deactivate() override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnDeactivated();
	void OnDeactivated_Implementation() { }
	
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, Server, Unreliable, WithValidation)
	void Server_OrderMinion(EMinionType Minion);
	virtual bool Server_OrderMinion_Validate(EMinionType Minion) { return IsActive(); }

	UFUNCTION(BlueprintPure)
	TArray<FMinionSpawnDataItem>& GetSpawnData() { return MinionSpawnData.Items; }

	UFUNCTION(BlueprintCallable)
	void Clear() { MinionSpawnData.Empty(); }
	
protected:
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool TryBuyMinion(UWalletComponent* MoneyComponent, EMinionType Minion);

protected:
	UPROPERTY(BlueprintReadOnly)
	APlayerController* OwningPlayer = nullptr;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UDataTable> MinionPrices;
	
	UPROPERTY(Replicated)
	FMinionSpawnDataArray MinionSpawnData;
};
