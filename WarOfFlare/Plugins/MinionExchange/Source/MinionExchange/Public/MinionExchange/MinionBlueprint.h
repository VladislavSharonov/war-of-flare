﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "Minions/Minion.h"

#include "MinionBlueprint.generated.h"

USTRUCT(BlueprintType)
struct FMinionBlueprint : public FTableRowBase
{
	GENERATED_BODY()

	FMinionBlueprint(TSubclassOf<AMinion> InBlueprintClass = AMinion::StaticClass()) 
		: BlueprintClass(InBlueprintClass) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "BlueprintClass", MakeStructureDefaultValue = "None"))
	TSubclassOf<AMinion> BlueprintClass = AMinion::StaticClass();
};

