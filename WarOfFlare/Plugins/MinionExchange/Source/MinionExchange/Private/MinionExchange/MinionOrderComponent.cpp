﻿// DemoDreams. All rights reserved.

#include <MinionExchange/MinionOrderComponent.h>

#include "GameFramework/PlayerState.h"
#include <Net/UnrealNetwork.h>

#include <Money/Price.h>
#include <Money/WalletComponent.h>

UMinionOrderComponent::UMinionOrderComponent()
{
	bAutoActivate = false;
	SetIsReplicatedByDefault(true);
}

void UMinionOrderComponent::BeginPlay()
{
	Super::BeginPlay();
	
	OwningPlayer = Cast<APlayerController>(GetOwner());
	if (OwningPlayer == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("MinionOrderComponent can be added only for PlayerController."));
	}
}

void UMinionOrderComponent::OnRep_IsActive()
{
	Super::OnRep_IsActive();

	if (IsActive())
		OnActivated(false);
	else
		OnDeactivated();
}

void UMinionOrderComponent::Activate(bool bReset)
{
	Super::Activate(bReset);

	if (bReset)
		Clear();
	
	if (GetOwner()->HasLocalNetOwner())
		OnActivated(bReset);
}

void UMinionOrderComponent::Deactivate()
{
	Super::Deactivate();
	
	if (GetOwner()->HasLocalNetOwner())
		OnDeactivated();
}

void UMinionOrderComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UMinionOrderComponent, MinionSpawnData);
}

void UMinionOrderComponent::Server_OrderMinion_Implementation(EMinionType Minion)
{
	APlayerState* playerState = Cast<APlayerState>(OwningPlayer->PlayerState);
	if (playerState == nullptr)
		return;

	UWalletComponent* moneyComponent = playerState->GetComponentByClass<UWalletComponent>();
	if (moneyComponent == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("OrderMinion: PlayerState has no moneyComponent."))
		return;
	}

	if (TryBuyMinion(moneyComponent, Minion))
		MinionSpawnData.Add(Minion);	
}

bool UMinionOrderComponent::TryBuyMinion(UWalletComponent* MoneyComponent, EMinionType Minion)
{
	const FName MinionName(StaticEnum<EMinionType>()->GetNameStringByValue(static_cast<int64>(Minion)));

	const FPrice* FoundRow = MinionPrices->FindRow<FPrice>(MinionName, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Store: A price for minion [\"%s\"] was not found."), *MinionName.ToString());
		return false;
	}

	return MoneyComponent->TryBuy(FoundRow->Amount);
}
