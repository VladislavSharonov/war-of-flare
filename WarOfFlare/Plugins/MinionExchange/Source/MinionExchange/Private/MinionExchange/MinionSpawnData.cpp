// DemoDreams. All rights reserved.

#include "MinionExchange/MinionSpawnData.h"

void FMinionSpawnDataArray::Add(EMinionType Minion)
{
	if (!Items.IsEmpty()
		&& Items.Last().Count > 0
		&& Items.Last().Count < TNumericLimits<uint8>::Max()
		&& Items.Last().Minion == Minion)
	{
		++Items.Last().Count;
		MarkItemDirty(Items.Last());
	}
	else
	{
		FMinionSpawnDataItem spawnData;
		spawnData.Count = 1;
		spawnData.Minion = Minion;
		MarkItemDirty(Items.Add_GetRef(spawnData));
	}
}

void FMinionSpawnDataArray::RemoveLast()
{
	if (Items.IsEmpty())
		return;

	if (Items.Last().Count == 1)
	{
		Items.RemoveAt(Items.Num() - 1);
		MarkArrayDirty();
	}
	else
	{
		FMinionSpawnDataItem& last = Items.Last();
		--last.Count;
		MarkItemDirty(last);
	}
}

void FMinionSpawnDataArray::Empty()
{
	Items.Empty();
	MarkArrayDirty();
}
