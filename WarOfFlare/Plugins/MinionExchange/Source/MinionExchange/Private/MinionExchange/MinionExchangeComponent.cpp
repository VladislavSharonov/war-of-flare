﻿// DemoDreams. All rights reserved.

#include <MinionExchange/MinionExchangeComponent.h>

#include "Kismet/GameplayStatics.h"
#include "MinionExchange/MinionBlueprint.h"
#include "Spawn/SpawnerCluster.h"

UMinionExchangeComponent::UMinionExchangeComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = false;
}

void UMinionExchangeComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UMinionExchangeComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	EndOrdering();
	Super::EndPlay(EndPlayReason);
}

void UMinionExchangeComponent::Activate(bool bReset)
{
	Super::Activate(bReset);
	StartOrdering(bReset);
}

void UMinionExchangeComponent::Deactivate()
{
	EndOrdering();
	Exchange();
	PlayersOrders.Empty(PlayersOrders.Num());
	
	Super::Deactivate();
}

void UMinionExchangeComponent::StartOrdering(bool bResetOld)
{
	for (auto PlayerControllerIterator = GetWorld()->GetPlayerControllerIterator(); PlayerControllerIterator; ++PlayerControllerIterator)
	{
		UMinionOrderComponent* MinionOrder = PlayerControllerIterator->Get()->GetComponentByClass<UMinionOrderComponent>();
		if (MinionOrder == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("PlayerController has no MinionOrderComponent"));
			continue;
		}
		MinionOrder->Activate(true);
		PlayersOrders.Add(PlayerControllerIterator->Get(), MinionOrder);
	}
}

void UMinionExchangeComponent::EndOrdering()
{
	for (auto& [Player, MinionOrder] : PlayersOrders)
	{
		if (!IsValid(MinionOrder))
			continue;
		
		MinionOrder->Deactivate();
	}
}

void UMinionExchangeComponent::Exchange()
{
	TArray<APlayerController*> Players;
	PlayersOrders.GenerateKeyArray(Players);
	for (int32 i = 0; i < PlayersOrders.Num(); ++i)
	{
		APlayerController* Recipient = Players[i];
		APlayerController* Sender = Players[(i + 1) % PlayersOrders.Num()];
		
		ASpawnerCluster* SpawnerCluster = GetSpawnerCluster(Recipient);
		
		if (SpawnerCluster != nullptr)
		{
			TArray<FMinionSpawnDataItem>& MinionSpawnData = PlayersOrders[Sender]->GetSpawnData();
			TArray<FSpawnDataItem> SpawnData;

			OnExchange.Broadcast(MinionSpawnData, Sender, Recipient);
			ConvertSpawnData(MinionSpawnData, SpawnData);
			
			SpawnerCluster->AppendSpawnQueue(SpawnData);
		}
	}
	
	for (int32 i = 0; i < PlayersOrders.Num(); ++i)
	{
		PlayersOrders[Players[i]]->Clear();
	}
}

ASpawnerCluster* UMinionExchangeComponent::GetSpawnerCluster(const APlayerController* InPlayer) const
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnerCluster::StaticClass(), Actors);
	for (const auto& Actor : Actors)
	{
		ASpawnerCluster* SpawnerCluster = Cast<ASpawnerCluster>(Actor);
		if (IsValid(SpawnerCluster) && SpawnerCluster->IsOwnedBy(InPlayer))
			return SpawnerCluster;
	}
	return nullptr;
}

void UMinionExchangeComponent::ConvertSpawnData(TArray<FMinionSpawnDataItem>& MinionSpawnData,
	TArray<FSpawnDataItem>& SpawnData) const
{
	SpawnData.Reset(MinionSpawnData.Num());
	for (FMinionSpawnDataItem& MinionSpawnDataItem : MinionSpawnData)
	{
		FSpawnDataItem SpawnDataItem;
		SpawnDataItem.Count = MinionSpawnDataItem.Count;

		FName Name(*StaticEnum<EMinionType>()->GetNameStringByValue(static_cast<int64>(MinionSpawnDataItem.Minion)));
		if (MinionBlueprints == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("The MinionBlueprints table of UMinionExchangeComponent is uninitialized."));
			return;
		}
		const FMinionBlueprint* MinionBlueprintHolder = MinionBlueprints->FindRow<FMinionBlueprint>(Name, "");

		if (MinionBlueprintHolder == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("%s: Minion with name \"%s\" was not found."), *GetName(), *Name.ToString())
			continue;
		}

		SpawnDataItem.ActorClass = MinionBlueprintHolder->BlueprintClass;
		SpawnData.Add(SpawnDataItem);
	}
}
