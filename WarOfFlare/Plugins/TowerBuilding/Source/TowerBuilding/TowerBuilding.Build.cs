﻿// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TowerBuilding : ModuleRules
{
	public TowerBuilding(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"ObjectPlacement",
				"Towers",
				"Money",
				"Grid"
			}
			);
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
			}
			);
	}
}
