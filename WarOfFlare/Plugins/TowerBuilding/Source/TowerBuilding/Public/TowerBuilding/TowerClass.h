// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "Tower/Tower.h"

#include "TowerClass.generated.h"

USTRUCT(BlueprintType)
struct FTowerClass : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ATower> Class;
};