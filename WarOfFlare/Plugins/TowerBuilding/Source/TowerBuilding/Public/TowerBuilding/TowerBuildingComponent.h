﻿// DemoDreams. All rights reserved.

#pragma once

#include "Engine/DataTable.h"

#include "ObjectPlacement/PlacementPreviewComponent.h"
#include "ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h"
#include "Tower/Tower.h"
#include "Tower/TowerType.h"

#include "TowerBuildingComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOWERBUILDING_API UTowerBuildingComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UTowerBuildingComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void OnRep_IsActive() override;
	
	virtual void Activate(bool bReset = false) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnActivated(bool bReset = false);
	
	virtual void Deactivate() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnDeactivated();
	
	UFUNCTION(BlueprintCallable)
	bool TryBuildTower();
	
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_SpawnTower(APlayerController* InTowerOwner, ETowerType InTowerType, const FTransform& InTransform);
	virtual void Server_SpawnTower_Implementation(APlayerController* InTowerOwner, ETowerType InTowerType, const FTransform& InTransform);

	UFUNCTION(BlueprintCallable)
	void TryPlace(APlayerController* InTowerOwner, ETowerType InTowerType, const FTransform& InTransform);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void StartBuilding(ETowerType InTowerType);
	virtual void StartBuilding_Implementation(ETowerType InTowerType);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EndBuilding();
	virtual void EndBuilding_Implementation();

protected:
	UFUNCTION(BlueprintCallable)
	TSubclassOf<ATower> GetTowerClass(ETowerType InTowerType) const;

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	bool TryBuyTower(APlayerController* InTowerOwner, ETowerType TowerType) const;
	
	UFUNCTION(BlueprintPure)
	bool CanBuyTower(const APlayerController* InTowerOwner, ETowerType TowerType) const;
	
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly/*, meta=(RowType="TowerClass")*/)
	TObjectPtr<UDataTable> TowerClasses = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly/*, meta=(RowType="Price")*/)
	TObjectPtr<UDataTable> TowerPrices = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<UPlacementPreviewComponent> PlacementPreviewClass = UPlacementPreviewComponent::StaticClass();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<UPositionSelectorComponent> PositionSelectorClass = UCursorPositionSelectorComponent::StaticClass();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UPlacementPreviewComponent* PlacementPreview = nullptr;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UPositionSelectorComponent* PositionSelector = nullptr;

	UPROPERTY(BlueprintReadwrite, VisibleInstanceOnly)
	ETowerType TargetTower = ETowerType::None;
};
