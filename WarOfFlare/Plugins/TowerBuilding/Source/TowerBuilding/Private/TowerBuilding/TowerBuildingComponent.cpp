﻿// DemoDreams. All rights reserved.

#include "TowerBuilding/TowerBuildingComponent.h"

#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"

#include "Grid/GridComponent.h"
#include "Tower/Tower.h"
#include "TowerBuilding/TowerClass.h"
#include "Money/WalletComponent.h"
#include "Money/Price.h"

UTowerBuildingComponent::UTowerBuildingComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = false;
	SetIsReplicatedByDefault(true);
}

void UTowerBuildingComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTowerBuildingComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void UTowerBuildingComponent::OnRep_IsActive()
{
	Super::OnRep_IsActive();
	if (IsActive())
		OnActivated(false);
	else
		OnDeactivated();
}

void UTowerBuildingComponent::Activate(bool bReset)
{
	Super::Activate(bReset);

	if (GetOwner()->HasLocalNetOwner())
		OnActivated(bReset);
}

void UTowerBuildingComponent::Deactivate()
{
	Super::Deactivate();

	if (GetOwner()->HasLocalNetOwner())
		OnDeactivated();
}

void UTowerBuildingComponent::OnActivated_Implementation(bool bReset)
{

}

void UTowerBuildingComponent::OnDeactivated_Implementation()
{
	EndBuilding();
}

bool UTowerBuildingComponent::TryBuildTower()
{
	if (!PlacementPreview->IsActive() || !PositionSelector->IsActive() || !PositionSelector->IsDetected() || TargetTower == ETowerType::None)
		return false;

	APlayerController* TowerOwner = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(PositionSelector->GetCurrentPosition());
	
	Server_SpawnTower_Implementation(TowerOwner, TargetTower, SpawnTransform);

	return true;
}

void UTowerBuildingComponent::Server_SpawnTower_Implementation(APlayerController* InTowerOwner, ETowerType InTowerType, const FTransform& InTransform)
{
	const AGameStateBase* GameState = UGameplayStatics::GetGameState(GetWorld());
	UGridComponent* Grid = GameState->GetComponentByClass<UGridComponent>();

	if (!IsValid(Grid))
		UE_LOG(LogTemp, Error, TEXT("The Grid component was not found."));;

	const FIntVector SpawnCell = Grid->GetCell(InTransform.GetLocation());

	// Check if cell already in use.
	if (Grid->IsOccupied(SpawnCell))
	{
		if (Cast<ATower>(Grid->GetObject(SpawnCell)) == nullptr)
			Grid->GetObject(SpawnCell)->Destroy();
		else
			return;
	}

	if (!TryBuyTower(InTowerOwner, InTowerType))
		return;

	// Spawn
	UWorld* World = GetWorld();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = InTowerOwner;

	FTransform SpawnTransform(InTransform);

	SpawnTransform.SetLocation(Grid->GetCenter(SpawnCell));

	ATower* BuildingBlock = World->SpawnActor<ATower>(GetTowerClass(InTowerType), SpawnTransform, SpawnParameters);

	Grid->Occupy(SpawnCell, BuildingBlock);

	BuildingBlock->AttackZone->SetZone(BuildingBlock->AttackZone->GetAttackZoneType());
}

void UTowerBuildingComponent::TryPlace(APlayerController* InTowerOwner, ETowerType InTowerType,
	const FTransform& InTransform)
{
	if (CanBuyTower(InTowerOwner, InTowerType))
		Server_SpawnTower_Implementation(InTowerOwner, InTowerType, InTransform);
}

void UTowerBuildingComponent::EndBuilding_Implementation()
{
	if (IsValid(PositionSelector))
	{
        if (PositionSelector->OnTransformChanged.IsAlreadyBound(PlacementPreview, &UPlacementPreviewComponent::UpdatePreviewTransform))
            PositionSelector->OnTransformChanged.RemoveDynamic(PlacementPreview, &UPlacementPreviewComponent::UpdatePreviewTransform);
    
        PositionSelector->DestroyComponent(true);
	}
	PositionSelector = nullptr;
	
	if (IsValid(PlacementPreview))
        PlacementPreview->DestroyComponent(true);
	
	PlacementPreview = nullptr;
}

void UTowerBuildingComponent::StartBuilding_Implementation(ETowerType InTowerType)
{
	EndBuilding(); // Cancel old Building

	if (InTowerType == ETowerType::None)
		return;
	
	AActor* Owner = GetOwner();

	TargetTower = InTowerType;
	
	PlacementPreview =
		Cast<UPlacementPreviewComponent>(Owner->AddComponentByClass(PlacementPreviewClass, false, FTransform::Identity, false));

	PositionSelector = Cast<UPositionSelectorComponent>(
		Owner->AddComponentByClass(PositionSelectorClass, false, FTransform::Identity, false));
	
	PlacementPreview->SetPreviewClass(GetTowerClass(TargetTower));
	PositionSelector->OnTransformChanged.AddUniqueDynamic(PlacementPreview, &UPlacementPreviewComponent::UpdatePreviewTransform);
	
	
	// Setup Grid
	AGameStateBase* GameState = UGameplayStatics::GetGameState(GetWorld());
	if (GameState == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTowerBuildingComponent: GameState was noty found."));
	}
	else
	{
		UGridComponent* Grid = GameState->GetComponentByClass<UGridComponent>();
		if (Grid == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTowerBuildingComponent: GameState has no grid."));
		}
		else
			PositionSelector->SetGridSnapped(Grid);
	}

	PlacementPreview->SetActive(true);
	PositionSelector->SetActive(true);
}

TSubclassOf<ATower> UTowerBuildingComponent::GetTowerClass(ETowerType InTowerType) const
{
	const FName TowerTypeName(StaticEnum<ETowerType>()->GetNameStringByValue(static_cast<int64>(InTowerType)));

	const FTowerClass* FoundRow = TowerClasses->FindRow<FTowerClass>(TowerTypeName, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("GetTowerClass: A class for tower type [\"%s\"] was not found."), *TowerTypeName.ToString());
		return ATower::StaticClass();
	}

	return FoundRow->Class.Get();
}

bool UTowerBuildingComponent::TryBuyTower(APlayerController* InTowerOwner, ETowerType TowerType) const
{
	UWalletComponent* Wallet = InTowerOwner->PlayerState->GetComponentByClass<UWalletComponent>();
	if (!IsValid(Wallet))
	{
		UE_LOG(LogTemp, Warning, TEXT("FindMoneyComponent: PlayerState has no UWalletComponent."));
		return false;
	}

	const FName Name(StaticEnum<ETowerType>()->GetNameStringByValue(static_cast<int64>(TowerType)));

	const FPrice* FoundRow = TowerPrices->FindRow<FPrice>(Name, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("TryBuyTower: A price for tower type [\"%s\"] was not found."), *Name.ToString());
		return false;
	}

	return Wallet->TryBuy(FoundRow->Amount);
}

bool UTowerBuildingComponent::CanBuyTower(const APlayerController* InTowerOwner, ETowerType TowerType) const
{
	UWalletComponent* Wallet = InTowerOwner->PlayerState->GetComponentByClass<UWalletComponent>();
	if (!IsValid(Wallet))
	{
		UE_LOG(LogTemp, Warning, TEXT("FindMoneyComponent: PlayerState has no UWalletComponent."));
		return false;
	}

	const FName Name(StaticEnum<ETowerType>()->GetNameStringByValue(static_cast<int64>(TowerType)));

	const FPrice* FoundRow = TowerPrices->FindRow<FPrice>(Name, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("TryBuyTower: A price for tower type [\"%s\"] was not found."), *Name.ToString());
		return false;
	}

	return Wallet->CanBuy(FoundRow->Amount);
}
