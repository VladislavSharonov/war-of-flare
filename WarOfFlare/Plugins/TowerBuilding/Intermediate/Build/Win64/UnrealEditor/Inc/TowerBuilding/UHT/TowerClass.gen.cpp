// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerBuilding/Public/TowerBuilding/TowerClass.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerClass() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	TOWERBUILDING_API UScriptStruct* Z_Construct_UScriptStruct_FTowerClass();
	TOWERS_API UClass* Z_Construct_UClass_ATower_NoRegister();
	UPackage* Z_Construct_UPackage__Script_TowerBuilding();
// End Cross Module References

static_assert(std::is_polymorphic<FTowerClass>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FTowerClass cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_TowerClass;
class UScriptStruct* FTowerClass::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_TowerClass.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_TowerClass.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FTowerClass, (UObject*)Z_Construct_UPackage__Script_TowerBuilding(), TEXT("TowerClass"));
	}
	return Z_Registration_Info_UScriptStruct_TowerClass.OuterSingleton;
}
template<> TOWERBUILDING_API UScriptStruct* StaticStruct<FTowerClass>()
{
	return FTowerClass::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FTowerClass_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTowerClass_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerClass.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTowerClass_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTowerClass>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTowerClass_Statics::NewProp_Class_MetaData[] = {
		{ "Category", "TowerClass" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerClass.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FTowerClass_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FTowerClass, Class), Z_Construct_UClass_UClass, Z_Construct_UClass_ATower_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTowerClass_Statics::NewProp_Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTowerClass_Statics::NewProp_Class_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTowerClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTowerClass_Statics::NewProp_Class,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTowerClass_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TowerBuilding,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"TowerClass",
		sizeof(FTowerClass),
		alignof(FTowerClass),
		Z_Construct_UScriptStruct_FTowerClass_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTowerClass_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTowerClass_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTowerClass_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTowerClass()
	{
		if (!Z_Registration_Info_UScriptStruct_TowerClass.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_TowerClass.InnerSingleton, Z_Construct_UScriptStruct_FTowerClass_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_TowerClass.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h_Statics::ScriptStructInfo[] = {
		{ FTowerClass::StaticStruct, Z_Construct_UScriptStruct_FTowerClass_Statics::NewStructOps, TEXT("TowerClass"), &Z_Registration_Info_UScriptStruct_TowerClass, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FTowerClass), 261585575U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h_2222795048(TEXT("/Script/TowerBuilding"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
