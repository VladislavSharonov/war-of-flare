// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerBuilding/Public/TowerBuilding/TowerBuildingComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerBuildingComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreviewComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPositionSelectorComponent_NoRegister();
	TOWERBUILDING_API UClass* Z_Construct_UClass_UTowerBuildingComponent();
	TOWERBUILDING_API UClass* Z_Construct_UClass_UTowerBuildingComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_ATower_NoRegister();
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_ETowerType();
	UPackage* Z_Construct_UPackage__Script_TowerBuilding();
// End Cross Module References
	DEFINE_FUNCTION(UTowerBuildingComponent::execCanBuyTower)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InTowerOwner);
		P_GET_ENUM(ETowerType,Z_Param_TowerType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanBuyTower(Z_Param_InTowerOwner,ETowerType(Z_Param_TowerType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execTryBuyTower)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InTowerOwner);
		P_GET_ENUM(ETowerType,Z_Param_TowerType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryBuyTower(Z_Param_InTowerOwner,ETowerType(Z_Param_TowerType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execGetTowerClass)
	{
		P_GET_ENUM(ETowerType,Z_Param_InTowerType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSubclassOf<ATower> *)Z_Param__Result=P_THIS->GetTowerClass(ETowerType(Z_Param_InTowerType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execEndBuilding)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndBuilding_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execStartBuilding)
	{
		P_GET_ENUM(ETowerType,Z_Param_InTowerType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartBuilding_Implementation(ETowerType(Z_Param_InTowerType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execTryPlace)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InTowerOwner);
		P_GET_ENUM(ETowerType,Z_Param_InTowerType);
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TryPlace(Z_Param_InTowerOwner,ETowerType(Z_Param_InTowerType),Z_Param_Out_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execServer_SpawnTower)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InTowerOwner);
		P_GET_ENUM(ETowerType,Z_Param_InTowerType);
		P_GET_STRUCT(FTransform,Z_Param_InTransform);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Server_SpawnTower_Implementation(Z_Param_InTowerOwner,ETowerType(Z_Param_InTowerType),Z_Param_InTransform);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execTryBuildTower)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryBuildTower();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execOnDeactivated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDeactivated_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerBuildingComponent::execOnActivated)
	{
		P_GET_UBOOL(Z_Param_bReset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnActivated_Implementation(Z_Param_bReset);
		P_NATIVE_END;
	}
	struct TowerBuildingComponent_eventOnActivated_Parms
	{
		bool bReset;
	};
	struct TowerBuildingComponent_eventServer_SpawnTower_Parms
	{
		APlayerController* InTowerOwner;
		ETowerType InTowerType;
		FTransform InTransform;
	};
	struct TowerBuildingComponent_eventStartBuilding_Parms
	{
		ETowerType InTowerType;
	};
	static FName NAME_UTowerBuildingComponent_EndBuilding = FName(TEXT("EndBuilding"));
	void UTowerBuildingComponent::EndBuilding()
	{
		ProcessEvent(FindFunctionChecked(NAME_UTowerBuildingComponent_EndBuilding),NULL);
	}
	static FName NAME_UTowerBuildingComponent_OnActivated = FName(TEXT("OnActivated"));
	void UTowerBuildingComponent::OnActivated(bool bReset)
	{
		TowerBuildingComponent_eventOnActivated_Parms Parms;
		Parms.bReset=bReset ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UTowerBuildingComponent_OnActivated),&Parms);
	}
	static FName NAME_UTowerBuildingComponent_OnDeactivated = FName(TEXT("OnDeactivated"));
	void UTowerBuildingComponent::OnDeactivated()
	{
		ProcessEvent(FindFunctionChecked(NAME_UTowerBuildingComponent_OnDeactivated),NULL);
	}
	static FName NAME_UTowerBuildingComponent_Server_SpawnTower = FName(TEXT("Server_SpawnTower"));
	void UTowerBuildingComponent::Server_SpawnTower(APlayerController* InTowerOwner, ETowerType InTowerType, FTransform const& InTransform)
	{
		TowerBuildingComponent_eventServer_SpawnTower_Parms Parms;
		Parms.InTowerOwner=InTowerOwner;
		Parms.InTowerType=InTowerType;
		Parms.InTransform=InTransform;
		ProcessEvent(FindFunctionChecked(NAME_UTowerBuildingComponent_Server_SpawnTower),&Parms);
	}
	static FName NAME_UTowerBuildingComponent_StartBuilding = FName(TEXT("StartBuilding"));
	void UTowerBuildingComponent::StartBuilding(ETowerType InTowerType)
	{
		TowerBuildingComponent_eventStartBuilding_Parms Parms;
		Parms.InTowerType=InTowerType;
		ProcessEvent(FindFunctionChecked(NAME_UTowerBuildingComponent_StartBuilding),&Parms);
	}
	void UTowerBuildingComponent::StaticRegisterNativesUTowerBuildingComponent()
	{
		UClass* Class = UTowerBuildingComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CanBuyTower", &UTowerBuildingComponent::execCanBuyTower },
			{ "EndBuilding", &UTowerBuildingComponent::execEndBuilding },
			{ "GetTowerClass", &UTowerBuildingComponent::execGetTowerClass },
			{ "OnActivated", &UTowerBuildingComponent::execOnActivated },
			{ "OnDeactivated", &UTowerBuildingComponent::execOnDeactivated },
			{ "Server_SpawnTower", &UTowerBuildingComponent::execServer_SpawnTower },
			{ "StartBuilding", &UTowerBuildingComponent::execStartBuilding },
			{ "TryBuildTower", &UTowerBuildingComponent::execTryBuildTower },
			{ "TryBuyTower", &UTowerBuildingComponent::execTryBuyTower },
			{ "TryPlace", &UTowerBuildingComponent::execTryPlace },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics
	{
		struct TowerBuildingComponent_eventCanBuyTower_Parms
		{
			const APlayerController* InTowerOwner;
			ETowerType TowerType;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InTowerOwner_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTowerOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_TowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_TowerType;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_InTowerOwner_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_InTowerOwner = { "InTowerOwner", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventCanBuyTower_Parms, InTowerOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_InTowerOwner_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_InTowerOwner_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_TowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_TowerType = { "TowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventCanBuyTower_Parms, TowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
	void Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TowerBuildingComponent_eventCanBuyTower_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(TowerBuildingComponent_eventCanBuyTower_Parms), &Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_InTowerOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_TowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_TowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "CanBuyTower", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::TowerBuildingComponent_eventCanBuyTower_Parms), Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "EndBuilding", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics
	{
		struct TowerBuildingComponent_eventGetTowerClass_Parms
		{
			ETowerType InTowerType;
			TSubclassOf<ATower>  ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InTowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InTowerType;
		static const UECodeGen_Private::FClassPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::NewProp_InTowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::NewProp_InTowerType = { "InTowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventGetTowerClass_Parms, InTowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventGetTowerClass_Parms, ReturnValue), Z_Construct_UClass_UClass, Z_Construct_UClass_ATower_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::NewProp_InTowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::NewProp_InTowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "GetTowerClass", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::TowerBuildingComponent_eventGetTowerClass_Parms), Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics
	{
		static void NewProp_bReset_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bReset;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::NewProp_bReset_SetBit(void* Obj)
	{
		((TowerBuildingComponent_eventOnActivated_Parms*)Obj)->bReset = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::NewProp_bReset = { "bReset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(TowerBuildingComponent_eventOnActivated_Parms), &Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::NewProp_bReset_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::NewProp_bReset,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::Function_MetaDataParams[] = {
		{ "CPP_Default_bReset", "false" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "OnActivated", nullptr, nullptr, sizeof(TowerBuildingComponent_eventOnActivated_Parms), Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_OnActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_OnActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "OnDeactivated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTowerOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_InTowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InTowerType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTowerOwner = { "InTowerOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventServer_SpawnTower_Parms, InTowerOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTowerType = { "InTowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventServer_SpawnTower_Parms, InTowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000082, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventServer_SpawnTower_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTransform_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTowerOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "Server_SpawnTower", nullptr, nullptr, sizeof(TowerBuildingComponent_eventServer_SpawnTower_Parms), Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04A20C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics
	{
		static const UECodeGen_Private::FBytePropertyParams NewProp_InTowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InTowerType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::NewProp_InTowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::NewProp_InTowerType = { "InTowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventStartBuilding_Parms, InTowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::NewProp_InTowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::NewProp_InTowerType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "StartBuilding", nullptr, nullptr, sizeof(TowerBuildingComponent_eventStartBuilding_Parms), Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics
	{
		struct TowerBuildingComponent_eventTryBuildTower_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TowerBuildingComponent_eventTryBuildTower_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(TowerBuildingComponent_eventTryBuildTower_Parms), &Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "TryBuildTower", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::TowerBuildingComponent_eventTryBuildTower_Parms), Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics
	{
		struct TowerBuildingComponent_eventTryBuyTower_Parms
		{
			APlayerController* InTowerOwner;
			ETowerType TowerType;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTowerOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_TowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_TowerType;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_InTowerOwner = { "InTowerOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventTryBuyTower_Parms, InTowerOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_TowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_TowerType = { "TowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventTryBuyTower_Parms, TowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
	void Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TowerBuildingComponent_eventTryBuyTower_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(TowerBuildingComponent_eventTryBuyTower_Parms), &Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_InTowerOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_TowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_TowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "TryBuyTower", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::TowerBuildingComponent_eventTryBuyTower_Parms), Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54080405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics
	{
		struct TowerBuildingComponent_eventTryPlace_Parms
		{
			APlayerController* InTowerOwner;
			ETowerType InTowerType;
			FTransform InTransform;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTowerOwner;
		static const UECodeGen_Private::FBytePropertyParams NewProp_InTowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InTowerType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InTransform_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_InTransform;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTowerOwner = { "InTowerOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventTryPlace_Parms, InTowerOwner), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTowerType = { "InTowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventTryPlace_Parms, InTowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTransform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTransform = { "InTransform", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerBuildingComponent_eventTryPlace_Parms, InTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTransform_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTowerOwner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::NewProp_InTransform,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerBuildingComponent, nullptr, "TryPlace", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::TowerBuildingComponent_eventTryPlace_Parms), Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerBuildingComponent_TryPlace()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerBuildingComponent_TryPlace_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UTowerBuildingComponent);
	UClass* Z_Construct_UClass_UTowerBuildingComponent_NoRegister()
	{
		return UTowerBuildingComponent::StaticClass();
	}
	struct Z_Construct_UClass_UTowerBuildingComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TowerClasses_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_TowerClasses;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TowerPrices_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_TowerPrices;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlacementPreviewClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_PlacementPreviewClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PositionSelectorClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_PositionSelectorClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlacementPreview_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PlacementPreview;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PositionSelector_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_PositionSelector;
		static const UECodeGen_Private::FBytePropertyParams NewProp_TargetTower_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TargetTower_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_TargetTower;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTowerBuildingComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerBuilding,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTowerBuildingComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTowerBuildingComponent_CanBuyTower, "CanBuyTower" }, // 3683027025
		{ &Z_Construct_UFunction_UTowerBuildingComponent_EndBuilding, "EndBuilding" }, // 932863260
		{ &Z_Construct_UFunction_UTowerBuildingComponent_GetTowerClass, "GetTowerClass" }, // 4130295741
		{ &Z_Construct_UFunction_UTowerBuildingComponent_OnActivated, "OnActivated" }, // 1317744159
		{ &Z_Construct_UFunction_UTowerBuildingComponent_OnDeactivated, "OnDeactivated" }, // 244776249
		{ &Z_Construct_UFunction_UTowerBuildingComponent_Server_SpawnTower, "Server_SpawnTower" }, // 3414786788
		{ &Z_Construct_UFunction_UTowerBuildingComponent_StartBuilding, "StartBuilding" }, // 50446151
		{ &Z_Construct_UFunction_UTowerBuildingComponent_TryBuildTower, "TryBuildTower" }, // 1704509870
		{ &Z_Construct_UFunction_UTowerBuildingComponent_TryBuyTower, "TryBuyTower" }, // 499097050
		{ &Z_Construct_UFunction_UTowerBuildingComponent_TryPlace, "TryPlace" }, // 3047133892
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "TowerBuilding/TowerBuildingComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerClasses_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "Comment", "/*, meta=(RowType=\"TowerClass\")*/" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
		{ "ToolTip", ", meta=(RowType=\"TowerClass\")" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerClasses = { "TowerClasses", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, TowerClasses), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerClasses_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerPrices_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "Comment", "/*, meta=(RowType=\"Price\")*/" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
		{ "ToolTip", ", meta=(RowType=\"Price\")" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerPrices = { "TowerPrices", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, TowerPrices), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerPrices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerPrices_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreviewClass_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreviewClass = { "PlacementPreviewClass", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, PlacementPreviewClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UPlacementPreviewComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreviewClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreviewClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelectorClass_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelectorClass = { "PositionSelectorClass", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, PositionSelectorClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UPositionSelectorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelectorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelectorClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreview_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreview = { "PlacementPreview", nullptr, (EPropertyFlags)0x002008000009001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, PlacementPreview), Z_Construct_UClass_UPlacementPreviewComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreview_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelector_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelector = { "PositionSelector", nullptr, (EPropertyFlags)0x002008000009001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, PositionSelector), Z_Construct_UClass_UPositionSelectorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelector_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelector_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower_MetaData[] = {
		{ "Category", "TowerBuildingComponent" },
		{ "ModuleRelativePath", "Public/TowerBuilding/TowerBuildingComponent.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower = { "TargetTower", nullptr, (EPropertyFlags)0x0020080000020805, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerBuildingComponent, TargetTower), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower_MetaData)) }; // 2535945531
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTowerBuildingComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerClasses,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TowerPrices,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreviewClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelectorClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PlacementPreview,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_PositionSelector,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerBuildingComponent_Statics::NewProp_TargetTower,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTowerBuildingComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTowerBuildingComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UTowerBuildingComponent_Statics::ClassParams = {
		&UTowerBuildingComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTowerBuildingComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTowerBuildingComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerBuildingComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTowerBuildingComponent()
	{
		if (!Z_Registration_Info_UClass_UTowerBuildingComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UTowerBuildingComponent.OuterSingleton, Z_Construct_UClass_UTowerBuildingComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UTowerBuildingComponent.OuterSingleton;
	}
	template<> TOWERBUILDING_API UClass* StaticClass<UTowerBuildingComponent>()
	{
		return UTowerBuildingComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTowerBuildingComponent);
	UTowerBuildingComponent::~UTowerBuildingComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UTowerBuildingComponent, UTowerBuildingComponent::StaticClass, TEXT("UTowerBuildingComponent"), &Z_Registration_Info_UClass_UTowerBuildingComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UTowerBuildingComponent), 3118801357U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_2961543759(TEXT("/Script/TowerBuilding"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
