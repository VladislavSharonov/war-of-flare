// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "TowerBuilding/TowerBuildingComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APlayerController;
class ATower;
enum class ETowerType : uint8;
#ifdef TOWERBUILDING_TowerBuildingComponent_generated_h
#error "TowerBuildingComponent.generated.h already included, missing '#pragma once' in TowerBuildingComponent.h"
#endif
#define TOWERBUILDING_TowerBuildingComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_RPC_WRAPPERS \
	virtual void EndBuilding_Implementation(); \
	virtual void StartBuilding_Implementation(ETowerType InTowerType); \
	virtual void Server_SpawnTower_Implementation(APlayerController* InTowerOwner, ETowerType InTowerType, FTransform const& InTransform); \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execCanBuyTower); \
	DECLARE_FUNCTION(execTryBuyTower); \
	DECLARE_FUNCTION(execGetTowerClass); \
	DECLARE_FUNCTION(execEndBuilding); \
	DECLARE_FUNCTION(execStartBuilding); \
	DECLARE_FUNCTION(execTryPlace); \
	DECLARE_FUNCTION(execServer_SpawnTower); \
	DECLARE_FUNCTION(execTryBuildTower); \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execCanBuyTower); \
	DECLARE_FUNCTION(execTryBuyTower); \
	DECLARE_FUNCTION(execGetTowerClass); \
	DECLARE_FUNCTION(execEndBuilding); \
	DECLARE_FUNCTION(execStartBuilding); \
	DECLARE_FUNCTION(execTryPlace); \
	DECLARE_FUNCTION(execServer_SpawnTower); \
	DECLARE_FUNCTION(execTryBuildTower); \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTowerBuildingComponent(); \
	friend struct Z_Construct_UClass_UTowerBuildingComponent_Statics; \
public: \
	DECLARE_CLASS(UTowerBuildingComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerBuilding"), NO_API) \
	DECLARE_SERIALIZER(UTowerBuildingComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTowerBuildingComponent(); \
	friend struct Z_Construct_UClass_UTowerBuildingComponent_Statics; \
public: \
	DECLARE_CLASS(UTowerBuildingComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerBuilding"), NO_API) \
	DECLARE_SERIALIZER(UTowerBuildingComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTowerBuildingComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerBuildingComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerBuildingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerBuildingComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerBuildingComponent(UTowerBuildingComponent&&); \
	NO_API UTowerBuildingComponent(const UTowerBuildingComponent&); \
public: \
	NO_API virtual ~UTowerBuildingComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerBuildingComponent(UTowerBuildingComponent&&); \
	NO_API UTowerBuildingComponent(const UTowerBuildingComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerBuildingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerBuildingComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerBuildingComponent) \
	NO_API virtual ~UTowerBuildingComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERBUILDING_API UClass* StaticClass<class UTowerBuildingComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerBuildingComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
