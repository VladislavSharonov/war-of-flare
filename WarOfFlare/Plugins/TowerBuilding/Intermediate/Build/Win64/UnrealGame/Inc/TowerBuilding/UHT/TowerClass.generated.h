// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "TowerBuilding/TowerClass.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERBUILDING_TowerClass_generated_h
#error "TowerClass.generated.h already included, missing '#pragma once' in TowerClass.h"
#endif
#define TOWERBUILDING_TowerClass_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTowerClass_Statics; \
	TOWERBUILDING_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> TOWERBUILDING_API UScriptStruct* StaticStruct<struct FTowerClass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_TowerBuilding_Source_TowerBuilding_Public_TowerBuilding_TowerClass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
