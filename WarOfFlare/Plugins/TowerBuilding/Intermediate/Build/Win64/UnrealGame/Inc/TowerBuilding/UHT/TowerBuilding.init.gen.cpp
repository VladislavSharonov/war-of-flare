// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerBuilding_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_TowerBuilding;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_TowerBuilding()
	{
		if (!Z_Registration_Info_UPackage__Script_TowerBuilding.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/TowerBuilding",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xE2C73762,
				0xB52E3A1B,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_TowerBuilding.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_TowerBuilding.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_TowerBuilding(Z_Construct_UPackage__Script_TowerBuilding, TEXT("/Script/TowerBuilding"), Z_Registration_Info_UPackage__Script_TowerBuilding, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xE2C73762, 0xB52E3A1B));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
