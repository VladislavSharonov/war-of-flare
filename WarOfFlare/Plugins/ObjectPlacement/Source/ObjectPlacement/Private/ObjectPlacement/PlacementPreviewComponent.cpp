﻿// DemoDreams. All rights reserved.

#include "ObjectPlacement/PlacementPreviewComponent.h"

#include "Kismet/GameplayStatics.h"
#include "ObjectPlacement/IPlacementPreview.h"

UPlacementPreviewComponent::UPlacementPreviewComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = false;
}

void UPlacementPreviewComponent::Activate(bool bReset)
{
	Super::Activate(bReset);
}

void UPlacementPreviewComponent::Deactivate()
{
	Super::Deactivate();

	if (IsValid(PreviewObject))
		PreviewObject->Destroy();
	
	PreviewObject = nullptr;
}

void UPlacementPreviewComponent::UpdatePreviewTransform(const FTransform& Transform, bool IsDetected)
{
	if (!IsActive())
		return;
	
	SetPreviewState(IsDetected ? EPlacementPreviewState::Accepted : EPlacementPreviewState::Restricted);
	
	if (IsDetected && IsValid(PreviewObject))
		PreviewObject->SetActorTransform(Transform, false, nullptr, ETeleportType::ResetPhysics);
}

void UPlacementPreviewComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	if (IsValid(PreviewObject))
	{
		UWorld* world = GetWorld();
		if (world != nullptr)
			world->DestroyActor(PreviewObject);
	}

	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

void UPlacementPreviewComponent::SetPreviewClass(TSubclassOf<AActor> InPreviewClass)
{
	PreviewObjectClass = InPreviewClass;
}

void UPlacementPreviewComponent::CreatePreviewObject()
{
	UWorld* World = GetWorld();

	FTransform Transform;
	// Remove old preview block
	if (IsValid(PreviewObject))
	{
		Transform.SetLocation(PreviewObject->GetActorLocation());
		Transform.SetRotation(PreviewObject->GetActorRotation().Quaternion());
		PreviewObject->Destroy();
		PreviewObject = nullptr;
	}

	if (!IsValid(PreviewObjectClass))
		return;
	
	// Create new preview block
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	APlayerController* PreviewOwner = UGameplayStatics::GetPlayerController(World, 0);
	SpawnParameters.Owner = PreviewOwner;
	SpawnParameters.Instigator = PreviewOwner->GetInstigator();
	
	PreviewObject = World->SpawnActor(
		PreviewObjectClass,
		&Transform,
		SpawnParameters);
	
	SetupOnSpawnPreview(PreviewObject);
}

void UPlacementPreviewComponent::SetPreviewState(EPlacementPreviewState NewPreviewState)
{
	PreviewState = NewPreviewState;
	if (PreviewState == EPlacementPreviewState::Accepted)
		OnPlacingAccepted();

	if (PreviewState == EPlacementPreviewState::Restricted)
		OnPlacingRestricted();
}

void UPlacementPreviewComponent::OnPlacingRestricted_Implementation()
{
	if (IsValid(PreviewObject))
	{
		PreviewObject->Destroy();
		PreviewObject = nullptr;
	}
}

void UPlacementPreviewComponent::OnPlacingAccepted_Implementation()
{
	if (!IsValid(PreviewObject))
		CreatePreviewObject();
}

void UPlacementPreviewComponent::SetupOnSpawnPreview_Implementation(AActor* InPreviewActor)
{
	if (InPreviewActor->GetClass()->ImplementsInterface(UPlacementPreview::StaticClass()))
		IPlacementPreview::Execute_SetPlacementPreview(InPreviewActor, true);
}
