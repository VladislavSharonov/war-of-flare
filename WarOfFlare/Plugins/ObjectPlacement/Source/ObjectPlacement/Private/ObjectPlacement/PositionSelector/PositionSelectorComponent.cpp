﻿// DemoDreams. All rights reserved.

#include "ObjectPlacement/PositionSelector/PositionSelectorComponent.h"

UPositionSelectorComponent::UPositionSelectorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	bAutoActivate = false;
}

void UPositionSelectorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	UpdateTransform();
}
