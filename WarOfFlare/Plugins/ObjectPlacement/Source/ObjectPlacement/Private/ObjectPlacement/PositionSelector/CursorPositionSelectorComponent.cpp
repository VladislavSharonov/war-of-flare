﻿// DemoDreams. All rights reserved.

#include "ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h"

#include "Kismet/KismetMathLibrary.h"
#include <Kismet/GameplayStatics.h>

bool UCursorPositionSelectorComponent::TryIntersectBuildingArea(const FVector& LineStart, const FVector& LineEnd, FHitResult& Hit)
{
	UWorld* world = GetWorld();
	FCollisionQueryParams collisionParams;
	collisionParams.bTraceComplex = true;

	return world->LineTraceSingleByChannel(Hit, LineStart, LineEnd, BuildingAreaChannel, collisionParams);
}

bool UCursorPositionSelectorComponent::TryDeprojectMousePositionToWorld(FVector& LineStart, FVector& LineEnd)
{
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!IsValid(playerController))
		return false;

	FVector worldDirection;

	bool result = playerController->DeprojectMousePositionToWorld(LineStart, worldDirection);
	if (!result) {
		double xLocation;
		double yLocation;
		bool isPressed;
		playerController->GetInputTouchState(ETouchIndex::Touch1, xLocation, yLocation, isPressed);
		result = isPressed;

		if(isPressed)
			playerController->DeprojectScreenPositionToWorld(xLocation, yLocation, LineStart, worldDirection);
	}
	
	LineEnd = LineStart + worldDirection * CheckDistance;
	return result;
}

void UCursorPositionSelectorComponent::UpdateTransform_Implementation()
{
	FVector LineStart;
	FVector LineEnd;
	if (!TryDeprojectMousePositionToWorld(LineStart, LineEnd))
	{
		Detected = false;
		OnTransformChanged.Broadcast({}, Detected);
		return;
	}

	FHitResult hit;
	if (TryIntersectBuildingArea(LineStart, LineEnd, hit))
	{
		Detected = true;
		
		if (Grid == nullptr)
			CurrentPosition = hit.Location;
		else
		{
			CurrentCell = Grid->GetCell(hit.Location);
			CurrentPosition = Grid->GetCenter(CurrentCell);
		}
		
		FTransform NewTransform;
		NewTransform.SetLocation(CurrentPosition);
		NewTransform.SetRotation(UKismetMathLibrary::MakeRotFromZ(hit.ImpactNormal).Quaternion());

		OnTransformChanged.Broadcast(NewTransform, Detected);
		return;
	}

	Detected = false;
	OnTransformChanged.Broadcast({}, Detected);
}
