﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "PlacementPreviewState.generated.h"

UENUM(BlueprintType)
enum class EPlacementPreviewState : uint8 {
	Accepted	UMETA(DisplayName = "Accepted"),
	Restricted	UMETA(DisplayName = "Restricted")
};