﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "PlacementPreviewState.h"

#include "PlacementPreviewComponent.generated.h"

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OBJECTPLACEMENT_API UPlacementPreviewComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UPlacementPreviewComponent();
	
	virtual void Activate(bool bReset = false) override;

	virtual void Deactivate() override;

	UFUNCTION(BlueprintCallable, Category = "PlacementPreview")
	void UpdatePreviewTransform(const FTransform& Transform, bool IsDetected);

	UFUNCTION(BlueprintCallable, Category = "PlacementPreview")
	void OnComponentDestroyed(bool bDestroyingHierarchy) override;

	UFUNCTION(BlueprintCallable, Category = "PlacementPreview")
	void SetPreviewClass(TSubclassOf<AActor> InPreviewClass);
	
	UFUNCTION(BlueprintCallable, Category = "PlacementPreview")
	void SetPreviewState(EPlacementPreviewState NewPreviewState = EPlacementPreviewState::Accepted);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PlacementPreview")
	void SetupOnSpawnPreview(AActor* InPreviewActor);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PlacementPreview")
	void OnPlacingAccepted();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PlacementPreview")
	void OnPlacingRestricted();

protected:
	UFUNCTION(BlueprintCallable, Category = "PlacementPreview")
	void CreatePreviewObject();

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PlacementPreview")
	TSubclassOf<AActor> PreviewObjectClass;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = "PlacementPreview")
	TObjectPtr<AActor> PreviewObject;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PlacementPreview")
	EPlacementPreviewState PreviewState = EPlacementPreviewState::Accepted;
};
