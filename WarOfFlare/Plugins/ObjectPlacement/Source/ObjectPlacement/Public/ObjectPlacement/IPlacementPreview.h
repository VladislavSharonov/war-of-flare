﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "IPlacementPreview.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UPlacementPreview : public UInterface
{
	GENERATED_BODY()
};

class OBJECTPLACEMENT_API IPlacementPreview
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Preview")
	void SetPlacementPreview(const bool InIsPreview);
};
