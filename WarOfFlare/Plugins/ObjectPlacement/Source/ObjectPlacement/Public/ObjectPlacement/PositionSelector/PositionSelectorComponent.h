﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include <Grid/GridComponent.h>

#include "PositionSelectorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPositionChanged, const FTransform&, Transform, bool, IsDetected);

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OBJECTPLACEMENT_API UPositionSelectorComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UPositionSelectorComponent();
	
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PositionSelector")
	void UpdateTransform();
	virtual void UpdateTransform_Implementation() { };

	UFUNCTION(BlueprintCallable, Category = "PositionSelector")
	bool IsGridSnapped() const { return IsValid(Grid); };

	UFUNCTION(BlueprintCallable, Category = "PositionSelector")
	void SetGridSnapped(UGridComponent* InGrid = nullptr) { Grid = InGrid; };

	UFUNCTION(BlueprintPure, Category = "PositionSelector")
	FVector GetCurrentPosition() const { return CurrentPosition; }

	UFUNCTION(BlueprintPure, Category = "PositionSelector")
	FIntVector GetCurrentCell() const { return CurrentCell; }

	UFUNCTION(BlueprintPure, Category = "PositionSelector")
	bool IsDetected() const { return Detected; }

	UPROPERTY(BlueprintAssignable, VisibleInstanceOnly, Category = "PositionSelector")
	FPositionChanged OnTransformChanged;
	
protected:
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "PositionSelector")
	FVector CurrentPosition = FVector::ZeroVector;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "PositionSelector")
	FIntVector CurrentCell = FIntVector::ZeroValue;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "PositionSelector")
	bool Detected = false;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "PositionSelector")
	TObjectPtr<UGridComponent> Grid;
};
