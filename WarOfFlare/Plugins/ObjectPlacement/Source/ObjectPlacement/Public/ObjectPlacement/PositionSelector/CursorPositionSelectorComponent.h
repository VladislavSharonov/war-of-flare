﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "PositionSelectorComponent.h"

#include "CursorPositionSelectorComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OBJECTPLACEMENT_API UCursorPositionSelectorComponent : public UPositionSelectorComponent
{
	GENERATED_BODY()

public:
	/** Check intersection with building base(floor). */
	UFUNCTION(BlueprintCallable, Category = "PositionSelector|Mouse")
	bool TryIntersectBuildingArea(const FVector& LineStart, const FVector& LineEnd, FHitResult& Hit);

	/** Get mouse vector to world. */
	UFUNCTION(BlueprintCallable, Category = "PositionSelector|Mouse")
	bool TryDeprojectMousePositionToWorld(FVector& LineStart, FVector& LineEnd);

	/** Detect if Cell is hovered. */
	virtual void UpdateTransform_Implementation() override;

public:
	/** Maximum distance that can be detected by this cell selector. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PositionSelector|Mouse")
	double CheckDistance = 100000000.0;

	/** The channel of building block. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PositionSelector|Mouse")
	TEnumAsByte<ECollisionChannel> BuildingAreaChannel = ECollisionChannel::ECC_Visibility;
};
