// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectPlacement/Public/ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h"
#include "../../Source/Runtime/Engine/Classes/Engine/HitResult.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCursorPositionSelectorComponent() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UCursorPositionSelectorComponent();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UCursorPositionSelectorComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPositionSelectorComponent();
	UPackage* Z_Construct_UPackage__Script_ObjectPlacement();
// End Cross Module References
	DEFINE_FUNCTION(UCursorPositionSelectorComponent::execTryDeprojectMousePositionToWorld)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LineStart);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LineEnd);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryDeprojectMousePositionToWorld(Z_Param_Out_LineStart,Z_Param_Out_LineEnd);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorPositionSelectorComponent::execTryIntersectBuildingArea)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LineStart);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LineEnd);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->TryIntersectBuildingArea(Z_Param_Out_LineStart,Z_Param_Out_LineEnd,Z_Param_Out_Hit);
		P_NATIVE_END;
	}
	void UCursorPositionSelectorComponent::StaticRegisterNativesUCursorPositionSelectorComponent()
	{
		UClass* Class = UCursorPositionSelectorComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "TryDeprojectMousePositionToWorld", &UCursorPositionSelectorComponent::execTryDeprojectMousePositionToWorld },
			{ "TryIntersectBuildingArea", &UCursorPositionSelectorComponent::execTryIntersectBuildingArea },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics
	{
		struct CursorPositionSelectorComponent_eventTryDeprojectMousePositionToWorld_Parms
		{
			FVector LineStart;
			FVector LineEnd;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_LineStart;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LineEnd;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_LineStart = { "LineStart", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorPositionSelectorComponent_eventTryDeprojectMousePositionToWorld_Parms, LineStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_LineEnd = { "LineEnd", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorPositionSelectorComponent_eventTryDeprojectMousePositionToWorld_Parms, LineEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CursorPositionSelectorComponent_eventTryDeprojectMousePositionToWorld_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CursorPositionSelectorComponent_eventTryDeprojectMousePositionToWorld_Parms), &Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_LineStart,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_LineEnd,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector|Mouse" },
		{ "Comment", "/** Get mouse vector to world. */" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h" },
		{ "ToolTip", "Get mouse vector to world." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorPositionSelectorComponent, nullptr, "TryDeprojectMousePositionToWorld", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::CursorPositionSelectorComponent_eventTryDeprojectMousePositionToWorld_Parms), Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics
	{
		struct CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms
		{
			FVector LineStart;
			FVector LineEnd;
			FHitResult Hit;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_LineStart_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_LineStart;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_LineEnd_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_LineEnd;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Hit;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineStart_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineStart = { "LineStart", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms, LineStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineStart_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineEnd_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineEnd = { "LineEnd", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms, LineEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineEnd_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_Hit = { "Hit", nullptr, (EPropertyFlags)0x0010008000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms, Hit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) }; // 1287526515
	void Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms), &Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineStart,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_LineEnd,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_Hit,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector|Mouse" },
		{ "Comment", "/** Check intersection with building base(floor). */" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h" },
		{ "ToolTip", "Check intersection with building base(floor)." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorPositionSelectorComponent, nullptr, "TryIntersectBuildingArea", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::CursorPositionSelectorComponent_eventTryIntersectBuildingArea_Parms), Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCursorPositionSelectorComponent);
	UClass* Z_Construct_UClass_UCursorPositionSelectorComponent_NoRegister()
	{
		return UCursorPositionSelectorComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCursorPositionSelectorComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CheckDistance_MetaData[];
#endif
		static const UECodeGen_Private::FDoublePropertyParams NewProp_CheckDistance;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_BuildingAreaChannel_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_BuildingAreaChannel;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPositionSelectorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectPlacement,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCursorPositionSelectorComponent_TryDeprojectMousePositionToWorld, "TryDeprojectMousePositionToWorld" }, // 4252323472
		{ &Z_Construct_UFunction_UCursorPositionSelectorComponent_TryIntersectBuildingArea, "TryIntersectBuildingArea" }, // 621610227
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_CheckDistance_MetaData[] = {
		{ "Category", "PositionSelector|Mouse" },
		{ "Comment", "/** Maximum distance that can be detected by this cell selector. */" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h" },
		{ "ToolTip", "Maximum distance that can be detected by this cell selector." },
	};
#endif
	const UECodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_CheckDistance = { "CheckDistance", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorPositionSelectorComponent, CheckDistance), METADATA_PARAMS(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_CheckDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_CheckDistance_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_BuildingAreaChannel_MetaData[] = {
		{ "Category", "PositionSelector|Mouse" },
		{ "Comment", "/** The channel of building block. */" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h" },
		{ "ToolTip", "The channel of building block." },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_BuildingAreaChannel = { "BuildingAreaChannel", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorPositionSelectorComponent, BuildingAreaChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_BuildingAreaChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_BuildingAreaChannel_MetaData)) }; // 727872708
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_CheckDistance,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::NewProp_BuildingAreaChannel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCursorPositionSelectorComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::ClassParams = {
		&UCursorPositionSelectorComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCursorPositionSelectorComponent()
	{
		if (!Z_Registration_Info_UClass_UCursorPositionSelectorComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCursorPositionSelectorComponent.OuterSingleton, Z_Construct_UClass_UCursorPositionSelectorComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCursorPositionSelectorComponent.OuterSingleton;
	}
	template<> OBJECTPLACEMENT_API UClass* StaticClass<UCursorPositionSelectorComponent>()
	{
		return UCursorPositionSelectorComponent::StaticClass();
	}
	UCursorPositionSelectorComponent::UCursorPositionSelectorComponent() {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCursorPositionSelectorComponent);
	UCursorPositionSelectorComponent::~UCursorPositionSelectorComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCursorPositionSelectorComponent, UCursorPositionSelectorComponent::StaticClass, TEXT("UCursorPositionSelectorComponent"), &Z_Registration_Info_UClass_UCursorPositionSelectorComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCursorPositionSelectorComponent), 3036450377U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_245654002(TEXT("/Script/ObjectPlacement"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
