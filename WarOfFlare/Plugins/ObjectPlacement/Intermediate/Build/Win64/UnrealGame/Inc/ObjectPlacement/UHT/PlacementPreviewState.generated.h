// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectPlacement/PlacementPreviewState.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OBJECTPLACEMENT_PlacementPreviewState_generated_h
#error "PlacementPreviewState.generated.h already included, missing '#pragma once' in PlacementPreviewState.h"
#endif
#define OBJECTPLACEMENT_PlacementPreviewState_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewState_h


#define FOREACH_ENUM_EPLACEMENTPREVIEWSTATE(op) \
	op(EPlacementPreviewState::Accepted) \
	op(EPlacementPreviewState::Restricted) 

enum class EPlacementPreviewState : uint8;
template<> struct TIsUEnumClass<EPlacementPreviewState> { enum { Value = true }; };
template<> OBJECTPLACEMENT_API UEnum* StaticEnum<EPlacementPreviewState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
