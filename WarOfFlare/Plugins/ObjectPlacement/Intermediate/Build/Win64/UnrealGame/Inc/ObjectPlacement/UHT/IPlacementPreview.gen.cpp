// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectPlacement/Public/ObjectPlacement/IPlacementPreview.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIPlacementPreview() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreview();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreview_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ObjectPlacement();
// End Cross Module References
	DEFINE_FUNCTION(IPlacementPreview::execSetPlacementPreview)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPlacementPreview_Implementation(Z_Param_InIsPreview);
		P_NATIVE_END;
	}
	struct PlacementPreview_eventSetPlacementPreview_Parms
	{
		bool InIsPreview;
	};
	void IPlacementPreview::SetPlacementPreview(bool InIsPreview)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_SetPlacementPreview instead.");
	}
	void UPlacementPreview::StaticRegisterNativesUPlacementPreview()
	{
		UClass* Class = UPlacementPreview::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetPlacementPreview", &IPlacementPreview::execSetPlacementPreview },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InIsPreview_MetaData[];
#endif
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((PlacementPreview_eventSetPlacementPreview_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(PlacementPreview_eventSetPlacementPreview_Parms), &Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::NewProp_InIsPreview,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/IPlacementPreview.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreview, nullptr, "SetPlacementPreview", nullptr, nullptr, sizeof(PlacementPreview_eventSetPlacementPreview_Parms), Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UPlacementPreview);
	UClass* Z_Construct_UClass_UPlacementPreview_NoRegister()
	{
		return UPlacementPreview::StaticClass();
	}
	struct Z_Construct_UClass_UPlacementPreview_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlacementPreview_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectPlacement,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPlacementPreview_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPlacementPreview_SetPlacementPreview, "SetPlacementPreview" }, // 2666175128
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlacementPreview_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/IPlacementPreview.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlacementPreview_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IPlacementPreview>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UPlacementPreview_Statics::ClassParams = {
		&UPlacementPreview::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UPlacementPreview_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlacementPreview_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlacementPreview()
	{
		if (!Z_Registration_Info_UClass_UPlacementPreview.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UPlacementPreview.OuterSingleton, Z_Construct_UClass_UPlacementPreview_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UPlacementPreview.OuterSingleton;
	}
	template<> OBJECTPLACEMENT_API UClass* StaticClass<UPlacementPreview>()
	{
		return UPlacementPreview::StaticClass();
	}
	UPlacementPreview::UPlacementPreview(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlacementPreview);
	UPlacementPreview::~UPlacementPreview() {}
	static FName NAME_UPlacementPreview_SetPlacementPreview = FName(TEXT("SetPlacementPreview"));
	void IPlacementPreview::Execute_SetPlacementPreview(UObject* O, bool InIsPreview)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UPlacementPreview::StaticClass()));
		PlacementPreview_eventSetPlacementPreview_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UPlacementPreview_SetPlacementPreview);
		if (Func)
		{
			Parms.InIsPreview=InIsPreview;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IPlacementPreview*)(O->GetNativeInterfaceAddress(UPlacementPreview::StaticClass())))
		{
			I->SetPlacementPreview_Implementation(InIsPreview);
		}
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UPlacementPreview, UPlacementPreview::StaticClass, TEXT("UPlacementPreview"), &Z_Registration_Info_UClass_UPlacementPreview, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UPlacementPreview), 174827905U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_1438751768(TEXT("/Script/ObjectPlacement"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
