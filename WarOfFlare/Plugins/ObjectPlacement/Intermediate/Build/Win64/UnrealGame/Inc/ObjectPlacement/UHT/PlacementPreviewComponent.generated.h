// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectPlacement/PlacementPreviewComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
enum class EPlacementPreviewState : uint8;
#ifdef OBJECTPLACEMENT_PlacementPreviewComponent_generated_h
#error "PlacementPreviewComponent.generated.h already included, missing '#pragma once' in PlacementPreviewComponent.h"
#endif
#define OBJECTPLACEMENT_PlacementPreviewComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_RPC_WRAPPERS \
	virtual void OnPlacingRestricted_Implementation(); \
	virtual void OnPlacingAccepted_Implementation(); \
	virtual void SetupOnSpawnPreview_Implementation(AActor* InPreviewActor); \
 \
	DECLARE_FUNCTION(execCreatePreviewObject); \
	DECLARE_FUNCTION(execOnPlacingRestricted); \
	DECLARE_FUNCTION(execOnPlacingAccepted); \
	DECLARE_FUNCTION(execSetupOnSpawnPreview); \
	DECLARE_FUNCTION(execSetPreviewState); \
	DECLARE_FUNCTION(execSetPreviewClass); \
	DECLARE_FUNCTION(execOnComponentDestroyed); \
	DECLARE_FUNCTION(execUpdatePreviewTransform);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnPlacingRestricted_Implementation(); \
	virtual void OnPlacingAccepted_Implementation(); \
	virtual void SetupOnSpawnPreview_Implementation(AActor* InPreviewActor); \
 \
	DECLARE_FUNCTION(execCreatePreviewObject); \
	DECLARE_FUNCTION(execOnPlacingRestricted); \
	DECLARE_FUNCTION(execOnPlacingAccepted); \
	DECLARE_FUNCTION(execSetupOnSpawnPreview); \
	DECLARE_FUNCTION(execSetPreviewState); \
	DECLARE_FUNCTION(execSetPreviewClass); \
	DECLARE_FUNCTION(execOnComponentDestroyed); \
	DECLARE_FUNCTION(execUpdatePreviewTransform);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlacementPreviewComponent(); \
	friend struct Z_Construct_UClass_UPlacementPreviewComponent_Statics; \
public: \
	DECLARE_CLASS(UPlacementPreviewComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), NO_API) \
	DECLARE_SERIALIZER(UPlacementPreviewComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPlacementPreviewComponent(); \
	friend struct Z_Construct_UClass_UPlacementPreviewComponent_Statics; \
public: \
	DECLARE_CLASS(UPlacementPreviewComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), NO_API) \
	DECLARE_SERIALIZER(UPlacementPreviewComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlacementPreviewComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlacementPreviewComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlacementPreviewComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlacementPreviewComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlacementPreviewComponent(UPlacementPreviewComponent&&); \
	NO_API UPlacementPreviewComponent(const UPlacementPreviewComponent&); \
public: \
	NO_API virtual ~UPlacementPreviewComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlacementPreviewComponent(UPlacementPreviewComponent&&); \
	NO_API UPlacementPreviewComponent(const UPlacementPreviewComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlacementPreviewComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlacementPreviewComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlacementPreviewComponent) \
	NO_API virtual ~UPlacementPreviewComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_12_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTPLACEMENT_API UClass* StaticClass<class UPlacementPreviewComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
