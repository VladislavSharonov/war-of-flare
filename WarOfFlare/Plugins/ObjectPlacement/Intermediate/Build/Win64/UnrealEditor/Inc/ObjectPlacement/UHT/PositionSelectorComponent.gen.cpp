// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectPlacement/Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePositionSelectorComponent() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	GRID_API UClass* Z_Construct_UClass_UGridComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPositionSelectorComponent();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPositionSelectorComponent_NoRegister();
	OBJECTPLACEMENT_API UFunction* Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_ObjectPlacement();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics
	{
		struct _Script_ObjectPlacement_eventPositionChanged_Parms
		{
			FTransform Transform;
			bool IsDetected;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Transform;
		static void NewProp_IsDetected_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsDetected;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_Transform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_ObjectPlacement_eventPositionChanged_Parms, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_Transform_MetaData)) };
	void Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_IsDetected_SetBit(void* Obj)
	{
		((_Script_ObjectPlacement_eventPositionChanged_Parms*)Obj)->IsDetected = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_IsDetected = { "IsDetected", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(_Script_ObjectPlacement_eventPositionChanged_Parms), &Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_IsDetected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_Transform,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::NewProp_IsDetected,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ObjectPlacement, nullptr, "PositionChanged__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::_Script_ObjectPlacement_eventPositionChanged_Parms), Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FPositionChanged_DelegateWrapper(const FMulticastScriptDelegate& PositionChanged, FTransform const& Transform, bool IsDetected)
{
	struct _Script_ObjectPlacement_eventPositionChanged_Parms
	{
		FTransform Transform;
		bool IsDetected;
	};
	_Script_ObjectPlacement_eventPositionChanged_Parms Parms;
	Parms.Transform=Transform;
	Parms.IsDetected=IsDetected ? true : false;
	PositionChanged.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(UPositionSelectorComponent::execIsDetected)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsDetected();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPositionSelectorComponent::execGetCurrentCell)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FIntVector*)Z_Param__Result=P_THIS->GetCurrentCell();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPositionSelectorComponent::execGetCurrentPosition)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetCurrentPosition();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPositionSelectorComponent::execSetGridSnapped)
	{
		P_GET_OBJECT(UGridComponent,Z_Param_InGrid);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetGridSnapped(Z_Param_InGrid);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPositionSelectorComponent::execIsGridSnapped)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsGridSnapped();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPositionSelectorComponent::execUpdateTransform)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateTransform_Implementation();
		P_NATIVE_END;
	}
	static FName NAME_UPositionSelectorComponent_UpdateTransform = FName(TEXT("UpdateTransform"));
	void UPositionSelectorComponent::UpdateTransform()
	{
		ProcessEvent(FindFunctionChecked(NAME_UPositionSelectorComponent_UpdateTransform),NULL);
	}
	void UPositionSelectorComponent::StaticRegisterNativesUPositionSelectorComponent()
	{
		UClass* Class = UPositionSelectorComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentCell", &UPositionSelectorComponent::execGetCurrentCell },
			{ "GetCurrentPosition", &UPositionSelectorComponent::execGetCurrentPosition },
			{ "IsDetected", &UPositionSelectorComponent::execIsDetected },
			{ "IsGridSnapped", &UPositionSelectorComponent::execIsGridSnapped },
			{ "SetGridSnapped", &UPositionSelectorComponent::execSetGridSnapped },
			{ "UpdateTransform", &UPositionSelectorComponent::execUpdateTransform },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics
	{
		struct PositionSelectorComponent_eventGetCurrentCell_Parms
		{
			FIntVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PositionSelectorComponent_eventGetCurrentCell_Parms, ReturnValue), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPositionSelectorComponent, nullptr, "GetCurrentCell", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::PositionSelectorComponent_eventGetCurrentCell_Parms), Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics
	{
		struct PositionSelectorComponent_eventGetCurrentPosition_Parms
		{
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PositionSelectorComponent_eventGetCurrentPosition_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPositionSelectorComponent, nullptr, "GetCurrentPosition", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::PositionSelectorComponent_eventGetCurrentPosition_Parms), Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics
	{
		struct PositionSelectorComponent_eventIsDetected_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PositionSelectorComponent_eventIsDetected_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(PositionSelectorComponent_eventIsDetected_Parms), &Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPositionSelectorComponent, nullptr, "IsDetected", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::PositionSelectorComponent_eventIsDetected_Parms), Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPositionSelectorComponent_IsDetected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPositionSelectorComponent_IsDetected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics
	{
		struct PositionSelectorComponent_eventIsGridSnapped_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PositionSelectorComponent_eventIsGridSnapped_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(PositionSelectorComponent_eventIsGridSnapped_Parms), &Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPositionSelectorComponent, nullptr, "IsGridSnapped", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::PositionSelectorComponent_eventIsGridSnapped_Parms), Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics
	{
		struct PositionSelectorComponent_eventSetGridSnapped_Parms
		{
			UGridComponent* InGrid;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InGrid_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InGrid;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::NewProp_InGrid_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::NewProp_InGrid = { "InGrid", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PositionSelectorComponent_eventSetGridSnapped_Parms, InGrid), Z_Construct_UClass_UGridComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::NewProp_InGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::NewProp_InGrid_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::NewProp_InGrid,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector" },
		{ "CPP_Default_InGrid", "None" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPositionSelectorComponent, nullptr, "SetGridSnapped", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::PositionSelectorComponent_eventSetGridSnapped_Parms), Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPositionSelectorComponent, nullptr, "UpdateTransform", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UPositionSelectorComponent);
	UClass* Z_Construct_UClass_UPositionSelectorComponent_NoRegister()
	{
		return UPositionSelectorComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPositionSelectorComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnTransformChanged_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTransformChanged;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentPosition_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_CurrentPosition;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentCell_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_CurrentCell;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Detected_MetaData[];
#endif
		static void NewProp_Detected_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Detected;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Grid_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_Grid;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPositionSelectorComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectPlacement,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPositionSelectorComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentCell, "GetCurrentCell" }, // 367736434
		{ &Z_Construct_UFunction_UPositionSelectorComponent_GetCurrentPosition, "GetCurrentPosition" }, // 2253719746
		{ &Z_Construct_UFunction_UPositionSelectorComponent_IsDetected, "IsDetected" }, // 251695898
		{ &Z_Construct_UFunction_UPositionSelectorComponent_IsGridSnapped, "IsGridSnapped" }, // 2043717200
		{ &Z_Construct_UFunction_UPositionSelectorComponent_SetGridSnapped, "SetGridSnapped" }, // 2507815769
		{ &Z_Construct_UFunction_UPositionSelectorComponent_UpdateTransform, "UpdateTransform" }, // 1834003959
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionSelectorComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_OnTransformChanged_MetaData[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_OnTransformChanged = { "OnTransformChanged", nullptr, (EPropertyFlags)0x00100000100a0801, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPositionSelectorComponent, OnTransformChanged), Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_OnTransformChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_OnTransformChanged_MetaData)) }; // 2274357192
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentPosition_MetaData[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentPosition = { "CurrentPosition", nullptr, (EPropertyFlags)0x0020080000020815, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPositionSelectorComponent, CurrentPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentPosition_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentCell_MetaData[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentCell = { "CurrentCell", nullptr, (EPropertyFlags)0x0020080000020815, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPositionSelectorComponent, CurrentCell), Z_Construct_UScriptStruct_FIntVector, METADATA_PARAMS(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentCell_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentCell_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected_MetaData[] = {
		{ "Category", "PositionSelector" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	void Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected_SetBit(void* Obj)
	{
		((UPositionSelectorComponent*)Obj)->Detected = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected = { "Detected", nullptr, (EPropertyFlags)0x0020080000020815, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UPositionSelectorComponent), &Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Grid_MetaData[] = {
		{ "Category", "PositionSelector" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PositionSelector/PositionSelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Grid = { "Grid", nullptr, (EPropertyFlags)0x00240800000a081d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPositionSelectorComponent, Grid), Z_Construct_UClass_UGridComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Grid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Grid_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPositionSelectorComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_OnTransformChanged,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentPosition,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_CurrentCell,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Detected,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPositionSelectorComponent_Statics::NewProp_Grid,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPositionSelectorComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPositionSelectorComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UPositionSelectorComponent_Statics::ClassParams = {
		&UPositionSelectorComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPositionSelectorComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPositionSelectorComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPositionSelectorComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPositionSelectorComponent()
	{
		if (!Z_Registration_Info_UClass_UPositionSelectorComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UPositionSelectorComponent.OuterSingleton, Z_Construct_UClass_UPositionSelectorComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UPositionSelectorComponent.OuterSingleton;
	}
	template<> OBJECTPLACEMENT_API UClass* StaticClass<UPositionSelectorComponent>()
	{
		return UPositionSelectorComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPositionSelectorComponent);
	UPositionSelectorComponent::~UPositionSelectorComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UPositionSelectorComponent, UPositionSelectorComponent::StaticClass, TEXT("UPositionSelectorComponent"), &Z_Registration_Info_UClass_UPositionSelectorComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UPositionSelectorComponent), 3140498271U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_2640539131(TEXT("/Script/ObjectPlacement"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
