// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectPlacement/PositionSelector/CursorPositionSelectorComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
#ifdef OBJECTPLACEMENT_CursorPositionSelectorComponent_generated_h
#error "CursorPositionSelectorComponent.generated.h already included, missing '#pragma once' in CursorPositionSelectorComponent.h"
#endif
#define OBJECTPLACEMENT_CursorPositionSelectorComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTryDeprojectMousePositionToWorld); \
	DECLARE_FUNCTION(execTryIntersectBuildingArea);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTryDeprojectMousePositionToWorld); \
	DECLARE_FUNCTION(execTryIntersectBuildingArea);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCursorPositionSelectorComponent(); \
	friend struct Z_Construct_UClass_UCursorPositionSelectorComponent_Statics; \
public: \
	DECLARE_CLASS(UCursorPositionSelectorComponent, UPositionSelectorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), NO_API) \
	DECLARE_SERIALIZER(UCursorPositionSelectorComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUCursorPositionSelectorComponent(); \
	friend struct Z_Construct_UClass_UCursorPositionSelectorComponent_Statics; \
public: \
	DECLARE_CLASS(UCursorPositionSelectorComponent, UPositionSelectorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), NO_API) \
	DECLARE_SERIALIZER(UCursorPositionSelectorComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCursorPositionSelectorComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCursorPositionSelectorComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCursorPositionSelectorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCursorPositionSelectorComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCursorPositionSelectorComponent(UCursorPositionSelectorComponent&&); \
	NO_API UCursorPositionSelectorComponent(const UCursorPositionSelectorComponent&); \
public: \
	NO_API virtual ~UCursorPositionSelectorComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCursorPositionSelectorComponent(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCursorPositionSelectorComponent(UCursorPositionSelectorComponent&&); \
	NO_API UCursorPositionSelectorComponent(const UCursorPositionSelectorComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCursorPositionSelectorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCursorPositionSelectorComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCursorPositionSelectorComponent) \
	NO_API virtual ~UCursorPositionSelectorComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_11_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTPLACEMENT_API UClass* StaticClass<class UCursorPositionSelectorComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_CursorPositionSelectorComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
