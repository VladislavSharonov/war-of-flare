// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeObjectPlacement_init() {}
	OBJECTPLACEMENT_API UFunction* Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_ObjectPlacement;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_ObjectPlacement()
	{
		if (!Z_Registration_Info_UPackage__Script_ObjectPlacement.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_ObjectPlacement_PositionChanged__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/ObjectPlacement",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x4C5CBE0E,
				0x5DBE75C7,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_ObjectPlacement.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_ObjectPlacement.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_ObjectPlacement(Z_Construct_UPackage__Script_ObjectPlacement, TEXT("/Script/ObjectPlacement"), Z_Registration_Info_UPackage__Script_ObjectPlacement, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x4C5CBE0E, 0x5DBE75C7));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
