// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectPlacement/PositionSelector/PositionSelectorComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UGridComponent;
#ifdef OBJECTPLACEMENT_PositionSelectorComponent_generated_h
#error "PositionSelectorComponent.generated.h already included, missing '#pragma once' in PositionSelectorComponent.h"
#endif
#define OBJECTPLACEMENT_PositionSelectorComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_12_DELEGATE \
OBJECTPLACEMENT_API void FPositionChanged_DelegateWrapper(const FMulticastScriptDelegate& PositionChanged, FTransform const& Transform, bool IsDetected);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_RPC_WRAPPERS \
	virtual void UpdateTransform_Implementation(); \
 \
	DECLARE_FUNCTION(execIsDetected); \
	DECLARE_FUNCTION(execGetCurrentCell); \
	DECLARE_FUNCTION(execGetCurrentPosition); \
	DECLARE_FUNCTION(execSetGridSnapped); \
	DECLARE_FUNCTION(execIsGridSnapped); \
	DECLARE_FUNCTION(execUpdateTransform);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsDetected); \
	DECLARE_FUNCTION(execGetCurrentCell); \
	DECLARE_FUNCTION(execGetCurrentPosition); \
	DECLARE_FUNCTION(execSetGridSnapped); \
	DECLARE_FUNCTION(execIsGridSnapped); \
	DECLARE_FUNCTION(execUpdateTransform);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPositionSelectorComponent(); \
	friend struct Z_Construct_UClass_UPositionSelectorComponent_Statics; \
public: \
	DECLARE_CLASS(UPositionSelectorComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), NO_API) \
	DECLARE_SERIALIZER(UPositionSelectorComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUPositionSelectorComponent(); \
	friend struct Z_Construct_UClass_UPositionSelectorComponent_Statics; \
public: \
	DECLARE_CLASS(UPositionSelectorComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), NO_API) \
	DECLARE_SERIALIZER(UPositionSelectorComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPositionSelectorComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPositionSelectorComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionSelectorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionSelectorComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionSelectorComponent(UPositionSelectorComponent&&); \
	NO_API UPositionSelectorComponent(const UPositionSelectorComponent&); \
public: \
	NO_API virtual ~UPositionSelectorComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPositionSelectorComponent(UPositionSelectorComponent&&); \
	NO_API UPositionSelectorComponent(const UPositionSelectorComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPositionSelectorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPositionSelectorComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPositionSelectorComponent) \
	NO_API virtual ~UPositionSelectorComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTPLACEMENT_API UClass* StaticClass<class UPositionSelectorComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PositionSelector_PositionSelectorComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
