// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectPlacement/Public/ObjectPlacement/PlacementPreviewComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlacementPreviewComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreviewComponent();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreviewComponent_NoRegister();
	OBJECTPLACEMENT_API UEnum* Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState();
	UPackage* Z_Construct_UPackage__Script_ObjectPlacement();
// End Cross Module References
	DEFINE_FUNCTION(UPlacementPreviewComponent::execCreatePreviewObject)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CreatePreviewObject();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execOnPlacingRestricted)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPlacingRestricted_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execOnPlacingAccepted)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPlacingAccepted_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execSetupOnSpawnPreview)
	{
		P_GET_OBJECT(AActor,Z_Param_InPreviewActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupOnSpawnPreview_Implementation(Z_Param_InPreviewActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execSetPreviewState)
	{
		P_GET_ENUM(EPlacementPreviewState,Z_Param_NewPreviewState);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPreviewState(EPlacementPreviewState(Z_Param_NewPreviewState));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execSetPreviewClass)
	{
		P_GET_OBJECT(UClass,Z_Param_InPreviewClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPreviewClass(Z_Param_InPreviewClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execOnComponentDestroyed)
	{
		P_GET_UBOOL(Z_Param_bDestroyingHierarchy);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnComponentDestroyed(Z_Param_bDestroyingHierarchy);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlacementPreviewComponent::execUpdatePreviewTransform)
	{
		P_GET_STRUCT_REF(FTransform,Z_Param_Out_Transform);
		P_GET_UBOOL(Z_Param_IsDetected);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdatePreviewTransform(Z_Param_Out_Transform,Z_Param_IsDetected);
		P_NATIVE_END;
	}
	struct PlacementPreviewComponent_eventSetupOnSpawnPreview_Parms
	{
		AActor* InPreviewActor;
	};
	static FName NAME_UPlacementPreviewComponent_OnPlacingAccepted = FName(TEXT("OnPlacingAccepted"));
	void UPlacementPreviewComponent::OnPlacingAccepted()
	{
		ProcessEvent(FindFunctionChecked(NAME_UPlacementPreviewComponent_OnPlacingAccepted),NULL);
	}
	static FName NAME_UPlacementPreviewComponent_OnPlacingRestricted = FName(TEXT("OnPlacingRestricted"));
	void UPlacementPreviewComponent::OnPlacingRestricted()
	{
		ProcessEvent(FindFunctionChecked(NAME_UPlacementPreviewComponent_OnPlacingRestricted),NULL);
	}
	static FName NAME_UPlacementPreviewComponent_SetupOnSpawnPreview = FName(TEXT("SetupOnSpawnPreview"));
	void UPlacementPreviewComponent::SetupOnSpawnPreview(AActor* InPreviewActor)
	{
		PlacementPreviewComponent_eventSetupOnSpawnPreview_Parms Parms;
		Parms.InPreviewActor=InPreviewActor;
		ProcessEvent(FindFunctionChecked(NAME_UPlacementPreviewComponent_SetupOnSpawnPreview),&Parms);
	}
	void UPlacementPreviewComponent::StaticRegisterNativesUPlacementPreviewComponent()
	{
		UClass* Class = UPlacementPreviewComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreatePreviewObject", &UPlacementPreviewComponent::execCreatePreviewObject },
			{ "OnComponentDestroyed", &UPlacementPreviewComponent::execOnComponentDestroyed },
			{ "OnPlacingAccepted", &UPlacementPreviewComponent::execOnPlacingAccepted },
			{ "OnPlacingRestricted", &UPlacementPreviewComponent::execOnPlacingRestricted },
			{ "SetPreviewClass", &UPlacementPreviewComponent::execSetPreviewClass },
			{ "SetPreviewState", &UPlacementPreviewComponent::execSetPreviewState },
			{ "SetupOnSpawnPreview", &UPlacementPreviewComponent::execSetupOnSpawnPreview },
			{ "UpdatePreviewTransform", &UPlacementPreviewComponent::execUpdatePreviewTransform },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "CreatePreviewObject", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics
	{
		struct PlacementPreviewComponent_eventOnComponentDestroyed_Parms
		{
			bool bDestroyingHierarchy;
		};
		static void NewProp_bDestroyingHierarchy_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bDestroyingHierarchy;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::NewProp_bDestroyingHierarchy_SetBit(void* Obj)
	{
		((PlacementPreviewComponent_eventOnComponentDestroyed_Parms*)Obj)->bDestroyingHierarchy = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::NewProp_bDestroyingHierarchy = { "bDestroyingHierarchy", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(PlacementPreviewComponent_eventOnComponentDestroyed_Parms), &Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::NewProp_bDestroyingHierarchy_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::NewProp_bDestroyingHierarchy,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "OnComponentDestroyed", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::PlacementPreviewComponent_eventOnComponentDestroyed_Parms), Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "OnPlacingAccepted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "OnPlacingRestricted", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics
	{
		struct PlacementPreviewComponent_eventSetPreviewClass_Parms
		{
			TSubclassOf<AActor>  InPreviewClass;
		};
		static const UECodeGen_Private::FClassPropertyParams NewProp_InPreviewClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::NewProp_InPreviewClass = { "InPreviewClass", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlacementPreviewComponent_eventSetPreviewClass_Parms, InPreviewClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::NewProp_InPreviewClass,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "SetPreviewClass", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::PlacementPreviewComponent_eventSetPreviewClass_Parms), Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics
	{
		struct PlacementPreviewComponent_eventSetPreviewState_Parms
		{
			EPlacementPreviewState NewPreviewState;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_NewPreviewState_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_NewPreviewState;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::NewProp_NewPreviewState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::NewProp_NewPreviewState = { "NewPreviewState", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlacementPreviewComponent_eventSetPreviewState_Parms, NewPreviewState), Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState, METADATA_PARAMS(nullptr, 0) }; // 739329692
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::NewProp_NewPreviewState_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::NewProp_NewPreviewState,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "CPP_Default_NewPreviewState", "Accepted" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "SetPreviewState", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::PlacementPreviewComponent_eventSetPreviewState_Parms), Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InPreviewActor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::NewProp_InPreviewActor = { "InPreviewActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlacementPreviewComponent_eventSetupOnSpawnPreview_Parms, InPreviewActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::NewProp_InPreviewActor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "SetupOnSpawnPreview", nullptr, nullptr, sizeof(PlacementPreviewComponent_eventSetupOnSpawnPreview_Parms), Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics
	{
		struct PlacementPreviewComponent_eventUpdatePreviewTransform_Parms
		{
			FTransform Transform;
			bool IsDetected;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Transform;
		static void NewProp_IsDetected_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsDetected;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_Transform_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlacementPreviewComponent_eventUpdatePreviewTransform_Parms, Transform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_Transform_MetaData)) };
	void Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_IsDetected_SetBit(void* Obj)
	{
		((PlacementPreviewComponent_eventUpdatePreviewTransform_Parms*)Obj)->IsDetected = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_IsDetected = { "IsDetected", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(PlacementPreviewComponent_eventUpdatePreviewTransform_Parms), &Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_IsDetected_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_Transform,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::NewProp_IsDetected,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlacementPreviewComponent, nullptr, "UpdatePreviewTransform", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::PlacementPreviewComponent_eventUpdatePreviewTransform_Parms), Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UPlacementPreviewComponent);
	UClass* Z_Construct_UClass_UPlacementPreviewComponent_NoRegister()
	{
		return UPlacementPreviewComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPlacementPreviewComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PreviewObjectClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_PreviewObjectClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PreviewObject_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_PreviewObject;
		static const UECodeGen_Private::FBytePropertyParams NewProp_PreviewState_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PreviewState_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_PreviewState;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlacementPreviewComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectPlacement,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPlacementPreviewComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_CreatePreviewObject, "CreatePreviewObject" }, // 2894181269
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_OnComponentDestroyed, "OnComponentDestroyed" }, // 563501832
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingAccepted, "OnPlacingAccepted" }, // 2672620282
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_OnPlacingRestricted, "OnPlacingRestricted" }, // 1525940260
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewClass, "SetPreviewClass" }, // 3509926406
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_SetPreviewState, "SetPreviewState" }, // 1024205339
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_SetupOnSpawnPreview, "SetupOnSpawnPreview" }, // 2077421557
		{ &Z_Construct_UFunction_UPlacementPreviewComponent_UpdatePreviewTransform, "UpdatePreviewTransform" }, // 246486308
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlacementPreviewComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "ObjectPlacement/PlacementPreviewComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObjectClass_MetaData[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObjectClass = { "PreviewObjectClass", nullptr, (EPropertyFlags)0x0024080000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPlacementPreviewComponent, PreviewObjectClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObjectClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObjectClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObject_MetaData[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObject = { "PreviewObject", nullptr, (EPropertyFlags)0x0024080000020805, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPlacementPreviewComponent, PreviewObject), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObject_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState_MetaData[] = {
		{ "Category", "PlacementPreview" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewComponent.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState = { "PreviewState", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPlacementPreviewComponent, PreviewState), Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState, METADATA_PARAMS(Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState_MetaData)) }; // 739329692
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlacementPreviewComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObjectClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewObject,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlacementPreviewComponent_Statics::NewProp_PreviewState,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlacementPreviewComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlacementPreviewComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UPlacementPreviewComponent_Statics::ClassParams = {
		&UPlacementPreviewComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPlacementPreviewComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlacementPreviewComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPlacementPreviewComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlacementPreviewComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlacementPreviewComponent()
	{
		if (!Z_Registration_Info_UClass_UPlacementPreviewComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UPlacementPreviewComponent.OuterSingleton, Z_Construct_UClass_UPlacementPreviewComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UPlacementPreviewComponent.OuterSingleton;
	}
	template<> OBJECTPLACEMENT_API UClass* StaticClass<UPlacementPreviewComponent>()
	{
		return UPlacementPreviewComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlacementPreviewComponent);
	UPlacementPreviewComponent::~UPlacementPreviewComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UPlacementPreviewComponent, UPlacementPreviewComponent::StaticClass, TEXT("UPlacementPreviewComponent"), &Z_Registration_Info_UClass_UPlacementPreviewComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UPlacementPreviewComponent), 2651045504U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_3226738324(TEXT("/Script/ObjectPlacement"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
