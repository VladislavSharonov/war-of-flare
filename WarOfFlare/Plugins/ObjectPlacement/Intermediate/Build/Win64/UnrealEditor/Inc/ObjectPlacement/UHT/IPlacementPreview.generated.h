// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectPlacement/IPlacementPreview.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OBJECTPLACEMENT_IPlacementPreview_generated_h
#error "IPlacementPreview.generated.h already included, missing '#pragma once' in IPlacementPreview.h"
#endif
#define OBJECTPLACEMENT_IPlacementPreview_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_RPC_WRAPPERS \
	virtual void SetPlacementPreview_Implementation(bool InIsPreview) {}; \
 \
	DECLARE_FUNCTION(execSetPlacementPreview);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void SetPlacementPreview_Implementation(bool InIsPreview) {}; \
 \
	DECLARE_FUNCTION(execSetPlacementPreview);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OBJECTPLACEMENT_API UPlacementPreview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlacementPreview) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OBJECTPLACEMENT_API, UPlacementPreview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlacementPreview); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OBJECTPLACEMENT_API UPlacementPreview(UPlacementPreview&&); \
	OBJECTPLACEMENT_API UPlacementPreview(const UPlacementPreview&); \
public: \
	OBJECTPLACEMENT_API virtual ~UPlacementPreview();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OBJECTPLACEMENT_API UPlacementPreview(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OBJECTPLACEMENT_API UPlacementPreview(UPlacementPreview&&); \
	OBJECTPLACEMENT_API UPlacementPreview(const UPlacementPreview&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OBJECTPLACEMENT_API, UPlacementPreview); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlacementPreview); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlacementPreview) \
	OBJECTPLACEMENT_API virtual ~UPlacementPreview();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUPlacementPreview(); \
	friend struct Z_Construct_UClass_UPlacementPreview_Statics; \
public: \
	DECLARE_CLASS(UPlacementPreview, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/ObjectPlacement"), OBJECTPLACEMENT_API) \
	DECLARE_SERIALIZER(UPlacementPreview)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_GENERATED_UINTERFACE_BODY() \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_GENERATED_UINTERFACE_BODY() \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IPlacementPreview() {} \
public: \
	typedef UPlacementPreview UClassType; \
	typedef IPlacementPreview ThisClass; \
	static void Execute_SetPlacementPreview(UObject* O, bool InIsPreview); \
	virtual UObject* _getUObject() const { return nullptr; }


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IPlacementPreview() {} \
public: \
	typedef UPlacementPreview UClassType; \
	typedef IPlacementPreview ThisClass; \
	static void Execute_SetPlacementPreview(UObject* O, bool InIsPreview); \
	virtual UObject* _getUObject() const { return nullptr; }


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_10_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTPLACEMENT_API UClass* StaticClass<class UPlacementPreview>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_IPlacementPreview_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
