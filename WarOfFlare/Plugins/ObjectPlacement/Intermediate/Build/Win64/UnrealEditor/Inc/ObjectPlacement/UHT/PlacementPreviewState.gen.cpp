// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectPlacement/Public/ObjectPlacement/PlacementPreviewState.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlacementPreviewState() {}
// Cross Module References
	OBJECTPLACEMENT_API UEnum* Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState();
	UPackage* Z_Construct_UPackage__Script_ObjectPlacement();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EPlacementPreviewState;
	static UEnum* EPlacementPreviewState_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EPlacementPreviewState.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EPlacementPreviewState.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState, (UObject*)Z_Construct_UPackage__Script_ObjectPlacement(), TEXT("EPlacementPreviewState"));
		}
		return Z_Registration_Info_UEnum_EPlacementPreviewState.OuterSingleton;
	}
	template<> OBJECTPLACEMENT_API UEnum* StaticEnum<EPlacementPreviewState>()
	{
		return EPlacementPreviewState_StaticEnum();
	}
	struct Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::Enumerators[] = {
		{ "EPlacementPreviewState::Accepted", (int64)EPlacementPreviewState::Accepted },
		{ "EPlacementPreviewState::Restricted", (int64)EPlacementPreviewState::Restricted },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::Enum_MetaDataParams[] = {
		{ "Accepted.DisplayName", "Accepted" },
		{ "Accepted.Name", "EPlacementPreviewState::Accepted" },
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/ObjectPlacement/PlacementPreviewState.h" },
		{ "Restricted.DisplayName", "Restricted" },
		{ "Restricted.Name", "EPlacementPreviewState::Restricted" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_ObjectPlacement,
		nullptr,
		"EPlacementPreviewState",
		"EPlacementPreviewState",
		Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState()
	{
		if (!Z_Registration_Info_UEnum_EPlacementPreviewState.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EPlacementPreviewState.InnerSingleton, Z_Construct_UEnum_ObjectPlacement_EPlacementPreviewState_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EPlacementPreviewState.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewState_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewState_h_Statics::EnumInfo[] = {
		{ EPlacementPreviewState_StaticEnum, TEXT("EPlacementPreviewState"), &Z_Registration_Info_UEnum_EPlacementPreviewState, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 739329692U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewState_h_4235549367(TEXT("/Script/ObjectPlacement"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewState_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectPlacement_Source_ObjectPlacement_Public_ObjectPlacement_PlacementPreviewState_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
