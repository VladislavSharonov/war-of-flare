// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Survey/Public/Readiness/ReadinessManagerComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReadinessManagerComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	SURVEY_API UClass* Z_Construct_UClass_UReadinessManagerComponent();
	SURVEY_API UClass* Z_Construct_UClass_UReadinessManagerComponent_NoRegister();
	SURVEY_API UClass* Z_Construct_UClass_UReadinessRespondentComponent_NoRegister();
	SURVEY_API UFunction* Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Survey();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Survey, nullptr, "AllReadySignature__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FAllReadySignature_DelegateWrapper(const FMulticastScriptDelegate& AllReadySignature)
{
	AllReadySignature.ProcessMulticastDelegate<UObject>(NULL);
}
	DEFINE_FUNCTION(UReadinessManagerComponent::execOnGetStateResponse)
	{
		P_GET_OBJECT(UReadinessRespondentComponent,Z_Param_Respondent);
		P_GET_UBOOL(Z_Param_bIsReady);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnGetStateResponse(Z_Param_Respondent,Z_Param_bIsReady);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessManagerComponent::execIsAllReady)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsAllReady();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessManagerComponent::execRemoveAllRespondents)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveAllRespondents();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessManagerComponent::execRemoveRespondent)
	{
		P_GET_OBJECT(AActor,Z_Param_Respondent);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveRespondent(Z_Param_Respondent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessManagerComponent::execAddRespondent)
	{
		P_GET_OBJECT(AActor,Z_Param_Respondent);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddRespondent(Z_Param_Respondent);
		P_NATIVE_END;
	}
	void UReadinessManagerComponent::StaticRegisterNativesUReadinessManagerComponent()
	{
		UClass* Class = UReadinessManagerComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddRespondent", &UReadinessManagerComponent::execAddRespondent },
			{ "IsAllReady", &UReadinessManagerComponent::execIsAllReady },
			{ "OnGetStateResponse", &UReadinessManagerComponent::execOnGetStateResponse },
			{ "RemoveAllRespondents", &UReadinessManagerComponent::execRemoveAllRespondents },
			{ "RemoveRespondent", &UReadinessManagerComponent::execRemoveRespondent },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics
	{
		struct ReadinessManagerComponent_eventAddRespondent_Parms
		{
			AActor* Respondent;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Respondent;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::NewProp_Respondent = { "Respondent", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ReadinessManagerComponent_eventAddRespondent_Parms, Respondent), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::NewProp_Respondent,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessManagerComponent, nullptr, "AddRespondent", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::ReadinessManagerComponent_eventAddRespondent_Parms), Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics
	{
		struct ReadinessManagerComponent_eventIsAllReady_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ReadinessManagerComponent_eventIsAllReady_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ReadinessManagerComponent_eventIsAllReady_Parms), &Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessManagerComponent, nullptr, "IsAllReady", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::ReadinessManagerComponent_eventIsAllReady_Parms), Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics
	{
		struct ReadinessManagerComponent_eventOnGetStateResponse_Parms
		{
			UReadinessRespondentComponent* Respondent;
			bool bIsReady;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Respondent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Respondent;
		static void NewProp_bIsReady_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsReady;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_Respondent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_Respondent = { "Respondent", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ReadinessManagerComponent_eventOnGetStateResponse_Parms, Respondent), Z_Construct_UClass_UReadinessRespondentComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_Respondent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_Respondent_MetaData)) };
	void Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_bIsReady_SetBit(void* Obj)
	{
		((ReadinessManagerComponent_eventOnGetStateResponse_Parms*)Obj)->bIsReady = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_bIsReady = { "bIsReady", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ReadinessManagerComponent_eventOnGetStateResponse_Parms), &Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_bIsReady_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_Respondent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::NewProp_bIsReady,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessManagerComponent, nullptr, "OnGetStateResponse", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::ReadinessManagerComponent_eventOnGetStateResponse_Parms), Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessManagerComponent, nullptr, "RemoveAllRespondents", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics
	{
		struct ReadinessManagerComponent_eventRemoveRespondent_Parms
		{
			const AActor* Respondent;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Respondent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Respondent;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::NewProp_Respondent_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::NewProp_Respondent = { "Respondent", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ReadinessManagerComponent_eventRemoveRespondent_Parms, Respondent), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::NewProp_Respondent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::NewProp_Respondent_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::NewProp_Respondent,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessManagerComponent, nullptr, "RemoveRespondent", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::ReadinessManagerComponent_eventRemoveRespondent_Parms), Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UReadinessManagerComponent);
	UClass* Z_Construct_UClass_UReadinessManagerComponent_NoRegister()
	{
		return UReadinessManagerComponent::StaticClass();
	}
	struct Z_Construct_UClass_UReadinessManagerComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnAllReady_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAllReady;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RespondentClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_RespondentClass;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Respondents_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Respondents_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Respondents;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReadinessManagerComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Survey,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UReadinessManagerComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UReadinessManagerComponent_AddRespondent, "AddRespondent" }, // 3246160956
		{ &Z_Construct_UFunction_UReadinessManagerComponent_IsAllReady, "IsAllReady" }, // 3931051199
		{ &Z_Construct_UFunction_UReadinessManagerComponent_OnGetStateResponse, "OnGetStateResponse" }, // 2018311194
		{ &Z_Construct_UFunction_UReadinessManagerComponent_RemoveAllRespondents, "RemoveAllRespondents" }, // 3591988999
		{ &Z_Construct_UFunction_UReadinessManagerComponent_RemoveRespondent, "RemoveRespondent" }, // 2123094101
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessManagerComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Readiness/ReadinessManagerComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_OnAllReady_MetaData[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_OnAllReady = { "OnAllReady", nullptr, (EPropertyFlags)0x0010000000080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UReadinessManagerComponent, OnAllReady), Z_Construct_UDelegateFunction_Survey_AllReadySignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_OnAllReady_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_OnAllReady_MetaData)) }; // 1183374227
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_RespondentClass_MetaData[] = {
		{ "Category", "ReadinessManagerComponent" },
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_RespondentClass = { "RespondentClass", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UReadinessManagerComponent, RespondentClass), Z_Construct_UClass_UClass, Z_Construct_UClass_UReadinessRespondentComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_RespondentClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_RespondentClass_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents_Inner = { "Respondents", nullptr, (EPropertyFlags)0x0000000000080008, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_UReadinessRespondentComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Readiness/ReadinessManagerComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents = { "Respondents", nullptr, (EPropertyFlags)0x0020088000000008, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UReadinessManagerComponent, Respondents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReadinessManagerComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_OnAllReady,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_RespondentClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReadinessManagerComponent_Statics::NewProp_Respondents,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReadinessManagerComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReadinessManagerComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UReadinessManagerComponent_Statics::ClassParams = {
		&UReadinessManagerComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UReadinessManagerComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessManagerComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UReadinessManagerComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessManagerComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReadinessManagerComponent()
	{
		if (!Z_Registration_Info_UClass_UReadinessManagerComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UReadinessManagerComponent.OuterSingleton, Z_Construct_UClass_UReadinessManagerComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UReadinessManagerComponent.OuterSingleton;
	}
	template<> SURVEY_API UClass* StaticClass<UReadinessManagerComponent>()
	{
		return UReadinessManagerComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReadinessManagerComponent);
	UReadinessManagerComponent::~UReadinessManagerComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UReadinessManagerComponent, UReadinessManagerComponent::StaticClass, TEXT("UReadinessManagerComponent"), &Z_Registration_Info_UClass_UReadinessManagerComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UReadinessManagerComponent), 3803789020U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_2165010954(TEXT("/Script/Survey"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
