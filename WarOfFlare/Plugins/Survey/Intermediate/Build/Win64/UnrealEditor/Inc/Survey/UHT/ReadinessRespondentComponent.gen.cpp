// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Survey/Public/Readiness/ReadinessRespondentComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReadinessRespondentComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	SURVEY_API UClass* Z_Construct_UClass_UReadinessRespondentComponent();
	SURVEY_API UClass* Z_Construct_UClass_UReadinessRespondentComponent_NoRegister();
	SURVEY_API UFunction* Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Survey();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics
	{
		struct _Script_Survey_eventReadyResponseSignature_Parms
		{
			UReadinessRespondentComponent* Respondent;
			bool IsReady;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Respondent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Respondent;
		static void NewProp_IsReady_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsReady;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_Respondent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_Respondent = { "Respondent", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Survey_eventReadyResponseSignature_Parms, Respondent), Z_Construct_UClass_UReadinessRespondentComponent_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_Respondent_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_Respondent_MetaData)) };
	void Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_IsReady_SetBit(void* Obj)
	{
		((_Script_Survey_eventReadyResponseSignature_Parms*)Obj)->IsReady = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_IsReady = { "IsReady", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(_Script_Survey_eventReadyResponseSignature_Parms), &Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_IsReady_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_Respondent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::NewProp_IsReady,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Survey, nullptr, "ReadyResponseSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::_Script_Survey_eventReadyResponseSignature_Parms), Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FReadyResponseSignature_DelegateWrapper(const FMulticastScriptDelegate& ReadyResponseSignature, UReadinessRespondentComponent* Respondent, bool IsReady)
{
	struct _Script_Survey_eventReadyResponseSignature_Parms
	{
		UReadinessRespondentComponent* Respondent;
		bool IsReady;
	};
	_Script_Survey_eventReadyResponseSignature_Parms Parms;
	Parms.Respondent=Respondent;
	Parms.IsReady=IsReady ? true : false;
	ReadyResponseSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(UReadinessRespondentComponent::execOnRep_IsReady)
	{
		P_GET_UBOOL(Z_Param_bOldIsReady);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_IsReady(Z_Param_bOldIsReady);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessRespondentComponent::execSetReady)
	{
		P_GET_UBOOL(Z_Param_bNewIsReady);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetReady(Z_Param_bNewIsReady);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessRespondentComponent::execServer_SetReady)
	{
		P_GET_UBOOL(Z_Param_bNewIsReady);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Server_SetReady_Implementation(Z_Param_bNewIsReady);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UReadinessRespondentComponent::execIsReady)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsReady();
		P_NATIVE_END;
	}
	struct ReadinessRespondentComponent_eventServer_SetReady_Parms
	{
		bool bNewIsReady;
	};
	static FName NAME_UReadinessRespondentComponent_Server_SetReady = FName(TEXT("Server_SetReady"));
	void UReadinessRespondentComponent::Server_SetReady(bool bNewIsReady)
	{
		ReadinessRespondentComponent_eventServer_SetReady_Parms Parms;
		Parms.bNewIsReady=bNewIsReady ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UReadinessRespondentComponent_Server_SetReady),&Parms);
	}
	void UReadinessRespondentComponent::StaticRegisterNativesUReadinessRespondentComponent()
	{
		UClass* Class = UReadinessRespondentComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsReady", &UReadinessRespondentComponent::execIsReady },
			{ "OnRep_IsReady", &UReadinessRespondentComponent::execOnRep_IsReady },
			{ "Server_SetReady", &UReadinessRespondentComponent::execServer_SetReady },
			{ "SetReady", &UReadinessRespondentComponent::execSetReady },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics
	{
		struct ReadinessRespondentComponent_eventIsReady_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ReadinessRespondentComponent_eventIsReady_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ReadinessRespondentComponent_eventIsReady_Parms), &Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessRespondentComponent, nullptr, "IsReady", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::ReadinessRespondentComponent_eventIsReady_Parms), Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessRespondentComponent_IsReady()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessRespondentComponent_IsReady_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics
	{
		struct ReadinessRespondentComponent_eventOnRep_IsReady_Parms
		{
			bool bOldIsReady;
		};
		static void NewProp_bOldIsReady_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bOldIsReady;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::NewProp_bOldIsReady_SetBit(void* Obj)
	{
		((ReadinessRespondentComponent_eventOnRep_IsReady_Parms*)Obj)->bOldIsReady = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::NewProp_bOldIsReady = { "bOldIsReady", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ReadinessRespondentComponent_eventOnRep_IsReady_Parms), &Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::NewProp_bOldIsReady_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::NewProp_bOldIsReady,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessRespondentComponent, nullptr, "OnRep_IsReady", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::ReadinessRespondentComponent_eventOnRep_IsReady_Parms), Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bNewIsReady_MetaData[];
#endif
		static void NewProp_bNewIsReady_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bNewIsReady;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady_SetBit(void* Obj)
	{
		((ReadinessRespondentComponent_eventServer_SetReady_Parms*)Obj)->bNewIsReady = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady = { "bNewIsReady", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ReadinessRespondentComponent_eventServer_SetReady_Parms), &Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::NewProp_bNewIsReady,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessRespondentComponent, nullptr, "Server_SetReady", nullptr, nullptr, sizeof(ReadinessRespondentComponent_eventServer_SetReady_Parms), Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04280C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics
	{
		struct ReadinessRespondentComponent_eventSetReady_Parms
		{
			bool bNewIsReady;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bNewIsReady_MetaData[];
#endif
		static void NewProp_bNewIsReady_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bNewIsReady;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady_SetBit(void* Obj)
	{
		((ReadinessRespondentComponent_eventSetReady_Parms*)Obj)->bNewIsReady = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady = { "bNewIsReady", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ReadinessRespondentComponent_eventSetReady_Parms), &Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::NewProp_bNewIsReady,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UReadinessRespondentComponent, nullptr, "SetReady", nullptr, nullptr, sizeof(Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::ReadinessRespondentComponent_eventSetReady_Parms), Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UReadinessRespondentComponent_SetReady()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UReadinessRespondentComponent_SetReady_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UReadinessRespondentComponent);
	UClass* Z_Construct_UClass_UReadinessRespondentComponent_NoRegister()
	{
		return UReadinessRespondentComponent::StaticClass();
	}
	struct Z_Construct_UClass_UReadinessRespondentComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnGetStateResponse_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnGetStateResponse;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bIsReady_MetaData[];
#endif
		static void NewProp_bIsReady_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsReady;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReadinessRespondentComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Survey,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UReadinessRespondentComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UReadinessRespondentComponent_IsReady, "IsReady" }, // 3527609395
		{ &Z_Construct_UFunction_UReadinessRespondentComponent_OnRep_IsReady, "OnRep_IsReady" }, // 340871346
		{ &Z_Construct_UFunction_UReadinessRespondentComponent_Server_SetReady, "Server_SetReady" }, // 1047168424
		{ &Z_Construct_UFunction_UReadinessRespondentComponent_SetReady, "SetReady" }, // 3263122786
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessRespondentComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Readiness/ReadinessRespondentComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_OnGetStateResponse_MetaData[] = {
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_OnGetStateResponse = { "OnGetStateResponse", nullptr, (EPropertyFlags)0x0010000000080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UReadinessRespondentComponent, OnGetStateResponse), Z_Construct_UDelegateFunction_Survey_ReadyResponseSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_OnGetStateResponse_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_OnGetStateResponse_MetaData)) }; // 3197902820
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady_MetaData[] = {
		{ "Category", "ReadinessRespondentComponent" },
		{ "ModuleRelativePath", "Public/Readiness/ReadinessRespondentComponent.h" },
	};
#endif
	void Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady_SetBit(void* Obj)
	{
		((UReadinessRespondentComponent*)Obj)->bIsReady = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady = { "bIsReady", "OnRep_IsReady", (EPropertyFlags)0x0020080100000024, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UReadinessRespondentComponent), &Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady_SetBit, METADATA_PARAMS(Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReadinessRespondentComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_OnGetStateResponse,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReadinessRespondentComponent_Statics::NewProp_bIsReady,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReadinessRespondentComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReadinessRespondentComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UReadinessRespondentComponent_Statics::ClassParams = {
		&UReadinessRespondentComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UReadinessRespondentComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessRespondentComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UReadinessRespondentComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReadinessRespondentComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReadinessRespondentComponent()
	{
		if (!Z_Registration_Info_UClass_UReadinessRespondentComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UReadinessRespondentComponent.OuterSingleton, Z_Construct_UClass_UReadinessRespondentComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UReadinessRespondentComponent.OuterSingleton;
	}
	template<> SURVEY_API UClass* StaticClass<UReadinessRespondentComponent>()
	{
		return UReadinessRespondentComponent::StaticClass();
	}

	void UReadinessRespondentComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_bIsReady(TEXT("bIsReady"));

		const bool bIsValid = true
			&& Name_bIsReady == ClassReps[(int32)ENetFields_Private::bIsReady].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UReadinessRespondentComponent"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReadinessRespondentComponent);
	UReadinessRespondentComponent::~UReadinessRespondentComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UReadinessRespondentComponent, UReadinessRespondentComponent::StaticClass, TEXT("UReadinessRespondentComponent"), &Z_Registration_Info_UClass_UReadinessRespondentComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UReadinessRespondentComponent), 3305036506U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_3909270386(TEXT("/Script/Survey"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
