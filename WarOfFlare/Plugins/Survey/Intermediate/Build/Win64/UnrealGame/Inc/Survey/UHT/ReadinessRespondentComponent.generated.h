// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Readiness/ReadinessRespondentComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UReadinessRespondentComponent;
#ifdef SURVEY_ReadinessRespondentComponent_generated_h
#error "ReadinessRespondentComponent.generated.h already included, missing '#pragma once' in ReadinessRespondentComponent.h"
#endif
#define SURVEY_ReadinessRespondentComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_12_DELEGATE \
SURVEY_API void FReadyResponseSignature_DelegateWrapper(const FMulticastScriptDelegate& ReadyResponseSignature, UReadinessRespondentComponent* Respondent, bool IsReady);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_RPC_WRAPPERS \
	virtual void Server_SetReady_Implementation(bool bNewIsReady); \
 \
	DECLARE_FUNCTION(execOnRep_IsReady); \
	DECLARE_FUNCTION(execSetReady); \
	DECLARE_FUNCTION(execServer_SetReady); \
	DECLARE_FUNCTION(execIsReady);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void Server_SetReady_Implementation(bool bNewIsReady); \
 \
	DECLARE_FUNCTION(execOnRep_IsReady); \
	DECLARE_FUNCTION(execSetReady); \
	DECLARE_FUNCTION(execServer_SetReady); \
	DECLARE_FUNCTION(execIsReady);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReadinessRespondentComponent(); \
	friend struct Z_Construct_UClass_UReadinessRespondentComponent_Statics; \
public: \
	DECLARE_CLASS(UReadinessRespondentComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Survey"), NO_API) \
	DECLARE_SERIALIZER(UReadinessRespondentComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		bIsReady=NETFIELD_REP_START, \
		NETFIELD_REP_END=bIsReady	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUReadinessRespondentComponent(); \
	friend struct Z_Construct_UClass_UReadinessRespondentComponent_Statics; \
public: \
	DECLARE_CLASS(UReadinessRespondentComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Survey"), NO_API) \
	DECLARE_SERIALIZER(UReadinessRespondentComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		bIsReady=NETFIELD_REP_START, \
		NETFIELD_REP_END=bIsReady	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReadinessRespondentComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReadinessRespondentComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReadinessRespondentComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReadinessRespondentComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReadinessRespondentComponent(UReadinessRespondentComponent&&); \
	NO_API UReadinessRespondentComponent(const UReadinessRespondentComponent&); \
public: \
	NO_API virtual ~UReadinessRespondentComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReadinessRespondentComponent(UReadinessRespondentComponent&&); \
	NO_API UReadinessRespondentComponent(const UReadinessRespondentComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReadinessRespondentComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReadinessRespondentComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReadinessRespondentComponent) \
	NO_API virtual ~UReadinessRespondentComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SURVEY_API UClass* StaticClass<class UReadinessRespondentComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessRespondentComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
