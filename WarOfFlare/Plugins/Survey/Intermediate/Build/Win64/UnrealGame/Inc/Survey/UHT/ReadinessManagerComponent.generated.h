// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Readiness/ReadinessManagerComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UReadinessRespondentComponent;
#ifdef SURVEY_ReadinessManagerComponent_generated_h
#error "ReadinessManagerComponent.generated.h already included, missing '#pragma once' in ReadinessManagerComponent.h"
#endif
#define SURVEY_ReadinessManagerComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_12_DELEGATE \
SURVEY_API void FAllReadySignature_DelegateWrapper(const FMulticastScriptDelegate& AllReadySignature);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnGetStateResponse); \
	DECLARE_FUNCTION(execIsAllReady); \
	DECLARE_FUNCTION(execRemoveAllRespondents); \
	DECLARE_FUNCTION(execRemoveRespondent); \
	DECLARE_FUNCTION(execAddRespondent);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnGetStateResponse); \
	DECLARE_FUNCTION(execIsAllReady); \
	DECLARE_FUNCTION(execRemoveAllRespondents); \
	DECLARE_FUNCTION(execRemoveRespondent); \
	DECLARE_FUNCTION(execAddRespondent);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReadinessManagerComponent(); \
	friend struct Z_Construct_UClass_UReadinessManagerComponent_Statics; \
public: \
	DECLARE_CLASS(UReadinessManagerComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Survey"), NO_API) \
	DECLARE_SERIALIZER(UReadinessManagerComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUReadinessManagerComponent(); \
	friend struct Z_Construct_UClass_UReadinessManagerComponent_Statics; \
public: \
	DECLARE_CLASS(UReadinessManagerComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Survey"), NO_API) \
	DECLARE_SERIALIZER(UReadinessManagerComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReadinessManagerComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReadinessManagerComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReadinessManagerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReadinessManagerComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReadinessManagerComponent(UReadinessManagerComponent&&); \
	NO_API UReadinessManagerComponent(const UReadinessManagerComponent&); \
public: \
	NO_API virtual ~UReadinessManagerComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReadinessManagerComponent(UReadinessManagerComponent&&); \
	NO_API UReadinessManagerComponent(const UReadinessManagerComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReadinessManagerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReadinessManagerComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UReadinessManagerComponent) \
	NO_API virtual ~UReadinessManagerComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SURVEY_API UClass* StaticClass<class UReadinessManagerComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Survey_Source_Survey_Public_Readiness_ReadinessManagerComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
