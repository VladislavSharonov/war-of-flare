// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "ReadinessRespondentComponent.generated.h"

class UReadinessRespondentComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FReadyResponseSignature, UReadinessRespondentComponent*, Respondent, bool, IsReady);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SURVEY_API UReadinessRespondentComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UReadinessRespondentComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION(BlueprintPure)
	bool IsReady() const { return bIsReady; }
	
	UPROPERTY()
	FReadyResponseSignature OnGetStateResponse;
	
protected:
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_SetReady(const bool bNewIsReady);

	UFUNCTION(BlueprintCallable)
	void SetReady(const bool bNewIsReady);
	
	UFUNCTION()
	void OnRep_IsReady(bool bOldIsReady);
	
protected:
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_IsReady)
	bool bIsReady = false;
};
