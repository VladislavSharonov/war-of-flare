// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "ReadinessRespondentComponent.h"

#include "ReadinessManagerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAllReadySignature);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SURVEY_API UReadinessManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UReadinessManagerComponent();

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION()
	void AddRespondent(AActor* Respondent);

	UFUNCTION()
	void RemoveRespondent(const AActor* Respondent);

	UFUNCTION()
	void RemoveAllRespondents();
	
	UFUNCTION(BlueprintPure)
	bool IsAllReady() const;
	
	UPROPERTY()
	FAllReadySignature OnAllReady;

protected:
	UFUNCTION()
	void OnGetStateResponse(UReadinessRespondentComponent* Respondent, bool bIsReady);
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<UReadinessRespondentComponent> RespondentClass = UReadinessRespondentComponent::StaticClass();
	
	UPROPERTY()
	TArray<UReadinessRespondentComponent*> Respondents;
};
