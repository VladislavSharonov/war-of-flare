// DemoDreams. All rights reserved.

#include "Readiness/ReadinessRespondentComponent.h"

#include "Net/UnrealNetwork.h"

UReadinessRespondentComponent::UReadinessRespondentComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	SetIsReplicatedByDefault(true);
}

void UReadinessRespondentComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(UReadinessRespondentComponent, bIsReady);
}

void UReadinessRespondentComponent::Server_SetReady_Implementation(const bool bNewIsReady)
{
	bIsReady = bNewIsReady;
	OnGetStateResponse.Broadcast(this, bIsReady);
}

void UReadinessRespondentComponent::SetReady(const bool bNewIsReady)
{
	bIsReady = bNewIsReady;
	OnGetStateResponse.Broadcast(this, bIsReady);
	Server_SetReady(bIsReady);
}

void UReadinessRespondentComponent::OnRep_IsReady(bool bOldIsReady)
{
	OnGetStateResponse.Broadcast(this, bIsReady);
}
