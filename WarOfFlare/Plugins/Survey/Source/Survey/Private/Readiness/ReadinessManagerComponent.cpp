// DemoDreams. All rights reserved.

#include "Readiness/ReadinessManagerComponent.h"

UReadinessManagerComponent::UReadinessManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(false);
}

void UReadinessManagerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	RemoveAllRespondents();
}

void UReadinessManagerComponent::AddRespondent(AActor* Respondent)
{
	if (!IsValid(Respondent))
		return;

	UActorComponent* Component = Respondent->AddComponentByClass(RespondentClass, false, FTransform::Identity,false);
	UReadinessRespondentComponent* RespondentComponent = Cast<UReadinessRespondentComponent>(Component);
	Respondents.Add(RespondentComponent);
	RespondentComponent->OnGetStateResponse.AddUniqueDynamic(this, &UReadinessManagerComponent::OnGetStateResponse);
}

void UReadinessManagerComponent::RemoveRespondent(const AActor* Respondent)
{
	for (int i = Respondents.Num() - 1; i >= 0; --i)
	{
		UReadinessRespondentComponent* RespondentComponent = Respondents[i];
		if (!IsValid(RespondentComponent))
			continue;
		
		if (RespondentComponent->GetOwner() == Respondent)
		{
			RespondentComponent->DestroyComponent(true);
		}
		
		Respondents.RemoveAt(i);
	}
}

void UReadinessManagerComponent::RemoveAllRespondents()
{
	for (int i = Respondents.Num() - 1; i >= 0; --i)
	{
		UReadinessRespondentComponent* RespondentComponent = Respondents[i];
		if (!IsValid(RespondentComponent))
			continue;
		
		RespondentComponent->DestroyComponent(true);
	}
	
	Respondents.Reset();
}

bool UReadinessManagerComponent::IsAllReady() const
{
	for (const auto& Respondent : Respondents)
	{
		if (IsValid(Respondent) && !Respondent->IsReady())
			return false;
	}
	
	return true; // If all players are ready or invalid.
}

void UReadinessManagerComponent::OnGetStateResponse(UReadinessRespondentComponent* Respondent, bool bIsReady)
{
	if (bIsReady && IsAllReady())
		OnAllReady.Broadcast();
}
