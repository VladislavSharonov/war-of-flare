// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/Attack/AttackZone/AttackZoneCoordinates.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERS_AttackZoneCoordinates_generated_h
#error "AttackZoneCoordinates.generated.h already included, missing '#pragma once' in AttackZoneCoordinates.h"
#endif
#define TOWERS_AttackZoneCoordinates_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics; \
	TOWERS_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> TOWERS_API UScriptStruct* StaticStruct<struct FAttackZoneCoordinates>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
