// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/AttackZone/AttackZoneType.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAttackZoneType() {}
// Cross Module References
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_EAttackZoneType();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EAttackZoneType;
	static UEnum* EAttackZoneType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EAttackZoneType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EAttackZoneType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_Towers_EAttackZoneType, (UObject*)Z_Construct_UPackage__Script_Towers(), TEXT("EAttackZoneType"));
		}
		return Z_Registration_Info_UEnum_EAttackZoneType.OuterSingleton;
	}
	template<> TOWERS_API UEnum* StaticEnum<EAttackZoneType>()
	{
		return EAttackZoneType_StaticEnum();
	}
	struct Z_Construct_UEnum_Towers_EAttackZoneType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_Towers_EAttackZoneType_Statics::Enumerators[] = {
		{ "EAttackZoneType::Empty", (int64)EAttackZoneType::Empty },
		{ "EAttackZoneType::Radius1", (int64)EAttackZoneType::Radius1 },
		{ "EAttackZoneType::Cross3", (int64)EAttackZoneType::Cross3 },
		{ "EAttackZoneType::Cross5", (int64)EAttackZoneType::Cross5 },
		{ "EAttackZoneType::Radius2", (int64)EAttackZoneType::Radius2 },
		{ "EAttackZoneType::Radius3", (int64)EAttackZoneType::Radius3 },
		{ "EAttackZoneType::EvenAlternation", (int64)EAttackZoneType::EvenAlternation },
		{ "EAttackZoneType::OddAlternation", (int64)EAttackZoneType::OddAlternation },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_Towers_EAttackZoneType_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Cross3.DisplayName", "Cross3" },
		{ "Cross3.Name", "EAttackZoneType::Cross3" },
		{ "Cross5.DisplayName", "Cross5" },
		{ "Cross5.Name", "EAttackZoneType::Cross5" },
		{ "Empty.DisplayName", "Empty" },
		{ "Empty.Hidden", "" },
		{ "Empty.Name", "EAttackZoneType::Empty" },
		{ "EvenAlternation.DisplayName", "EvenAlternation" },
		{ "EvenAlternation.Name", "EAttackZoneType::EvenAlternation" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneType.h" },
		{ "OddAlternation.DisplayName", "OddAlternation" },
		{ "OddAlternation.Name", "EAttackZoneType::OddAlternation" },
		{ "Radius1.DisplayName", "Radius1" },
		{ "Radius1.Name", "EAttackZoneType::Radius1" },
		{ "Radius2.DisplayName", "Radius2" },
		{ "Radius2.Name", "EAttackZoneType::Radius2" },
		{ "Radius3.DisplayName", "Radius3" },
		{ "Radius3.Name", "EAttackZoneType::Radius3" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_Towers_EAttackZoneType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_Towers,
		nullptr,
		"EAttackZoneType",
		"EAttackZoneType",
		Z_Construct_UEnum_Towers_EAttackZoneType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_Towers_EAttackZoneType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_Towers_EAttackZoneType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_Towers_EAttackZoneType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_Towers_EAttackZoneType()
	{
		if (!Z_Registration_Info_UEnum_EAttackZoneType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EAttackZoneType.InnerSingleton, Z_Construct_UEnum_Towers_EAttackZoneType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EAttackZoneType.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneType_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneType_h_Statics::EnumInfo[] = {
		{ EAttackZoneType_StaticEnum, TEXT("EAttackZoneType"), &Z_Registration_Info_UEnum_EAttackZoneType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 3810168422U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneType_h_955812421(TEXT("/Script/Towers"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneType_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneType_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
