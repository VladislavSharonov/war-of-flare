// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/Attack/Projectile/HomingMovementComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERS_HomingMovementComponent_generated_h
#error "HomingMovementComponent.generated.h already included, missing '#pragma once' in HomingMovementComponent.h"
#endif
#define TOWERS_HomingMovementComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_10_DELEGATE \
TOWERS_API void FTargetReachedSignature_DelegateWrapper(const FMulticastScriptDelegate& TargetReachedSignature);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_RPC_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHomingMovementComponent(); \
	friend struct Z_Construct_UClass_UHomingMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UHomingMovementComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UHomingMovementComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUHomingMovementComponent(); \
	friend struct Z_Construct_UClass_UHomingMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UHomingMovementComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UHomingMovementComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHomingMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHomingMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHomingMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHomingMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHomingMovementComponent(UHomingMovementComponent&&); \
	NO_API UHomingMovementComponent(const UHomingMovementComponent&); \
public: \
	NO_API virtual ~UHomingMovementComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHomingMovementComponent(UHomingMovementComponent&&); \
	NO_API UHomingMovementComponent(const UHomingMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHomingMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHomingMovementComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHomingMovementComponent) \
	NO_API virtual ~UHomingMovementComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_12_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERS_API UClass* StaticClass<class UHomingMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
