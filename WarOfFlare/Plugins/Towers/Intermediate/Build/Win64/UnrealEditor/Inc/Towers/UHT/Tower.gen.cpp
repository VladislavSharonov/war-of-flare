// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Tower.h"
#include "ElementalDamage/Public/ElementalDamage/Damage.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTower() {}
// Cross Module References
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	ELEMENTALDAMAGE_API UScriptStruct* Z_Construct_UScriptStruct_FDamage();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	OBJECTPLACEMENT_API UClass* Z_Construct_UClass_UPlacementPreview_NoRegister();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectable_NoRegister();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectionHandlerComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_ATower();
	TOWERS_API UClass* Z_Construct_UClass_ATower_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UAttackZoneComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UTowerModificationComponent_NoRegister();
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_ETowerType();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	DEFINE_FUNCTION(ATower::execOnHoverEnd)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHoverEnd_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execOnHoverBegin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHoverBegin_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execOnDeselect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDeselect_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execOnSelect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnSelect_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execSetPlacementPreview)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPlacementPreview_Implementation(Z_Param_InIsPreview);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execAttack)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Attack_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execGetDamage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDamage*)Z_Param__Result=P_THIS->GetDamage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execOnRep_Damage)
	{
		P_GET_STRUCT_REF(FDamage,Z_Param_Out_InOldDamage);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_Damage(Z_Param_Out_InOldDamage);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execOnTowerElementalDamageTypeChanged)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnTowerElementalDamageTypeChanged_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execSetElementalDamageType)
	{
		P_GET_ENUM(EElementalDamageType,Z_Param_InElementalDamageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetElementalDamageType(EElementalDamageType(Z_Param_InElementalDamageType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execSetDamage)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InDamage);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDamage(Z_Param_InDamage);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execEndAttack)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndAttack_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execStartAttack)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartAttack_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execSetVisibleLocally)
	{
		P_GET_UBOOL(Z_Param_IsVisible);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetVisibleLocally_Implementation(Z_Param_IsVisible);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execEndOverlap)
	{
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndOverlap(Z_Param_OverlappedActor,Z_Param_OtherActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATower::execBeginOverlap)
	{
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BeginOverlap(Z_Param_OverlappedActor,Z_Param_OtherActor);
		P_NATIVE_END;
	}
	struct Tower_eventSetPlacementPreview_Parms
	{
		bool InIsPreview;
	};
	struct Tower_eventSetVisibleLocally_Parms
	{
		bool IsVisible;
	};
	static FName NAME_ATower_Attack = FName(TEXT("Attack"));
	void ATower::Attack()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_Attack),NULL);
	}
	static FName NAME_ATower_EndAttack = FName(TEXT("EndAttack"));
	void ATower::EndAttack()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_EndAttack),NULL);
	}
	static FName NAME_ATower_OnDeselect = FName(TEXT("OnDeselect"));
	void ATower::OnDeselect()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_OnDeselect),NULL);
	}
	static FName NAME_ATower_OnHoverBegin = FName(TEXT("OnHoverBegin"));
	void ATower::OnHoverBegin()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_OnHoverBegin),NULL);
	}
	static FName NAME_ATower_OnHoverEnd = FName(TEXT("OnHoverEnd"));
	void ATower::OnHoverEnd()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_OnHoverEnd),NULL);
	}
	static FName NAME_ATower_OnSelect = FName(TEXT("OnSelect"));
	void ATower::OnSelect()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_OnSelect),NULL);
	}
	static FName NAME_ATower_OnTowerElementalDamageTypeChanged = FName(TEXT("OnTowerElementalDamageTypeChanged"));
	void ATower::OnTowerElementalDamageTypeChanged()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_OnTowerElementalDamageTypeChanged),NULL);
	}
	static FName NAME_ATower_SetPlacementPreview = FName(TEXT("SetPlacementPreview"));
	void ATower::SetPlacementPreview(bool InIsPreview)
	{
		Tower_eventSetPlacementPreview_Parms Parms;
		Parms.InIsPreview=InIsPreview ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_ATower_SetPlacementPreview),&Parms);
	}
	static FName NAME_ATower_SetVisibleLocally = FName(TEXT("SetVisibleLocally"));
	void ATower::SetVisibleLocally(bool IsVisible)
	{
		Tower_eventSetVisibleLocally_Parms Parms;
		Parms.IsVisible=IsVisible ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_ATower_SetVisibleLocally),&Parms);
	}
	static FName NAME_ATower_StartAttack = FName(TEXT("StartAttack"));
	void ATower::StartAttack()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATower_StartAttack),NULL);
	}
	void ATower::StaticRegisterNativesATower()
	{
		UClass* Class = ATower::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Attack", &ATower::execAttack },
			{ "BeginOverlap", &ATower::execBeginOverlap },
			{ "EndAttack", &ATower::execEndAttack },
			{ "EndOverlap", &ATower::execEndOverlap },
			{ "GetDamage", &ATower::execGetDamage },
			{ "OnDeselect", &ATower::execOnDeselect },
			{ "OnHoverBegin", &ATower::execOnHoverBegin },
			{ "OnHoverEnd", &ATower::execOnHoverEnd },
			{ "OnRep_Damage", &ATower::execOnRep_Damage },
			{ "OnSelect", &ATower::execOnSelect },
			{ "OnTowerElementalDamageTypeChanged", &ATower::execOnTowerElementalDamageTypeChanged },
			{ "SetDamage", &ATower::execSetDamage },
			{ "SetElementalDamageType", &ATower::execSetElementalDamageType },
			{ "SetPlacementPreview", &ATower::execSetPlacementPreview },
			{ "SetVisibleLocally", &ATower::execSetVisibleLocally },
			{ "StartAttack", &ATower::execStartAttack },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATower_Attack_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_Attack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_Attack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "Attack", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_Attack_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_Attack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_Attack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_Attack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_BeginOverlap_Statics
	{
		struct Tower_eventBeginOverlap_Parms
		{
			AActor* OverlappedActor;
			AActor* OtherActor;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OverlappedActor;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATower_BeginOverlap_Statics::NewProp_OverlappedActor = { "OverlappedActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventBeginOverlap_Parms, OverlappedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATower_BeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_BeginOverlap_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_BeginOverlap_Statics::NewProp_OverlappedActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_BeginOverlap_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_BeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_BeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "BeginOverlap", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATower_BeginOverlap_Statics::Tower_eventBeginOverlap_Parms), Z_Construct_UFunction_ATower_BeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_BeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_BeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_BeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_BeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_BeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_EndAttack_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_EndAttack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "Comment", "// Call by attack zone component on minions come out the attack zone.\n" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
		{ "ToolTip", "Call by attack zone component on minions come out the attack zone." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_EndAttack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "EndAttack", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_EndAttack_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_EndAttack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_EndAttack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_EndAttack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_EndOverlap_Statics
	{
		struct Tower_eventEndOverlap_Parms
		{
			AActor* OverlappedActor;
			AActor* OtherActor;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OverlappedActor;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATower_EndOverlap_Statics::NewProp_OverlappedActor = { "OverlappedActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventEndOverlap_Parms, OverlappedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATower_EndOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_EndOverlap_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_EndOverlap_Statics::NewProp_OverlappedActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_EndOverlap_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_EndOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_EndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "EndOverlap", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATower_EndOverlap_Statics::Tower_eventEndOverlap_Parms), Z_Construct_UFunction_ATower_EndOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_EndOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_EndOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_EndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_EndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_EndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_GetDamage_Statics
	{
		struct Tower_eventGetDamage_Parms
		{
			FDamage ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_GetDamage_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATower_GetDamage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000008000582, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventGetDamage_Parms, ReturnValue), Z_Construct_UScriptStruct_FDamage, METADATA_PARAMS(Z_Construct_UFunction_ATower_GetDamage_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_GetDamage_Statics::NewProp_ReturnValue_MetaData)) }; // 550170420
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_GetDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_GetDamage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_GetDamage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_GetDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "GetDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATower_GetDamage_Statics::Tower_eventGetDamage_Parms), Z_Construct_UFunction_ATower_GetDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_GetDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_GetDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_GetDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_GetDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_GetDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_OnDeselect_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnDeselect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_OnDeselect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "OnDeselect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnDeselect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnDeselect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_OnDeselect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_OnDeselect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_OnHoverBegin_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnHoverBegin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_OnHoverBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "OnHoverBegin", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnHoverBegin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnHoverBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_OnHoverBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_OnHoverBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_OnHoverEnd_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnHoverEnd_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_OnHoverEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "OnHoverEnd", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnHoverEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnHoverEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_OnHoverEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_OnHoverEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_OnRep_Damage_Statics
	{
		struct Tower_eventOnRep_Damage_Parms
		{
			FDamage InOldDamage;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InOldDamage_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_InOldDamage;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnRep_Damage_Statics::NewProp_InOldDamage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ATower_OnRep_Damage_Statics::NewProp_InOldDamage = { "InOldDamage", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventOnRep_Damage_Parms, InOldDamage), Z_Construct_UScriptStruct_FDamage, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnRep_Damage_Statics::NewProp_InOldDamage_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnRep_Damage_Statics::NewProp_InOldDamage_MetaData)) }; // 550170420
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_OnRep_Damage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_OnRep_Damage_Statics::NewProp_InOldDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnRep_Damage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack|Replication" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_OnRep_Damage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "OnRep_Damage", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATower_OnRep_Damage_Statics::Tower_eventOnRep_Damage_Parms), Z_Construct_UFunction_ATower_OnRep_Damage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnRep_Damage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnRep_Damage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnRep_Damage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_OnRep_Damage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_OnRep_Damage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_OnSelect_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnSelect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_OnSelect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "OnSelect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnSelect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnSelect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_OnSelect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_OnSelect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "Comment", "// It will be called in OnRep_Damage when ElementalType changed.\n" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
		{ "ToolTip", "It will be called in OnRep_Damage when ElementalType changed." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "OnTowerElementalDamageTypeChanged", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_SetDamage_Statics
	{
		struct Tower_eventSetDamage_Parms
		{
			float InDamage;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InDamage_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_InDamage;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetDamage_Statics::NewProp_InDamage_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATower_SetDamage_Statics::NewProp_InDamage = { "InDamage", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventSetDamage_Parms, InDamage), METADATA_PARAMS(Z_Construct_UFunction_ATower_SetDamage_Statics::NewProp_InDamage_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetDamage_Statics::NewProp_InDamage_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_SetDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_SetDamage_Statics::NewProp_InDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetDamage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "Comment", "// It's not pure virtual! There is call of parent function.\n" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
		{ "ToolTip", "It's not pure virtual! There is call of parent function." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_SetDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "SetDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATower_SetDamage_Statics::Tower_eventSetDamage_Parms), Z_Construct_UFunction_ATower_SetDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_SetDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_SetDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_SetDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_SetElementalDamageType_Statics
	{
		struct Tower_eventSetElementalDamageType_Parms
		{
			EElementalDamageType InElementalDamageType;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InElementalDamageType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InElementalDamageType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InElementalDamageType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType = { "InElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Tower_eventSetElementalDamageType_Parms, InElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType_MetaData)) }; // 1545068981
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::NewProp_InElementalDamageType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "SetElementalDamageType", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::Tower_eventSetElementalDamageType_Parms), Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_SetElementalDamageType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_SetElementalDamageType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_SetPlacementPreview_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InIsPreview_MetaData[];
#endif
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((Tower_eventSetPlacementPreview_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(Tower_eventSetPlacementPreview_Parms), &Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::NewProp_InIsPreview,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "SetPlacementPreview", nullptr, nullptr, sizeof(Tower_eventSetPlacementPreview_Parms), Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_SetPlacementPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_SetPlacementPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_SetVisibleLocally_Statics
	{
		static void NewProp_IsVisible_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsVisible;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::NewProp_IsVisible_SetBit(void* Obj)
	{
		((Tower_eventSetVisibleLocally_Parms*)Obj)->IsVisible = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::NewProp_IsVisible = { "IsVisible", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(Tower_eventSetVisibleLocally_Parms), &Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::NewProp_IsVisible_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::NewProp_IsVisible,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "SetVisibleLocally", nullptr, nullptr, sizeof(Tower_eventSetVisibleLocally_Parms), Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_SetVisibleLocally()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_SetVisibleLocally_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATower_StartAttack_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATower_StartAttack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|Attack" },
		{ "Comment", "// Call by attack zone component on minions come in the attack zone.\n" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
		{ "ToolTip", "Call by attack zone component on minions come in the attack zone." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATower_StartAttack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATower, nullptr, "StartAttack", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATower_StartAttack_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATower_StartAttack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATower_StartAttack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATower_StartAttack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATower);
	UClass* Z_Construct_UClass_ATower_NoRegister()
	{
		return ATower::StaticClass();
	}
	struct Z_Construct_UClass_ATower_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Damage;
		static const UECodeGen_Private::FBytePropertyParams NewProp_TowerType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TowerType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_TowerType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsPreview_MetaData[];
#endif
		static void NewProp_IsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsPreview;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DefaultRootComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_DefaultRootComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ProjectileSources_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_ProjectileSources;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackZone_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_AttackZone;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SelectionHandler_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_SelectionHandler;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TowerModification_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_TowerModification;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATower_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATower_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATower_Attack, "Attack" }, // 1322060474
		{ &Z_Construct_UFunction_ATower_BeginOverlap, "BeginOverlap" }, // 1169432533
		{ &Z_Construct_UFunction_ATower_EndAttack, "EndAttack" }, // 455080026
		{ &Z_Construct_UFunction_ATower_EndOverlap, "EndOverlap" }, // 3152739029
		{ &Z_Construct_UFunction_ATower_GetDamage, "GetDamage" }, // 2146920553
		{ &Z_Construct_UFunction_ATower_OnDeselect, "OnDeselect" }, // 1162962063
		{ &Z_Construct_UFunction_ATower_OnHoverBegin, "OnHoverBegin" }, // 1168489915
		{ &Z_Construct_UFunction_ATower_OnHoverEnd, "OnHoverEnd" }, // 3823651199
		{ &Z_Construct_UFunction_ATower_OnRep_Damage, "OnRep_Damage" }, // 1148326323
		{ &Z_Construct_UFunction_ATower_OnSelect, "OnSelect" }, // 2768591603
		{ &Z_Construct_UFunction_ATower_OnTowerElementalDamageTypeChanged, "OnTowerElementalDamageTypeChanged" }, // 3280635248
		{ &Z_Construct_UFunction_ATower_SetDamage, "SetDamage" }, // 4250667494
		{ &Z_Construct_UFunction_ATower_SetElementalDamageType, "SetElementalDamageType" }, // 3973057988
		{ &Z_Construct_UFunction_ATower_SetPlacementPreview, "SetPlacementPreview" }, // 759106810
		{ &Z_Construct_UFunction_ATower_SetVisibleLocally, "SetVisibleLocally" }, // 2357844898
		{ &Z_Construct_UFunction_ATower_StartAttack, "StartAttack" }, // 3305183827
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tower/Tower.h" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "Tower" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_Damage = { "Damage", "OnRep_Damage", (EPropertyFlags)0x0010000100000025, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, Damage), Z_Construct_UScriptStruct_FDamage, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_Damage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_Damage_MetaData)) }; // 550170420
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_ATower_Statics::NewProp_TowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_TowerType_MetaData[] = {
		{ "Category", "Tower" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_TowerType = { "TowerType", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, TowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_TowerType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_TowerType_MetaData)) }; // 2535945531
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_IsPreview_MetaData[] = {
		{ "Category", "Tower" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	void Z_Construct_UClass_ATower_Statics::NewProp_IsPreview_SetBit(void* Obj)
	{
		((ATower*)Obj)->IsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_IsPreview = { "IsPreview", nullptr, (EPropertyFlags)0x0010000000000014, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(ATower), &Z_Construct_UClass_ATower_Statics::NewProp_IsPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_IsPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_IsPreview_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_DefaultRootComponent_MetaData[] = {
		{ "Category", "Tower" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_DefaultRootComponent = { "DefaultRootComponent", nullptr, (EPropertyFlags)0x001400000008001c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, DefaultRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_DefaultRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_DefaultRootComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_ProjectileSources_MetaData[] = {
		{ "Category", "Tower" },
		{ "Comment", "/** Under this component will be only components-sources */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
		{ "ToolTip", "Under this component will be only components-sources" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_ProjectileSources = { "ProjectileSources", nullptr, (EPropertyFlags)0x001400000008001c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, ProjectileSources), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_ProjectileSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_ProjectileSources_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_AttackZone_MetaData[] = {
		{ "Category", "Tower" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_AttackZone = { "AttackZone", nullptr, (EPropertyFlags)0x001400000008003c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, AttackZone), Z_Construct_UClass_UAttackZoneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_AttackZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_AttackZone_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_SelectionHandler_MetaData[] = {
		{ "Category", "Tower" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_SelectionHandler = { "SelectionHandler", nullptr, (EPropertyFlags)0x001400000008003c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, SelectionHandler), Z_Construct_UClass_USelectionHandlerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_SelectionHandler_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_SelectionHandler_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATower_Statics::NewProp_TowerModification_MetaData[] = {
		{ "Category", "Tower" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Tower.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ATower_Statics::NewProp_TowerModification = { "TowerModification", nullptr, (EPropertyFlags)0x001400000008003c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATower, TowerModification), Z_Construct_UClass_UTowerModificationComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::NewProp_TowerModification_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::NewProp_TowerModification_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATower_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_Damage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_TowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_TowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_IsPreview,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_DefaultRootComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_ProjectileSources,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_AttackZone,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_SelectionHandler,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATower_Statics::NewProp_TowerModification,
	};
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ATower_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UPlacementPreview_NoRegister, (int32)VTABLE_OFFSET(ATower, IPlacementPreview), false },  // 174827905
			{ Z_Construct_UClass_USelectable_NoRegister, (int32)VTABLE_OFFSET(ATower, ISelectable), false },  // 3916643512
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATower_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATower>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATower_Statics::ClassParams = {
		&ATower::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATower_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATower_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATower_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATower()
	{
		if (!Z_Registration_Info_UClass_ATower.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATower.OuterSingleton, Z_Construct_UClass_ATower_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATower.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<ATower>()
	{
		return ATower::StaticClass();
	}

	void ATower::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_Damage(TEXT("Damage"));
		static const FName Name_AttackZone(TEXT("AttackZone"));
		static const FName Name_SelectionHandler(TEXT("SelectionHandler"));
		static const FName Name_TowerModification(TEXT("TowerModification"));

		const bool bIsValid = true
			&& Name_Damage == ClassReps[(int32)ENetFields_Private::Damage].Property->GetFName()
			&& Name_AttackZone == ClassReps[(int32)ENetFields_Private::AttackZone].Property->GetFName()
			&& Name_SelectionHandler == ClassReps[(int32)ENetFields_Private::SelectionHandler].Property->GetFName()
			&& Name_TowerModification == ClassReps[(int32)ENetFields_Private::TowerModification].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in ATower"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATower);
	ATower::~ATower() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Tower_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Tower_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATower, ATower::StaticClass, TEXT("ATower"), &Z_Registration_Info_UClass_ATower, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATower), 3832674169U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Tower_h_3267844343(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Tower_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Tower_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
