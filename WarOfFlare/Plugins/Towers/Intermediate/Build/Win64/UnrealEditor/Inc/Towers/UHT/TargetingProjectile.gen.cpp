// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/Projectile/TargetingProjectile.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTargetingProjectile() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_AProjectile();
	TOWERS_API UClass* Z_Construct_UClass_ATargetingProjectile();
	TOWERS_API UClass* Z_Construct_UClass_ATargetingProjectile_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UHomingMovementComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	DEFINE_FUNCTION(ATargetingProjectile::execHit)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Hit();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATargetingProjectile::execSetTarget)
	{
		P_GET_OBJECT(USceneComponent,Z_Param_ProjectileTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTarget(Z_Param_ProjectileTarget);
		P_NATIVE_END;
	}
	void ATargetingProjectile::StaticRegisterNativesATargetingProjectile()
	{
		UClass* Class = ATargetingProjectile::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Hit", &ATargetingProjectile::execHit },
			{ "SetTarget", &ATargetingProjectile::execSetTarget },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATargetingProjectile_Hit_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATargetingProjectile_Hit_Statics::Function_MetaDataParams[] = {
		{ "Category", "Projectile|Targeting" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/TargetingProjectile.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATargetingProjectile_Hit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATargetingProjectile, nullptr, "Hit", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATargetingProjectile_Hit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATargetingProjectile_Hit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATargetingProjectile_Hit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATargetingProjectile_Hit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics
	{
		struct TargetingProjectile_eventSetTarget_Parms
		{
			USceneComponent* ProjectileTarget;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ProjectileTarget_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ProjectileTarget;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::NewProp_ProjectileTarget_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::NewProp_ProjectileTarget = { "ProjectileTarget", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetingProjectile_eventSetTarget_Parms, ProjectileTarget), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::NewProp_ProjectileTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::NewProp_ProjectileTarget_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::NewProp_ProjectileTarget,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Projectile|Targeting" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/TargetingProjectile.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATargetingProjectile, nullptr, "SetTarget", nullptr, nullptr, sizeof(Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::TargetingProjectile_eventSetTarget_Parms), Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATargetingProjectile_SetTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ATargetingProjectile_SetTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATargetingProjectile);
	UClass* Z_Construct_UClass_ATargetingProjectile_NoRegister()
	{
		return ATargetingProjectile::StaticClass();
	}
	struct Z_Construct_UClass_ATargetingProjectile_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ProjectileMovement_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_ProjectileMovement;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATargetingProjectile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AProjectile,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATargetingProjectile_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATargetingProjectile_Hit, "Hit" }, // 2268317303
		{ &Z_Construct_UFunction_ATargetingProjectile_SetTarget, "SetTarget" }, // 2514364482
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargetingProjectile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tower/Attack/Projectile/TargetingProjectile.h" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/TargetingProjectile.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATargetingProjectile_Statics::NewProp_ProjectileMovement_MetaData[] = {
		{ "Category", "Projectile" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/TargetingProjectile.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ATargetingProjectile_Statics::NewProp_ProjectileMovement = { "ProjectileMovement", nullptr, (EPropertyFlags)0x002408000008001c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ATargetingProjectile, ProjectileMovement), Z_Construct_UClass_UHomingMovementComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATargetingProjectile_Statics::NewProp_ProjectileMovement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATargetingProjectile_Statics::NewProp_ProjectileMovement_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATargetingProjectile_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATargetingProjectile_Statics::NewProp_ProjectileMovement,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATargetingProjectile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATargetingProjectile>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATargetingProjectile_Statics::ClassParams = {
		&ATargetingProjectile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATargetingProjectile_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATargetingProjectile_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATargetingProjectile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATargetingProjectile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATargetingProjectile()
	{
		if (!Z_Registration_Info_UClass_ATargetingProjectile.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATargetingProjectile.OuterSingleton, Z_Construct_UClass_ATargetingProjectile_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATargetingProjectile.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<ATargetingProjectile>()
	{
		return ATargetingProjectile::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATargetingProjectile);
	ATargetingProjectile::~ATargetingProjectile() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_TargetingProjectile_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_TargetingProjectile_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATargetingProjectile, ATargetingProjectile::StaticClass, TEXT("ATargetingProjectile"), &Z_Registration_Info_UClass_ATargetingProjectile, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATargetingProjectile), 2926984338U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_TargetingProjectile_h_2169970(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_TargetingProjectile_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_TargetingProjectile_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
