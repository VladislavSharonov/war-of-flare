// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/TowerModificationComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerModificationComponent() {}
// Cross Module References
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_ATower_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UTowerModificationComponent();
	TOWERS_API UClass* Z_Construct_UClass_UTowerModificationComponent_NoRegister();
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_EAttackZoneType();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	DEFINE_FUNCTION(UTowerModificationComponent::execServer_TryBuyElementalDamage)
	{
		P_GET_ENUM(EElementalDamageType,Z_Param_InElementalDamageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Server_TryBuyElementalDamage_Implementation(EElementalDamageType(Z_Param_InElementalDamageType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerModificationComponent::execServer_TryBuyAttackZone)
	{
		P_GET_ENUM(EAttackZoneType,Z_Param_InAttackZoneType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Server_TryBuyAttackZone_Implementation(EAttackZoneType(Z_Param_InAttackZoneType));
		P_NATIVE_END;
	}
	struct TowerModificationComponent_eventServer_TryBuyAttackZone_Parms
	{
		EAttackZoneType InAttackZoneType;
	};
	struct TowerModificationComponent_eventServer_TryBuyElementalDamage_Parms
	{
		EElementalDamageType InElementalDamageType;
	};
	static FName NAME_UTowerModificationComponent_Server_TryBuyAttackZone = FName(TEXT("Server_TryBuyAttackZone"));
	void UTowerModificationComponent::Server_TryBuyAttackZone(const EAttackZoneType InAttackZoneType)
	{
		TowerModificationComponent_eventServer_TryBuyAttackZone_Parms Parms;
		Parms.InAttackZoneType=InAttackZoneType;
		ProcessEvent(FindFunctionChecked(NAME_UTowerModificationComponent_Server_TryBuyAttackZone),&Parms);
	}
	static FName NAME_UTowerModificationComponent_Server_TryBuyElementalDamage = FName(TEXT("Server_TryBuyElementalDamage"));
	void UTowerModificationComponent::Server_TryBuyElementalDamage(const EElementalDamageType InElementalDamageType)
	{
		TowerModificationComponent_eventServer_TryBuyElementalDamage_Parms Parms;
		Parms.InElementalDamageType=InElementalDamageType;
		ProcessEvent(FindFunctionChecked(NAME_UTowerModificationComponent_Server_TryBuyElementalDamage),&Parms);
	}
	void UTowerModificationComponent::StaticRegisterNativesUTowerModificationComponent()
	{
		UClass* Class = UTowerModificationComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Server_TryBuyAttackZone", &UTowerModificationComponent::execServer_TryBuyAttackZone },
			{ "Server_TryBuyElementalDamage", &UTowerModificationComponent::execServer_TryBuyElementalDamage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics
	{
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAttackZoneType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InAttackZoneType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAttackZoneType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType = { "InAttackZoneType", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerModificationComponent_eventServer_TryBuyAttackZone_Parms, InAttackZoneType), Z_Construct_UEnum_Towers_EAttackZoneType, METADATA_PARAMS(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType_MetaData)) }; // 3810168422
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::NewProp_InAttackZoneType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerModificationComponent, nullptr, "Server_TryBuyAttackZone", nullptr, nullptr, sizeof(TowerModificationComponent_eventServer_TryBuyAttackZone_Parms), Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04220C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics
	{
		static const UECodeGen_Private::FBytePropertyParams NewProp_InElementalDamageType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InElementalDamageType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InElementalDamageType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType = { "InElementalDamageType", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerModificationComponent_eventServer_TryBuyElementalDamage_Parms, InElementalDamageType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType_MetaData)) }; // 1545068981
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::NewProp_InElementalDamageType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerModificationComponent, nullptr, "Server_TryBuyElementalDamage", nullptr, nullptr, sizeof(TowerModificationComponent_eventServer_TryBuyElementalDamage_Parms), Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04220C40, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UTowerModificationComponent);
	UClass* Z_Construct_UClass_UTowerModificationComponent_NoRegister()
	{
		return UTowerModificationComponent::StaticClass();
	}
	struct Z_Construct_UClass_UTowerModificationComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CanAttackZoneBeModified_MetaData[];
#endif
		static void NewProp_CanAttackZoneBeModified_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_CanAttackZoneBeModified;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CanElementalDamageBeModified_MetaData[];
#endif
		static void NewProp_CanElementalDamageBeModified_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_CanElementalDamageBeModified;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackZonePrices_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_AttackZonePrices;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DamageElementPrices_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_DamageElementPrices;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Tower_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Tower;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTowerModificationComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTowerModificationComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyAttackZone, "Server_TryBuyAttackZone" }, // 896811186
		{ &Z_Construct_UFunction_UTowerModificationComponent_Server_TryBuyElementalDamage, "Server_TryBuyElementalDamage" }, // 518277634
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerModificationComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Tower/TowerModificationComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified_MetaData[] = {
		{ "Category", "TowerModificationComponent" },
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
	};
#endif
	void Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified_SetBit(void* Obj)
	{
		((UTowerModificationComponent*)Obj)->CanAttackZoneBeModified = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified = { "CanAttackZoneBeModified", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UTowerModificationComponent), &Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified_MetaData[] = {
		{ "Category", "TowerModificationComponent" },
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
	};
#endif
	void Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified_SetBit(void* Obj)
	{
		((UTowerModificationComponent*)Obj)->CanElementalDamageBeModified = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified = { "CanElementalDamageBeModified", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UTowerModificationComponent), &Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_AttackZonePrices_MetaData[] = {
		{ "Category", "TowerModificationComponent" },
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
		{ "RowType", "Price" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_AttackZonePrices = { "AttackZonePrices", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerModificationComponent, AttackZonePrices), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_AttackZonePrices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_AttackZonePrices_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_DamageElementPrices_MetaData[] = {
		{ "Category", "TowerModificationComponent" },
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
		{ "RowType", "Price" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_DamageElementPrices = { "DamageElementPrices", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerModificationComponent, DamageElementPrices), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_DamageElementPrices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_DamageElementPrices_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_Tower_MetaData[] = {
		{ "Category", "TowerModificationComponent" },
		{ "ModuleRelativePath", "Public/Tower/TowerModificationComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_Tower = { "Tower", nullptr, (EPropertyFlags)0x0020080000020015, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerModificationComponent, Tower), Z_Construct_UClass_ATower_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_Tower_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_Tower_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTowerModificationComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanAttackZoneBeModified,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_CanElementalDamageBeModified,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_AttackZonePrices,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_DamageElementPrices,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerModificationComponent_Statics::NewProp_Tower,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTowerModificationComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTowerModificationComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UTowerModificationComponent_Statics::ClassParams = {
		&UTowerModificationComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTowerModificationComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTowerModificationComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerModificationComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTowerModificationComponent()
	{
		if (!Z_Registration_Info_UClass_UTowerModificationComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UTowerModificationComponent.OuterSingleton, Z_Construct_UClass_UTowerModificationComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UTowerModificationComponent.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<UTowerModificationComponent>()
	{
		return UTowerModificationComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTowerModificationComponent);
	UTowerModificationComponent::~UTowerModificationComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UTowerModificationComponent, UTowerModificationComponent::StaticClass, TEXT("UTowerModificationComponent"), &Z_Registration_Info_UClass_UTowerModificationComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UTowerModificationComponent), 3791588944U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_2571774888(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
