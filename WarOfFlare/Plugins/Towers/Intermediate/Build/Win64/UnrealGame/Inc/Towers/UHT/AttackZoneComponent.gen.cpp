// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/AttackZone/AttackZoneComponent.h"
#include "../../Source/Runtime/Engine/Classes/Engine/HitResult.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAttackZoneComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	GRID_API UClass* Z_Construct_UClass_UGridComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_AAttackZoneBlock_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UAttackZoneComponent();
	TOWERS_API UClass* Z_Construct_UClass_UAttackZoneComponent_NoRegister();
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_EAttackZoneType();
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature();
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Towers, nullptr, "StartAttack__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FStartAttack_DelegateWrapper(const FMulticastScriptDelegate& StartAttack)
{
	StartAttack.ProcessMulticastDelegate<UObject>(NULL);
}
	struct Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Minions began to enter the attack zone\n" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "Minions began to enter the attack zone" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Towers, nullptr, "EndAttack__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FEndAttack_DelegateWrapper(const FMulticastScriptDelegate& EndAttack)
{
	EndAttack.ProcessMulticastDelegate<UObject>(NULL);
}
	DEFINE_FUNCTION(UAttackZoneComponent::execOnAttackZoneBlockDestroyed)
	{
		P_GET_OBJECT(AActor,Z_Param_DestroyedActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnAttackZoneBlockDestroyed(Z_Param_DestroyedActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execOnEndOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execOnBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execSetPreviewForBlocks)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_GET_TARRAY_REF(AAttackZoneBlock*,Z_Param_Out_AttackZoneBlocks);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPreviewForBlocks(Z_Param_InIsPreview,Z_Param_Out_AttackZoneBlocks);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execDestroyBlock)
	{
		P_GET_OBJECT(AAttackZoneBlock,Z_Param_AttackZoneBlock);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DestroyBlock_Implementation(Z_Param_AttackZoneBlock);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execDeinitializeCollider)
	{
		P_GET_OBJECT(AAttackZoneBlock,Z_Param_AttackZoneBlock);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeinitializeCollider_Implementation(Z_Param_AttackZoneBlock);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execInitializeCollider)
	{
		P_GET_OBJECT(AAttackZoneBlock,Z_Param_AttackZoneBlock);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitializeCollider_Implementation(Z_Param_AttackZoneBlock);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execGetAttackZoneType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EAttackZoneType*)Z_Param__Result=P_THIS->GetAttackZoneType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execOnRep_AttackColliders)
	{
		P_GET_TARRAY_REF(AAttackZoneBlock*,Z_Param_Out_OldAttackColliders);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_AttackColliders(Z_Param_Out_OldAttackColliders);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execSetPreview)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPreview_Implementation(Z_Param_InIsPreview);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execGetTargetsInZone)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Targets);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTargetsInZone(Z_Param_Out_Targets);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execClearZone)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearZone();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execReconstructZone)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ReconstructZone();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAttackZoneComponent::execSetZone)
	{
		P_GET_ENUM(EAttackZoneType,Z_Param_InAttackZoneType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetZone(EAttackZoneType(Z_Param_InAttackZoneType));
		P_NATIVE_END;
	}
	struct AttackZoneComponent_eventDeinitializeCollider_Parms
	{
		AAttackZoneBlock* AttackZoneBlock;
	};
	struct AttackZoneComponent_eventDestroyBlock_Parms
	{
		AAttackZoneBlock* AttackZoneBlock;
	};
	struct AttackZoneComponent_eventInitializeCollider_Parms
	{
		AAttackZoneBlock* AttackZoneBlock;
	};
	struct AttackZoneComponent_eventSetPreview_Parms
	{
		bool InIsPreview;
	};
	static FName NAME_UAttackZoneComponent_DeinitializeCollider = FName(TEXT("DeinitializeCollider"));
	void UAttackZoneComponent::DeinitializeCollider(AAttackZoneBlock* AttackZoneBlock)
	{
		AttackZoneComponent_eventDeinitializeCollider_Parms Parms;
		Parms.AttackZoneBlock=AttackZoneBlock;
		ProcessEvent(FindFunctionChecked(NAME_UAttackZoneComponent_DeinitializeCollider),&Parms);
	}
	static FName NAME_UAttackZoneComponent_DestroyBlock = FName(TEXT("DestroyBlock"));
	void UAttackZoneComponent::DestroyBlock(AAttackZoneBlock* AttackZoneBlock)
	{
		AttackZoneComponent_eventDestroyBlock_Parms Parms;
		Parms.AttackZoneBlock=AttackZoneBlock;
		ProcessEvent(FindFunctionChecked(NAME_UAttackZoneComponent_DestroyBlock),&Parms);
	}
	static FName NAME_UAttackZoneComponent_InitializeCollider = FName(TEXT("InitializeCollider"));
	void UAttackZoneComponent::InitializeCollider(AAttackZoneBlock* AttackZoneBlock)
	{
		AttackZoneComponent_eventInitializeCollider_Parms Parms;
		Parms.AttackZoneBlock=AttackZoneBlock;
		ProcessEvent(FindFunctionChecked(NAME_UAttackZoneComponent_InitializeCollider),&Parms);
	}
	static FName NAME_UAttackZoneComponent_SetPreview = FName(TEXT("SetPreview"));
	void UAttackZoneComponent::SetPreview(bool InIsPreview)
	{
		AttackZoneComponent_eventSetPreview_Parms Parms;
		Parms.InIsPreview=InIsPreview ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UAttackZoneComponent_SetPreview),&Parms);
	}
	void UAttackZoneComponent::StaticRegisterNativesUAttackZoneComponent()
	{
		UClass* Class = UAttackZoneComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClearZone", &UAttackZoneComponent::execClearZone },
			{ "DeinitializeCollider", &UAttackZoneComponent::execDeinitializeCollider },
			{ "DestroyBlock", &UAttackZoneComponent::execDestroyBlock },
			{ "GetAttackZoneType", &UAttackZoneComponent::execGetAttackZoneType },
			{ "GetTargetsInZone", &UAttackZoneComponent::execGetTargetsInZone },
			{ "InitializeCollider", &UAttackZoneComponent::execInitializeCollider },
			{ "OnAttackZoneBlockDestroyed", &UAttackZoneComponent::execOnAttackZoneBlockDestroyed },
			{ "OnBeginOverlap", &UAttackZoneComponent::execOnBeginOverlap },
			{ "OnEndOverlap", &UAttackZoneComponent::execOnEndOverlap },
			{ "OnRep_AttackColliders", &UAttackZoneComponent::execOnRep_AttackColliders },
			{ "ReconstructZone", &UAttackZoneComponent::execReconstructZone },
			{ "SetPreview", &UAttackZoneComponent::execSetPreview },
			{ "SetPreviewForBlocks", &UAttackZoneComponent::execSetPreviewForBlocks },
			{ "SetZone", &UAttackZoneComponent::execSetZone },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_ClearZone_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_ClearZone_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_ClearZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "ClearZone", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_ClearZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_ClearZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_ClearZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_ClearZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AttackZoneBlock;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::NewProp_AttackZoneBlock = { "AttackZoneBlock", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventDeinitializeCollider_Parms, AttackZoneBlock), Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::NewProp_AttackZoneBlock,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "DeinitializeCollider", nullptr, nullptr, sizeof(AttackZoneComponent_eventDeinitializeCollider_Parms), Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AttackZoneBlock;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::NewProp_AttackZoneBlock = { "AttackZoneBlock", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventDestroyBlock_Parms, AttackZoneBlock), Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::NewProp_AttackZoneBlock,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "DestroyBlock", nullptr, nullptr, sizeof(AttackZoneComponent_eventDestroyBlock_Parms), Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics
	{
		struct AttackZoneComponent_eventGetAttackZoneType_Parms
		{
			EAttackZoneType ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventGetAttackZoneType_Parms, ReturnValue), Z_Construct_UEnum_Towers_EAttackZoneType, METADATA_PARAMS(nullptr, 0) }; // 3810168422
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "GetAttackZoneType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::AttackZoneComponent_eventGetAttackZoneType_Parms), Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics
	{
		struct AttackZoneComponent_eventGetTargetsInZone_Parms
		{
			TArray<AActor*> Targets;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Targets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Targets;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::NewProp_Targets_Inner = { "Targets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::NewProp_Targets = { "Targets", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventGetTargetsInZone_Parms, Targets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::NewProp_Targets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::NewProp_Targets,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "GetTargetsInZone", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::AttackZoneComponent_eventGetTargetsInZone_Parms), Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics
	{
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AttackZoneBlock;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::NewProp_AttackZoneBlock = { "AttackZoneBlock", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventInitializeCollider_Parms, AttackZoneBlock), Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::NewProp_AttackZoneBlock,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "InitializeCollider", nullptr, nullptr, sizeof(AttackZoneComponent_eventInitializeCollider_Parms), Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics
	{
		struct AttackZoneComponent_eventOnAttackZoneBlockDestroyed_Parms
		{
			AActor* DestroyedActor;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DestroyedActor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::NewProp_DestroyedActor = { "DestroyedActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnAttackZoneBlockDestroyed_Parms, DestroyedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::NewProp_DestroyedActor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// reqired\n" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "reqired" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "OnAttackZoneBlockDestroyed", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::AttackZoneComponent_eventOnAttackZoneBlockDestroyed_Parms), Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics
	{
		struct AttackZoneComponent_eventOnBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnBeginOverlap_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OverlappedComp_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnBeginOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((AttackZoneComponent_eventOnBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AttackZoneComponent_eventOnBeginOverlap_Parms), &Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_SweepResult_MetaData)) }; // 1287526515
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OverlappedComp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherComp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_bFromSweep,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::NewProp_SweepResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// reqired\n" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "reqired" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "OnBeginOverlap", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::AttackZoneComponent_eventOnBeginOverlap_Parms), Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00440401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics
	{
		struct AttackZoneComponent_eventOnEndOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UECodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnEndOverlap_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OverlappedComp_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnEndOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnEndOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OverlappedComp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherComp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::NewProp_OtherBodyIndex,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// reqired\n" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "reqired" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "OnEndOverlap", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::AttackZoneComponent_eventOnEndOverlap_Parms), Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics
	{
		struct AttackZoneComponent_eventOnRep_AttackColliders_Parms
		{
			TArray<AAttackZoneBlock*> OldAttackColliders;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OldAttackColliders_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OldAttackColliders_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_OldAttackColliders;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders_Inner = { "OldAttackColliders", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders = { "OldAttackColliders", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventOnRep_AttackColliders_Parms, OldAttackColliders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::NewProp_OldAttackColliders,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::Function_MetaDataParams[] = {
		{ "Category", "Tower|AttackZone|Replication" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "OnRep_AttackColliders", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::AttackZoneComponent_eventOnRep_AttackColliders_Parms), Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "ReconstructZone", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics
	{
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((AttackZoneComponent_eventSetPreview_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AttackZoneComponent_eventSetPreview_Parms), &Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::NewProp_InIsPreview,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "SetPreview", nullptr, nullptr, sizeof(AttackZoneComponent_eventSetPreview_Parms), Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_SetPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_SetPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics
	{
		struct AttackZoneComponent_eventSetPreviewForBlocks_Parms
		{
			bool InIsPreview;
			TArray<AAttackZoneBlock*> AttackZoneBlocks;
		};
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AttackZoneBlocks_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackZoneBlocks_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_AttackZoneBlocks;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((AttackZoneComponent_eventSetPreviewForBlocks_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AttackZoneComponent_eventSetPreviewForBlocks_Parms), &Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks_Inner = { "AttackZoneBlocks", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks = { "AttackZoneBlocks", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventSetPreviewForBlocks_Parms, AttackZoneBlocks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_InIsPreview,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::NewProp_AttackZoneBlocks,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "SetPreviewForBlocks", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::AttackZoneComponent_eventSetPreviewForBlocks_Parms), Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04480401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics
	{
		struct AttackZoneComponent_eventSetZone_Parms
		{
			EAttackZoneType InAttackZoneType;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InAttackZoneType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InAttackZoneType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::NewProp_InAttackZoneType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::NewProp_InAttackZoneType = { "InAttackZoneType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneComponent_eventSetZone_Parms, InAttackZoneType), Z_Construct_UEnum_Towers_EAttackZoneType, METADATA_PARAMS(nullptr, 0) }; // 3810168422
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::NewProp_InAttackZoneType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::NewProp_InAttackZoneType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::Function_MetaDataParams[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAttackZoneComponent, nullptr, "SetZone", nullptr, nullptr, sizeof(Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::AttackZoneComponent_eventSetZone_Parms), Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAttackZoneComponent_SetZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UAttackZoneComponent_SetZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAttackZoneComponent);
	UClass* Z_Construct_UClass_UAttackZoneComponent_NoRegister()
	{
		return UAttackZoneComponent::StaticClass();
	}
	struct Z_Construct_UClass_UAttackZoneComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AttackColliders_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackColliders_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_AttackColliders;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Grid_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_Grid;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackZoneBlockClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_AttackZoneBlockClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnStartAttack_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStartAttack;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnEndAttack_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnEndAttack;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsUnbound_MetaData[];
#endif
		static void NewProp_IsUnbound_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsUnbound;
		static const UECodeGen_Private::FBytePropertyParams NewProp_AttackZoneType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackZoneType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_AttackZoneType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AttackPatterns_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_AttackPatterns;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsPreview_MetaData[];
#endif
		static void NewProp_IsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsPreview;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_TargetsInZone_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TargetsInZone_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_TargetsInZone;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAttackZoneComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAttackZoneComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAttackZoneComponent_ClearZone, "ClearZone" }, // 2707273015
		{ &Z_Construct_UFunction_UAttackZoneComponent_DeinitializeCollider, "DeinitializeCollider" }, // 2139985002
		{ &Z_Construct_UFunction_UAttackZoneComponent_DestroyBlock, "DestroyBlock" }, // 1554595176
		{ &Z_Construct_UFunction_UAttackZoneComponent_GetAttackZoneType, "GetAttackZoneType" }, // 2858366856
		{ &Z_Construct_UFunction_UAttackZoneComponent_GetTargetsInZone, "GetTargetsInZone" }, // 2974534351
		{ &Z_Construct_UFunction_UAttackZoneComponent_InitializeCollider, "InitializeCollider" }, // 696668617
		{ &Z_Construct_UFunction_UAttackZoneComponent_OnAttackZoneBlockDestroyed, "OnAttackZoneBlockDestroyed" }, // 665295378
		{ &Z_Construct_UFunction_UAttackZoneComponent_OnBeginOverlap, "OnBeginOverlap" }, // 928669216
		{ &Z_Construct_UFunction_UAttackZoneComponent_OnEndOverlap, "OnEndOverlap" }, // 1572558515
		{ &Z_Construct_UFunction_UAttackZoneComponent_OnRep_AttackColliders, "OnRep_AttackColliders" }, // 2978070857
		{ &Z_Construct_UFunction_UAttackZoneComponent_ReconstructZone, "ReconstructZone" }, // 4252259521
		{ &Z_Construct_UFunction_UAttackZoneComponent_SetPreview, "SetPreview" }, // 1942275450
		{ &Z_Construct_UFunction_UAttackZoneComponent_SetPreviewForBlocks, "SetPreviewForBlocks" }, // 3392767578
		{ &Z_Construct_UFunction_UAttackZoneComponent_SetZone, "SetZone" }, // 3010428041
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "Comment", "// All minions have left the attack zone\n" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "All minions have left the attack zone" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders_Inner = { "AttackColliders", nullptr, (EPropertyFlags)0x0000000000020000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders = { "AttackColliders", "OnRep_AttackColliders", (EPropertyFlags)0x0010000100020835, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, AttackColliders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_Grid_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "Comment", "// Items can be nullptr due to replication.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "Items can be nullptr due to replication." },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_Grid = { "Grid", nullptr, (EPropertyFlags)0x00140000000a002d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, Grid), Z_Construct_UClass_UGridComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_Grid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_Grid_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneBlockClass_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneBlockClass = { "AttackZoneBlockClass", nullptr, (EPropertyFlags)0x0014000000010015, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, AttackZoneBlockClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AAttackZoneBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneBlockClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneBlockClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnStartAttack_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnStartAttack = { "OnStartAttack", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, OnStartAttack), Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnStartAttack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnStartAttack_MetaData)) }; // 2470510072
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnEndAttack_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnEndAttack = { "OnEndAttack", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, OnEndAttack), Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnEndAttack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnEndAttack_MetaData)) }; // 1402424331
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	void Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound_SetBit(void* Obj)
	{
		((UAttackZoneComponent*)Obj)->IsUnbound = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound = { "IsUnbound", nullptr, (EPropertyFlags)0x0010000000000025, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UAttackZoneComponent), &Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType_MetaData[] = {
		{ "Category", "AttackZone" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType = { "AttackZoneType", nullptr, (EPropertyFlags)0x0020080000000025, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, AttackZoneType), Z_Construct_UEnum_Towers_EAttackZoneType, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType_MetaData)) }; // 3810168422
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackPatterns_MetaData[] = {
		{ "Category", "AttackZoneComponent" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "RowType", "AttackZoneCoordinates" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackPatterns = { "AttackPatterns", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, AttackPatterns), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackPatterns_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackPatterns_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview_MetaData[] = {
		{ "Category", "AttackZoneComponent" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
	};
#endif
	void Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview_SetBit(void* Obj)
	{
		((UAttackZoneComponent*)Obj)->IsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview = { "IsPreview", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UAttackZoneComponent), &Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone_Inner = { "TargetsInZone", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone_MetaData[] = {
		{ "Comment", "// Every player has they own state of preview.\n" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneComponent.h" },
		{ "ToolTip", "Every player has they own state of preview." },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone = { "TargetsInZone", nullptr, (EPropertyFlags)0x0020080000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UAttackZoneComponent, TargetsInZone), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAttackZoneComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackColliders,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_Grid,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneBlockClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnStartAttack,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_OnEndAttack,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsUnbound,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackZoneType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_AttackPatterns,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_IsPreview,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAttackZoneComponent_Statics::NewProp_TargetsInZone,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAttackZoneComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAttackZoneComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAttackZoneComponent_Statics::ClassParams = {
		&UAttackZoneComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAttackZoneComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAttackZoneComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAttackZoneComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAttackZoneComponent()
	{
		if (!Z_Registration_Info_UClass_UAttackZoneComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAttackZoneComponent.OuterSingleton, Z_Construct_UClass_UAttackZoneComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAttackZoneComponent.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<UAttackZoneComponent>()
	{
		return UAttackZoneComponent::StaticClass();
	}

	void UAttackZoneComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_AttackColliders(TEXT("AttackColliders"));
		static const FName Name_Grid(TEXT("Grid"));
		static const FName Name_IsUnbound(TEXT("IsUnbound"));
		static const FName Name_AttackZoneType(TEXT("AttackZoneType"));

		const bool bIsValid = true
			&& Name_AttackColliders == ClassReps[(int32)ENetFields_Private::AttackColliders].Property->GetFName()
			&& Name_Grid == ClassReps[(int32)ENetFields_Private::Grid].Property->GetFName()
			&& Name_IsUnbound == ClassReps[(int32)ENetFields_Private::IsUnbound].Property->GetFName()
			&& Name_AttackZoneType == ClassReps[(int32)ENetFields_Private::AttackZoneType].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UAttackZoneComponent"));
	}
	UAttackZoneComponent::UAttackZoneComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAttackZoneComponent);
	UAttackZoneComponent::~UAttackZoneComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAttackZoneComponent, UAttackZoneComponent::StaticClass, TEXT("UAttackZoneComponent"), &Z_Registration_Info_UClass_UAttackZoneComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAttackZoneComponent), 1371192534U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_3885403096(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
