// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/TowerType.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerType() {}
// Cross Module References
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_ETowerType();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_ETowerType;
	static UEnum* ETowerType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_ETowerType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_ETowerType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_Towers_ETowerType, (UObject*)Z_Construct_UPackage__Script_Towers(), TEXT("ETowerType"));
		}
		return Z_Registration_Info_UEnum_ETowerType.OuterSingleton;
	}
	template<> TOWERS_API UEnum* StaticEnum<ETowerType>()
	{
		return ETowerType_StaticEnum();
	}
	struct Z_Construct_UEnum_Towers_ETowerType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_Towers_ETowerType_Statics::Enumerators[] = {
		{ "ETowerType::None", (int64)ETowerType::None },
		{ "ETowerType::SingleTarget", (int64)ETowerType::SingleTarget },
		{ "ETowerType::MultiTarget", (int64)ETowerType::MultiTarget },
		{ "ETowerType::AreaDamage", (int64)ETowerType::AreaDamage },
		{ "ETowerType::MassDamage", (int64)ETowerType::MassDamage },
		{ "ETowerType::Machinegun", (int64)ETowerType::Machinegun },
		{ "ETowerType::Sniper", (int64)ETowerType::Sniper },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_Towers_ETowerType_Statics::Enum_MetaDataParams[] = {
		{ "AreaDamage.DisplayName", "AreaDamage" },
		{ "AreaDamage.Name", "ETowerType::AreaDamage" },
		{ "BlueprintType", "true" },
		{ "Machinegun.DisplayName", "Machinegun" },
		{ "Machinegun.Name", "ETowerType::Machinegun" },
		{ "MassDamage.DisplayName", "MassDamage" },
		{ "MassDamage.Name", "ETowerType::MassDamage" },
		{ "ModuleRelativePath", "Public/Tower/TowerType.h" },
		{ "MultiTarget.DisplayName", "MultiTarget" },
		{ "MultiTarget.Name", "ETowerType::MultiTarget" },
		{ "None.DisplayName", "None" },
		{ "None.Hidden", "" },
		{ "None.Name", "ETowerType::None" },
		{ "SingleTarget.DisplayName", "SingleTarget" },
		{ "SingleTarget.Name", "ETowerType::SingleTarget" },
		{ "Sniper.DisplayName", "Sniper" },
		{ "Sniper.Name", "ETowerType::Sniper" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_Towers_ETowerType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_Towers,
		nullptr,
		"ETowerType",
		"ETowerType",
		Z_Construct_UEnum_Towers_ETowerType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_Towers_ETowerType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_Towers_ETowerType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_Towers_ETowerType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_Towers_ETowerType()
	{
		if (!Z_Registration_Info_UEnum_ETowerType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_ETowerType.InnerSingleton, Z_Construct_UEnum_Towers_ETowerType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_ETowerType.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerType_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerType_h_Statics::EnumInfo[] = {
		{ ETowerType_StaticEnum, TEXT("ETowerType"), &Z_Registration_Info_UEnum_ETowerType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 2535945531U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerType_h_23410450(TEXT("/Script/Towers"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerType_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerType_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
