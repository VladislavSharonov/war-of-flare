// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/Projectile/HomingMovementComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHomingMovementComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UHomingMovementComponent();
	TOWERS_API UClass* Z_Construct_UClass_UHomingMovementComponent_NoRegister();
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/HomingMovementComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Towers, nullptr, "TargetReachedSignature__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FTargetReachedSignature_DelegateWrapper(const FMulticastScriptDelegate& TargetReachedSignature)
{
	TargetReachedSignature.ProcessMulticastDelegate<UObject>(NULL);
}
	void UHomingMovementComponent::StaticRegisterNativesUHomingMovementComponent()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UHomingMovementComponent);
	UClass* Z_Construct_UClass_UHomingMovementComponent_NoRegister()
	{
		return UHomingMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UHomingMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Speed_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Speed;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HomingTargetComponent_MetaData[];
#endif
		static const UECodeGen_Private::FWeakObjectPropertyParams NewProp_HomingTargetComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnTargetReached_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnTargetReached;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHomingMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHomingMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Tower/Attack/Projectile/HomingMovementComponent.h" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/HomingMovementComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_Speed_MetaData[] = {
		{ "Category", "Projectile|Homing" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/HomingMovementComponent.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_Speed = { "Speed", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UHomingMovementComponent, Speed), METADATA_PARAMS(Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_Speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_Speed_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_HomingTargetComponent_MetaData[] = {
		{ "Category", "Projectile|Homing" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/HomingMovementComponent.h" },
	};
#endif
	const UECodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_HomingTargetComponent = { "HomingTargetComponent", nullptr, (EPropertyFlags)0x001400000008000d, UECodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UHomingMovementComponent, HomingTargetComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_HomingTargetComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_HomingTargetComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_OnTargetReached_MetaData[] = {
		{ "Category", "Projectile|Homing" },
		{ "ModuleRelativePath", "Public/Tower/Attack/Projectile/HomingMovementComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_OnTargetReached = { "OnTargetReached", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UHomingMovementComponent, OnTargetReached), Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_OnTargetReached_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_OnTargetReached_MetaData)) }; // 2545827234
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHomingMovementComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_Speed,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_HomingTargetComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHomingMovementComponent_Statics::NewProp_OnTargetReached,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHomingMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHomingMovementComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UHomingMovementComponent_Statics::ClassParams = {
		&UHomingMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHomingMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHomingMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UHomingMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHomingMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHomingMovementComponent()
	{
		if (!Z_Registration_Info_UClass_UHomingMovementComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UHomingMovementComponent.OuterSingleton, Z_Construct_UClass_UHomingMovementComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UHomingMovementComponent.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<UHomingMovementComponent>()
	{
		return UHomingMovementComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHomingMovementComponent);
	UHomingMovementComponent::~UHomingMovementComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UHomingMovementComponent, UHomingMovementComponent::StaticClass, TEXT("UHomingMovementComponent"), &Z_Registration_Info_UClass_UHomingMovementComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UHomingMovementComponent), 583056701U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_3944724908(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_Projectile_HomingMovementComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
