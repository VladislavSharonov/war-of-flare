// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/Attack/TargetFiltersLibrary.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
enum class ETowerType : uint8;
#ifdef TOWERS_TargetFiltersLibrary_generated_h
#error "TargetFiltersLibrary.generated.h already included, missing '#pragma once' in TargetFiltersLibrary.h"
#endif
#define TOWERS_TargetFiltersLibrary_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMaxHealthFilter); \
	DECLARE_FUNCTION(execCountFilter); \
	DECLARE_FUNCTION(execDoNotFilter); \
	DECLARE_FUNCTION(execFilterForTowerType);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMaxHealthFilter); \
	DECLARE_FUNCTION(execCountFilter); \
	DECLARE_FUNCTION(execDoNotFilter); \
	DECLARE_FUNCTION(execFilterForTowerType);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTargetFiltersLibrary(); \
	friend struct Z_Construct_UClass_UTargetFiltersLibrary_Statics; \
public: \
	DECLARE_CLASS(UTargetFiltersLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UTargetFiltersLibrary)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUTargetFiltersLibrary(); \
	friend struct Z_Construct_UClass_UTargetFiltersLibrary_Statics; \
public: \
	DECLARE_CLASS(UTargetFiltersLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UTargetFiltersLibrary)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTargetFiltersLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTargetFiltersLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTargetFiltersLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTargetFiltersLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTargetFiltersLibrary(UTargetFiltersLibrary&&); \
	NO_API UTargetFiltersLibrary(const UTargetFiltersLibrary&); \
public: \
	NO_API virtual ~UTargetFiltersLibrary();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTargetFiltersLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTargetFiltersLibrary(UTargetFiltersLibrary&&); \
	NO_API UTargetFiltersLibrary(const UTargetFiltersLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTargetFiltersLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTargetFiltersLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTargetFiltersLibrary) \
	NO_API virtual ~UTargetFiltersLibrary();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_12_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERS_API UClass* StaticClass<class UTargetFiltersLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
