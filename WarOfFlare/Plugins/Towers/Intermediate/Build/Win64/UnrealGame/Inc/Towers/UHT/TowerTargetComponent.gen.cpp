// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/TowerTargetComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerTargetComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	TOWERS_API UClass* Z_Construct_UClass_UTowerTargetComponent();
	TOWERS_API UClass* Z_Construct_UClass_UTowerTargetComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	void UTowerTargetComponent::StaticRegisterNativesUTowerTargetComponent()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UTowerTargetComponent);
	UClass* Z_Construct_UClass_UTowerTargetComponent_NoRegister()
	{
		return UTowerTargetComponent::StaticClass();
	}
	struct Z_Construct_UClass_UTowerTargetComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTowerTargetComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerTargetComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Tower/TowerTargetComponent.h" },
		{ "ModuleRelativePath", "Public/Tower/TowerTargetComponent.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTowerTargetComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTowerTargetComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UTowerTargetComponent_Statics::ClassParams = {
		&UTowerTargetComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTowerTargetComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerTargetComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTowerTargetComponent()
	{
		if (!Z_Registration_Info_UClass_UTowerTargetComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UTowerTargetComponent.OuterSingleton, Z_Construct_UClass_UTowerTargetComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UTowerTargetComponent.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<UTowerTargetComponent>()
	{
		return UTowerTargetComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTowerTargetComponent);
	UTowerTargetComponent::~UTowerTargetComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerTargetComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerTargetComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UTowerTargetComponent, UTowerTargetComponent::StaticClass, TEXT("UTowerTargetComponent"), &Z_Registration_Info_UClass_UTowerTargetComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UTowerTargetComponent), 3677542443U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerTargetComponent_h_1666924396(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerTargetComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerTargetComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
