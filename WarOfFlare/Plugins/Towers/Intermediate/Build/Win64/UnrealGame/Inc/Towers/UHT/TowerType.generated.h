// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/TowerType.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERS_TowerType_generated_h
#error "TowerType.generated.h already included, missing '#pragma once' in TowerType.h"
#endif
#define TOWERS_TowerType_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerType_h


#define FOREACH_ENUM_ETOWERTYPE(op) \
	op(ETowerType::None) \
	op(ETowerType::SingleTarget) \
	op(ETowerType::MultiTarget) \
	op(ETowerType::AreaDamage) \
	op(ETowerType::MassDamage) \
	op(ETowerType::Machinegun) \
	op(ETowerType::Sniper) 

enum class ETowerType : uint8;
template<> struct TIsUEnumClass<ETowerType> { enum { Value = true }; };
template<> TOWERS_API UEnum* StaticEnum<ETowerType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
