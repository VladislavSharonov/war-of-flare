// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/TargetFiltersLibrary.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTargetFiltersLibrary() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	TOWERS_API UClass* Z_Construct_UClass_UTargetFiltersLibrary();
	TOWERS_API UClass* Z_Construct_UClass_UTargetFiltersLibrary_NoRegister();
	TOWERS_API UEnum* Z_Construct_UEnum_Towers_ETowerType();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	DEFINE_FUNCTION(UTargetFiltersLibrary::execMaxHealthFilter)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_InTargets);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_OutFilteredTargets);
		P_FINISH;
		P_NATIVE_BEGIN;
		UTargetFiltersLibrary::MaxHealthFilter(Z_Param_Out_InTargets,Z_Param_Out_OutFilteredTargets);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTargetFiltersLibrary::execCountFilter)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InTargetCount);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_InTargets);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_OutFilteredTargets);
		P_FINISH;
		P_NATIVE_BEGIN;
		UTargetFiltersLibrary::CountFilter(Z_Param_InTargetCount,Z_Param_Out_InTargets,Z_Param_Out_OutFilteredTargets);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTargetFiltersLibrary::execDoNotFilter)
	{
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_InTargets);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_OutFilteredTargets);
		P_FINISH;
		P_NATIVE_BEGIN;
		UTargetFiltersLibrary::DoNotFilter(Z_Param_Out_InTargets,Z_Param_Out_OutFilteredTargets);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTargetFiltersLibrary::execFilterForTowerType)
	{
		P_GET_ENUM(ETowerType,Z_Param_InTowerType);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_InTargets);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_OutFilteredTargets);
		P_FINISH;
		P_NATIVE_BEGIN;
		UTargetFiltersLibrary::FilterForTowerType(ETowerType(Z_Param_InTowerType),Z_Param_Out_InTargets,Z_Param_Out_OutFilteredTargets);
		P_NATIVE_END;
	}
	void UTargetFiltersLibrary::StaticRegisterNativesUTargetFiltersLibrary()
	{
		UClass* Class = UTargetFiltersLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CountFilter", &UTargetFiltersLibrary::execCountFilter },
			{ "DoNotFilter", &UTargetFiltersLibrary::execDoNotFilter },
			{ "FilterForTowerType", &UTargetFiltersLibrary::execFilterForTowerType },
			{ "MaxHealthFilter", &UTargetFiltersLibrary::execMaxHealthFilter },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics
	{
		struct TargetFiltersLibrary_eventCountFilter_Parms
		{
			int32 InTargetCount;
			TArray<AActor*> InTargets;
			TArray<AActor*> OutFilteredTargets;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_InTargetCount;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InTargets;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OutFilteredTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_OutFilteredTargets;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_InTargetCount = { "InTargetCount", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventCountFilter_Parms, InTargetCount), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_InTargets_Inner = { "InTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_InTargets = { "InTargets", nullptr, (EPropertyFlags)0x0010000008000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventCountFilter_Parms, InTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_OutFilteredTargets_Inner = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_OutFilteredTargets = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventCountFilter_Parms, OutFilteredTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_InTargetCount,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_InTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_InTargets,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_OutFilteredTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::NewProp_OutFilteredTargets,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "TargetFilter" },
		{ "ModuleRelativePath", "Public/Tower/Attack/TargetFiltersLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTargetFiltersLibrary, nullptr, "CountFilter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::TargetFiltersLibrary_eventCountFilter_Parms), Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics
	{
		struct TargetFiltersLibrary_eventDoNotFilter_Parms
		{
			TArray<AActor*> InTargets;
			TArray<AActor*> OutFilteredTargets;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InTargets;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OutFilteredTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_OutFilteredTargets;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_InTargets_Inner = { "InTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_InTargets = { "InTargets", nullptr, (EPropertyFlags)0x0010000008000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventDoNotFilter_Parms, InTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_OutFilteredTargets_Inner = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_OutFilteredTargets = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventDoNotFilter_Parms, OutFilteredTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_InTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_InTargets,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_OutFilteredTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::NewProp_OutFilteredTargets,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "TargetFilter" },
		{ "ModuleRelativePath", "Public/Tower/Attack/TargetFiltersLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTargetFiltersLibrary, nullptr, "DoNotFilter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::TargetFiltersLibrary_eventDoNotFilter_Parms), Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics
	{
		struct TargetFiltersLibrary_eventFilterForTowerType_Parms
		{
			ETowerType InTowerType;
			TArray<AActor*> InTargets;
			TArray<AActor*> OutFilteredTargets;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InTowerType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InTowerType;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InTargets;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OutFilteredTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_OutFilteredTargets;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTowerType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTowerType = { "InTowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventFilterForTowerType_Parms, InTowerType), Z_Construct_UEnum_Towers_ETowerType, METADATA_PARAMS(nullptr, 0) }; // 2535945531
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTargets_Inner = { "InTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTargets = { "InTargets", nullptr, (EPropertyFlags)0x0010000008000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventFilterForTowerType_Parms, InTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_OutFilteredTargets_Inner = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_OutFilteredTargets = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventFilterForTowerType_Parms, OutFilteredTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTowerType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTowerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_InTargets,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_OutFilteredTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::NewProp_OutFilteredTargets,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::Function_MetaDataParams[] = {
		{ "Category", "TargetFilter" },
		{ "ModuleRelativePath", "Public/Tower/Attack/TargetFiltersLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTargetFiltersLibrary, nullptr, "FilterForTowerType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::TargetFiltersLibrary_eventFilterForTowerType_Parms), Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics
	{
		struct TargetFiltersLibrary_eventMaxHealthFilter_Parms
		{
			TArray<AActor*> InTargets;
			TArray<AActor*> OutFilteredTargets;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InTargets;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OutFilteredTargets_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_OutFilteredTargets;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_InTargets_Inner = { "InTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_InTargets = { "InTargets", nullptr, (EPropertyFlags)0x0010000008000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventMaxHealthFilter_Parms, InTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_OutFilteredTargets_Inner = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_OutFilteredTargets = { "OutFilteredTargets", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TargetFiltersLibrary_eventMaxHealthFilter_Parms, OutFilteredTargets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_InTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_InTargets,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_OutFilteredTargets_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::NewProp_OutFilteredTargets,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "TargetFilter" },
		{ "ModuleRelativePath", "Public/Tower/Attack/TargetFiltersLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTargetFiltersLibrary, nullptr, "MaxHealthFilter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::TargetFiltersLibrary_eventMaxHealthFilter_Parms), Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UTargetFiltersLibrary);
	UClass* Z_Construct_UClass_UTargetFiltersLibrary_NoRegister()
	{
		return UTargetFiltersLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UTargetFiltersLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTargetFiltersLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTargetFiltersLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTargetFiltersLibrary_CountFilter, "CountFilter" }, // 2426563956
		{ &Z_Construct_UFunction_UTargetFiltersLibrary_DoNotFilter, "DoNotFilter" }, // 404944089
		{ &Z_Construct_UFunction_UTargetFiltersLibrary_FilterForTowerType, "FilterForTowerType" }, // 3994180402
		{ &Z_Construct_UFunction_UTargetFiltersLibrary_MaxHealthFilter, "MaxHealthFilter" }, // 4050700751
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTargetFiltersLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tower/Attack/TargetFiltersLibrary.h" },
		{ "ModuleRelativePath", "Public/Tower/Attack/TargetFiltersLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTargetFiltersLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTargetFiltersLibrary>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UTargetFiltersLibrary_Statics::ClassParams = {
		&UTargetFiltersLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTargetFiltersLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTargetFiltersLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTargetFiltersLibrary()
	{
		if (!Z_Registration_Info_UClass_UTargetFiltersLibrary.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UTargetFiltersLibrary.OuterSingleton, Z_Construct_UClass_UTargetFiltersLibrary_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UTargetFiltersLibrary.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<UTargetFiltersLibrary>()
	{
		return UTargetFiltersLibrary::StaticClass();
	}
	UTargetFiltersLibrary::UTargetFiltersLibrary(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTargetFiltersLibrary);
	UTargetFiltersLibrary::~UTargetFiltersLibrary() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UTargetFiltersLibrary, UTargetFiltersLibrary::StaticClass, TEXT("UTargetFiltersLibrary"), &Z_Registration_Info_UClass_UTargetFiltersLibrary, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UTargetFiltersLibrary), 787593263U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_3697686910(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_TargetFiltersLibrary_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
