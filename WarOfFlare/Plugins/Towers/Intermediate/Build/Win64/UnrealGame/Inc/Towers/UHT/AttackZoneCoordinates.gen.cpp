// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/AttackZone/AttackZoneCoordinates.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAttackZoneCoordinates() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	TOWERS_API UScriptStruct* Z_Construct_UScriptStruct_FAttackZoneCoordinates();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References

static_assert(std::is_polymorphic<FAttackZoneCoordinates>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FAttackZoneCoordinates cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_AttackZoneCoordinates;
class UScriptStruct* FAttackZoneCoordinates::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_AttackZoneCoordinates.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_AttackZoneCoordinates.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FAttackZoneCoordinates, (UObject*)Z_Construct_UPackage__Script_Towers(), TEXT("AttackZoneCoordinates"));
	}
	return Z_Registration_Info_UScriptStruct_AttackZoneCoordinates.OuterSingleton;
}
template<> TOWERS_API UScriptStruct* StaticStruct<FAttackZoneCoordinates>()
{
	return FAttackZoneCoordinates::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FStructPropertyParams NewProp_RelativeLocations_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RelativeLocations_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_RelativeLocations;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneCoordinates.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAttackZoneCoordinates>();
	}
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations_Inner = { "RelativeLocations", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations_MetaData[] = {
		{ "Category", "AttackZoneCoordinates" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneCoordinates.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations = { "RelativeLocations", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FAttackZoneCoordinates, RelativeLocations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewProp_RelativeLocations,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"AttackZoneCoordinates",
		sizeof(FAttackZoneCoordinates),
		alignof(FAttackZoneCoordinates),
		Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAttackZoneCoordinates()
	{
		if (!Z_Registration_Info_UScriptStruct_AttackZoneCoordinates.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_AttackZoneCoordinates.InnerSingleton, Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_AttackZoneCoordinates.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h_Statics::ScriptStructInfo[] = {
		{ FAttackZoneCoordinates::StaticStruct, Z_Construct_UScriptStruct_FAttackZoneCoordinates_Statics::NewStructOps, TEXT("AttackZoneCoordinates"), &Z_Registration_Info_UScriptStruct_AttackZoneCoordinates, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FAttackZoneCoordinates), 3496738030U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h_4216221699(TEXT("/Script/Towers"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneCoordinates_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
