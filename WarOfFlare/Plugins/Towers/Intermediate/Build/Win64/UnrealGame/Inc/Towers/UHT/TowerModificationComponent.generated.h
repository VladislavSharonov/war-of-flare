// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/TowerModificationComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EAttackZoneType : uint8;
enum class EElementalDamageType : uint8;
#ifdef TOWERS_TowerModificationComponent_generated_h
#error "TowerModificationComponent.generated.h already included, missing '#pragma once' in TowerModificationComponent.h"
#endif
#define TOWERS_TowerModificationComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_RPC_WRAPPERS \
	virtual void Server_TryBuyElementalDamage_Implementation(const EElementalDamageType InElementalDamageType); \
	virtual void Server_TryBuyAttackZone_Implementation(const EAttackZoneType InAttackZoneType); \
 \
	DECLARE_FUNCTION(execServer_TryBuyElementalDamage); \
	DECLARE_FUNCTION(execServer_TryBuyAttackZone);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void Server_TryBuyElementalDamage_Implementation(const EElementalDamageType InElementalDamageType); \
	virtual void Server_TryBuyAttackZone_Implementation(const EAttackZoneType InAttackZoneType); \
 \
	DECLARE_FUNCTION(execServer_TryBuyElementalDamage); \
	DECLARE_FUNCTION(execServer_TryBuyAttackZone);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTowerModificationComponent(); \
	friend struct Z_Construct_UClass_UTowerModificationComponent_Statics; \
public: \
	DECLARE_CLASS(UTowerModificationComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UTowerModificationComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTowerModificationComponent(); \
	friend struct Z_Construct_UClass_UTowerModificationComponent_Statics; \
public: \
	DECLARE_CLASS(UTowerModificationComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UTowerModificationComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTowerModificationComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerModificationComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerModificationComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerModificationComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerModificationComponent(UTowerModificationComponent&&); \
	NO_API UTowerModificationComponent(const UTowerModificationComponent&); \
public: \
	NO_API virtual ~UTowerModificationComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerModificationComponent(UTowerModificationComponent&&); \
	NO_API UTowerModificationComponent(const UTowerModificationComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerModificationComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerModificationComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTowerModificationComponent) \
	NO_API virtual ~UTowerModificationComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERS_API UClass* StaticClass<class UTowerModificationComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_TowerModificationComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
