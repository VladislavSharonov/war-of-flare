// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/Attack/AttackZone/AttackZoneComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class AAttackZoneBlock;
class UPrimitiveComponent;
enum class EAttackZoneType : uint8;
struct FHitResult;
#ifdef TOWERS_AttackZoneComponent_generated_h
#error "AttackZoneComponent.generated.h already included, missing '#pragma once' in AttackZoneComponent.h"
#endif
#define TOWERS_AttackZoneComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_17_DELEGATE \
TOWERS_API void FStartAttack_DelegateWrapper(const FMulticastScriptDelegate& StartAttack);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_18_DELEGATE \
TOWERS_API void FEndAttack_DelegateWrapper(const FMulticastScriptDelegate& EndAttack);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_RPC_WRAPPERS \
	virtual void DestroyBlock_Implementation(AAttackZoneBlock* AttackZoneBlock); \
	virtual void DeinitializeCollider_Implementation(AAttackZoneBlock* AttackZoneBlock); \
	virtual void InitializeCollider_Implementation(AAttackZoneBlock* AttackZoneBlock); \
	virtual void SetPreview_Implementation(bool InIsPreview); \
 \
	DECLARE_FUNCTION(execOnAttackZoneBlockDestroyed); \
	DECLARE_FUNCTION(execOnEndOverlap); \
	DECLARE_FUNCTION(execOnBeginOverlap); \
	DECLARE_FUNCTION(execSetPreviewForBlocks); \
	DECLARE_FUNCTION(execDestroyBlock); \
	DECLARE_FUNCTION(execDeinitializeCollider); \
	DECLARE_FUNCTION(execInitializeCollider); \
	DECLARE_FUNCTION(execGetAttackZoneType); \
	DECLARE_FUNCTION(execOnRep_AttackColliders); \
	DECLARE_FUNCTION(execSetPreview); \
	DECLARE_FUNCTION(execGetTargetsInZone); \
	DECLARE_FUNCTION(execClearZone); \
	DECLARE_FUNCTION(execReconstructZone); \
	DECLARE_FUNCTION(execSetZone);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnAttackZoneBlockDestroyed); \
	DECLARE_FUNCTION(execOnEndOverlap); \
	DECLARE_FUNCTION(execOnBeginOverlap); \
	DECLARE_FUNCTION(execSetPreviewForBlocks); \
	DECLARE_FUNCTION(execDestroyBlock); \
	DECLARE_FUNCTION(execDeinitializeCollider); \
	DECLARE_FUNCTION(execInitializeCollider); \
	DECLARE_FUNCTION(execGetAttackZoneType); \
	DECLARE_FUNCTION(execOnRep_AttackColliders); \
	DECLARE_FUNCTION(execSetPreview); \
	DECLARE_FUNCTION(execGetTargetsInZone); \
	DECLARE_FUNCTION(execClearZone); \
	DECLARE_FUNCTION(execReconstructZone); \
	DECLARE_FUNCTION(execSetZone);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAttackZoneComponent(); \
	friend struct Z_Construct_UClass_UAttackZoneComponent_Statics; \
public: \
	DECLARE_CLASS(UAttackZoneComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UAttackZoneComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		AttackColliders=NETFIELD_REP_START, \
		Grid, \
		IsUnbound, \
		AttackZoneType, \
		NETFIELD_REP_END=AttackZoneType	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUAttackZoneComponent(); \
	friend struct Z_Construct_UClass_UAttackZoneComponent_Statics; \
public: \
	DECLARE_CLASS(UAttackZoneComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Towers"), NO_API) \
	DECLARE_SERIALIZER(UAttackZoneComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		AttackColliders=NETFIELD_REP_START, \
		Grid, \
		IsUnbound, \
		AttackZoneType, \
		NETFIELD_REP_END=AttackZoneType	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAttackZoneComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAttackZoneComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAttackZoneComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAttackZoneComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAttackZoneComponent(UAttackZoneComponent&&); \
	NO_API UAttackZoneComponent(const UAttackZoneComponent&); \
public: \
	NO_API virtual ~UAttackZoneComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAttackZoneComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAttackZoneComponent(UAttackZoneComponent&&); \
	NO_API UAttackZoneComponent(const UAttackZoneComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAttackZoneComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAttackZoneComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAttackZoneComponent) \
	NO_API virtual ~UAttackZoneComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_20_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERS_API UClass* StaticClass<class UAttackZoneComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
