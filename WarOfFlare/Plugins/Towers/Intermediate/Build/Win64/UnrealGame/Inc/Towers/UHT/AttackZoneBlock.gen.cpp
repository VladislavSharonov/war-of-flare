// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Towers/Public/Tower/Attack/AttackZone/AttackZoneBlock.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAttackZoneBlock() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_AAttackZoneBlock();
	TOWERS_API UClass* Z_Construct_UClass_AAttackZoneBlock_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Towers();
// End Cross Module References
	DEFINE_FUNCTION(AAttackZoneBlock::execGetNetOwner)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(const AActor**)Z_Param__Result=P_THIS->GetNetOwner();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAttackZoneBlock::execRemoveOwner)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveOwner();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAttackZoneBlock::execAddOwner)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddOwner();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAttackZoneBlock::execGetOwnerCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetOwnerCount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AAttackZoneBlock::execSetPreview)
	{
		P_GET_UBOOL(Z_Param_InIsPreview);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPreview_Implementation(Z_Param_InIsPreview);
		P_NATIVE_END;
	}
	struct AttackZoneBlock_eventSetPreview_Parms
	{
		bool InIsPreview;
	};
	static FName NAME_AAttackZoneBlock_SetPreview = FName(TEXT("SetPreview"));
	void AAttackZoneBlock::SetPreview(bool InIsPreview)
	{
		AttackZoneBlock_eventSetPreview_Parms Parms;
		Parms.InIsPreview=InIsPreview ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_AAttackZoneBlock_SetPreview),&Parms);
	}
	void AAttackZoneBlock::StaticRegisterNativesAAttackZoneBlock()
	{
		UClass* Class = AAttackZoneBlock::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddOwner", &AAttackZoneBlock::execAddOwner },
			{ "GetNetOwner", &AAttackZoneBlock::execGetNetOwner },
			{ "GetOwnerCount", &AAttackZoneBlock::execGetOwnerCount },
			{ "RemoveOwner", &AAttackZoneBlock::execRemoveOwner },
			{ "SetPreview", &AAttackZoneBlock::execSetPreview },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AAttackZoneBlock_AddOwner_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAttackZoneBlock_AddOwner_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAttackZoneBlock_AddOwner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAttackZoneBlock, nullptr, "AddOwner", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAttackZoneBlock_AddOwner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_AddOwner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAttackZoneBlock_AddOwner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAttackZoneBlock_AddOwner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics
	{
		struct AttackZoneBlock_eventGetNetOwner_Parms
		{
			const AActor* ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneBlock_eventGetNetOwner_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::NewProp_ReturnValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::Function_MetaDataParams[] = {
		{ "Category", "Actor" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAttackZoneBlock, nullptr, "GetNetOwner", nullptr, nullptr, sizeof(Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::AttackZoneBlock_eventGetNetOwner_Parms), Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics
	{
		struct AttackZoneBlock_eventGetOwnerCount_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AttackZoneBlock_eventGetOwnerCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAttackZoneBlock, nullptr, "GetOwnerCount", nullptr, nullptr, sizeof(Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::AttackZoneBlock_eventGetOwnerCount_Parms), Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAttackZoneBlock, nullptr, "RemoveOwner", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics
	{
		static void NewProp_InIsPreview_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsPreview;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::NewProp_InIsPreview_SetBit(void* Obj)
	{
		((AttackZoneBlock_eventSetPreview_Parms*)Obj)->InIsPreview = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::NewProp_InIsPreview = { "InIsPreview", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(AttackZoneBlock_eventSetPreview_Parms), &Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::NewProp_InIsPreview_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::NewProp_InIsPreview,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::Function_MetaDataParams[] = {
		{ "Category", "Preview" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AAttackZoneBlock, nullptr, "SetPreview", nullptr, nullptr, sizeof(AttackZoneBlock_eventSetPreview_Parms), Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AAttackZoneBlock_SetPreview()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AAttackZoneBlock_SetPreview_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AAttackZoneBlock);
	UClass* Z_Construct_UClass_AAttackZoneBlock_NoRegister()
	{
		return AAttackZoneBlock::StaticClass();
	}
	struct Z_Construct_UClass_AAttackZoneBlock_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DefaultRootComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_DefaultRootComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Collider_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_Collider;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OwnerCount_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_OwnerCount;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAttackZoneBlock_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Towers,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AAttackZoneBlock_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AAttackZoneBlock_AddOwner, "AddOwner" }, // 2492211552
		{ &Z_Construct_UFunction_AAttackZoneBlock_GetNetOwner, "GetNetOwner" }, // 3068272187
		{ &Z_Construct_UFunction_AAttackZoneBlock_GetOwnerCount, "GetOwnerCount" }, // 138567191
		{ &Z_Construct_UFunction_AAttackZoneBlock_RemoveOwner, "RemoveOwner" }, // 3784553617
		{ &Z_Construct_UFunction_AAttackZoneBlock_SetPreview, "SetPreview" }, // 1633556990
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAttackZoneBlock_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Tower/Attack/AttackZone/AttackZoneBlock.h" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_DefaultRootComponent_MetaData[] = {
		{ "Category", "AttackZone|Block" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_DefaultRootComponent = { "DefaultRootComponent", nullptr, (EPropertyFlags)0x001400000008001c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AAttackZoneBlock, DefaultRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_DefaultRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_DefaultRootComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_Collider_MetaData[] = {
		{ "Category", "AttackZone|Block" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_Collider = { "Collider", nullptr, (EPropertyFlags)0x00140000000a081d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AAttackZoneBlock, Collider), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_Collider_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_Collider_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_OwnerCount_MetaData[] = {
		{ "Category", "AttackZone|Block" },
		{ "ModuleRelativePath", "Public/Tower/Attack/AttackZone/AttackZoneBlock.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_OwnerCount = { "OwnerCount", nullptr, (EPropertyFlags)0x0020080000020815, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AAttackZoneBlock, OwnerCount), METADATA_PARAMS(Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_OwnerCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_OwnerCount_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAttackZoneBlock_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_DefaultRootComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_Collider,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAttackZoneBlock_Statics::NewProp_OwnerCount,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAttackZoneBlock_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAttackZoneBlock>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AAttackZoneBlock_Statics::ClassParams = {
		&AAttackZoneBlock::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AAttackZoneBlock_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AAttackZoneBlock_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAttackZoneBlock_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAttackZoneBlock_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAttackZoneBlock()
	{
		if (!Z_Registration_Info_UClass_AAttackZoneBlock.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AAttackZoneBlock.OuterSingleton, Z_Construct_UClass_AAttackZoneBlock_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AAttackZoneBlock.OuterSingleton;
	}
	template<> TOWERS_API UClass* StaticClass<AAttackZoneBlock>()
	{
		return AAttackZoneBlock::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAttackZoneBlock);
	AAttackZoneBlock::~AAttackZoneBlock() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneBlock_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneBlock_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AAttackZoneBlock, AAttackZoneBlock::StaticClass, TEXT("AAttackZoneBlock"), &Z_Registration_Info_UClass_AAttackZoneBlock, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AAttackZoneBlock), 2574970463U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneBlock_h_1323375860(TEXT("/Script/Towers"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneBlock_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneBlock_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
