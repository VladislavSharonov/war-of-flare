// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowers_init() {}
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature();
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_ProjectileHitSignature__DelegateSignature();
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature();
	TOWERS_API UFunction* Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_Towers;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_Towers()
	{
		if (!Z_Registration_Info_UPackage__Script_Towers.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_Towers_EndAttack__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Towers_ProjectileHitSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Towers_StartAttack__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Towers_TargetReachedSignature__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/Towers",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x037AF686,
				0x04D872D6,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_Towers.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_Towers.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_Towers(Z_Construct_UPackage__Script_Towers, TEXT("/Script/Towers"), Z_Registration_Info_UPackage__Script_Towers, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x037AF686, 0x04D872D6));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
