// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Tower/Attack/AttackZone/AttackZoneType.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERS_AttackZoneType_generated_h
#error "AttackZoneType.generated.h already included, missing '#pragma once' in AttackZoneType.h"
#endif
#define TOWERS_AttackZoneType_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Towers_Source_Towers_Public_Tower_Attack_AttackZone_AttackZoneType_h


#define FOREACH_ENUM_EATTACKZONETYPE(op) \
	op(EAttackZoneType::Empty) \
	op(EAttackZoneType::Radius1) \
	op(EAttackZoneType::Cross3) \
	op(EAttackZoneType::Cross5) \
	op(EAttackZoneType::Radius2) \
	op(EAttackZoneType::Radius3) \
	op(EAttackZoneType::EvenAlternation) \
	op(EAttackZoneType::OddAlternation) 

enum class EAttackZoneType : uint8;
template<> struct TIsUEnumClass<EAttackZoneType> { enum { Value = true }; };
template<> TOWERS_API UEnum* StaticEnum<EAttackZoneType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
