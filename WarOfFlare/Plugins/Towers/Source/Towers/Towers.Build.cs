﻿// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Towers : ModuleRules
{
	public Towers(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"ElementalDamage",
				"Grid",
				"ObjectPlacement",
				"ObjectSelection",
				"Health", 
				"Money"
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
			}
			);
	}
}
