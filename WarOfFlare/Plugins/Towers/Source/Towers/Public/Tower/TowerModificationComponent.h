// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"

#include "Attack/AttackZone/AttackZoneType.h"
#include "ElementalDamage/ElementalDamageType.h"

#include "TowerModificationComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOWERS_API UTowerModificationComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UTowerModificationComponent();

	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_TryBuyAttackZone(const EAttackZoneType InAttackZoneType);
	
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void Server_TryBuyElementalDamage(const EElementalDamageType InElementalDamageType);
	
protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool CanAttackZoneBeModified = true;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool CanElementalDamageBeModified = true;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(RowType="Price"))
	TObjectPtr<UDataTable> AttackZonePrices = nullptr;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(RowType="Price"))
	TObjectPtr<UDataTable> DamageElementPrices = nullptr;
	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class ATower* Tower = nullptr;
};
