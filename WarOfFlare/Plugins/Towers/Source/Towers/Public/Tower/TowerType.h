﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"

#include "TowerType.generated.h"

UENUM(BlueprintType)
enum class ETowerType : uint8 
{
    None            UMETA(Hidden, DisplayName = "None"),
    SingleTarget    UMETA(DisplayName = "SingleTarget"),
    MultiTarget     UMETA(DisplayName = "MultiTarget"),
    AreaDamage      UMETA(DisplayName = "AreaDamage"),
    MassDamage      UMETA(DisplayName = "MassDamage"),
    Machinegun      UMETA(DisplayName = "Machinegun"),
    Sniper          UMETA(DisplayName = "Sniper")
};

ENUM_RANGE_BY_FIRST_AND_LAST(ETowerType, ETowerType::SingleTarget, ETowerType::Sniper);