﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "NativeGameplayTags.h"
#include "Components/ActorComponent.h"

#include "TowerTargetComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOWERS_API UTowerTargetComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UTowerTargetComponent();

protected:
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
};