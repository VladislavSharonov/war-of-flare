﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "TowerType.h"
#include "ObjectPlacement/IPlacementPreview.h"
#include "Attack/AttackZone/AttackZoneComponent.h"
#include "Attack/AttackZone/AttackZoneType.h"
#include "Grid/GridComponent.h"
#include "ObjectSelection/ISelectable.h"
#include "ElementalDamage/Damage.h"
#include "ObjectSelection/SelectionHandlerComponent.h"
#include "TowerModificationComponent.h"

#include "Tower.generated.h"

UCLASS()
class TOWERS_API ATower : public AActor, public IPlacementPreview, public ISelectable
{
	GENERATED_BODY()
	
public:
	ATower();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION( )
	void BeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	
	UFUNCTION( )
	void EndOverlap(AActor* OverlappedActor, AActor* OtherActor);
	
public:	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Tower")
	void SetVisibleLocally(bool IsVisible);
	virtual void SetVisibleLocally_Implementation(bool IsVisible);

#pragma region Attack
public:
	// Call by attack zone component on minions come in the attack zone.
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Tower|Attack")
	void StartAttack();
	virtual void StartAttack_Implementation() {} // It's not pure virtual! There is call of parent function.

	// Call by attack zone component on minions come out the attack zone.
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Tower|Attack")
	void EndAttack();
	virtual void EndAttack_Implementation() {} // It's not pure virtual! There is call of parent function.
	
	UFUNCTION(BlueprintCallable, Category = "Tower|Attack")
	void SetDamage(const float InDamage);
	
	UFUNCTION(BlueprintCallable, Category = "Tower|Attack")
	void SetElementalDamageType(const EElementalDamageType InElementalDamageType);
	
	// It will be called in OnRep_Damage when ElementalType changed.
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Tower|Attack")
	void OnTowerElementalDamageTypeChanged();
	virtual void OnTowerElementalDamageTypeChanged_Implementation() {};

	UFUNCTION(Category = "Tower|Attack|Replication")
	void OnRep_Damage(const FDamage& InOldDamage);

	UFUNCTION(BlueprintCallable, Category = "Tower|Attack")
	const FDamage& GetDamage() const { return Damage; };
	
protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Tower|Attack")
	void Attack();
	virtual void Attack_Implementation() {} // It's not pure virtual! There is call of parent function.

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing = OnRep_Damage, Category = "Tower")
	FDamage Damage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tower")
	ETowerType TowerType = ETowerType::None;
#pragma endregion

#pragma region IPlacementPreview
public:
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Preview")
    void SetPlacementPreview(const bool InIsPreview);

public:
    UPROPERTY(BlueprintReadOnly, Category = "Tower")
    bool IsPreview = false;
#pragma endregion

#pragma region ISelectable
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnSelect();
	virtual void OnSelect_Implementation() override { }
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnDeselect();
	virtual void OnDeselect_Implementation() override { }
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHoverBegin();
	virtual void OnHoverBegin_Implementation() override { }
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHoverEnd();
	virtual void OnHoverEnd_Implementation() override { }
#pragma endregion
	
#pragma region Components
public:
	UPROPERTY(BlueprintReadOnly, Category = "Tower")
	TObjectPtr<USceneComponent> DefaultRootComponent;

	/** Under this component will be only components-sources */
	UPROPERTY(BlueprintReadOnly, Category = "Tower")
	TObjectPtr<USceneComponent> ProjectileSources;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Tower")
	TObjectPtr<UAttackZoneComponent> AttackZone;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Tower")
	TObjectPtr<USelectionHandlerComponent> SelectionHandler;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Tower")
	TObjectPtr<UTowerModificationComponent> TowerModification;
#pragma endregion
};
