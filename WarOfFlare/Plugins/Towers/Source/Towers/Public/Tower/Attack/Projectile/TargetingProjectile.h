﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "HomingMovementComponent.h"
#include "GameFramework/Actor.h"

#include "Projectile.h"

#include "TargetingProjectile.generated.h"

UCLASS()
class TOWERS_API ATargetingProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:	
	ATargetingProjectile();

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION(BlueprintCallable, Category = "Projectile|Targeting")
	void SetTarget(USceneComponent* ProjectileTarget) const;

	virtual void Tick(float DeltaTime) override;
	
protected:
	UFUNCTION(BlueprintCallable, Category = "Projectile|Targeting")
	void Hit();

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Projectile")
	TObjectPtr<UHomingMovementComponent> ProjectileMovement;
};
