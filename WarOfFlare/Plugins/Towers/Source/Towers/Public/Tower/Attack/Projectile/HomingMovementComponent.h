// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "HomingMovementComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTargetReachedSignature);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERS_API UHomingMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHomingMovementComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Projectile|Homing")
	float Speed = 800.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Projectile|Homing")
	TWeakObjectPtr<USceneComponent> HomingTargetComponent;

	UPROPERTY(BlueprintAssignable, Category = "Projectile|Homing")
	FTargetReachedSignature OnTargetReached;
};
