﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Projectile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FProjectileHitSignature, AActor*, Target);

UCLASS()
class TOWERS_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Projectile")
	TObjectPtr<USceneComponent> DefaultRootComponent;

	UPROPERTY(BlueprintAssignable, Category = "Projectile")
	FProjectileHitSignature OnHit;
};
