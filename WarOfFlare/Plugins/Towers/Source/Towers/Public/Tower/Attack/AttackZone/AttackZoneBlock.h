﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"

#include "AttackZoneBlock.generated.h"

UCLASS()
class TOWERS_API AAttackZoneBlock : public AActor
{
	GENERATED_BODY()
	
public:
	AAttackZoneBlock();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Preview")
	void SetPreview(bool InIsPreview);
	virtual void SetPreview_Implementation(bool InIsPreview) {};

	UFUNCTION(BlueprintCallable, Category = "Preview")
	int32 GetOwnerCount() const { return OwnerCount;  }

	UFUNCTION(BlueprintCallable, Category = "Preview")
	void AddOwner() { ++OwnerCount; }

	UFUNCTION(BlueprintCallable, Category = "Preview")
	void RemoveOwner() { --OwnerCount; }

protected:
	UFUNCTION(BlueprintPure, Category = "Actor")
	virtual const AActor* GetNetOwner() const override { return Super::GetNetOwner(); }

public:
	UPROPERTY(BlueprintReadOnly, Category = "AttackZone|Block")
	TObjectPtr<USceneComponent> DefaultRootComponent;
	
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "AttackZone|Block")
	TObjectPtr<UBoxComponent> Collider;

protected:
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "AttackZone|Block")
	int32 OwnerCount = 0;
};
