﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "Tower/TowerType.h"

#include "TargetFiltersLibrary.generated.h"

UCLASS()
class TOWERS_API UTargetFiltersLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "TargetFilter")
	static void FilterForTowerType(ETowerType InTowerType, UPARAM(ref) TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets);

	UFUNCTION(BlueprintCallable, Category = "TargetFilter")
	static void DoNotFilter(UPARAM(ref) TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets);

	UFUNCTION(BlueprintCallable, Category = "TargetFilter")
	static void CountFilter(int32 InTargetCount, UPARAM(ref) TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets);

	UFUNCTION(BlueprintCallable, Category = "TargetFilter")
	static void MaxHealthFilter(UPARAM(ref) TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets);
};
