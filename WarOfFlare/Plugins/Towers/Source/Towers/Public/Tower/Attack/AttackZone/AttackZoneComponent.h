﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Math/Vector2D.h"

#include "Grid/GridComponent.h"
#include "AttackZoneBlock.h"
#include "AttackZoneCoordinates.h"
#include "AttackZoneType.h"

#include "AttackZoneComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStartAttack); // Minions began to enter the attack zone
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEndAttack); // All minions have left the attack zone

UCLASS( Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class TOWERS_API UAttackZoneComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable, Category = "AttackZone")
	void SetZone(EAttackZoneType InAttackZoneType);

	UFUNCTION(BlueprintCallable, Category = "AttackZone")
	void ReconstructZone();

	UFUNCTION(BlueprintCallable, Category = "AttackZone")
	void ClearZone();

	UFUNCTION(BlueprintCallable, Category = "AttackZone")
	void GetTargetsInZone(TArray<AActor*>& Targets);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AttackZone")
	void SetPreview(bool InIsPreview);
	virtual void SetPreview_Implementation(bool InIsPreview);

	UFUNCTION(Category = "Tower|AttackZone|Replication")
	void OnRep_AttackColliders(const TArray<AAttackZoneBlock*>& OldAttackColliders);
	
	UFUNCTION(BlueprintPure)
	EAttackZoneType GetAttackZoneType() const { return AttackZoneType; }
	
public:
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, ReplicatedUsing=OnRep_AttackColliders, Category = "AttackZone")
	TArray<AAttackZoneBlock*> AttackColliders; // Items can be nullptr due to replication.

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Replicated, Category = "AttackZone")
	TObjectPtr<UGridComponent> Grid;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AttackZone")
	TSubclassOf<AAttackZoneBlock> AttackZoneBlockClass = AAttackZoneBlock::StaticClass();

	UPROPERTY(BlueprintAssignable, Category = "AttackZone")
	FStartAttack OnStartAttack;

	UPROPERTY(BlueprintAssignable, Category = "AttackZone")
	FEndAttack OnEndAttack;

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "AttackZone")
	bool IsUnbound = false;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AttackZone")
	void InitializeCollider(AAttackZoneBlock* AttackZoneBlock);
	virtual void InitializeCollider_Implementation(AAttackZoneBlock* AttackZoneBlock);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AttackZone")
	void DeinitializeCollider(AAttackZoneBlock* AttackZoneBlock);
	virtual void DeinitializeCollider_Implementation(AAttackZoneBlock* AttackZoneBlock);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AttackZone")
	void DestroyBlock(AAttackZoneBlock* AttackZoneBlock);
	virtual void DestroyBlock_Implementation(AAttackZoneBlock* AttackZoneBlock);

	UFUNCTION(BlueprintCallable, Category = "AttackZone")
	void SetPreviewForBlocks(bool InIsPreview, const TArray<AAttackZoneBlock*>& AttackZoneBlocks);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "AttackZone")
	EAttackZoneType AttackZoneType = EAttackZoneType::Empty;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(RowType="AttackZoneCoordinates"))
	TObjectPtr<UDataTable> AttackPatterns;

private:
	UFUNCTION() // reqired
	void OnBeginOverlap(
		UPrimitiveComponent* OverlappedComp, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, 
		const FHitResult& SweepResult);
	
	UFUNCTION() // reqired
	void OnEndOverlap(
		UPrimitiveComponent* OverlappedComp, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex);

	UFUNCTION() // reqired
	void OnAttackZoneBlockDestroyed(AActor* DestroyedActor);

	FAttackZoneCoordinates* GetCoordinates(EAttackZoneType InAttackZoneType) const;

	inline void SetupAttackZoneCollider(UBoxComponent* Collider) const;

protected:
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool IsPreview = false; // Every player has they own state of preview.
	
	UPROPERTY()
	TArray<AActor*> TargetsInZone;
};
