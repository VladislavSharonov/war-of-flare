﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "AttackZoneCoordinates.generated.h"

USTRUCT(BlueprintType)
struct FAttackZoneCoordinates : public FTableRowBase
{
	GENERATED_BODY()

	FAttackZoneCoordinates(TArray<FVector2D> InRelativeLocations = {}) : RelativeLocations(InRelativeLocations) { }

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector2D> RelativeLocations;
};