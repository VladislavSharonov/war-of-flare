﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "AttackZoneType.generated.h"

UENUM(BlueprintType)
enum class EAttackZoneType : uint8
{
    Empty               UMETA(Hidden, DisplayName = "Empty"),
    Radius1             UMETA(DisplayName = "Radius1"),
    Cross3              UMETA(DisplayName = "Cross3"),
    Cross5              UMETA(DisplayName = "Cross5"),
    Radius2             UMETA(DisplayName = "Radius2"),
    Radius3             UMETA(DisplayName = "Radius3"),
    EvenAlternation     UMETA(DisplayName = "EvenAlternation"),
    OddAlternation      UMETA(DisplayName = "OddAlternation"),
};