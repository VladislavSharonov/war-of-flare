// DemoDreams. All rights reserved.


#include "Tower/TowerModificationComponent.h"

#include "GameFramework/PlayerState.h"
#include "Money/Price.h"
#include "Money/WalletComponent.h"
#include "Tower/TowerType.h"
#include "Tower/Tower.h"

// Sets default values for this component's properties
UTowerModificationComponent::UTowerModificationComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTowerModificationComponent::BeginPlay()
{
	Super::BeginPlay();
	
	Tower = Cast<ATower>(GetOwner());
}

void UTowerModificationComponent::Server_TryBuyAttackZone_Implementation(const EAttackZoneType InAttackZoneType)
{
	if (!IsValid(Tower) || !IsValid(Tower->AttackZone) || Tower->AttackZone->GetAttackZoneType() == InAttackZoneType)
		return;

	APlayerController* TowerOwner = Cast<APlayerController>(Tower->GetOwner());

	if (!IsValid(TowerOwner))
		return;
	
	UWalletComponent* Wallet = TowerOwner->PlayerState->GetComponentByClass<UWalletComponent>();
	if (!IsValid(Wallet))
	{
		UE_LOG(LogTemp, Warning, TEXT("FindMoneyComponent: PlayerState has no moneyComponent."));
		return;
	}

	const FName Name(StaticEnum<EAttackZoneType>()->GetNameStringByValue(static_cast<int64>(InAttackZoneType)));

	const FPrice* FoundRow = AttackZonePrices->FindRow<FPrice>(Name, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Server_TryBuyAttackZone: A price for attack zone [\"%s\"] was not found."), *Name.ToString());
		return;
	}

	if (Wallet->TryBuy(FoundRow->Amount))
	{
		Tower->AttackZone->ClearZone();
		Tower->AttackZone->SetZone(InAttackZoneType);
	}
}

void UTowerModificationComponent::Server_TryBuyElementalDamage_Implementation(const EElementalDamageType InElementalDamageType)
{
	if (!IsValid(Tower) || Tower->Damage.ElementalType == InElementalDamageType)
		return;

	const APlayerController* TowerOwner = Cast<APlayerController>(Tower->GetOwner());

	if (!IsValid(TowerOwner))
		return;
	
	UWalletComponent* Wallet = TowerOwner->PlayerState->GetComponentByClass<UWalletComponent>();
	if (!IsValid(Wallet))
	{
		UE_LOG(LogTemp, Warning, TEXT("FindMoneyComponent: PlayerState has no moneyComponent."));
		return;
	}

	const FName Name(StaticEnum<EElementalDamageType>()->GetNameStringByValue(static_cast<int64>(InElementalDamageType)));

	const FPrice* FoundRow = DamageElementPrices->FindRow<FPrice>(Name, "");
	if (FoundRow == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("TryBuyElementalDamage: A price for damage element [\"%s\"] was not found."), *Name.ToString());
		return;
	}

	if (Wallet->TryBuy(FoundRow->Amount))
		Tower->SetElementalDamageType(InElementalDamageType);
}
