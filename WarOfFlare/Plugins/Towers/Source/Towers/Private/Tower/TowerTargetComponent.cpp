﻿// DemoDreams. All rights reserved.

#include "Tower/TowerTargetComponent.h"

#include "GameplayTagAssetInterface.h"

// Sets default values for this component's properties
UTowerTargetComponent::UTowerTargetComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UTowerTargetComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTowerTargetComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}




