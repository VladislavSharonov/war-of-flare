﻿// DemoDreams. All rights reserved.

#include "Tower/Attack/Projectile/Projectile.h"

AProjectile::AProjectile()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);
}
