﻿// DemoDreams. All rights reserved.

#include "Tower/Attack/AttackZone/AttackZoneComponent.h"

#include "EngineUtils.h"
#include "GameFramework/Character.h"
#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

#include "Tower/TowerTargetComponent.h"

void UAttackZoneComponent::BeginPlay()
{
	Super::BeginPlay();
	
	Grid = UGameplayStatics::GetGameState(GetWorld())->GetComponentByClass<UGridComponent>();
}

void UAttackZoneComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	OnStartAttack.Clear();
	OnEndAttack.Clear();
	
	if (GetOwner()->HasAuthority())
	{
		if (IsValid(Grid))
		{
			const FIntVector AttackZoneRootCell = Grid->GetCell(GetOwner()->GetActorLocation());
			Grid->Deoccupy(AttackZoneRootCell);
			
			ClearZone();
			
			// Reconstruct zones.
			TArray<AActor*> Objects;
			Grid->GetOccupiedCells().GenerateValueArray(Objects);

			for (auto& Obj : Objects)
			{
				if (!IsValid(Obj))
					continue;
				
				UAttackZoneComponent* AttackZoneComponent = Obj->GetComponentByClass<UAttackZoneComponent>();
				if (IsValid(AttackZoneComponent))
					AttackZoneComponent->ReconstructZone();
			}
		}
	}
	
	Super::EndPlay(EndPlayReason);
}

void UAttackZoneComponent::SetZone(const EAttackZoneType InAttackZoneType)
{
	const bool bOldIsPreview = IsPreview;
	if (bOldIsPreview)
		SetPreviewForBlocks(false, AttackColliders); 

	FAttackZoneCoordinates* ZoneCoordinates = GetCoordinates(InAttackZoneType);

	if (ZoneCoordinates == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Attack Pattern was not found."));
		return;
	}

	AttackZoneType = InAttackZoneType;
	uint32 i = 0;

	const FIntVector AttackZoneRootCell = Grid->GetCell(GetOwner()->GetActorLocation());

	for (const auto& Cell : ZoneCoordinates->RelativeLocations)
	{
		FIntVector AbsoluteCell = AttackZoneRootCell + FIntVector(Cell.X, Cell.Y, 0);

		AAttackZoneBlock* AttackZone = nullptr;
		if (Grid->IsOccupied(AbsoluteCell))
		{
			AttackZone = Cast<AAttackZoneBlock>(Grid->GetObject(AbsoluteCell));
			if (AttackZone == nullptr)
				continue; // There is not an AttackZone at the cell. It is occupied by something else.
		}
		else
		{
			FVector Location = Grid->GetCenter(AbsoluteCell);
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParameters.Owner = GetOwner();
			AttackZone = GetWorld()->SpawnActor<AAttackZoneBlock>(AttackZoneBlockClass.Get(), Location, FRotator::ZeroRotator, SpawnParameters);

			if(!IsValid(AttackZone)) // ToDo: Fix bug on destroy level in Reconstruct in ATower::EndPlay. It is a fixer-placeholder.
				return;
			
			SetupAttackZoneCollider(AttackZone->Collider);

			Grid->Occupy(AbsoluteCell, AttackZone);
		}

		AttackColliders.Add(AttackZone);
		InitializeCollider(AttackZone);

		AttackZone->OnDestroyed.AddUniqueDynamic(this, &UAttackZoneComponent::OnAttackZoneBlockDestroyed);
		AttackZone->AddOwner();

		++i;
	}
	
	if (bOldIsPreview)
		SetPreviewForBlocks(bOldIsPreview, AttackColliders);
}

void UAttackZoneComponent::ReconstructZone()
{
	SetZone(AttackZoneType);
}

void UAttackZoneComponent::ClearZone()
{
	SetPreviewForBlocks(false, AttackColliders); // Listening server only.
	for (const auto& AttackZone : AttackColliders)
	{
		if (IsValid(AttackZone))
		{
			DeinitializeCollider(AttackZone);
			AttackZone->OnDestroyed.RemoveDynamic(this, &UAttackZoneComponent::OnAttackZoneBlockDestroyed);
			AttackZone->RemoveOwner();

			if (AttackZone->GetOwnerCount() == 0)
				DestroyBlock(AttackZone);
		}
	}

	AttackColliders.Reset();
	AttackZoneType = EAttackZoneType::Empty;
}

void UAttackZoneComponent::GetTargetsInZone(TArray<AActor*>& Targets)
{
	if (IsUnbound)
	{
		for(TActorIterator<ACharacter> Actor = TActorIterator<ACharacter>(GetWorld()); Actor; ++Actor)
		{
			if (!IsValid(*Actor))
				continue;

			Targets.Add(*Actor);
		}

		return;
	}

	if (TargetsInZone.Num() <= 0)
		return;

	Targets.Empty();
	Targets.Append(TargetsInZone);
}

void UAttackZoneComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UAttackZoneComponent, IsUnbound);
	DOREPLIFETIME(UAttackZoneComponent, AttackColliders);
	DOREPLIFETIME(UAttackZoneComponent, AttackZoneType);
	DOREPLIFETIME(UAttackZoneComponent, Grid);
}

void UAttackZoneComponent::SetPreview_Implementation(bool InIsPreview)
{
	IsPreview = InIsPreview;
	SetPreviewForBlocks(IsPreview, AttackColliders);
}

void UAttackZoneComponent::OnRep_AttackColliders(const TArray<AAttackZoneBlock*>& OldAttackColliders)
{
	SetPreviewForBlocks(false, OldAttackColliders);
	SetPreviewForBlocks(IsPreview, AttackColliders);

	for (auto& Collider : OldAttackColliders)
	{
		if (IsValid(Collider) && !AttackColliders.Contains(Collider))
			DeinitializeCollider(Collider);
	}

	for (auto& Collider : AttackColliders)
	{
		if (IsValid(Collider) && !OldAttackColliders.Contains(Collider))
		{
			InitializeCollider(Collider);
		}
	}
}

void UAttackZoneComponent::InitializeCollider_Implementation(AAttackZoneBlock* AttackZoneBlock)
{
	if (!IsValid(AttackZoneBlock))
		return;

	AttackZoneBlock->Collider->OnComponentBeginOverlap.AddUniqueDynamic(this, &UAttackZoneComponent::OnBeginOverlap);
	AttackZoneBlock->Collider->OnComponentEndOverlap.AddUniqueDynamic(this, &UAttackZoneComponent::OnEndOverlap);
}

void UAttackZoneComponent::DeinitializeCollider_Implementation(AAttackZoneBlock* AttackZoneBlock)
{
	if (!IsValid(AttackZoneBlock))
		return;
	
	AttackZoneBlock->Collider->OnComponentEndOverlap.RemoveDynamic(this, &UAttackZoneComponent::OnEndOverlap);
	AttackZoneBlock->Collider->OnComponentBeginOverlap.RemoveDynamic(this, &UAttackZoneComponent::OnBeginOverlap);
}

void UAttackZoneComponent::DestroyBlock_Implementation(AAttackZoneBlock* AttackZoneBlock)
{
	AttackZoneBlock->Destroy();
	const FIntVector AttackZoneRootCell = Grid->GetCell(GetComponentLocation());
	const FIntVector LocalCell = Grid->GetCell(AttackZoneBlock->GetActorLocation() - GetComponentLocation());
	Grid->Deoccupy(AttackZoneRootCell + FIntVector(LocalCell.X, LocalCell.Y, 0));
}

void UAttackZoneComponent::SetPreviewForBlocks(const bool InIsPreview, const TArray<AAttackZoneBlock*>& AttackZoneBlocks)
{
	for (auto& ZoneBlock : AttackZoneBlocks)
	{
		if (IsValid(ZoneBlock))
			ZoneBlock->SetPreview(InIsPreview);
	}
}

void UAttackZoneComponent::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!IsValid(OtherActor) || !OtherActor->FindComponentByClass<UTowerTargetComponent>())
		return;

	bool bIsAttackStart = false;
	if (TargetsInZone.IsEmpty())
		bIsAttackStart = true;

	TargetsInZone.AddUnique(OtherActor);
	if(bIsAttackStart)
		OnStartAttack.Broadcast();
}

void UAttackZoneComponent::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor->FindComponentByClass<UTowerTargetComponent>())
	{
		for (auto const& AttackZoneBlock : AttackColliders)
		{
			if (!IsValid(AttackZoneBlock))
				continue;

			auto& Collider = AttackZoneBlock->Collider;
			if (Collider != OverlappedComp && Collider->IsOverlappingActor(OtherActor))
				return;
		}
	}

	if (IsValid(OtherActor))
		TargetsInZone.Remove(OtherActor);

	if (TargetsInZone.IsEmpty())
		OnEndAttack.Broadcast();
}

void UAttackZoneComponent::OnAttackZoneBlockDestroyed(AActor* DestroyedActor)
{
	AAttackZoneBlock* ZoneBlock = Cast<AAttackZoneBlock>(DestroyedActor);
	AttackColliders.Remove(ZoneBlock);
}

FAttackZoneCoordinates* UAttackZoneComponent::GetCoordinates(EAttackZoneType InAttackZoneType) const
{
	const FName PatternName(*StaticEnum<EAttackZoneType>()->GetNameStringByValue(static_cast<int64>(InAttackZoneType)));
	return AttackPatterns->FindRow<FAttackZoneCoordinates>(PatternName, "");
}

void UAttackZoneComponent::SetupAttackZoneCollider(UBoxComponent* Collider) const
{
	const float BoxSize = Grid->GetCellSize() / 2;
	Collider->InitBoxExtent(FVector(BoxSize, BoxSize, BoxSize));
	Collider->SetGenerateOverlapEvents(true);
	Collider->SetCanEverAffectNavigation(false);
}
