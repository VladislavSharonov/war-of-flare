// DemoDreams. All rights reserved.

#include "Tower/Attack/Projectile/HomingMovementComponent.h"


UHomingMovementComponent::UHomingMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bAutoActivate = true;
}

void UHomingMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UHomingMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	const USceneComponent* Target = HomingTargetComponent.Get();
	AActor* MovementOwner =  GetOwner();
	if (!IsValid(Target) || !IsValid(MovementOwner))
		return;

	const FVector From = MovementOwner->GetActorLocation();
	const FVector To = Target->GetComponentLocation();

	const double Distance = FVector::Distance(From, To);
	const double DistanceToPassThisFrame = Speed * DeltaTime;

	if (DistanceToPassThisFrame >= Distance)
	{
		MovementOwner->SetActorLocation(To);
		OnTargetReached.Broadcast();
		return;
	}

	FVector Direction = To - From;
	Direction.Normalize();

	Direction *= DistanceToPassThisFrame;
	MovementOwner->SetActorLocation(From + Direction);
}
