﻿// DemoDreams. All rights reserved.


#include "Tower/Attack/TargetFiltersLibrary.h"

#include "Health/HealthComponent.h"

void UTargetFiltersLibrary::FilterForTowerType(const ETowerType InTowerType, UPARAM(ref)TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets)
{
	switch (InTowerType)
	{
	case ETowerType::SingleTarget:
		CountFilter(1, InTargets, OutFilteredTargets);
		break;
	case ETowerType::MultiTarget:
		CountFilter(3, InTargets, OutFilteredTargets);
		break;
	case ETowerType::AreaDamage:
		DoNotFilter(InTargets, OutFilteredTargets);
		break;
	case ETowerType::MassDamage:
		DoNotFilter(InTargets, OutFilteredTargets);
		break;
	case ETowerType::Machinegun:
		CountFilter(1, InTargets, OutFilteredTargets);
		break;
	case ETowerType::Sniper:
		MaxHealthFilter(InTargets, OutFilteredTargets);
		break;
	case ETowerType::None:
		UE_LOG(LogTemp, Error, TEXT("FilterForAttackType: Attack Type was not selected for tower"));
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("FilterForAttackType: Unknown attack type"));
		CountFilter(1, InTargets, OutFilteredTargets);
		break;
	}
}

void UTargetFiltersLibrary::DoNotFilter(UPARAM(ref)TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets)
{
	OutFilteredTargets = InTargets;
}

void UTargetFiltersLibrary::CountFilter(const int32 InTargetCount, UPARAM(ref)TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets)
{
	OutFilteredTargets.Empty();

	if (InTargetCount > 0 && InTargets.Num() > 0)
	{
		const int32 Count = FMath::Min(InTargetCount, InTargets.Num());
		OutFilteredTargets.SetNumUninitialized(Count);
		FMemory::Memcpy(OutFilteredTargets.GetData(), InTargets.GetData(), Count * InTargets.GetTypeSize());
	}
}

void UTargetFiltersLibrary::MaxHealthFilter(UPARAM(ref)TArray<AActor*>& InTargets, TArray<AActor*>& OutFilteredTargets)
{
	OutFilteredTargets.Empty();

	AActor* maxHpTarget = nullptr;
	float maxHp = 0;

	for (auto& target : InTargets)
	{
		if (!IsValid(target))
			continue;

		float hp = target->FindComponentByClass<UHealthComponent>()->GetHealth();
		if (hp > maxHp)
		{
			maxHp = hp;
			maxHpTarget = target;
		}
	}

	if (maxHpTarget != nullptr)
		OutFilteredTargets.Add(maxHpTarget);
}
