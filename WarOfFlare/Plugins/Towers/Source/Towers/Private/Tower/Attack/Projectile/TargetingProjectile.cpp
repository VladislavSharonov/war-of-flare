﻿// DemoDreams. All rights reserved.

#include "Tower/Attack/Projectile/TargetingProjectile.h"

#include "Net/UnrealNetwork.h"

ATargetingProjectile::ATargetingProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
	ProjectileMovement = CreateDefaultSubobject<UHomingMovementComponent>(TEXT("ProjectileMovement"));
}

void ATargetingProjectile::BeginPlay()
{
	Super::BeginPlay();
	ProjectileMovement->OnTargetReached.AddDynamic(this, &ATargetingProjectile::Hit);
}

void ATargetingProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	ProjectileMovement->OnTargetReached.RemoveDynamic(this, &ATargetingProjectile::Hit);
}

void ATargetingProjectile::SetTarget(USceneComponent* ProjectileTarget) const
{
	ProjectileMovement->HomingTargetComponent = ProjectileTarget;
}

void ATargetingProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Destroy if target is not exist
	if (!IsValid( ProjectileMovement->HomingTargetComponent.Get()))
		Destroy();
}

void ATargetingProjectile::Hit()
{
	const TWeakObjectPtr<USceneComponent> Target = ProjectileMovement->HomingTargetComponent;
	if (!IsValid(Target.Get()))
		return;

	if (!IsValid(Target->GetOwner()))
		return;

	OnHit.Broadcast(Target->GetOwner());
	Destroy();
}

