﻿// DemoDreams. All rights reserved.


#include "Tower/Attack/AttackZone/AttackZoneBlock.h"

// Sets default values
AAttackZoneBlock::AAttackZoneBlock()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);
	
    Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collider"));
	Collider->InitBoxExtent(FVector(50, 50, 50));
	Collider->SetGenerateOverlapEvents(true);
	Collider->SetCanEverAffectNavigation(false);
	Collider->SetupAttachment(DefaultRootComponent);
	
	bReplicates = true;
}
