﻿// DemoDreams. All rights reserved.

#include <Tower/Tower.h>

#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

ATower::ATower()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);

	ProjectileSources = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSources"));
	ProjectileSources->SetupAttachment(DefaultRootComponent);

	AttackZone = CreateDefaultSubobject<UAttackZoneComponent>(TEXT("AttackZone"));
	AttackZone->SetupAttachment(DefaultRootComponent);
	AttackZone->SetIsReplicated(true);

	SelectionHandler = CreateDefaultSubobject<USelectionHandlerComponent>(TEXT("SelectionHandler"));
	
	TowerModification = CreateDefaultSubobject<UTowerModificationComponent>(TEXT("TowerModification"));
	TowerModification->SetIsReplicated(true);
	
	bReplicates = true;
}

void ATower::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATower, AttackZone);
	DOREPLIFETIME(ATower, Damage);
	DOREPLIFETIME(ATower, TowerModification);
}

void ATower::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &ATower::BeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &ATower::EndOverlap);
	
	AttackZone->OnEndAttack.AddUniqueDynamic(this, &ATower::EndAttack);
	AttackZone->OnStartAttack.AddUniqueDynamic(this, &ATower::StartAttack);
}

void ATower::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void ATower::BeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (!OtherActor->IsA<ATower>())
		return;

	if(IsPreview)
	{
		SetActorHiddenInGame(true);
	}
}

void ATower::EndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (!OtherActor->IsA<ATower>())
		return;

	if(IsPreview)
	{
		TArray<AActor*> OverlappingActors;
		OverlappedActor->GetOverlappingActors(OverlappingActors, ATower::StaticClass());

		if (OverlappingActors.IsEmpty())
			SetActorHiddenInGame(false);
	}
}

void ATower::SetVisibleLocally_Implementation(bool IsVisible)
{
	SetActorEnableCollision(IsVisible);
}

#pragma region Attack
void ATower::SetDamage(const float InDamage)
{
	const FDamage OldDamage(Damage);
	
	Damage.Damage = InDamage;
	
	if (GetNetMode() == ENetMode::NM_ListenServer)
		OnRep_Damage(OldDamage);
}

void ATower::SetElementalDamageType(const EElementalDamageType InElementalDamageType)
{
	if (const bool bIsElementalTypeChanged = InElementalDamageType != Damage.ElementalType)
	{
		const FDamage OldDamage(Damage);
		Damage.ElementalType = InElementalDamageType;

		if (bIsElementalTypeChanged)
		{
			if (GetNetMode() == ENetMode::NM_ListenServer)
				OnRep_Damage(OldDamage);
			
			OnTowerElementalDamageTypeChanged();
		}
	}
}

void ATower::OnRep_Damage(const FDamage& InOldDamage)
{
	if (InOldDamage.ElementalType != Damage.ElementalType)
		OnTowerElementalDamageTypeChanged();
}
#pragma endregion

#pragma region IPlacementPreview
void ATower::SetPlacementPreview_Implementation(const bool InIsPreview)
{
	//SetActorEnableCollision(!InIsPreview); // Always enabled to hide preview when overlapped.
	SetReplicates(!InIsPreview);
	IsPreview = InIsPreview;
	AttackZone->SetPreview(InIsPreview);
}
#pragma endregion