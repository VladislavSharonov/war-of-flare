﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "CursorHoverComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHoverBeginSignature, AActor*, Actor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHoverEndSignature, AActor*, Actor);

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OBJECTHOVERING_API UCursorHoverComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UCursorHoverComponent();

	virtual void Activate(bool bReset) override;
	
	virtual void Deactivate() override;
	
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable, Category = "Hover")
	void Hover();

#pragma region Filtering
public:
	UFUNCTION(BlueprintCallable, Category = "Filtering")
	void AddFilter(const TSubclassOf<AActor> FilterClass) { ClassFilter.Add(FilterClass); }

	UFUNCTION(BlueprintCallable, Category = "Filtering")
	void RemoveFilter(const TSubclassOf<AActor> FilterClass) { ClassFilter.Remove(FilterClass); }

	UFUNCTION(BlueprintCallable, Category = "Filtering")
	void ClearFilter(const TSubclassOf<AActor> FilterClass) { ClassFilter.Empty(); }
	
protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Filtering")
	TArray<TSubclassOf<AActor>> ClassFilter;
#pragma endregion
	
#pragma region SettersAndGetters
public:
	UFUNCTION(BlueprintPure, Category = "Hover")
	AActor* GetHoveredActor() const { return Hovered; }
	
	UFUNCTION(BlueprintCallable, Category = "Hover")
	void SetTraceChannel(const ECollisionChannel InCollisionChannel) { CollisionChannel = InCollisionChannel; }

	UFUNCTION(BlueprintPure, Category = "Hover")
	ECollisionChannel GetTraceChannel() const { return CollisionChannel; }

	UFUNCTION(BlueprintCallable, Category = "Hover")
	void SetCheckDistance(float InCheckDistance) { CheckDistance = InCheckDistance; }

	UFUNCTION(BlueprintPure, Category = "Hover")
	float GetCheckDistance() const { return CheckDistance; }
#pragma endregion

public:
	UPROPERTY(BlueprintAssignable, Category = "Hover")
	FOnHoverBeginSignature OnHoverBegin;

	UPROPERTY(BlueprintAssignable, Category = "Hover")
	FOnHoverEndSignature OnHoverEnd;

protected:
	bool IsChanged(AActor* InNewActor) const;

	bool TryDeprojectMousePositionToWorld(FVector& LineStart, FVector& LineEnd) const;
	
	bool TryIntersect(const FVector& LineStart, const FVector& LineEnd, FHitResult& Hit) const;

	void SetHovered(AActor* Actor, bool InIsHovered = true);

	bool IsSupported(const AActor* Actor);
	
protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Hover")
	TObjectPtr<AActor> Hovered;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Hover")
	TEnumAsByte<ECollisionChannel> CollisionChannel = ECollisionChannel::ECC_Visibility;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Hover")
	float CheckDistance = 1000000.0f;
};
