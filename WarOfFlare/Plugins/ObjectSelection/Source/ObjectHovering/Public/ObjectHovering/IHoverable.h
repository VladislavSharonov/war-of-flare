﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "IHoverable.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UHoverable : public UInterface
{
	GENERATED_BODY()
};

class OBJECTHOVERING_API IHoverable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHoverBegin();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHoverEnd();
};
