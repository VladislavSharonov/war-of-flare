﻿ // DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "CursorHoverHandlerComponent.generated.h"

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class OBJECTHOVERING_API UCursorHoverHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UCursorHoverHandlerComponent();

	UFUNCTION(BlueprintPure)
	bool IsHovered() const { return bIsHovered; }
	
	UFUNCTION(BlueprintCallable)
	void BeginHover();

	UFUNCTION(BlueprintCallable)
	void EndHover();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

protected:
	UPROPERTY()
	bool bIsHovered = false;
};
