﻿// DemoDreams. All rights reserved.

#include "ObjectHovering//CursorHoverHandlerComponent.h"

#include "ObjectHovering/IHoverable.h"

UCursorHoverHandlerComponent::UCursorHoverHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UCursorHoverHandlerComponent::BeginHover()
{
	bIsHovered = true;

	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner->GetClass()->ImplementsInterface(UHoverable::StaticClass()))
		IHoverable::Execute_OnHoverBegin(ComponentOwner);
}

void UCursorHoverHandlerComponent::EndHover()
{
	bIsHovered = false;

	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner->GetClass()->ImplementsInterface(UHoverable::StaticClass()))
		IHoverable::Execute_OnHoverEnd(ComponentOwner);
}

void UCursorHoverHandlerComponent::BeginPlay()
{
	Super::BeginPlay();
}
