﻿// DemoDreams. All rights reserved.

#include "ObjectHovering/CursorHoverComponent.h"
#include "ObjectHovering/CursorHoverHandlerComponent.h"

#include "Kismet/GameplayStatics.h"

UCursorHoverComponent::UCursorHoverComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	
	bAutoActivate = false;
}

void UCursorHoverComponent::Activate(bool bReset)
{
	Super::Activate(bReset);
}

void UCursorHoverComponent::Deactivate()
{
	Super::Deactivate();
	
	if (IsValid(Hovered))
		SetHovered(Hovered, false);
}

void UCursorHoverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Hover();
}

void UCursorHoverComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	if (IsValid(Hovered))
		SetHovered(Hovered, false);
}

void UCursorHoverComponent::Hover()
{
	FVector LineStart;
	FVector LineEnd;

	FHitResult Hit;
	if (!TryDeprojectMousePositionToWorld(LineStart, LineEnd) || !TryIntersect(LineStart, LineEnd, Hit))
	{
		SetHovered(Hovered, false);
		return;
	}

	AActor* Target = Hit.GetActor();

	if (!IsChanged(Target))
		return;

	SetHovered(Hovered, false);

	if (!IsSupported(Target))
	{
		Hovered = nullptr;
		return;
	}
	
	Hovered = Target;
	SetHovered(Hovered, true);
}

bool UCursorHoverComponent::IsChanged(AActor* InNewActor) const
{
	return Hovered != InNewActor;
}

bool UCursorHoverComponent::TryDeprojectMousePositionToWorld(FVector& LineStart, FVector& LineEnd) const
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (!IsValid(PlayerController))
		return false;

	FVector WorldDirection;

	const bool bResult = PlayerController->DeprojectMousePositionToWorld(LineStart, WorldDirection);
	LineEnd = LineStart + WorldDirection * CheckDistance;
	return bResult;
}

bool UCursorHoverComponent::TryIntersect(const FVector& LineStart, const FVector& LineEnd, FHitResult& Hit) const
{
	UWorld* World = GetWorld();
	FCollisionQueryParams CollisionParams;
	CollisionParams.bTraceComplex = true;

	return World->LineTraceSingleByChannel(Hit, LineStart, LineEnd, CollisionChannel, CollisionParams);
}

void UCursorHoverComponent::SetHovered(AActor* Actor, const bool InIsHovered)
{
	if (!IsValid(Actor))
		return;
	
	UCursorHoverHandlerComponent* HoverComponent = Actor->FindComponentByClass<UCursorHoverHandlerComponent>();
	if (IsValid(HoverComponent))
	{
		if (InIsHovered)
		{
			HoverComponent->BeginHover();
			Hovered = Actor;
		}
		else
		{
			HoverComponent->EndHover();
			Hovered = nullptr;
		}
	}
}

bool UCursorHoverComponent::IsSupported(const AActor* Actor)
{
	if (IsValid(Actor))
	{
		for(const auto& Filter : ClassFilter)
		{
			if(Actor->IsA(Filter))
				return true;
		}
	}
	
	return ClassFilter.IsEmpty();
}
