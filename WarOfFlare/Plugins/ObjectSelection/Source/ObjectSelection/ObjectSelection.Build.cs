﻿// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ObjectSelection : ModuleRules
{
	public ObjectSelection(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"ObjectHovering"
			}
			);

		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine"
			}
			);
	}
}
