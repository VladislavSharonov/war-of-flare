﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "ObjectHovering/CursorHoverComponent.h"

#include "SelectorComponent.generated.h"

// Not work with bEnableClickEvents in PlayerController
UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OBJECTSELECTION_API USelectorComponent : public UCursorHoverComponent
{
	GENERATED_BODY()

public:
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION(BlueprintCallable, Category = "ObjectSelector")
	AActor* Select();
	
	UFUNCTION(BlueprintCallable, Category = "ObjectSelector")
	void DeselectAll();

#pragma region SettersAndGetters
	UFUNCTION(BlueprintCallable, Category = "ObjectSelector")
	void SetExclusive(bool InIsExclusive) { IsExclusive = InIsExclusive; }

	UFUNCTION(BlueprintPure, Category = "ObjectSelector")
	bool GetIsExclusive() const { return IsExclusive; }

	UFUNCTION(BlueprintCallable, Category = "ObjectSelector")
	void SetDeselectIfCantSelect(bool InDeselectIfCantSelect) { DeselectIfCantSelect = InDeselectIfCantSelect; }

	UFUNCTION(BlueprintPure, Category = "ObjectSelector")
	bool GetDeselectIfCantSelect() const { return DeselectIfCantSelect; }
#pragma endregion

protected:
	UFUNCTION(BlueprintCallable, Category = "ObjectSelector")
	bool IsChanged(AActor* NewActor);

	static void SetSelected(const AActor* Actor, bool IsSelected = true);
	
protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "ObjectSelector")
	TArray<TObjectPtr<AActor>> Selected;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "ObjectSelector")
	bool IsExclusive = true;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "ObjectSelector")
	bool DeselectIfCantSelect = true;
};
