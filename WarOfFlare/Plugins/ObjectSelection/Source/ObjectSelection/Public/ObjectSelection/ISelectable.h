﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "ObjectHovering/IHoverable.h"

#include "ISelectable.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class USelectable : public UHoverable
{
	GENERATED_BODY()
};

class OBJECTSELECTION_API ISelectable : public IHoverable
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnSelect();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnDeselect();
};
