﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "ObjectHovering/CursorHoverHandlerComponent.h"

#include "SelectionHandlerComponent.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class OBJECTSELECTION_API USelectionHandlerComponent : public UCursorHoverHandlerComponent
{
	GENERATED_BODY()

public:
	USelectionHandlerComponent();
	
	UFUNCTION(BlueprintPure)
	bool IsActorSelected() const { return bIsSelected; }
	
	UFUNCTION(BlueprintCallable)
	void Select();

	UFUNCTION(BlueprintCallable)
	void Deselect();

protected:
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:
	UPROPERTY()
	bool bIsSelected = false;
};
