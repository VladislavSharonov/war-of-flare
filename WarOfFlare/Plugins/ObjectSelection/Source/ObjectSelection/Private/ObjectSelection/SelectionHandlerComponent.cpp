﻿// DemoDreams. All rights reserved.


#include "ObjectSelection/SelectionHandlerComponent.h"

#include "ObjectSelection/ISelectable.h"

USelectionHandlerComponent::USelectionHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void USelectionHandlerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void USelectionHandlerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	Deselect();
}

void USelectionHandlerComponent::Select()
{
	bIsSelected = true;
	
	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner->GetClass()->ImplementsInterface(USelectable::StaticClass()))
		ISelectable::Execute_OnSelect(ComponentOwner);
}

void USelectionHandlerComponent::Deselect()
{
	bIsSelected = false;
	
	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner->GetClass()->ImplementsInterface(USelectable::StaticClass()))
		ISelectable::Execute_OnDeselect(ComponentOwner);
}
