﻿// DemoDreams. All rights reserved.

#include "ObjectSelection/SelectorComponent.h"

#include "ObjectSelection/SelectionHandlerComponent.h"

void USelectorComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	DeselectAll();
}

AActor* USelectorComponent::Select()
{
	FVector LineStart;
	FVector LineEnd;

	FHitResult Hit;
	if (!TryDeprojectMousePositionToWorld(LineStart, LineEnd) || !TryIntersect(LineStart, LineEnd, Hit))
	{
		if (DeselectIfCantSelect)
			DeselectAll();

		return nullptr;
	}

	AActor* Target = Hit.GetActor();

	if (IsExclusive && IsChanged(Target))
		DeselectAll();

	if (!IsSupported(Target))
	{
		return nullptr;
	}
	
	if (!Selected.Contains(Target))
	{
		SetSelected(Target, true);
		Selected.Add(Target);
	}

	return Target;
}

bool USelectorComponent::IsChanged(AActor* NewActor)
{
	return !Selected.Contains(NewActor);
}

void USelectorComponent::SetSelected(const AActor* Actor, const bool IsSelected)
{
	if (!IsValid(Actor))
		return;

	USelectionHandlerComponent* SelectionComponent = Actor->FindComponentByClass<USelectionHandlerComponent>();
	if (IsValid(SelectionComponent))
	{
		if (IsSelected)
			SelectionComponent->Select();
		else
			SelectionComponent->Deselect();
	}
}

void USelectorComponent::DeselectAll()
{
	for (auto SelectedObject : Selected)
		SetSelected(SelectedObject, false);

	Selected.Empty();
}
