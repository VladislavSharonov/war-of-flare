// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectHovering/Public/ObjectHovering/IHoverable.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIHoverable() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UHoverable();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UHoverable_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ObjectHovering();
// End Cross Module References
	DEFINE_FUNCTION(IHoverable::execOnHoverEnd)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHoverEnd_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IHoverable::execOnHoverBegin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHoverBegin_Implementation();
		P_NATIVE_END;
	}
	void IHoverable::OnHoverBegin()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnHoverBegin instead.");
	}
	void IHoverable::OnHoverEnd()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnHoverEnd instead.");
	}
	void UHoverable::StaticRegisterNativesUHoverable()
	{
		UClass* Class = UHoverable::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnHoverBegin", &IHoverable::execOnHoverBegin },
			{ "OnHoverEnd", &IHoverable::execOnHoverEnd },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UHoverable_OnHoverBegin_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoverable_OnHoverBegin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/IHoverable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoverable_OnHoverBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoverable, nullptr, "OnHoverBegin", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoverable_OnHoverBegin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoverable_OnHoverBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoverable_OnHoverBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UHoverable_OnHoverBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UHoverable_OnHoverEnd_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UHoverable_OnHoverEnd_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/IHoverable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UHoverable_OnHoverEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UHoverable, nullptr, "OnHoverEnd", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UHoverable_OnHoverEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UHoverable_OnHoverEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UHoverable_OnHoverEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UHoverable_OnHoverEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UHoverable);
	UClass* Z_Construct_UClass_UHoverable_NoRegister()
	{
		return UHoverable::StaticClass();
	}
	struct Z_Construct_UClass_UHoverable_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHoverable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectHovering,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UHoverable_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UHoverable_OnHoverBegin, "OnHoverBegin" }, // 3451116949
		{ &Z_Construct_UFunction_UHoverable_OnHoverEnd, "OnHoverEnd" }, // 1917490715
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHoverable_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectHovering/IHoverable.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHoverable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IHoverable>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UHoverable_Statics::ClassParams = {
		&UHoverable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UHoverable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHoverable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHoverable()
	{
		if (!Z_Registration_Info_UClass_UHoverable.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UHoverable.OuterSingleton, Z_Construct_UClass_UHoverable_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UHoverable.OuterSingleton;
	}
	template<> OBJECTHOVERING_API UClass* StaticClass<UHoverable>()
	{
		return UHoverable::StaticClass();
	}
	UHoverable::UHoverable(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHoverable);
	UHoverable::~UHoverable() {}
	static FName NAME_UHoverable_OnHoverBegin = FName(TEXT("OnHoverBegin"));
	void IHoverable::Execute_OnHoverBegin(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UHoverable::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_UHoverable_OnHoverBegin);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (IHoverable*)(O->GetNativeInterfaceAddress(UHoverable::StaticClass())))
		{
			I->OnHoverBegin_Implementation();
		}
	}
	static FName NAME_UHoverable_OnHoverEnd = FName(TEXT("OnHoverEnd"));
	void IHoverable::Execute_OnHoverEnd(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UHoverable::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_UHoverable_OnHoverEnd);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (IHoverable*)(O->GetNativeInterfaceAddress(UHoverable::StaticClass())))
		{
			I->OnHoverEnd_Implementation();
		}
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UHoverable, UHoverable::StaticClass, TEXT("UHoverable"), &Z_Registration_Info_UClass_UHoverable, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UHoverable), 4226604227U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_2030843278(TEXT("/Script/ObjectHovering"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
