// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectSelection/SelectorComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef OBJECTSELECTION_SelectorComponent_generated_h
#error "SelectorComponent.generated.h already included, missing '#pragma once' in SelectorComponent.h"
#endif
#define OBJECTSELECTION_SelectorComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsChanged); \
	DECLARE_FUNCTION(execGetDeselectIfCantSelect); \
	DECLARE_FUNCTION(execSetDeselectIfCantSelect); \
	DECLARE_FUNCTION(execGetIsExclusive); \
	DECLARE_FUNCTION(execSetExclusive); \
	DECLARE_FUNCTION(execDeselectAll); \
	DECLARE_FUNCTION(execSelect);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsChanged); \
	DECLARE_FUNCTION(execGetDeselectIfCantSelect); \
	DECLARE_FUNCTION(execSetDeselectIfCantSelect); \
	DECLARE_FUNCTION(execGetIsExclusive); \
	DECLARE_FUNCTION(execSetExclusive); \
	DECLARE_FUNCTION(execDeselectAll); \
	DECLARE_FUNCTION(execSelect);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSelectorComponent(); \
	friend struct Z_Construct_UClass_USelectorComponent_Statics; \
public: \
	DECLARE_CLASS(USelectorComponent, UCursorHoverComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectSelection"), NO_API) \
	DECLARE_SERIALIZER(USelectorComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUSelectorComponent(); \
	friend struct Z_Construct_UClass_USelectorComponent_Statics; \
public: \
	DECLARE_CLASS(USelectorComponent, UCursorHoverComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectSelection"), NO_API) \
	DECLARE_SERIALIZER(USelectorComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectorComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectorComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectorComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectorComponent(USelectorComponent&&); \
	NO_API USelectorComponent(const USelectorComponent&); \
public: \
	NO_API virtual ~USelectorComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USelectorComponent(); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USelectorComponent(USelectorComponent&&); \
	NO_API USelectorComponent(const USelectorComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USelectorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectorComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USelectorComponent) \
	NO_API virtual ~USelectorComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_13_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTSELECTION_API UClass* StaticClass<class USelectorComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
