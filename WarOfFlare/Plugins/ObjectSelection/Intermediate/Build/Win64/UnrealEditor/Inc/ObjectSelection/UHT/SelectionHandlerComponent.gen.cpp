// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectSelection/Public/ObjectSelection/SelectionHandlerComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSelectionHandlerComponent() {}
// Cross Module References
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverHandlerComponent();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectionHandlerComponent();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectionHandlerComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ObjectSelection();
// End Cross Module References
	DEFINE_FUNCTION(USelectionHandlerComponent::execDeselect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Deselect();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectionHandlerComponent::execSelect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Select();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectionHandlerComponent::execIsActorSelected)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsActorSelected();
		P_NATIVE_END;
	}
	void USelectionHandlerComponent::StaticRegisterNativesUSelectionHandlerComponent()
	{
		UClass* Class = USelectionHandlerComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Deselect", &USelectionHandlerComponent::execDeselect },
			{ "IsActorSelected", &USelectionHandlerComponent::execIsActorSelected },
			{ "Select", &USelectionHandlerComponent::execSelect },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USelectionHandlerComponent_Deselect_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectionHandlerComponent_Deselect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectionHandlerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectionHandlerComponent_Deselect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectionHandlerComponent, nullptr, "Deselect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectionHandlerComponent_Deselect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectionHandlerComponent_Deselect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectionHandlerComponent_Deselect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectionHandlerComponent_Deselect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics
	{
		struct SelectionHandlerComponent_eventIsActorSelected_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SelectionHandlerComponent_eventIsActorSelected_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(SelectionHandlerComponent_eventIsActorSelected_Parms), &Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectionHandlerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectionHandlerComponent, nullptr, "IsActorSelected", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::SelectionHandlerComponent_eventIsActorSelected_Parms), Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectionHandlerComponent_Select_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectionHandlerComponent_Select_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectionHandlerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectionHandlerComponent_Select_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectionHandlerComponent, nullptr, "Select", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectionHandlerComponent_Select_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectionHandlerComponent_Select_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectionHandlerComponent_Select()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectionHandlerComponent_Select_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USelectionHandlerComponent);
	UClass* Z_Construct_UClass_USelectionHandlerComponent_NoRegister()
	{
		return USelectionHandlerComponent::StaticClass();
	}
	struct Z_Construct_UClass_USelectionHandlerComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bIsSelected_MetaData[];
#endif
		static void NewProp_bIsSelected_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsSelected;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectionHandlerComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCursorHoverHandlerComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectSelection,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USelectionHandlerComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USelectionHandlerComponent_Deselect, "Deselect" }, // 1723744177
		{ &Z_Construct_UFunction_USelectionHandlerComponent_IsActorSelected, "IsActorSelected" }, // 4257394615
		{ &Z_Construct_UFunction_USelectionHandlerComponent_Select, "Select" }, // 399132959
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectionHandlerComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "ObjectSelection/SelectionHandlerComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectionHandlerComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectionHandlerComponent.h" },
	};
#endif
	void Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected_SetBit(void* Obj)
	{
		((USelectionHandlerComponent*)Obj)->bIsSelected = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected = { "bIsSelected", nullptr, (EPropertyFlags)0x0020080000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(USelectionHandlerComponent), &Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USelectionHandlerComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelectionHandlerComponent_Statics::NewProp_bIsSelected,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectionHandlerComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectionHandlerComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USelectionHandlerComponent_Statics::ClassParams = {
		&USelectionHandlerComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_USelectionHandlerComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_USelectionHandlerComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USelectionHandlerComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectionHandlerComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectionHandlerComponent()
	{
		if (!Z_Registration_Info_UClass_USelectionHandlerComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USelectionHandlerComponent.OuterSingleton, Z_Construct_UClass_USelectionHandlerComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USelectionHandlerComponent.OuterSingleton;
	}
	template<> OBJECTSELECTION_API UClass* StaticClass<USelectionHandlerComponent>()
	{
		return USelectionHandlerComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectionHandlerComponent);
	USelectionHandlerComponent::~USelectionHandlerComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectionHandlerComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectionHandlerComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USelectionHandlerComponent, USelectionHandlerComponent::StaticClass, TEXT("USelectionHandlerComponent"), &Z_Registration_Info_UClass_USelectionHandlerComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USelectionHandlerComponent), 3964625730U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectionHandlerComponent_h_2199347330(TEXT("/Script/ObjectSelection"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectionHandlerComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectionHandlerComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
