// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeObjectHovering_init() {}
	OBJECTHOVERING_API UFunction* Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature();
	OBJECTHOVERING_API UFunction* Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_ObjectHovering;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_ObjectHovering()
	{
		if (!Z_Registration_Info_UPackage__Script_ObjectHovering.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/ObjectHovering",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xBC16C272,
				0x41BD40B4,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_ObjectHovering.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_ObjectHovering.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_ObjectHovering(Z_Construct_UPackage__Script_ObjectHovering, TEXT("/Script/ObjectHovering"), Z_Registration_Info_UPackage__Script_ObjectHovering, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xBC16C272, 0x41BD40B4));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
