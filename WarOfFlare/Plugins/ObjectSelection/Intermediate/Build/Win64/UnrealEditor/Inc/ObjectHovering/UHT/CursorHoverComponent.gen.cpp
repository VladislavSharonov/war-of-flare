// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectHovering/Public/ObjectHovering/CursorHoverComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCursorHoverComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverComponent();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverComponent_NoRegister();
	OBJECTHOVERING_API UFunction* Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature();
	OBJECTHOVERING_API UFunction* Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_ObjectHovering();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics
	{
		struct _Script_ObjectHovering_eventOnHoverBeginSignature_Parms
		{
			AActor* Actor;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_ObjectHovering_eventOnHoverBeginSignature_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ObjectHovering, nullptr, "OnHoverBeginSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::_Script_ObjectHovering_eventOnHoverBeginSignature_Parms), Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FOnHoverBeginSignature_DelegateWrapper(const FMulticastScriptDelegate& OnHoverBeginSignature, AActor* Actor)
{
	struct _Script_ObjectHovering_eventOnHoverBeginSignature_Parms
	{
		AActor* Actor;
	};
	_Script_ObjectHovering_eventOnHoverBeginSignature_Parms Parms;
	Parms.Actor=Actor;
	OnHoverBeginSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	struct Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics
	{
		struct _Script_ObjectHovering_eventOnHoverEndSignature_Parms
		{
			AActor* Actor;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_ObjectHovering_eventOnHoverEndSignature_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ObjectHovering, nullptr, "OnHoverEndSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::_Script_ObjectHovering_eventOnHoverEndSignature_Parms), Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FOnHoverEndSignature_DelegateWrapper(const FMulticastScriptDelegate& OnHoverEndSignature, AActor* Actor)
{
	struct _Script_ObjectHovering_eventOnHoverEndSignature_Parms
	{
		AActor* Actor;
	};
	_Script_ObjectHovering_eventOnHoverEndSignature_Parms Parms;
	Parms.Actor=Actor;
	OnHoverEndSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(UCursorHoverComponent::execGetCheckDistance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetCheckDistance();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execSetCheckDistance)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InCheckDistance);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCheckDistance(Z_Param_InCheckDistance);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execGetTraceChannel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TEnumAsByte<ECollisionChannel>*)Z_Param__Result=P_THIS->GetTraceChannel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execSetTraceChannel)
	{
		P_GET_PROPERTY(FByteProperty,Z_Param_InCollisionChannel);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTraceChannel(ECollisionChannel(Z_Param_InCollisionChannel));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execGetHoveredActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=P_THIS->GetHoveredActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execClearFilter)
	{
		P_GET_OBJECT(UClass,Z_Param_FilterClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearFilter(Z_Param_FilterClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execRemoveFilter)
	{
		P_GET_OBJECT(UClass,Z_Param_FilterClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveFilter(Z_Param_FilterClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execAddFilter)
	{
		P_GET_OBJECT(UClass,Z_Param_FilterClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddFilter(Z_Param_FilterClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverComponent::execHover)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Hover();
		P_NATIVE_END;
	}
	void UCursorHoverComponent::StaticRegisterNativesUCursorHoverComponent()
	{
		UClass* Class = UCursorHoverComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddFilter", &UCursorHoverComponent::execAddFilter },
			{ "ClearFilter", &UCursorHoverComponent::execClearFilter },
			{ "GetCheckDistance", &UCursorHoverComponent::execGetCheckDistance },
			{ "GetHoveredActor", &UCursorHoverComponent::execGetHoveredActor },
			{ "GetTraceChannel", &UCursorHoverComponent::execGetTraceChannel },
			{ "Hover", &UCursorHoverComponent::execHover },
			{ "RemoveFilter", &UCursorHoverComponent::execRemoveFilter },
			{ "SetCheckDistance", &UCursorHoverComponent::execSetCheckDistance },
			{ "SetTraceChannel", &UCursorHoverComponent::execSetTraceChannel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics
	{
		struct CursorHoverComponent_eventAddFilter_Parms
		{
			const TSubclassOf<AActor>  FilterClass;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FilterClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_FilterClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::NewProp_FilterClass_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::NewProp_FilterClass = { "FilterClass", nullptr, (EPropertyFlags)0x0014000000000082, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventAddFilter_Parms, FilterClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::NewProp_FilterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::NewProp_FilterClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::NewProp_FilterClass,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filtering" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "AddFilter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::CursorHoverComponent_eventAddFilter_Parms), Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_AddFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_AddFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics
	{
		struct CursorHoverComponent_eventClearFilter_Parms
		{
			const TSubclassOf<AActor>  FilterClass;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FilterClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_FilterClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::NewProp_FilterClass_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::NewProp_FilterClass = { "FilterClass", nullptr, (EPropertyFlags)0x0014000000000082, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventClearFilter_Parms, FilterClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::NewProp_FilterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::NewProp_FilterClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::NewProp_FilterClass,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filtering" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "ClearFilter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::CursorHoverComponent_eventClearFilter_Parms), Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_ClearFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_ClearFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics
	{
		struct CursorHoverComponent_eventGetCheckDistance_Parms
		{
			float ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventGetCheckDistance_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "GetCheckDistance", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::CursorHoverComponent_eventGetCheckDistance_Parms), Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics
	{
		struct CursorHoverComponent_eventGetHoveredActor_Parms
		{
			AActor* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventGetHoveredActor_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "GetHoveredActor", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::CursorHoverComponent_eventGetHoveredActor_Parms), Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics
	{
		struct CursorHoverComponent_eventGetTraceChannel_Parms
		{
			TEnumAsByte<ECollisionChannel> ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventGetTraceChannel_Parms, ReturnValue), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(nullptr, 0) }; // 727872708
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "GetTraceChannel", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::CursorHoverComponent_eventGetTraceChannel_Parms), Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_Hover_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_Hover_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_Hover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "Hover", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_Hover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_Hover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_Hover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_Hover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics
	{
		struct CursorHoverComponent_eventRemoveFilter_Parms
		{
			const TSubclassOf<AActor>  FilterClass;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FilterClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_FilterClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::NewProp_FilterClass_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::NewProp_FilterClass = { "FilterClass", nullptr, (EPropertyFlags)0x0014000000000082, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventRemoveFilter_Parms, FilterClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::NewProp_FilterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::NewProp_FilterClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::NewProp_FilterClass,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "Filtering" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "RemoveFilter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::CursorHoverComponent_eventRemoveFilter_Parms), Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics
	{
		struct CursorHoverComponent_eventSetCheckDistance_Parms
		{
			float InCheckDistance;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_InCheckDistance;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::NewProp_InCheckDistance = { "InCheckDistance", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventSetCheckDistance_Parms, InCheckDistance), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::NewProp_InCheckDistance,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "SetCheckDistance", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::CursorHoverComponent_eventSetCheckDistance_Parms), Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics
	{
		struct CursorHoverComponent_eventSetTraceChannel_Parms
		{
			TEnumAsByte<ECollisionChannel> InCollisionChannel;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InCollisionChannel_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_InCollisionChannel;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::NewProp_InCollisionChannel_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::NewProp_InCollisionChannel = { "InCollisionChannel", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CursorHoverComponent_eventSetTraceChannel_Parms, InCollisionChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::NewProp_InCollisionChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::NewProp_InCollisionChannel_MetaData)) }; // 727872708
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::NewProp_InCollisionChannel,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::Function_MetaDataParams[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverComponent, nullptr, "SetTraceChannel", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::CursorHoverComponent_eventSetTraceChannel_Parms), Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCursorHoverComponent);
	UClass* Z_Construct_UClass_UCursorHoverComponent_NoRegister()
	{
		return UCursorHoverComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCursorHoverComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_ClassFilter_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ClassFilter_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ClassFilter;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnHoverBegin_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnHoverBegin;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnHoverEnd_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnHoverEnd;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Hovered_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_Hovered;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CollisionChannel_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_CollisionChannel;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CheckDistance_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_CheckDistance;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCursorHoverComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectHovering,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCursorHoverComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCursorHoverComponent_AddFilter, "AddFilter" }, // 781782318
		{ &Z_Construct_UFunction_UCursorHoverComponent_ClearFilter, "ClearFilter" }, // 3234588964
		{ &Z_Construct_UFunction_UCursorHoverComponent_GetCheckDistance, "GetCheckDistance" }, // 3173104695
		{ &Z_Construct_UFunction_UCursorHoverComponent_GetHoveredActor, "GetHoveredActor" }, // 1288549530
		{ &Z_Construct_UFunction_UCursorHoverComponent_GetTraceChannel, "GetTraceChannel" }, // 3033502656
		{ &Z_Construct_UFunction_UCursorHoverComponent_Hover, "Hover" }, // 2224104416
		{ &Z_Construct_UFunction_UCursorHoverComponent_RemoveFilter, "RemoveFilter" }, // 1254041840
		{ &Z_Construct_UFunction_UCursorHoverComponent_SetCheckDistance, "SetCheckDistance" }, // 3057256693
		{ &Z_Construct_UFunction_UCursorHoverComponent_SetTraceChannel, "SetTraceChannel" }, // 3319867532
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "ObjectHovering/CursorHoverComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter_Inner = { "ClassFilter", nullptr, (EPropertyFlags)0x0004000000000000, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter_MetaData[] = {
		{ "Category", "Filtering" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter = { "ClassFilter", nullptr, (EPropertyFlags)0x0024080000000005, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorHoverComponent, ClassFilter), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverBegin_MetaData[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverBegin = { "OnHoverBegin", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorHoverComponent, OnHoverBegin), Z_Construct_UDelegateFunction_ObjectHovering_OnHoverBeginSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverBegin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverBegin_MetaData)) }; // 2083601345
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverEnd_MetaData[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverEnd = { "OnHoverEnd", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorHoverComponent, OnHoverEnd), Z_Construct_UDelegateFunction_ObjectHovering_OnHoverEndSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverEnd_MetaData)) }; // 3869096704
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_Hovered_MetaData[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_Hovered = { "Hovered", nullptr, (EPropertyFlags)0x0024080000020015, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorHoverComponent, Hovered), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_Hovered_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_Hovered_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CollisionChannel_MetaData[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CollisionChannel = { "CollisionChannel", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorHoverComponent, CollisionChannel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CollisionChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CollisionChannel_MetaData)) }; // 727872708
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CheckDistance_MetaData[] = {
		{ "Category", "Hover" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverComponent.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CheckDistance = { "CheckDistance", nullptr, (EPropertyFlags)0x0020080000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCursorHoverComponent, CheckDistance), METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CheckDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CheckDistance_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCursorHoverComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_ClassFilter,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverBegin,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_OnHoverEnd,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_Hovered,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CollisionChannel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverComponent_Statics::NewProp_CheckDistance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCursorHoverComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCursorHoverComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCursorHoverComponent_Statics::ClassParams = {
		&UCursorHoverComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCursorHoverComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCursorHoverComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCursorHoverComponent()
	{
		if (!Z_Registration_Info_UClass_UCursorHoverComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCursorHoverComponent.OuterSingleton, Z_Construct_UClass_UCursorHoverComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCursorHoverComponent.OuterSingleton;
	}
	template<> OBJECTHOVERING_API UClass* StaticClass<UCursorHoverComponent>()
	{
		return UCursorHoverComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCursorHoverComponent);
	UCursorHoverComponent::~UCursorHoverComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCursorHoverComponent, UCursorHoverComponent::StaticClass, TEXT("UCursorHoverComponent"), &Z_Registration_Info_UClass_UCursorHoverComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCursorHoverComponent), 767438899U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_2000832463(TEXT("/Script/ObjectHovering"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
