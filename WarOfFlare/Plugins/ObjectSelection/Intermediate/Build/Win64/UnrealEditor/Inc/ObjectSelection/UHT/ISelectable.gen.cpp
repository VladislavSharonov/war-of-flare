// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectSelection/Public/ObjectSelection/ISelectable.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeISelectable() {}
// Cross Module References
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UHoverable();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectable();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectable_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ObjectSelection();
// End Cross Module References
	DEFINE_FUNCTION(ISelectable::execOnDeselect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDeselect_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ISelectable::execOnSelect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnSelect_Implementation();
		P_NATIVE_END;
	}
	void ISelectable::OnDeselect()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnDeselect instead.");
	}
	void ISelectable::OnSelect()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnSelect instead.");
	}
	void USelectable::StaticRegisterNativesUSelectable()
	{
		UClass* Class = USelectable::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnDeselect", &ISelectable::execOnDeselect },
			{ "OnSelect", &ISelectable::execOnSelect },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USelectable_OnDeselect_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectable_OnDeselect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectSelection/ISelectable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectable_OnDeselect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectable, nullptr, "OnDeselect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectable_OnDeselect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectable_OnDeselect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectable_OnDeselect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectable_OnDeselect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectable_OnSelect_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectable_OnSelect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectSelection/ISelectable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectable_OnSelect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectable, nullptr, "OnSelect", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectable_OnSelect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectable_OnSelect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectable_OnSelect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectable_OnSelect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USelectable);
	UClass* Z_Construct_UClass_USelectable_NoRegister()
	{
		return USelectable::StaticClass();
	}
	struct Z_Construct_UClass_USelectable_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UHoverable,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectSelection,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USelectable_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USelectable_OnDeselect, "OnDeselect" }, // 829707158
		{ &Z_Construct_UFunction_USelectable_OnSelect, "OnSelect" }, // 400003192
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectable_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectSelection/ISelectable.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ISelectable>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USelectable_Statics::ClassParams = {
		&USelectable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_USelectable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectable()
	{
		if (!Z_Registration_Info_UClass_USelectable.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USelectable.OuterSingleton, Z_Construct_UClass_USelectable_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USelectable.OuterSingleton;
	}
	template<> OBJECTSELECTION_API UClass* StaticClass<USelectable>()
	{
		return USelectable::StaticClass();
	}
	USelectable::USelectable(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectable);
	USelectable::~USelectable() {}
	static FName NAME_USelectable_OnDeselect = FName(TEXT("OnDeselect"));
	void ISelectable::Execute_OnDeselect(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(USelectable::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_USelectable_OnDeselect);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (ISelectable*)(O->GetNativeInterfaceAddress(USelectable::StaticClass())))
		{
			I->OnDeselect_Implementation();
		}
	}
	static FName NAME_USelectable_OnSelect = FName(TEXT("OnSelect"));
	void ISelectable::Execute_OnSelect(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(USelectable::StaticClass()));
		UFunction* const Func = O->FindFunction(NAME_USelectable_OnSelect);
		if (Func)
		{
			O->ProcessEvent(Func, NULL);
		}
		else if (auto I = (ISelectable*)(O->GetNativeInterfaceAddress(USelectable::StaticClass())))
		{
			I->OnSelect_Implementation();
		}
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USelectable, USelectable::StaticClass, TEXT("USelectable"), &Z_Registration_Info_UClass_USelectable, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USelectable), 3916643512U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_1396970579(TEXT("/Script/ObjectSelection"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
