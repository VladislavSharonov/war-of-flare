// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectHovering/CursorHoverHandlerComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OBJECTHOVERING_CursorHoverHandlerComponent_generated_h
#error "CursorHoverHandlerComponent.generated.h already included, missing '#pragma once' in CursorHoverHandlerComponent.h"
#endif
#define OBJECTHOVERING_CursorHoverHandlerComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEndHover); \
	DECLARE_FUNCTION(execBeginHover); \
	DECLARE_FUNCTION(execIsHovered);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEndHover); \
	DECLARE_FUNCTION(execBeginHover); \
	DECLARE_FUNCTION(execIsHovered);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCursorHoverHandlerComponent(); \
	friend struct Z_Construct_UClass_UCursorHoverHandlerComponent_Statics; \
public: \
	DECLARE_CLASS(UCursorHoverHandlerComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectHovering"), NO_API) \
	DECLARE_SERIALIZER(UCursorHoverHandlerComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUCursorHoverHandlerComponent(); \
	friend struct Z_Construct_UClass_UCursorHoverHandlerComponent_Statics; \
public: \
	DECLARE_CLASS(UCursorHoverHandlerComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectHovering"), NO_API) \
	DECLARE_SERIALIZER(UCursorHoverHandlerComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCursorHoverHandlerComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCursorHoverHandlerComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCursorHoverHandlerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCursorHoverHandlerComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCursorHoverHandlerComponent(UCursorHoverHandlerComponent&&); \
	NO_API UCursorHoverHandlerComponent(const UCursorHoverHandlerComponent&); \
public: \
	NO_API virtual ~UCursorHoverHandlerComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCursorHoverHandlerComponent(UCursorHoverHandlerComponent&&); \
	NO_API UCursorHoverHandlerComponent(const UCursorHoverHandlerComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCursorHoverHandlerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCursorHoverHandlerComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCursorHoverHandlerComponent) \
	NO_API virtual ~UCursorHoverHandlerComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_10_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTHOVERING_API UClass* StaticClass<class UCursorHoverHandlerComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
