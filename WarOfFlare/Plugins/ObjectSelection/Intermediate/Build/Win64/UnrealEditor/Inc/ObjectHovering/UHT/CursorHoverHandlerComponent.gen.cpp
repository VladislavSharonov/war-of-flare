// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectHovering/Public/ObjectHovering/CursorHoverHandlerComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCursorHoverHandlerComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverHandlerComponent();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverHandlerComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ObjectHovering();
// End Cross Module References
	DEFINE_FUNCTION(UCursorHoverHandlerComponent::execEndHover)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndHover();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverHandlerComponent::execBeginHover)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BeginHover();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCursorHoverHandlerComponent::execIsHovered)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsHovered();
		P_NATIVE_END;
	}
	void UCursorHoverHandlerComponent::StaticRegisterNativesUCursorHoverHandlerComponent()
	{
		UClass* Class = UCursorHoverHandlerComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BeginHover", &UCursorHoverHandlerComponent::execBeginHover },
			{ "EndHover", &UCursorHoverHandlerComponent::execEndHover },
			{ "IsHovered", &UCursorHoverHandlerComponent::execIsHovered },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverHandlerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverHandlerComponent, nullptr, "BeginHover", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverHandlerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverHandlerComponent, nullptr, "EndHover", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics
	{
		struct CursorHoverHandlerComponent_eventIsHovered_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CursorHoverHandlerComponent_eventIsHovered_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CursorHoverHandlerComponent_eventIsHovered_Parms), &Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverHandlerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCursorHoverHandlerComponent, nullptr, "IsHovered", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::CursorHoverHandlerComponent_eventIsHovered_Parms), Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCursorHoverHandlerComponent);
	UClass* Z_Construct_UClass_UCursorHoverHandlerComponent_NoRegister()
	{
		return UCursorHoverHandlerComponent::StaticClass();
	}
	struct Z_Construct_UClass_UCursorHoverHandlerComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_bIsHovered_MetaData[];
#endif
		static void NewProp_bIsHovered_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bIsHovered;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectHovering,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCursorHoverHandlerComponent_BeginHover, "BeginHover" }, // 3830213093
		{ &Z_Construct_UFunction_UCursorHoverHandlerComponent_EndHover, "EndHover" }, // 1296633343
		{ &Z_Construct_UFunction_UCursorHoverHandlerComponent_IsHovered, "IsHovered" }, // 21838933
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "ObjectHovering/CursorHoverHandlerComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverHandlerComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered_MetaData[] = {
		{ "ModuleRelativePath", "Public/ObjectHovering/CursorHoverHandlerComponent.h" },
	};
#endif
	void Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered_SetBit(void* Obj)
	{
		((UCursorHoverHandlerComponent*)Obj)->bIsHovered = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered = { "bIsHovered", nullptr, (EPropertyFlags)0x0020080000000000, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(UCursorHoverHandlerComponent), &Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::NewProp_bIsHovered,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCursorHoverHandlerComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::ClassParams = {
		&UCursorHoverHandlerComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCursorHoverHandlerComponent()
	{
		if (!Z_Registration_Info_UClass_UCursorHoverHandlerComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCursorHoverHandlerComponent.OuterSingleton, Z_Construct_UClass_UCursorHoverHandlerComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCursorHoverHandlerComponent.OuterSingleton;
	}
	template<> OBJECTHOVERING_API UClass* StaticClass<UCursorHoverHandlerComponent>()
	{
		return UCursorHoverHandlerComponent::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCursorHoverHandlerComponent);
	UCursorHoverHandlerComponent::~UCursorHoverHandlerComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCursorHoverHandlerComponent, UCursorHoverHandlerComponent::StaticClass, TEXT("UCursorHoverHandlerComponent"), &Z_Registration_Info_UClass_UCursorHoverHandlerComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCursorHoverHandlerComponent), 174515339U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_4286918095(TEXT("/Script/ObjectHovering"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverHandlerComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
