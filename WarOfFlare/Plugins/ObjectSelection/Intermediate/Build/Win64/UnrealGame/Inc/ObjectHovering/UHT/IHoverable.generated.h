// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectHovering/IHoverable.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OBJECTHOVERING_IHoverable_generated_h
#error "IHoverable.generated.h already included, missing '#pragma once' in IHoverable.h"
#endif
#define OBJECTHOVERING_IHoverable_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_RPC_WRAPPERS \
	virtual void OnHoverEnd_Implementation() {}; \
	virtual void OnHoverBegin_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnHoverEnd); \
	DECLARE_FUNCTION(execOnHoverBegin);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnHoverEnd_Implementation() {}; \
	virtual void OnHoverBegin_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnHoverEnd); \
	DECLARE_FUNCTION(execOnHoverBegin);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OBJECTHOVERING_API UHoverable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoverable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OBJECTHOVERING_API, UHoverable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoverable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OBJECTHOVERING_API UHoverable(UHoverable&&); \
	OBJECTHOVERING_API UHoverable(const UHoverable&); \
public: \
	OBJECTHOVERING_API virtual ~UHoverable();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OBJECTHOVERING_API UHoverable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OBJECTHOVERING_API UHoverable(UHoverable&&); \
	OBJECTHOVERING_API UHoverable(const UHoverable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OBJECTHOVERING_API, UHoverable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHoverable); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHoverable) \
	OBJECTHOVERING_API virtual ~UHoverable();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUHoverable(); \
	friend struct Z_Construct_UClass_UHoverable_Statics; \
public: \
	DECLARE_CLASS(UHoverable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/ObjectHovering"), OBJECTHOVERING_API) \
	DECLARE_SERIALIZER(UHoverable)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_GENERATED_UINTERFACE_BODY() \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_GENERATED_UINTERFACE_BODY() \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IHoverable() {} \
public: \
	typedef UHoverable UClassType; \
	typedef IHoverable ThisClass; \
	static void Execute_OnHoverBegin(UObject* O); \
	static void Execute_OnHoverEnd(UObject* O); \
	virtual UObject* _getUObject() const { return nullptr; }


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IHoverable() {} \
public: \
	typedef UHoverable UClassType; \
	typedef IHoverable ThisClass; \
	static void Execute_OnHoverBegin(UObject* O); \
	static void Execute_OnHoverEnd(UObject* O); \
	virtual UObject* _getUObject() const { return nullptr; }


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_10_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTHOVERING_API UClass* StaticClass<class UHoverable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_IHoverable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
