// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectSelection/ISelectable.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OBJECTSELECTION_ISelectable_generated_h
#error "ISelectable.generated.h already included, missing '#pragma once' in ISelectable.h"
#endif
#define OBJECTSELECTION_ISelectable_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_RPC_WRAPPERS \
	virtual void OnDeselect_Implementation() {}; \
	virtual void OnSelect_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnDeselect); \
	DECLARE_FUNCTION(execOnSelect);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnDeselect_Implementation() {}; \
	virtual void OnSelect_Implementation() {}; \
 \
	DECLARE_FUNCTION(execOnDeselect); \
	DECLARE_FUNCTION(execOnSelect);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OBJECTSELECTION_API USelectable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OBJECTSELECTION_API, USelectable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OBJECTSELECTION_API USelectable(USelectable&&); \
	OBJECTSELECTION_API USelectable(const USelectable&); \
public: \
	OBJECTSELECTION_API virtual ~USelectable();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OBJECTSELECTION_API USelectable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OBJECTSELECTION_API USelectable(USelectable&&); \
	OBJECTSELECTION_API USelectable(const USelectable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OBJECTSELECTION_API, USelectable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USelectable); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USelectable) \
	OBJECTSELECTION_API virtual ~USelectable();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUSelectable(); \
	friend struct Z_Construct_UClass_USelectable_Statics; \
public: \
	DECLARE_CLASS(USelectable, UHoverable, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/ObjectSelection"), OBJECTSELECTION_API) \
	DECLARE_SERIALIZER(USelectable)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_GENERATED_UINTERFACE_BODY() \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_GENERATED_UINTERFACE_BODY() \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ISelectable() {} \
public: \
	typedef USelectable UClassType; \
	typedef ISelectable ThisClass; \
	static void Execute_OnDeselect(UObject* O); \
	static void Execute_OnSelect(UObject* O); \
	virtual UObject* _getUObject() const { return nullptr; }


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_INCLASS_IINTERFACE \
protected: \
	virtual ~ISelectable() {} \
public: \
	typedef USelectable UClassType; \
	typedef ISelectable ThisClass; \
	static void Execute_OnDeselect(UObject* O); \
	static void Execute_OnSelect(UObject* O); \
	virtual UObject* _getUObject() const { return nullptr; }


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_11_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h_14_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTSELECTION_API UClass* StaticClass<class USelectable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_ISelectable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
