// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ObjectSelection/Public/ObjectSelection/SelectorComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSelectorComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UCursorHoverComponent();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectorComponent();
	OBJECTSELECTION_API UClass* Z_Construct_UClass_USelectorComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ObjectSelection();
// End Cross Module References
	DEFINE_FUNCTION(USelectorComponent::execIsChanged)
	{
		P_GET_OBJECT(AActor,Z_Param_NewActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsChanged(Z_Param_NewActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectorComponent::execGetDeselectIfCantSelect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetDeselectIfCantSelect();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectorComponent::execSetDeselectIfCantSelect)
	{
		P_GET_UBOOL(Z_Param_InDeselectIfCantSelect);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDeselectIfCantSelect(Z_Param_InDeselectIfCantSelect);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectorComponent::execGetIsExclusive)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetIsExclusive();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectorComponent::execSetExclusive)
	{
		P_GET_UBOOL(Z_Param_InIsExclusive);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetExclusive(Z_Param_InIsExclusive);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectorComponent::execDeselectAll)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeselectAll();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USelectorComponent::execSelect)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=P_THIS->Select();
		P_NATIVE_END;
	}
	void USelectorComponent::StaticRegisterNativesUSelectorComponent()
	{
		UClass* Class = USelectorComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DeselectAll", &USelectorComponent::execDeselectAll },
			{ "GetDeselectIfCantSelect", &USelectorComponent::execGetDeselectIfCantSelect },
			{ "GetIsExclusive", &USelectorComponent::execGetIsExclusive },
			{ "IsChanged", &USelectorComponent::execIsChanged },
			{ "Select", &USelectorComponent::execSelect },
			{ "SetDeselectIfCantSelect", &USelectorComponent::execSetDeselectIfCantSelect },
			{ "SetExclusive", &USelectorComponent::execSetExclusive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USelectorComponent_DeselectAll_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_DeselectAll_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_DeselectAll_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "DeselectAll", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_DeselectAll_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_DeselectAll_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_DeselectAll()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_DeselectAll_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics
	{
		struct SelectorComponent_eventGetDeselectIfCantSelect_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SelectorComponent_eventGetDeselectIfCantSelect_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(SelectorComponent_eventGetDeselectIfCantSelect_Parms), &Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "GetDeselectIfCantSelect", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::SelectorComponent_eventGetDeselectIfCantSelect_Parms), Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics
	{
		struct SelectorComponent_eventGetIsExclusive_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SelectorComponent_eventGetIsExclusive_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(SelectorComponent_eventGetIsExclusive_Parms), &Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "GetIsExclusive", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::SelectorComponent_eventGetIsExclusive_Parms), Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_GetIsExclusive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_GetIsExclusive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectorComponent_IsChanged_Statics
	{
		struct SelectorComponent_eventIsChanged_Parms
		{
			AActor* NewActor;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_NewActor;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::NewProp_NewActor = { "NewActor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SelectorComponent_eventIsChanged_Parms, NewActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SelectorComponent_eventIsChanged_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(SelectorComponent_eventIsChanged_Parms), &Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::NewProp_NewActor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "IsChanged", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::SelectorComponent_eventIsChanged_Parms), Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_IsChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_IsChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectorComponent_Select_Statics
	{
		struct SelectorComponent_eventSelect_Parms
		{
			AActor* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USelectorComponent_Select_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SelectorComponent_eventSelect_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectorComponent_Select_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_Select_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_Select_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_Select_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "Select", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectorComponent_Select_Statics::SelectorComponent_eventSelect_Parms), Z_Construct_UFunction_USelectorComponent_Select_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_Select_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_Select_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_Select_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_Select()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_Select_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics
	{
		struct SelectorComponent_eventSetDeselectIfCantSelect_Parms
		{
			bool InDeselectIfCantSelect;
		};
		static void NewProp_InDeselectIfCantSelect_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InDeselectIfCantSelect;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::NewProp_InDeselectIfCantSelect_SetBit(void* Obj)
	{
		((SelectorComponent_eventSetDeselectIfCantSelect_Parms*)Obj)->InDeselectIfCantSelect = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::NewProp_InDeselectIfCantSelect = { "InDeselectIfCantSelect", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(SelectorComponent_eventSetDeselectIfCantSelect_Parms), &Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::NewProp_InDeselectIfCantSelect_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::NewProp_InDeselectIfCantSelect,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "SetDeselectIfCantSelect", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::SelectorComponent_eventSetDeselectIfCantSelect_Parms), Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics
	{
		struct SelectorComponent_eventSetExclusive_Parms
		{
			bool InIsExclusive;
		};
		static void NewProp_InIsExclusive_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_InIsExclusive;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::NewProp_InIsExclusive_SetBit(void* Obj)
	{
		((SelectorComponent_eventSetExclusive_Parms*)Obj)->InIsExclusive = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::NewProp_InIsExclusive = { "InIsExclusive", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(SelectorComponent_eventSetExclusive_Parms), &Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::NewProp_InIsExclusive_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::NewProp_InIsExclusive,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::Function_MetaDataParams[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USelectorComponent, nullptr, "SetExclusive", nullptr, nullptr, sizeof(Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::SelectorComponent_eventSetExclusive_Parms), Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USelectorComponent_SetExclusive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USelectorComponent_SetExclusive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USelectorComponent);
	UClass* Z_Construct_UClass_USelectorComponent_NoRegister()
	{
		return USelectorComponent::StaticClass();
	}
	struct Z_Construct_UClass_USelectorComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_Selected_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Selected_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Selected;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsExclusive_MetaData[];
#endif
		static void NewProp_IsExclusive_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsExclusive;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DeselectIfCantSelect_MetaData[];
#endif
		static void NewProp_DeselectIfCantSelect_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_DeselectIfCantSelect;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USelectorComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCursorHoverComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ObjectSelection,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USelectorComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USelectorComponent_DeselectAll, "DeselectAll" }, // 4119345984
		{ &Z_Construct_UFunction_USelectorComponent_GetDeselectIfCantSelect, "GetDeselectIfCantSelect" }, // 4286426217
		{ &Z_Construct_UFunction_USelectorComponent_GetIsExclusive, "GetIsExclusive" }, // 1283665321
		{ &Z_Construct_UFunction_USelectorComponent_IsChanged, "IsChanged" }, // 2336675233
		{ &Z_Construct_UFunction_USelectorComponent_Select, "Select" }, // 1445093990
		{ &Z_Construct_UFunction_USelectorComponent_SetDeselectIfCantSelect, "SetDeselectIfCantSelect" }, // 4212577808
		{ &Z_Construct_UFunction_USelectorComponent_SetExclusive, "SetExclusive" }, // 656765128
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectorComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "Comment", "// Not work with bEnableClickEvents in PlayerController\n" },
		{ "IncludePath", "ObjectSelection/SelectorComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
		{ "ToolTip", "Not work with bEnableClickEvents in PlayerController" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected_Inner = { "Selected", nullptr, (EPropertyFlags)0x0004000000020000, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected_MetaData[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected = { "Selected", nullptr, (EPropertyFlags)0x0024080000020015, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USelectorComponent, Selected), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive_MetaData[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	void Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive_SetBit(void* Obj)
	{
		((USelectorComponent*)Obj)->IsExclusive = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive = { "IsExclusive", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(USelectorComponent), &Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect_MetaData[] = {
		{ "Category", "ObjectSelector" },
		{ "ModuleRelativePath", "Public/ObjectSelection/SelectorComponent.h" },
	};
#endif
	void Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect_SetBit(void* Obj)
	{
		((USelectorComponent*)Obj)->DeselectIfCantSelect = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect = { "DeselectIfCantSelect", nullptr, (EPropertyFlags)0x0020080000000015, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(USelectorComponent), &Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect_SetBit, METADATA_PARAMS(Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USelectorComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelectorComponent_Statics::NewProp_Selected,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelectorComponent_Statics::NewProp_IsExclusive,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USelectorComponent_Statics::NewProp_DeselectIfCantSelect,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USelectorComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USelectorComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USelectorComponent_Statics::ClassParams = {
		&USelectorComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_USelectorComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_USelectorComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USelectorComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USelectorComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USelectorComponent()
	{
		if (!Z_Registration_Info_UClass_USelectorComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USelectorComponent.OuterSingleton, Z_Construct_UClass_USelectorComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USelectorComponent.OuterSingleton;
	}
	template<> OBJECTSELECTION_API UClass* StaticClass<USelectorComponent>()
	{
		return USelectorComponent::StaticClass();
	}
	USelectorComponent::USelectorComponent() {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USelectorComponent);
	USelectorComponent::~USelectorComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USelectorComponent, USelectorComponent::StaticClass, TEXT("USelectorComponent"), &Z_Registration_Info_UClass_USelectorComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USelectorComponent), 4079208376U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_2964015690(TEXT("/Script/ObjectSelection"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectSelection_Public_ObjectSelection_SelectorComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
