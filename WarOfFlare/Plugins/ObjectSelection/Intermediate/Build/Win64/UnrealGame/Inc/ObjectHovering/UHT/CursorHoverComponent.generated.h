// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ObjectHovering/CursorHoverComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef OBJECTHOVERING_CursorHoverComponent_generated_h
#error "CursorHoverComponent.generated.h already included, missing '#pragma once' in CursorHoverComponent.h"
#endif
#define OBJECTHOVERING_CursorHoverComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_10_DELEGATE \
OBJECTHOVERING_API void FOnHoverBeginSignature_DelegateWrapper(const FMulticastScriptDelegate& OnHoverBeginSignature, AActor* Actor);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_12_DELEGATE \
OBJECTHOVERING_API void FOnHoverEndSignature_DelegateWrapper(const FMulticastScriptDelegate& OnHoverEndSignature, AActor* Actor);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCheckDistance); \
	DECLARE_FUNCTION(execSetCheckDistance); \
	DECLARE_FUNCTION(execGetTraceChannel); \
	DECLARE_FUNCTION(execSetTraceChannel); \
	DECLARE_FUNCTION(execGetHoveredActor); \
	DECLARE_FUNCTION(execClearFilter); \
	DECLARE_FUNCTION(execRemoveFilter); \
	DECLARE_FUNCTION(execAddFilter); \
	DECLARE_FUNCTION(execHover);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCheckDistance); \
	DECLARE_FUNCTION(execSetCheckDistance); \
	DECLARE_FUNCTION(execGetTraceChannel); \
	DECLARE_FUNCTION(execSetTraceChannel); \
	DECLARE_FUNCTION(execGetHoveredActor); \
	DECLARE_FUNCTION(execClearFilter); \
	DECLARE_FUNCTION(execRemoveFilter); \
	DECLARE_FUNCTION(execAddFilter); \
	DECLARE_FUNCTION(execHover);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCursorHoverComponent(); \
	friend struct Z_Construct_UClass_UCursorHoverComponent_Statics; \
public: \
	DECLARE_CLASS(UCursorHoverComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectHovering"), NO_API) \
	DECLARE_SERIALIZER(UCursorHoverComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCursorHoverComponent(); \
	friend struct Z_Construct_UClass_UCursorHoverComponent_Statics; \
public: \
	DECLARE_CLASS(UCursorHoverComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ObjectHovering"), NO_API) \
	DECLARE_SERIALIZER(UCursorHoverComponent)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCursorHoverComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCursorHoverComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCursorHoverComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCursorHoverComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCursorHoverComponent(UCursorHoverComponent&&); \
	NO_API UCursorHoverComponent(const UCursorHoverComponent&); \
public: \
	NO_API virtual ~UCursorHoverComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCursorHoverComponent(UCursorHoverComponent&&); \
	NO_API UCursorHoverComponent(const UCursorHoverComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCursorHoverComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCursorHoverComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCursorHoverComponent) \
	NO_API virtual ~UCursorHoverComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OBJECTHOVERING_API UClass* StaticClass<class UCursorHoverComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_ObjectSelection_Source_ObjectHovering_Public_ObjectHovering_CursorHoverComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
