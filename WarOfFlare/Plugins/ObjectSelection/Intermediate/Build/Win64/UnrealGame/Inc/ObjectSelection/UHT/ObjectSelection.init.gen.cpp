// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeObjectSelection_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_ObjectSelection;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_ObjectSelection()
	{
		if (!Z_Registration_Info_UPackage__Script_ObjectSelection.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/ObjectSelection",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0x31C25B2C,
				0x6C2C9568,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_ObjectSelection.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_ObjectSelection.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_ObjectSelection(Z_Construct_UPackage__Script_ObjectSelection, TEXT("/Script/ObjectSelection"), Z_Registration_Info_UPackage__Script_ObjectSelection, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x31C25B2C, 0x6C2C9568));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
