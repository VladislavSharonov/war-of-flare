// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Spawn/Public/Spawn/SpawnerCluster.h"
#include "Spawn/Public/Spawn/SpawnData.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawnerCluster() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	SPAWN_API UClass* Z_Construct_UClass_ASpawnerCluster();
	SPAWN_API UClass* Z_Construct_UClass_ASpawnerCluster_NoRegister();
	SPAWN_API UClass* Z_Construct_UClass_ASpawnPoint_NoRegister();
	SPAWN_API UFunction* Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature();
	SPAWN_API UFunction* Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature();
	SPAWN_API UScriptStruct* Z_Construct_UScriptStruct_FSpawnDataItem();
	UPackage* Z_Construct_UPackage__Script_Spawn();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics
	{
		struct _Script_Spawn_eventActorSpawnedSignature_Parms
		{
			AActor* Actor;
			AActor* OwningPlayer;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_OwningPlayer;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Spawn_eventActorSpawnedSignature_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::NewProp_OwningPlayer = { "OwningPlayer", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Spawn_eventActorSpawnedSignature_Parms, OwningPlayer), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::NewProp_Actor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::NewProp_OwningPlayer,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Spawn, nullptr, "ActorSpawnedSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::_Script_Spawn_eventActorSpawnedSignature_Parms), Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FActorSpawnedSignature_DelegateWrapper(const FMulticastScriptDelegate& ActorSpawnedSignature, AActor* Actor, AActor* OwningPlayer)
{
	struct _Script_Spawn_eventActorSpawnedSignature_Parms
	{
		AActor* Actor;
		AActor* OwningPlayer;
	};
	_Script_Spawn_eventActorSpawnedSignature_Parms Parms;
	Parms.Actor=Actor;
	Parms.OwningPlayer=OwningPlayer;
	ActorSpawnedSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	struct Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics
	{
		struct _Script_Spawn_eventSpawnCompletedSignature_Parms
		{
			ASpawnerCluster* SpawnerCluster;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_SpawnerCluster;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::NewProp_SpawnerCluster = { "SpawnerCluster", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Spawn_eventSpawnCompletedSignature_Parms, SpawnerCluster), Z_Construct_UClass_ASpawnerCluster_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::NewProp_SpawnerCluster,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Spawn, nullptr, "SpawnCompletedSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::_Script_Spawn_eventSpawnCompletedSignature_Parms), Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FSpawnCompletedSignature_DelegateWrapper(const FMulticastScriptDelegate& SpawnCompletedSignature, ASpawnerCluster* SpawnerCluster)
{
	struct _Script_Spawn_eventSpawnCompletedSignature_Parms
	{
		ASpawnerCluster* SpawnerCluster;
	};
	_Script_Spawn_eventSpawnCompletedSignature_Parms Parms;
	Parms.SpawnerCluster=SpawnerCluster;
	SpawnCompletedSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(ASpawnerCluster::execOnSpawnerReady)
	{
		P_GET_OBJECT(ASpawnPoint,Z_Param_SpawnPoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnSpawnerReady(Z_Param_SpawnPoint);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ASpawnerCluster::execGetNextActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSubclassOf<AActor> *)Z_Param__Result=P_THIS->GetNextActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ASpawnerCluster::execAppendSpawnQueue)
	{
		P_GET_TARRAY(FSpawnDataItem,Z_Param_InSpawnData);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AppendSpawnQueue(Z_Param_InSpawnData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ASpawnerCluster::execSpawn)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Spawn();
		P_NATIVE_END;
	}
	void ASpawnerCluster::StaticRegisterNativesASpawnerCluster()
	{
		UClass* Class = ASpawnerCluster::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AppendSpawnQueue", &ASpawnerCluster::execAppendSpawnQueue },
			{ "GetNextActor", &ASpawnerCluster::execGetNextActor },
			{ "OnSpawnerReady", &ASpawnerCluster::execOnSpawnerReady },
			{ "Spawn", &ASpawnerCluster::execSpawn },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics
	{
		struct SpawnerCluster_eventAppendSpawnQueue_Parms
		{
			TArray<FSpawnDataItem> InSpawnData;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_InSpawnData_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InSpawnData_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_InSpawnData;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData_Inner = { "InSpawnData", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FSpawnDataItem, METADATA_PARAMS(nullptr, 0) }; // 3125251731
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData = { "InSpawnData", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpawnerCluster_eventAppendSpawnQueue_Parms, InSpawnData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData_MetaData)) }; // 3125251731
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::NewProp_InSpawnData,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASpawnerCluster, nullptr, "AppendSpawnQueue", nullptr, nullptr, sizeof(Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::SpawnerCluster_eventAppendSpawnQueue_Parms), Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics
	{
		struct SpawnerCluster_eventGetNextActor_Parms
		{
			TSubclassOf<AActor>  ReturnValue;
		};
		static const UECodeGen_Private::FClassPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpawnerCluster_eventGetNextActor_Parms, ReturnValue), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASpawnerCluster, nullptr, "GetNextActor", nullptr, nullptr, sizeof(Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::SpawnerCluster_eventGetNextActor_Parms), Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASpawnerCluster_GetNextActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ASpawnerCluster_GetNextActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics
	{
		struct SpawnerCluster_eventOnSpawnerReady_Parms
		{
			ASpawnPoint* SpawnPoint;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_SpawnPoint;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::NewProp_SpawnPoint = { "SpawnPoint", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpawnerCluster_eventOnSpawnerReady_Parms, SpawnPoint), Z_Construct_UClass_ASpawnPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::NewProp_SpawnPoint,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASpawnerCluster, nullptr, "OnSpawnerReady", nullptr, nullptr, sizeof(Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::SpawnerCluster_eventOnSpawnerReady_Parms), Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ASpawnerCluster_Spawn_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ASpawnerCluster_Spawn_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ASpawnerCluster_Spawn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ASpawnerCluster, nullptr, "Spawn", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ASpawnerCluster_Spawn_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ASpawnerCluster_Spawn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ASpawnerCluster_Spawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ASpawnerCluster_Spawn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ASpawnerCluster);
	UClass* Z_Construct_UClass_ASpawnerCluster_NoRegister()
	{
		return ASpawnerCluster::StaticClass();
	}
	struct Z_Construct_UClass_ASpawnerCluster_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnActorSpawned_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnActorSpawned;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnSpawnCompleted_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSpawnCompleted;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DefaultRootComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_DefaultRootComponent;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_SpawnPoints_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SpawnPoints_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_SpawnPoints;
		static const UECodeGen_Private::FStructPropertyParams NewProp_SpawnData_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SpawnData_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_SpawnData;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpawnerCluster_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Spawn,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ASpawnerCluster_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ASpawnerCluster_AppendSpawnQueue, "AppendSpawnQueue" }, // 2280431709
		{ &Z_Construct_UFunction_ASpawnerCluster_GetNextActor, "GetNextActor" }, // 1881317878
		{ &Z_Construct_UFunction_ASpawnerCluster_OnSpawnerReady, "OnSpawnerReady" }, // 3978511741
		{ &Z_Construct_UFunction_ASpawnerCluster_Spawn, "Spawn" }, // 3696822622
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerCluster_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Spawn/SpawnerCluster.h" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnActorSpawned_MetaData[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnActorSpawned = { "OnActorSpawned", nullptr, (EPropertyFlags)0x0010000000080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ASpawnerCluster, OnActorSpawned), Z_Construct_UDelegateFunction_Spawn_ActorSpawnedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnActorSpawned_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnActorSpawned_MetaData)) }; // 2325858793
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnSpawnCompleted_MetaData[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnSpawnCompleted = { "OnSpawnCompleted", nullptr, (EPropertyFlags)0x0010000000080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ASpawnerCluster, OnSpawnCompleted), Z_Construct_UDelegateFunction_Spawn_SpawnCompletedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnSpawnCompleted_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnSpawnCompleted_MetaData)) }; // 231508104
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_DefaultRootComponent_MetaData[] = {
		{ "Category", "SpawnerCluster" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_DefaultRootComponent = { "DefaultRootComponent", nullptr, (EPropertyFlags)0x002408000008001c, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ASpawnerCluster, DefaultRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_DefaultRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_DefaultRootComponent_MetaData)) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints_Inner = { "SpawnPoints", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_ASpawnPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints_MetaData[] = {
		{ "Category", "SpawnerCluster" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints = { "SpawnPoints", nullptr, (EPropertyFlags)0x0020080000000004, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ASpawnerCluster, SpawnPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData_Inner = { "SpawnData", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FSpawnDataItem, METADATA_PARAMS(nullptr, 0) }; // 3125251731
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData_MetaData[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnerCluster.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData = { "SpawnData", nullptr, (EPropertyFlags)0x0020080000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(ASpawnerCluster, SpawnData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData_MetaData)) }; // 3125251731
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASpawnerCluster_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnActorSpawned,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_OnSpawnCompleted,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_DefaultRootComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnPoints,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpawnerCluster_Statics::NewProp_SpawnData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpawnerCluster_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpawnerCluster>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ASpawnerCluster_Statics::ClassParams = {
		&ASpawnerCluster::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ASpawnerCluster_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpawnerCluster_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpawnerCluster_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpawnerCluster()
	{
		if (!Z_Registration_Info_UClass_ASpawnerCluster.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ASpawnerCluster.OuterSingleton, Z_Construct_UClass_ASpawnerCluster_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ASpawnerCluster.OuterSingleton;
	}
	template<> SPAWN_API UClass* StaticClass<ASpawnerCluster>()
	{
		return ASpawnerCluster::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpawnerCluster);
	ASpawnerCluster::~ASpawnerCluster() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ASpawnerCluster, ASpawnerCluster::StaticClass, TEXT("ASpawnerCluster"), &Z_Registration_Info_UClass_ASpawnerCluster, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ASpawnerCluster), 1203144934U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_1868600632(TEXT("/Script/Spawn"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
