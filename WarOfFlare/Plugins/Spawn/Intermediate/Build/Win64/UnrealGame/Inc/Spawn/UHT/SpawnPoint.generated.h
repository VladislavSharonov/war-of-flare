// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Spawn/SpawnPoint.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class APlayerController;
class ASpawnPoint;
class UPrimitiveComponent;
#ifdef SPAWN_SpawnPoint_generated_h
#error "SpawnPoint.generated.h already included, missing '#pragma once' in SpawnPoint.h"
#endif
#define SPAWN_SpawnPoint_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_15_DELEGATE \
SPAWN_API void FOnSpawnPointReadyToSpawnSignature_DelegateWrapper(const FScriptDelegate& OnSpawnPointReadyToSpawnSignature, ASpawnPoint* SpawnPoint);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOccupationTriggerOverlapEnd); \
	DECLARE_FUNCTION(execSpawn); \
	DECLARE_FUNCTION(execStartSpawning);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOccupationTriggerOverlapEnd); \
	DECLARE_FUNCTION(execSpawn); \
	DECLARE_FUNCTION(execStartSpawning);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnPoint(); \
	friend struct Z_Construct_UClass_ASpawnPoint_Statics; \
public: \
	DECLARE_CLASS(ASpawnPoint, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spawn"), NO_API) \
	DECLARE_SERIALIZER(ASpawnPoint)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_INCLASS \
private: \
	static void StaticRegisterNativesASpawnPoint(); \
	friend struct Z_Construct_UClass_ASpawnPoint_Statics; \
public: \
	DECLARE_CLASS(ASpawnPoint, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spawn"), NO_API) \
	DECLARE_SERIALIZER(ASpawnPoint)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnPoint(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnPoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnPoint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnPoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnPoint(ASpawnPoint&&); \
	NO_API ASpawnPoint(const ASpawnPoint&); \
public: \
	NO_API virtual ~ASpawnPoint();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnPoint(ASpawnPoint&&); \
	NO_API ASpawnPoint(const ASpawnPoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnPoint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnPoint); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnPoint) \
	NO_API virtual ~ASpawnPoint();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_17_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPAWN_API UClass* StaticClass<class ASpawnPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnPoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
