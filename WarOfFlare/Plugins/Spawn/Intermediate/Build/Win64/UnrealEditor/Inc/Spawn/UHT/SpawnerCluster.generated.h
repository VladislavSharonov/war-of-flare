// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Spawn/SpawnerCluster.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class ASpawnerCluster;
class ASpawnPoint;
struct FSpawnDataItem;
#ifdef SPAWN_SpawnerCluster_generated_h
#error "SpawnerCluster.generated.h already included, missing '#pragma once' in SpawnerCluster.h"
#endif
#define SPAWN_SpawnerCluster_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_12_DELEGATE \
SPAWN_API void FActorSpawnedSignature_DelegateWrapper(const FMulticastScriptDelegate& ActorSpawnedSignature, AActor* Actor, AActor* OwningPlayer);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_13_DELEGATE \
SPAWN_API void FSpawnCompletedSignature_DelegateWrapper(const FMulticastScriptDelegate& SpawnCompletedSignature, ASpawnerCluster* SpawnerCluster);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnSpawnerReady); \
	DECLARE_FUNCTION(execGetNextActor); \
	DECLARE_FUNCTION(execAppendSpawnQueue); \
	DECLARE_FUNCTION(execSpawn);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnSpawnerReady); \
	DECLARE_FUNCTION(execGetNextActor); \
	DECLARE_FUNCTION(execAppendSpawnQueue); \
	DECLARE_FUNCTION(execSpawn);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnerCluster(); \
	friend struct Z_Construct_UClass_ASpawnerCluster_Statics; \
public: \
	DECLARE_CLASS(ASpawnerCluster, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spawn"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerCluster)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_INCLASS \
private: \
	static void StaticRegisterNativesASpawnerCluster(); \
	friend struct Z_Construct_UClass_ASpawnerCluster_Statics; \
public: \
	DECLARE_CLASS(ASpawnerCluster, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spawn"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerCluster)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnerCluster(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnerCluster) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerCluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerCluster); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerCluster(ASpawnerCluster&&); \
	NO_API ASpawnerCluster(const ASpawnerCluster&); \
public: \
	NO_API virtual ~ASpawnerCluster();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerCluster(ASpawnerCluster&&); \
	NO_API ASpawnerCluster(const ASpawnerCluster&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerCluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerCluster); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnerCluster) \
	NO_API virtual ~ASpawnerCluster();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_15_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPAWN_API UClass* StaticClass<class ASpawnerCluster>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnerCluster_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
