// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Spawn/Public/Spawn/SpawnData.h"
#include "Net/Serialization/FastArraySerializerImplementation.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawnData() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NETCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFastArraySerializer();
	NETCORE_API UScriptStruct* Z_Construct_UScriptStruct_FFastArraySerializerItem();
	SPAWN_API UScriptStruct* Z_Construct_UScriptStruct_FSpawnDataArray();
	SPAWN_API UScriptStruct* Z_Construct_UScriptStruct_FSpawnDataItem();
	UPackage* Z_Construct_UPackage__Script_Spawn();
// End Cross Module References

static_assert(std::is_polymorphic<FSpawnDataItem>() == std::is_polymorphic<FFastArraySerializerItem>(), "USTRUCT FSpawnDataItem cannot be polymorphic unless super FFastArraySerializerItem is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_SpawnDataItem;
class UScriptStruct* FSpawnDataItem::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_SpawnDataItem.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_SpawnDataItem.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FSpawnDataItem, (UObject*)Z_Construct_UPackage__Script_Spawn(), TEXT("SpawnDataItem"));
	}
	return Z_Registration_Info_UScriptStruct_SpawnDataItem.OuterSingleton;
}
template<> SPAWN_API UScriptStruct* StaticStruct<FSpawnDataItem>()
{
	return FSpawnDataItem::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FSpawnDataItem_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ActorClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_Count;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDataItem_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSpawnDataItem>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_ActorClass_MetaData[] = {
		{ "Category", "SpawnDataItem" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnData.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_ActorClass = { "ActorClass", nullptr, (EPropertyFlags)0x0014000000000005, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FSpawnDataItem, ActorClass), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_ActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_ActorClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_Count_MetaData[] = {
		{ "Category", "SpawnDataItem" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnData.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_Count = { "Count", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FSpawnDataItem, Count), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_Count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_Count_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSpawnDataItem_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_ActorClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewProp_Count,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSpawnDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Spawn,
		Z_Construct_UScriptStruct_FFastArraySerializerItem,
		&NewStructOps,
		"SpawnDataItem",
		sizeof(FSpawnDataItem),
		alignof(FSpawnDataItem),
		Z_Construct_UScriptStruct_FSpawnDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSpawnDataItem()
	{
		if (!Z_Registration_Info_UScriptStruct_SpawnDataItem.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_SpawnDataItem.InnerSingleton, Z_Construct_UScriptStruct_FSpawnDataItem_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_SpawnDataItem.InnerSingleton;
	}

static_assert(std::is_polymorphic<FSpawnDataArray>() == std::is_polymorphic<FFastArraySerializer>(), "USTRUCT FSpawnDataArray cannot be polymorphic unless super FFastArraySerializer is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_SpawnDataArray;
class UScriptStruct* FSpawnDataArray::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_SpawnDataArray.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_SpawnDataArray.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FSpawnDataArray, (UObject*)Z_Construct_UPackage__Script_Spawn(), TEXT("SpawnDataArray"));
	}
	return Z_Registration_Info_UScriptStruct_SpawnDataArray.OuterSingleton;
}
template<> SPAWN_API UScriptStruct* StaticStruct<FSpawnDataArray>()
{
	return FSpawnDataArray::StaticStruct();
}
#if defined(UE_NET_HAS_IRIS_FASTARRAY_BINDING) && UE_NET_HAS_IRIS_FASTARRAY_BINDING
	UE_NET_IMPLEMENT_FASTARRAY(FSpawnDataArray);
#else
	UE_NET_IMPLEMENT_FASTARRAY_STUB(FSpawnDataArray);
#endif
	struct Z_Construct_UScriptStruct_FSpawnDataArray_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FStructPropertyParams NewProp_Items_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Items_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Items;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDataArray_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Spawn/SpawnData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSpawnDataArray>();
	}
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items_Inner = { "Items", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FSpawnDataItem, METADATA_PARAMS(nullptr, 0) }; // 3125251731
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items_MetaData[] = {
		{ "Comment", "//ToDo: operator=.\n" },
		{ "ModuleRelativePath", "Public/Spawn/SpawnData.h" },
		{ "ToolTip", "ToDo: operator=." },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items = { "Items", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FSpawnDataArray, Items), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items_MetaData)) }; // 3125251731
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSpawnDataArray_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewProp_Items,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSpawnDataArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Spawn,
		Z_Construct_UScriptStruct_FFastArraySerializer,
		&NewStructOps,
		"SpawnDataArray",
		sizeof(FSpawnDataArray),
		alignof(FSpawnDataArray),
		Z_Construct_UScriptStruct_FSpawnDataArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSpawnDataArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSpawnDataArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSpawnDataArray()
	{
		if (!Z_Registration_Info_UScriptStruct_SpawnDataArray.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_SpawnDataArray.InnerSingleton, Z_Construct_UScriptStruct_FSpawnDataArray_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_SpawnDataArray.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_Statics::ScriptStructInfo[] = {
		{ FSpawnDataItem::StaticStruct, Z_Construct_UScriptStruct_FSpawnDataItem_Statics::NewStructOps, TEXT("SpawnDataItem"), &Z_Registration_Info_UScriptStruct_SpawnDataItem, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FSpawnDataItem), 3125251731U) },
		{ FSpawnDataArray::StaticStruct, Z_Construct_UScriptStruct_FSpawnDataArray_Statics::NewStructOps, TEXT("SpawnDataArray"), &Z_Registration_Info_UScriptStruct_SpawnDataArray, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FSpawnDataArray), 3675098951U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_18225006(TEXT("/Script/Spawn"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
