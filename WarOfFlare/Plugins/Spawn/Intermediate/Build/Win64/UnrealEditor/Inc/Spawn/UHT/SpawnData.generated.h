// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Spawn/SpawnData.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPAWN_SpawnData_generated_h
#error "SpawnData.generated.h already included, missing '#pragma once' in SpawnData.h"
#endif
#define SPAWN_SpawnData_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSpawnDataItem_Statics; \
	SPAWN_API static class UScriptStruct* StaticStruct(); \
	typedef FFastArraySerializerItem Super;


template<> SPAWN_API UScriptStruct* StaticStruct<struct FSpawnDataItem>();

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSpawnDataArray_Statics; \
	SPAWN_API static class UScriptStruct* StaticStruct(); \
	typedef FFastArraySerializer Super; \
	UE_NET_DECLARE_FASTARRAY(FSpawnDataArray, Items, SPAWN_API );


template<> SPAWN_API UScriptStruct* StaticStruct<struct FSpawnDataArray>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Spawn_Source_Spawn_Public_Spawn_SpawnData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
