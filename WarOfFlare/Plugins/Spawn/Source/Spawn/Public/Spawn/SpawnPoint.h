﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"

#include "SpawnPoint.generated.h"


class ASpawnPoint;

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnSpawnPointReadyToSpawnSignature, ASpawnPoint*, SpawnPoint);

UCLASS()
class SPAWN_API ASpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:
	ASpawnPoint();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void StartSpawning();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	AActor* Spawn(TSubclassOf<AActor> ActorClass, APlayerController* OwnerPlayerController);
	
protected:
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void OnOccupationTriggerOverlapEnd
	(
		UPrimitiveComponent* OverlappedComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex
	);
	
public:
	UPROPERTY()
	FOnSpawnPointReadyToSpawnSignature OnSpawnPointReadyToSpawn;
	
protected:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<USceneComponent> DefaultRootComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UBoxComponent> OccupationCollider;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<USceneComponent> SpawnPoint;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TObjectPtr<UArrowComponent> ArrowComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	AActor* LastSpawned;
};
