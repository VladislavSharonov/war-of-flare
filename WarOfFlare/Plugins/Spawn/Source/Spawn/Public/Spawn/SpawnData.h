﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/NetSerialization.h"
#include "Net/Serialization/FastArraySerializer.h"

#include "SpawnData.generated.h"

USTRUCT(BlueprintType)
struct FSpawnDataItem : public FFastArraySerializerItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> ActorClass = AActor::StaticClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Count = 0;
};

USTRUCT()
struct FSpawnDataArray : public FFastArraySerializer
{
	GENERATED_USTRUCT_BODY()

	//ToDo: operator=.
	
	UPROPERTY()
	TArray<FSpawnDataItem> Items;

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParms)
	{
		return FFastArraySerializer::FastArrayDeltaSerialize<FSpawnDataItem, FSpawnDataArray>(Items, DeltaParms, *this);
	}

	void Add(TSubclassOf<AActor> ActorClass);

	void RemoveLast();
};

template<>
struct TStructOpsTypeTraits<FSpawnDataArray> : public TStructOpsTypeTraitsBase2<FSpawnDataArray>
{
	enum
	{
		WithNetDeltaSerializer = true,
	};
};
