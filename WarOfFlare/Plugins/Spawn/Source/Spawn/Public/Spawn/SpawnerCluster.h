// DemoDreams. All rights reserved.

#pragma once

#include "SpawnData.h"
#include "Spawn/SpawnPoint.h"

#include "SpawnerCluster.generated.h"

class ASpawnerCluster;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FActorSpawnedSignature, AActor*, Actor, AActor*, OwningPlayer);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSpawnCompletedSignature, ASpawnerCluster*, SpawnerCluster);

UCLASS()
class SPAWN_API ASpawnerCluster : public AActor
{
	GENERATED_BODY()

public:
	ASpawnerCluster();
	
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION(BlueprintCallable)
	void Spawn();

	UFUNCTION(BlueprintCallable)
	void AppendSpawnQueue(const TArray<FSpawnDataItem> InSpawnData);
	
protected:
	UFUNCTION(BlueprintCallable)
	TSubclassOf<AActor> GetNextActor();

	UFUNCTION()
	void OnSpawnerReady(ASpawnPoint* SpawnPoint);

public:
	UPROPERTY()
	FActorSpawnedSignature OnActorSpawned;
	
	UPROPERTY()
	FSpawnCompletedSignature OnSpawnCompleted;
	
protected:
	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<USceneComponent> DefaultRootComponent;
	
	UPROPERTY(BlueprintReadWrite)
	TArray<ASpawnPoint*> SpawnPoints;

	UPROPERTY()
	TArray<FSpawnDataItem> SpawnData;

	int32 SpawnPointsInWorkNumber = 0;
};