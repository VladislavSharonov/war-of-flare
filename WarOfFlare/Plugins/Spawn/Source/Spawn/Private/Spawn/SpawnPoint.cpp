﻿// DemoDreams. All rights reserved.

#include "Spawn/SpawnPoint.h"
#include "Kismet/GameplayStatics.h"

ASpawnPoint::ASpawnPoint()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ArrowComponent->SetupAttachment(DefaultRootComponent);

	SpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnPoint"));
	SpawnPoint->SetupAttachment(DefaultRootComponent);

	OccupationCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("OccupationCollider"));
	OccupationCollider->SetupAttachment(DefaultRootComponent);
	OccupationCollider->SetBoxExtent(FVector(100.0f, 100.0f, 100.0f));
	OccupationCollider->SetGenerateOverlapEvents(true);
}

void ASpawnPoint::StartSpawning()
{
	OnSpawnPointReadyToSpawn.ExecuteIfBound(this);
}

AActor* ASpawnPoint::Spawn(TSubclassOf<AActor> ActorClass, APlayerController* OwnerPlayerController)
{
	FTransform SpawnPosition;
	SpawnPosition.SetLocation(SpawnPoint->GetComponentLocation());
	SpawnPosition.SetRotation(SpawnPoint->GetComponentRotation().Quaternion());
	SpawnPosition.SetScale3D(SpawnPoint->GetComponentScale());
	AActor* NewActor = GetWorld()->SpawnActorDeferred<AActor>(
		ActorClass.Get(),
		SpawnPosition,
		OwnerPlayerController,
		OwnerPlayerController->GetPawn(),
		ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);

	// Compute offset on spawner.
	FVector Origin;
	FVector BoxExtent;
	NewActor->GetActorBounds(false, Origin, BoxExtent);
	SpawnPosition.SetLocation(SpawnPosition.GetLocation() + BoxExtent.Z);

	UGameplayStatics::FinishSpawningActor(NewActor, SpawnPosition);
	LastSpawned = NewActor;
	return NewActor;
}

void ASpawnPoint::BeginPlay()
{
	OccupationCollider->OnComponentEndOverlap.AddDynamic(this, &ASpawnPoint::OnOccupationTriggerOverlapEnd);
}

void ASpawnPoint::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	OccupationCollider->OnComponentEndOverlap.RemoveDynamic(this, &ASpawnPoint::OnOccupationTriggerOverlapEnd);
}

void ASpawnPoint::OnOccupationTriggerOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (LastSpawned == nullptr)
		return;

	if (LastSpawned == OtherActor && !OverlappedComp->IsOverlappingActor(OtherActor))
	{
		LastSpawned = nullptr;
		OnSpawnPointReadyToSpawn.ExecuteIfBound(this);
	}
}
