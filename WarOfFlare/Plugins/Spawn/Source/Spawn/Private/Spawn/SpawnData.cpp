// DemoDreams. All rights reserved.

#include "Spawn/SpawnData.h"

void FSpawnDataArray::Add(const TSubclassOf<AActor> ActorClass)
{
	if (!Items.IsEmpty()
		&& Items.Last().Count > 0
		&& Items.Last().Count < TNumericLimits<uint8>::Max()
		&& Items.Last().ActorClass.Get() == ActorClass.Get())
	{
		++Items.Last().Count;
		MarkItemDirty(Items.Last());
	}
	else
	{
		FSpawnDataItem SpawnData;
		SpawnData.Count = 1;
		SpawnData.ActorClass = ActorClass;
		MarkItemDirty(Items.Add_GetRef(SpawnData));
	}
}

void FSpawnDataArray::RemoveLast()
{
	if (Items.IsEmpty())
		return;

	if (Items.Last().Count == 1)
	{
		Items.RemoveAt(Items.Num() - 1);
		MarkArrayDirty();
	}
	else
	{
		FSpawnDataItem& Last = Items.Last();
		--Last.Count;
		MarkItemDirty(Last);
	}
}
