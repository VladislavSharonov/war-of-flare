// DemoDreams. All rights reserved.

#include "Spawn/SpawnerCluster.h"

ASpawnerCluster::ASpawnerCluster()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);
}

void ASpawnerCluster::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> ChildrenSpawnPoints;
	GetAttachedActors(ChildrenSpawnPoints, false, true);
	for (const auto& Child : ChildrenSpawnPoints)
	{
		ASpawnPoint* SpawnPoint = Cast<ASpawnPoint>(Child);
		if (SpawnPoint != nullptr)
			SpawnPoints.AddUnique(SpawnPoint);
	}
}

void ASpawnerCluster::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}


void ASpawnerCluster::Spawn()
{
	for (const auto& SpawnPoint : SpawnPoints)
	{
		SpawnPoint->OnSpawnPointReadyToSpawn.BindDynamic(this, &ASpawnerCluster::OnSpawnerReady);
		++SpawnPointsInWorkNumber;
		SpawnPoint->StartSpawning();
	}
}

void ASpawnerCluster::AppendSpawnQueue(const TArray<FSpawnDataItem> InSpawnData)
{
	SpawnData.Append(InSpawnData);
}

TSubclassOf<AActor> ASpawnerCluster::GetNextActor()
{
	if (!SpawnData.IsEmpty())
	{
		const TSubclassOf<AActor> Actor = SpawnData[0].ActorClass;
		SpawnData[0].Count = SpawnData[0].Count - 1;
		if (SpawnData[0].Count == 0)
			SpawnData.RemoveAt(0);

		return Actor;
	}

	return nullptr;
}

void ASpawnerCluster::OnSpawnerReady(ASpawnPoint* SpawnPoint)
{
	--SpawnPointsInWorkNumber; // Free Spawn Point
	const TSubclassOf<AActor> Actor = GetNextActor();
	if (Actor == nullptr)
	{
		// No objects to spawn
		if (SpawnPointsInWorkNumber == 0)
			OnSpawnCompleted.Broadcast(this);
		
		return;
	}
	++SpawnPointsInWorkNumber; // Occupy Spawn Point
	AActor* SpawnedActor = SpawnPoint->Spawn(Actor, Cast<APlayerController>(GetOwner()));
	OnActorSpawned.Broadcast(SpawnedActor, GetOwner());
}
