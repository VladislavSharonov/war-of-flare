﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "RoundStage.h"

#include "RoundControllerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRoundChangedSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRoundStageChangedSignature, URoundStage*, RoundStage);

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ROUNDS_API URoundControllerComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	URoundControllerComponent();
	
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void Activate(bool bReset = false) override;

	virtual void Deactivate() override;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	uint8 GetCurrentRoundNumber() const { return CurrentRoundNumber; }

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
	void MoveToNextStage();
	
public:
	UPROPERTY(BlueprintAssignable)
	FRoundChangedSignature OnRoundChanged;

protected:
	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Replicated)
	uint8 CurrentRoundNumber = 0;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Replicated)
	TObjectPtr<URoundStage> CurrentStage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<TSubclassOf<URoundStage>> RoundStagesOrder;

protected:
	int32 CurrentStageNumber = 0;
};
