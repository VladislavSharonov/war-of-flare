﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RoundStage.generated.h"

class URoundStage;
class URoundControllerComponent;

DECLARE_DYNAMIC_DELEGATE(FRoundStageEndedSignature);

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class ROUNDS_API URoundStage : public USceneComponent
{
	GENERATED_BODY()

public:
	URoundStage();
	
	UFUNCTION(BlueprintCallable)
	void EndStage() const { OnStageEnded.Execute(); }

	virtual void OnRep_IsActive() override;
	
	UFUNCTION(BlueprintAuthorityOnly)
	void SetRoundController(URoundControllerComponent* InRoundController) { RoundController = InRoundController; }
	
	UPROPERTY()
	FRoundStageEndedSignature OnStageEnded;

	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	virtual void Activate(bool bReset = false) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintAuthorityOnly)
	void OnActivated(bool bReset = false);
	
	virtual void Deactivate() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintAuthorityOnly)
	void OnDeactivated();
	
protected:
	UPROPERTY(BlueprintReadOnly)
	URoundControllerComponent* RoundController = nullptr;
};
