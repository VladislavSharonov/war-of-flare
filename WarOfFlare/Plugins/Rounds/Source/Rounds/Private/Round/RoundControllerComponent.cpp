﻿// DemoDreams. All rights reserved.

#include "Round/RoundControllerComponent.h"
#include "Net/UnrealNetwork.h"

// ToDo PRINT_WAVE_STAGE
#define PRINT_WAVE_STAGE false

URoundControllerComponent::URoundControllerComponent()
{
	bAutoActivate = false;
}

void URoundControllerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void URoundControllerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void URoundControllerComponent::Activate(bool bReset)
{
	Super::Activate(bReset);

	CurrentRoundNumber = 0;
	CurrentStageNumber = -1;

	MoveToNextStage();
}

void URoundControllerComponent::Deactivate()
{
	Super::Deactivate();

	if (IsValid(CurrentStage))
	{
		CurrentStage->Deactivate();
		CurrentStage->DestroyComponent(true);
	}
	CurrentRoundNumber = 0;
	CurrentStageNumber = -1;
}

void URoundControllerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(URoundControllerComponent, CurrentRoundNumber);
	DOREPLIFETIME(URoundControllerComponent, CurrentStage);
}

void URoundControllerComponent::MoveToNextStage()
{
	if (IsValid(CurrentStage))
	{
		CurrentStage->Deactivate();
		CurrentStage->DestroyComponent(true);
	}
	
	if (RoundStagesOrder.IsEmpty())
	{
		UE_LOG(LogTemp, Warning, TEXT("Round Controller has no stages."));
		return;
	}
	
	CurrentStageNumber = (CurrentStageNumber + 1) % RoundStagesOrder.Num();
	if (CurrentStageNumber == 0)
		CurrentRoundNumber = CurrentRoundNumber == TNumericLimits<uint8>::Max() ? 1 : CurrentRoundNumber + 1;
	
	const TSubclassOf<URoundStage> StageClass = RoundStagesOrder[CurrentStageNumber];
	UE_LOG(LogTemp, Display, TEXT("New stage: %d (%s)"), CurrentStageNumber, *StageClass->GetName());
	
	AActor* Owner = GetOwner();
	CurrentStage = Cast<URoundStage>(Owner->AddComponentByClass(StageClass, false, FTransform::Identity, false));
	CurrentStage->SetIsReplicated(true);
	CurrentStage->SetRoundController(this);
	CurrentStage->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
	CurrentStage->OnStageEnded.BindDynamic(this, &URoundControllerComponent::MoveToNextStage);
	
	CurrentStage->Activate();
}

#undef PRINT_WAVE_STAGE