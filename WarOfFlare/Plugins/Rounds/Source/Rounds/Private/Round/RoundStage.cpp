﻿// DemoDreams. All rights reserved.

#include "Round/RoundStage.h"

URoundStage::URoundStage()
{
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = false;
}

void URoundStage::OnRep_IsActive()
{
	Super::OnRep_IsActive();
}

void URoundStage::BeginPlay()
{
	Super::BeginPlay();
}

void URoundStage::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void URoundStage::Activate(bool bReset)
{
	Super::Activate(bReset);
	OnActivated(bReset);
}

void URoundStage::Deactivate()
{
	Super::Deactivate();
	OnDeactivated();
}

void URoundStage::OnActivated_Implementation(bool bReset)
{

}

void URoundStage::OnDeactivated_Implementation()
{
	
}