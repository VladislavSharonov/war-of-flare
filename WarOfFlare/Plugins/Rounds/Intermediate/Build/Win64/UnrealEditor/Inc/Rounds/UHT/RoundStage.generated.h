// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Round/RoundStage.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class URoundControllerComponent;
#ifdef ROUNDS_RoundStage_generated_h
#error "RoundStage.generated.h already included, missing '#pragma once' in RoundStage.h"
#endif
#define ROUNDS_RoundStage_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_12_DELEGATE \
ROUNDS_API void FRoundStageEndedSignature_DelegateWrapper(const FScriptDelegate& RoundStageEndedSignature);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_RPC_WRAPPERS \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated); \
	DECLARE_FUNCTION(execSetRoundController); \
	DECLARE_FUNCTION(execEndStage);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnDeactivated_Implementation(); \
	virtual void OnActivated_Implementation(bool bReset); \
 \
	DECLARE_FUNCTION(execOnDeactivated); \
	DECLARE_FUNCTION(execOnActivated); \
	DECLARE_FUNCTION(execSetRoundController); \
	DECLARE_FUNCTION(execEndStage);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURoundStage(); \
	friend struct Z_Construct_UClass_URoundStage_Statics; \
public: \
	DECLARE_CLASS(URoundStage, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Rounds"), NO_API) \
	DECLARE_SERIALIZER(URoundStage)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURoundStage(); \
	friend struct Z_Construct_UClass_URoundStage_Statics; \
public: \
	DECLARE_CLASS(URoundStage, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Rounds"), NO_API) \
	DECLARE_SERIALIZER(URoundStage)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URoundStage(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URoundStage) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URoundStage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URoundStage); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URoundStage(URoundStage&&); \
	NO_API URoundStage(const URoundStage&); \
public: \
	NO_API virtual ~URoundStage();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URoundStage(URoundStage&&); \
	NO_API URoundStage(const URoundStage&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URoundStage); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URoundStage); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URoundStage) \
	NO_API virtual ~URoundStage();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_14_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ROUNDS_API UClass* StaticClass<class URoundStage>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
