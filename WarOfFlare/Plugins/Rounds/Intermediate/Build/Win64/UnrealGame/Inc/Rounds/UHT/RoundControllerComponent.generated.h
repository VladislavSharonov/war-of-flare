// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Round/RoundControllerComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class URoundStage;
#ifdef ROUNDS_RoundControllerComponent_generated_h
#error "RoundControllerComponent.generated.h already included, missing '#pragma once' in RoundControllerComponent.h"
#endif
#define ROUNDS_RoundControllerComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_12_DELEGATE \
ROUNDS_API void FRoundChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& RoundChangedSignature);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_13_DELEGATE \
ROUNDS_API void FRoundStageChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& RoundStageChangedSignature, URoundStage* RoundStage);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMoveToNextStage); \
	DECLARE_FUNCTION(execGetCurrentRoundNumber);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMoveToNextStage); \
	DECLARE_FUNCTION(execGetCurrentRoundNumber);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURoundControllerComponent(); \
	friend struct Z_Construct_UClass_URoundControllerComponent_Statics; \
public: \
	DECLARE_CLASS(URoundControllerComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Rounds"), NO_API) \
	DECLARE_SERIALIZER(URoundControllerComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentRoundNumber=NETFIELD_REP_START, \
		CurrentStage, \
		NETFIELD_REP_END=CurrentStage	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURoundControllerComponent(); \
	friend struct Z_Construct_UClass_URoundControllerComponent_Statics; \
public: \
	DECLARE_CLASS(URoundControllerComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Rounds"), NO_API) \
	DECLARE_SERIALIZER(URoundControllerComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		CurrentRoundNumber=NETFIELD_REP_START, \
		CurrentStage, \
		NETFIELD_REP_END=CurrentStage	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URoundControllerComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URoundControllerComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URoundControllerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URoundControllerComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URoundControllerComponent(URoundControllerComponent&&); \
	NO_API URoundControllerComponent(const URoundControllerComponent&); \
public: \
	NO_API virtual ~URoundControllerComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URoundControllerComponent(URoundControllerComponent&&); \
	NO_API URoundControllerComponent(const URoundControllerComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URoundControllerComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URoundControllerComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URoundControllerComponent) \
	NO_API virtual ~URoundControllerComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_15_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ROUNDS_API UClass* StaticClass<class URoundControllerComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
