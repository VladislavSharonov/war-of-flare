// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Rounds/Public/Round/RoundStage.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRoundStage() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ROUNDS_API UClass* Z_Construct_UClass_URoundControllerComponent_NoRegister();
	ROUNDS_API UClass* Z_Construct_UClass_URoundStage();
	ROUNDS_API UClass* Z_Construct_UClass_URoundStage_NoRegister();
	ROUNDS_API UFunction* Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Rounds();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Rounds, nullptr, "RoundStageEndedSignature__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FRoundStageEndedSignature_DelegateWrapper(const FScriptDelegate& RoundStageEndedSignature)
{
	RoundStageEndedSignature.ProcessDelegate<UObject>(NULL);
}
	DEFINE_FUNCTION(URoundStage::execOnDeactivated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDeactivated_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URoundStage::execOnActivated)
	{
		P_GET_UBOOL(Z_Param_bReset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnActivated_Implementation(Z_Param_bReset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URoundStage::execSetRoundController)
	{
		P_GET_OBJECT(URoundControllerComponent,Z_Param_InRoundController);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRoundController(Z_Param_InRoundController);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URoundStage::execEndStage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndStage();
		P_NATIVE_END;
	}
	struct RoundStage_eventOnActivated_Parms
	{
		bool bReset;
	};
	static FName NAME_URoundStage_OnActivated = FName(TEXT("OnActivated"));
	void URoundStage::OnActivated(bool bReset)
	{
		RoundStage_eventOnActivated_Parms Parms;
		Parms.bReset=bReset ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_URoundStage_OnActivated),&Parms);
	}
	static FName NAME_URoundStage_OnDeactivated = FName(TEXT("OnDeactivated"));
	void URoundStage::OnDeactivated()
	{
		ProcessEvent(FindFunctionChecked(NAME_URoundStage_OnDeactivated),NULL);
	}
	void URoundStage::StaticRegisterNativesURoundStage()
	{
		UClass* Class = URoundStage::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EndStage", &URoundStage::execEndStage },
			{ "OnActivated", &URoundStage::execOnActivated },
			{ "OnDeactivated", &URoundStage::execOnDeactivated },
			{ "SetRoundController", &URoundStage::execSetRoundController },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URoundStage_EndStage_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundStage_EndStage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URoundStage_EndStage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URoundStage, nullptr, "EndStage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URoundStage_EndStage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_EndStage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URoundStage_EndStage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URoundStage_EndStage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URoundStage_OnActivated_Statics
	{
		static void NewProp_bReset_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bReset;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_URoundStage_OnActivated_Statics::NewProp_bReset_SetBit(void* Obj)
	{
		((RoundStage_eventOnActivated_Parms*)Obj)->bReset = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URoundStage_OnActivated_Statics::NewProp_bReset = { "bReset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(RoundStage_eventOnActivated_Parms), &Z_Construct_UFunction_URoundStage_OnActivated_Statics::NewProp_bReset_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URoundStage_OnActivated_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URoundStage_OnActivated_Statics::NewProp_bReset,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundStage_OnActivated_Statics::Function_MetaDataParams[] = {
		{ "CPP_Default_bReset", "false" },
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URoundStage_OnActivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URoundStage, nullptr, "OnActivated", nullptr, nullptr, sizeof(RoundStage_eventOnActivated_Parms), Z_Construct_UFunction_URoundStage_OnActivated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_OnActivated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C04, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URoundStage_OnActivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_OnActivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URoundStage_OnActivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URoundStage_OnActivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URoundStage_OnDeactivated_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundStage_OnDeactivated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URoundStage_OnDeactivated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URoundStage, nullptr, "OnDeactivated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C04, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URoundStage_OnDeactivated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_OnDeactivated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URoundStage_OnDeactivated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URoundStage_OnDeactivated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URoundStage_SetRoundController_Statics
	{
		struct RoundStage_eventSetRoundController_Parms
		{
			URoundControllerComponent* InRoundController;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InRoundController_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InRoundController;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundStage_SetRoundController_Statics::NewProp_InRoundController_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_URoundStage_SetRoundController_Statics::NewProp_InRoundController = { "InRoundController", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RoundStage_eventSetRoundController_Parms, InRoundController), Z_Construct_UClass_URoundControllerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_URoundStage_SetRoundController_Statics::NewProp_InRoundController_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_SetRoundController_Statics::NewProp_InRoundController_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URoundStage_SetRoundController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URoundStage_SetRoundController_Statics::NewProp_InRoundController,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundStage_SetRoundController_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URoundStage_SetRoundController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URoundStage, nullptr, "SetRoundController", nullptr, nullptr, sizeof(Z_Construct_UFunction_URoundStage_SetRoundController_Statics::RoundStage_eventSetRoundController_Parms), Z_Construct_UFunction_URoundStage_SetRoundController_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_SetRoundController_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URoundStage_SetRoundController_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundStage_SetRoundController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URoundStage_SetRoundController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URoundStage_SetRoundController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(URoundStage);
	UClass* Z_Construct_UClass_URoundStage_NoRegister()
	{
		return URoundStage::StaticClass();
	}
	struct Z_Construct_UClass_URoundStage_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnStageEnded_MetaData[];
#endif
		static const UECodeGen_Private::FDelegatePropertyParams NewProp_OnStageEnded;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RoundController_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_RoundController;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URoundStage_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Rounds,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URoundStage_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URoundStage_EndStage, "EndStage" }, // 1929394709
		{ &Z_Construct_UFunction_URoundStage_OnActivated, "OnActivated" }, // 52728816
		{ &Z_Construct_UFunction_URoundStage_OnDeactivated, "OnDeactivated" }, // 3829790631
		{ &Z_Construct_UFunction_URoundStage_SetRoundController, "SetRoundController" }, // 3450557111
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundStage_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Round/RoundStage.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundStage_Statics::NewProp_OnStageEnded_MetaData[] = {
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_URoundStage_Statics::NewProp_OnStageEnded = { "OnStageEnded", nullptr, (EPropertyFlags)0x0010000000080000, UECodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(URoundStage, OnStageEnded), Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URoundStage_Statics::NewProp_OnStageEnded_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoundStage_Statics::NewProp_OnStageEnded_MetaData)) }; // 22300684
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundStage_Statics::NewProp_RoundController_MetaData[] = {
		{ "Category", "RoundStage" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Round/RoundStage.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URoundStage_Statics::NewProp_RoundController = { "RoundController", nullptr, (EPropertyFlags)0x002008000008001c, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(URoundStage, RoundController), Z_Construct_UClass_URoundControllerComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URoundStage_Statics::NewProp_RoundController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoundStage_Statics::NewProp_RoundController_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URoundStage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundStage_Statics::NewProp_OnStageEnded,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundStage_Statics::NewProp_RoundController,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URoundStage_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URoundStage>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_URoundStage_Statics::ClassParams = {
		&URoundStage::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_URoundStage_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_URoundStage_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URoundStage_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URoundStage_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URoundStage()
	{
		if (!Z_Registration_Info_UClass_URoundStage.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_URoundStage.OuterSingleton, Z_Construct_UClass_URoundStage_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_URoundStage.OuterSingleton;
	}
	template<> ROUNDS_API UClass* StaticClass<URoundStage>()
	{
		return URoundStage::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(URoundStage);
	URoundStage::~URoundStage() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_URoundStage, URoundStage::StaticClass, TEXT("URoundStage"), &Z_Registration_Info_UClass_URoundStage, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(URoundStage), 2507882052U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_500748684(TEXT("/Script/Rounds"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundStage_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
