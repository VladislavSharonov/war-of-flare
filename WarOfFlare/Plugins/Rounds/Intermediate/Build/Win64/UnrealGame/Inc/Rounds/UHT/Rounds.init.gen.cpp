// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRounds_init() {}
	ROUNDS_API UFunction* Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature();
	ROUNDS_API UFunction* Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature();
	ROUNDS_API UFunction* Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_Rounds;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_Rounds()
	{
		if (!Z_Registration_Info_UPackage__Script_Rounds.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Rounds_RoundStageEndedSignature__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/Rounds",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xC7B29975,
				0xD4C0CB4E,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_Rounds.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_Rounds.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_Rounds(Z_Construct_UPackage__Script_Rounds, TEXT("/Script/Rounds"), Z_Registration_Info_UPackage__Script_Rounds, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xC7B29975, 0xD4C0CB4E));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
