// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Rounds/Public/Round/RoundControllerComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRoundControllerComponent() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ROUNDS_API UClass* Z_Construct_UClass_URoundControllerComponent();
	ROUNDS_API UClass* Z_Construct_UClass_URoundControllerComponent_NoRegister();
	ROUNDS_API UClass* Z_Construct_UClass_URoundStage_NoRegister();
	ROUNDS_API UFunction* Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature();
	ROUNDS_API UFunction* Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Rounds();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Rounds, nullptr, "RoundChangedSignature__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FRoundChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& RoundChangedSignature)
{
	RoundChangedSignature.ProcessMulticastDelegate<UObject>(NULL);
}
	struct Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics
	{
		struct _Script_Rounds_eventRoundStageChangedSignature_Parms
		{
			URoundStage* RoundStage;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RoundStage_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_RoundStage;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::NewProp_RoundStage_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::NewProp_RoundStage = { "RoundStage", nullptr, (EPropertyFlags)0x0010000000080080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Rounds_eventRoundStageChangedSignature_Parms, RoundStage), Z_Construct_UClass_URoundStage_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::NewProp_RoundStage_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::NewProp_RoundStage_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::NewProp_RoundStage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Rounds, nullptr, "RoundStageChangedSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::_Script_Rounds_eventRoundStageChangedSignature_Parms), Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Rounds_RoundStageChangedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FRoundStageChangedSignature_DelegateWrapper(const FMulticastScriptDelegate& RoundStageChangedSignature, URoundStage* RoundStage)
{
	struct _Script_Rounds_eventRoundStageChangedSignature_Parms
	{
		URoundStage* RoundStage;
	};
	_Script_Rounds_eventRoundStageChangedSignature_Parms Parms;
	Parms.RoundStage=RoundStage;
	RoundStageChangedSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(URoundControllerComponent::execMoveToNextStage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MoveToNextStage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URoundControllerComponent::execGetCurrentRoundNumber)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(uint8*)Z_Param__Result=P_THIS->GetCurrentRoundNumber();
		P_NATIVE_END;
	}
	void URoundControllerComponent::StaticRegisterNativesURoundControllerComponent()
	{
		UClass* Class = URoundControllerComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentRoundNumber", &URoundControllerComponent::execGetCurrentRoundNumber },
			{ "MoveToNextStage", &URoundControllerComponent::execMoveToNextStage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics
	{
		struct RoundControllerComponent_eventGetCurrentRoundNumber_Parms
		{
			uint8 ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(RoundControllerComponent_eventGetCurrentRoundNumber_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URoundControllerComponent, nullptr, "GetCurrentRoundNumber", nullptr, nullptr, sizeof(Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::RoundControllerComponent_eventGetCurrentRoundNumber_Parms), Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URoundControllerComponent, nullptr, "MoveToNextStage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(URoundControllerComponent);
	UClass* Z_Construct_UClass_URoundControllerComponent_NoRegister()
	{
		return URoundControllerComponent::StaticClass();
	}
	struct Z_Construct_UClass_URoundControllerComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnRoundChanged_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnRoundChanged;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentRoundNumber_MetaData[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_CurrentRoundNumber;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CurrentStage_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_CurrentStage;
		static const UECodeGen_Private::FClassPropertyParams NewProp_RoundStagesOrder_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RoundStagesOrder_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_RoundStagesOrder;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URoundControllerComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Rounds,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URoundControllerComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URoundControllerComponent_GetCurrentRoundNumber, "GetCurrentRoundNumber" }, // 37523101
		{ &Z_Construct_UFunction_URoundControllerComponent_MoveToNextStage, "MoveToNextStage" }, // 943011446
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundControllerComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Round/RoundControllerComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_OnRoundChanged_MetaData[] = {
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_OnRoundChanged = { "OnRoundChanged", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(URoundControllerComponent, OnRoundChanged), Z_Construct_UDelegateFunction_Rounds_RoundChangedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_OnRoundChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_OnRoundChanged_MetaData)) }; // 3204340406
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentRoundNumber_MetaData[] = {
		{ "Category", "RoundControllerComponent" },
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentRoundNumber = { "CurrentRoundNumber", nullptr, (EPropertyFlags)0x0020080000020825, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(URoundControllerComponent, CurrentRoundNumber), nullptr, METADATA_PARAMS(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentRoundNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentRoundNumber_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentStage_MetaData[] = {
		{ "Category", "RoundControllerComponent" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentStage = { "CurrentStage", nullptr, (EPropertyFlags)0x00240800000a082d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(URoundControllerComponent, CurrentStage), Z_Construct_UClass_URoundStage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentStage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentStage_MetaData)) };
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder_Inner = { "RoundStagesOrder", nullptr, (EPropertyFlags)0x0004000000000000, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_UClass, Z_Construct_UClass_URoundStage_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder_MetaData[] = {
		{ "Category", "RoundControllerComponent" },
		{ "ModuleRelativePath", "Public/Round/RoundControllerComponent.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder = { "RoundStagesOrder", nullptr, (EPropertyFlags)0x0024080000010015, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(URoundControllerComponent, RoundStagesOrder), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URoundControllerComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_OnRoundChanged,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentRoundNumber,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_CurrentStage,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoundControllerComponent_Statics::NewProp_RoundStagesOrder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URoundControllerComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URoundControllerComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_URoundControllerComponent_Statics::ClassParams = {
		&URoundControllerComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_URoundControllerComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_URoundControllerComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URoundControllerComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URoundControllerComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URoundControllerComponent()
	{
		if (!Z_Registration_Info_UClass_URoundControllerComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_URoundControllerComponent.OuterSingleton, Z_Construct_UClass_URoundControllerComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_URoundControllerComponent.OuterSingleton;
	}
	template<> ROUNDS_API UClass* StaticClass<URoundControllerComponent>()
	{
		return URoundControllerComponent::StaticClass();
	}

	void URoundControllerComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_CurrentRoundNumber(TEXT("CurrentRoundNumber"));
		static const FName Name_CurrentStage(TEXT("CurrentStage"));

		const bool bIsValid = true
			&& Name_CurrentRoundNumber == ClassReps[(int32)ENetFields_Private::CurrentRoundNumber].Property->GetFName()
			&& Name_CurrentStage == ClassReps[(int32)ENetFields_Private::CurrentStage].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in URoundControllerComponent"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(URoundControllerComponent);
	URoundControllerComponent::~URoundControllerComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_URoundControllerComponent, URoundControllerComponent::StaticClass, TEXT("URoundControllerComponent"), &Z_Registration_Info_UClass_URoundControllerComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(URoundControllerComponent), 1795184343U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_683945019(TEXT("/Script/Rounds"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Rounds_Source_Rounds_Public_Round_RoundControllerComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
