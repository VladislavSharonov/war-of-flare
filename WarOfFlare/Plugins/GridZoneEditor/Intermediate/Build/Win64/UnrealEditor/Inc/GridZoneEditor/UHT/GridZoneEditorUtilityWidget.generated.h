// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "GridZoneEditorUtilityWidget.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GRIDZONEEDITOR_GridZoneEditorUtilityWidget_generated_h
#error "GridZoneEditorUtilityWidget.generated.h already included, missing '#pragma once' in GridZoneEditorUtilityWidget.h"
#endif
#define GRIDZONEEDITOR_GridZoneEditorUtilityWidget_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_RPC_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridZoneEditorUtilityWidget(); \
	friend struct Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics; \
public: \
	DECLARE_CLASS(UGridZoneEditorUtilityWidget, UEditorUtilityWidget, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GridZoneEditor"), NO_API) \
	DECLARE_SERIALIZER(UGridZoneEditorUtilityWidget)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGridZoneEditorUtilityWidget(); \
	friend struct Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics; \
public: \
	DECLARE_CLASS(UGridZoneEditorUtilityWidget, UEditorUtilityWidget, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GridZoneEditor"), NO_API) \
	DECLARE_SERIALIZER(UGridZoneEditorUtilityWidget)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridZoneEditorUtilityWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridZoneEditorUtilityWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridZoneEditorUtilityWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridZoneEditorUtilityWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridZoneEditorUtilityWidget(UGridZoneEditorUtilityWidget&&); \
	NO_API UGridZoneEditorUtilityWidget(const UGridZoneEditorUtilityWidget&); \
public: \
	NO_API virtual ~UGridZoneEditorUtilityWidget();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridZoneEditorUtilityWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridZoneEditorUtilityWidget(UGridZoneEditorUtilityWidget&&); \
	NO_API UGridZoneEditorUtilityWidget(const UGridZoneEditorUtilityWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridZoneEditorUtilityWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridZoneEditorUtilityWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridZoneEditorUtilityWidget) \
	NO_API virtual ~UGridZoneEditorUtilityWidget();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_12_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GRIDZONEEDITOR_API UClass* StaticClass<class UGridZoneEditorUtilityWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
