// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "GridZone.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GRIDZONEEDITOR_GridZone_generated_h
#error "GridZone.generated.h already included, missing '#pragma once' in GridZone.h"
#endif
#define GRIDZONEEDITOR_GridZone_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_RPC_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridZone(); \
	friend struct Z_Construct_UClass_UGridZone_Statics; \
public: \
	DECLARE_CLASS(UGridZone, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GridZoneEditor"), NO_API) \
	DECLARE_SERIALIZER(UGridZone)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUGridZone(); \
	friend struct Z_Construct_UClass_UGridZone_Statics; \
public: \
	DECLARE_CLASS(UGridZone, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GridZoneEditor"), NO_API) \
	DECLARE_SERIALIZER(UGridZone)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridZone(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridZone) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridZone); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridZone(UGridZone&&); \
	NO_API UGridZone(const UGridZone&); \
public: \
	NO_API virtual ~UGridZone();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridZone(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridZone(UGridZone&&); \
	NO_API UGridZone(const UGridZone&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridZone); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridZone) \
	NO_API virtual ~UGridZone();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_11_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GRIDZONEEDITOR_API UClass* StaticClass<class UGridZone>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
