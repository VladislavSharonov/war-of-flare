// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "GridZoneFactory.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GRIDZONEEDITOR_GridZoneFactory_generated_h
#error "GridZoneFactory.generated.h already included, missing '#pragma once' in GridZoneFactory.h"
#endif
#define GRIDZONEEDITOR_GridZoneFactory_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_RPC_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridZoneFactory(); \
	friend struct Z_Construct_UClass_UGridZoneFactory_Statics; \
public: \
	DECLARE_CLASS(UGridZoneFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GridZoneEditor"), NO_API) \
	DECLARE_SERIALIZER(UGridZoneFactory)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUGridZoneFactory(); \
	friend struct Z_Construct_UClass_UGridZoneFactory_Statics; \
public: \
	DECLARE_CLASS(UGridZoneFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GridZoneEditor"), NO_API) \
	DECLARE_SERIALIZER(UGridZoneFactory)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridZoneFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridZoneFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridZoneFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridZoneFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridZoneFactory(UGridZoneFactory&&); \
	NO_API UGridZoneFactory(const UGridZoneFactory&); \
public: \
	NO_API virtual ~UGridZoneFactory();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridZoneFactory(UGridZoneFactory&&); \
	NO_API UGridZoneFactory(const UGridZoneFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridZoneFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridZoneFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridZoneFactory) \
	NO_API virtual ~UGridZoneFactory();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_12_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GRIDZONEEDITOR_API UClass* StaticClass<class UGridZoneFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
