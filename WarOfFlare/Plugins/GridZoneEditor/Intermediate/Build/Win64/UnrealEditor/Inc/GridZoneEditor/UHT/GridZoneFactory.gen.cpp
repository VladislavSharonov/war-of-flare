// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GridZoneEditor/Public/GridZoneFactory.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGridZoneFactory() {}
// Cross Module References
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZoneFactory();
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZoneFactory_NoRegister();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_GridZoneEditor();
// End Cross Module References
	void UGridZoneFactory::StaticRegisterNativesUGridZoneFactory()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGridZoneFactory);
	UClass* Z_Construct_UClass_UGridZoneFactory_NoRegister()
	{
		return UGridZoneFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGridZoneFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridZoneFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GridZoneEditor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZoneFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GridZoneFactory.h" },
		{ "ModuleRelativePath", "Public/GridZoneFactory.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridZoneFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridZoneFactory>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGridZoneFactory_Statics::ClassParams = {
		&UGridZoneFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGridZoneFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZoneFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridZoneFactory()
	{
		if (!Z_Registration_Info_UClass_UGridZoneFactory.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGridZoneFactory.OuterSingleton, Z_Construct_UClass_UGridZoneFactory_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGridZoneFactory.OuterSingleton;
	}
	template<> GRIDZONEEDITOR_API UClass* StaticClass<UGridZoneFactory>()
	{
		return UGridZoneFactory::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridZoneFactory);
	UGridZoneFactory::~UGridZoneFactory() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGridZoneFactory, UGridZoneFactory::StaticClass, TEXT("UGridZoneFactory"), &Z_Registration_Info_UClass_UGridZoneFactory, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGridZoneFactory), 2040861935U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_2518632999(TEXT("/Script/GridZoneEditor"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneFactory_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
