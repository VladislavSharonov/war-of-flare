// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GridZoneEditor/Public/GridZoneEditorUtilityWidget.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGridZoneEditorUtilityWidget() {}
// Cross Module References
	BLUTILITY_API UClass* Z_Construct_UClass_UEditorUtilityWidget();
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZone_NoRegister();
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZoneEditorUtilityWidget();
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZoneEditorUtilityWidget_NoRegister();
	UPackage* Z_Construct_UPackage__Script_GridZoneEditor();
// End Cross Module References
	void UGridZoneEditorUtilityWidget::StaticRegisterNativesUGridZoneEditorUtilityWidget()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGridZoneEditorUtilityWidget);
	UClass* Z_Construct_UClass_UGridZoneEditorUtilityWidget_NoRegister()
	{
		return UGridZoneEditorUtilityWidget::StaticClass();
	}
	struct Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_GridZone_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_GridZone;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditorUtilityWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_GridZoneEditor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GridZoneEditorUtilityWidget.h" },
		{ "ModuleRelativePath", "Public/GridZoneEditorUtilityWidget.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::NewProp_GridZone_MetaData[] = {
		{ "Category", "GridZoneEditor" },
		{ "ModuleRelativePath", "Public/GridZoneEditorUtilityWidget.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::NewProp_GridZone = { "GridZone", nullptr, (EPropertyFlags)0x0014000000020005, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UGridZoneEditorUtilityWidget, GridZone), Z_Construct_UClass_UGridZone_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::NewProp_GridZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::NewProp_GridZone_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::NewProp_GridZone,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridZoneEditorUtilityWidget>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::ClassParams = {
		&UGridZoneEditorUtilityWidget::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridZoneEditorUtilityWidget()
	{
		if (!Z_Registration_Info_UClass_UGridZoneEditorUtilityWidget.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGridZoneEditorUtilityWidget.OuterSingleton, Z_Construct_UClass_UGridZoneEditorUtilityWidget_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGridZoneEditorUtilityWidget.OuterSingleton;
	}
	template<> GRIDZONEEDITOR_API UClass* StaticClass<UGridZoneEditorUtilityWidget>()
	{
		return UGridZoneEditorUtilityWidget::StaticClass();
	}
	UGridZoneEditorUtilityWidget::UGridZoneEditorUtilityWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridZoneEditorUtilityWidget);
	UGridZoneEditorUtilityWidget::~UGridZoneEditorUtilityWidget() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGridZoneEditorUtilityWidget, UGridZoneEditorUtilityWidget::StaticClass, TEXT("UGridZoneEditorUtilityWidget"), &Z_Registration_Info_UClass_UGridZoneEditorUtilityWidget, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGridZoneEditorUtilityWidget), 4028994037U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_2306878912(TEXT("/Script/GridZoneEditor"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZoneEditorUtilityWidget_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
