// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GridZoneEditor/Public/GridZone.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGridZone() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZone();
	GRIDZONEEDITOR_API UClass* Z_Construct_UClass_UGridZone_NoRegister();
	UPackage* Z_Construct_UPackage__Script_GridZoneEditor();
// End Cross Module References
	void UGridZone::StaticRegisterNativesUGridZone()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UGridZone);
	UClass* Z_Construct_UClass_UGridZone_NoRegister()
	{
		return UGridZone::StaticClass();
	}
	struct Z_Construct_UClass_UGridZone_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Size;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CellSize_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_CellSize;
		static const UECodeGen_Private::FStructPropertyParams NewProp_OccupiedCells_ElementProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OccupiedCells_MetaData[];
#endif
		static const UECodeGen_Private::FSetPropertyParams NewProp_OccupiedCells;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridZone_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_GridZoneEditor,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZone_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "GridZone.h" },
		{ "ModuleRelativePath", "Public/GridZone.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZone_Statics::NewProp_Size_MetaData[] = {
		{ "Category", "GridZone" },
		{ "ModuleRelativePath", "Public/GridZone.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGridZone_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000020005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UGridZone, Size), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UGridZone_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZone_Statics::NewProp_Size_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZone_Statics::NewProp_CellSize_MetaData[] = {
		{ "Category", "GridZone" },
		{ "ModuleRelativePath", "Public/GridZone.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UGridZone_Statics::NewProp_CellSize = { "CellSize", nullptr, (EPropertyFlags)0x0010000000020005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UGridZone, CellSize), METADATA_PARAMS(Z_Construct_UClass_UGridZone_Statics::NewProp_CellSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZone_Statics::NewProp_CellSize_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells_ElementProp = { "OccupiedCells", nullptr, (EPropertyFlags)0x0000000000020001, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells_MetaData[] = {
		{ "Category", "GridZone" },
		{ "ModuleRelativePath", "Public/GridZone.h" },
	};
#endif
	static_assert(TModels<CGetTypeHashable, FVector2D>::Value, "The structure 'FVector2D' is used in a TSet but does not have a GetValueTypeHash defined");
	const UECodeGen_Private::FSetPropertyParams Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells = { "OccupiedCells", nullptr, (EPropertyFlags)0x0010000000020005, UECodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UGridZone, OccupiedCells), METADATA_PARAMS(Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGridZone_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridZone_Statics::NewProp_Size,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridZone_Statics::NewProp_CellSize,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells_ElementProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGridZone_Statics::NewProp_OccupiedCells,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridZone_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridZone>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UGridZone_Statics::ClassParams = {
		&UGridZone::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGridZone_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGridZone_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGridZone_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridZone_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridZone()
	{
		if (!Z_Registration_Info_UClass_UGridZone.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UGridZone.OuterSingleton, Z_Construct_UClass_UGridZone_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UGridZone.OuterSingleton;
	}
	template<> GRIDZONEEDITOR_API UClass* StaticClass<UGridZone>()
	{
		return UGridZone::StaticClass();
	}
	UGridZone::UGridZone(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridZone);
	UGridZone::~UGridZone() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UGridZone, UGridZone::StaticClass, TEXT("UGridZone"), &Z_Registration_Info_UClass_UGridZone, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UGridZone), 835656989U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_3847092137(TEXT("/Script/GridZoneEditor"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_GridZoneEditor_Source_GridZoneEditor_Public_GridZone_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
