// DemoDreams. All rights reserved.

#include "GridZoneFactory.h"

const FString UGridZoneFactory::fileExtention = TEXT("gridzoneasset");

UGridZoneFactory::UGridZoneFactory(const FObjectInitializer& ObjectInitializer) 
    : Super(ObjectInitializer)
{
    SupportedClass = UGridZone::StaticClass();
    Formats.Add(fileExtention);

    bCreateNew = true;
    bText = false;
    bEditorImport = false;
    bEditAfterNew = false;
}

UObject* UGridZoneFactory::FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn, FName CallingContext)
{
    check(InClass->IsChildOf(UGridZone::StaticClass()));
    return NewObject<UGridZone>(InParent, InClass, InName, Flags);
}
