// Copyright Epic Games, Inc. All Rights Reserved.

#include "GridZoneEditor.h"

#define LOCTEXT_NAMESPACE "FGridZoneEditorModule"

void FGridZoneEditorModule::StartupModule()
{
	//Register
	IAssetTools& AssetToolsModule = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();

	EAssetTypeCategories::Type AssetCategoryBit = AssetToolsModule.RegisterAdvancedAssetCategory(FName(TEXT("Tools")), LOCTEXT("Tools", "Tools"));
	
	GridZoneAssetTypeActions = MakeShareable(new FAssetTypeActions_GridZone(AssetCategoryBit));
	AssetToolsModule.RegisterAssetTypeActions(GridZoneAssetTypeActions.ToSharedRef());
}

void FGridZoneEditorModule::ShutdownModule()
{
	if (!FModuleManager::Get().IsModuleLoaded("AssetTools"))
		return;

	FAssetToolsModule::GetModule()
		.Get()
		.UnregisterAssetTypeActions(GridZoneAssetTypeActions.ToSharedRef());
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FGridZoneEditorModule, GridZoneEditor)