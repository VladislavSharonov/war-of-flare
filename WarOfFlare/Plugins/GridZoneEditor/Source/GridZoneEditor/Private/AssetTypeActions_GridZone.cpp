// DemoDreams. All rights reserved.

#include "AssetTypeActions_GridZone.h"

FAssetTypeActions_GridZone::FAssetTypeActions_GridZone(EAssetTypeCategories::Type InAssetCategory)
    : AssetCategory(InAssetCategory)
{
}

void FAssetTypeActions_GridZone::OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor)
{
    for (auto object : InObjects)
    { 
        UGridZone* asset = Cast<UGridZone>(object);
        if (asset == nullptr)
            continue;

        UObject* Blueprint = UEditorAssetLibrary::LoadAsset(FString(TEXT("/Script/Blutility.EditorUtilityWidgetBlueprint'/GridZoneEditor/Editors/WBP_GridZoneEditor.WBP_GridZoneEditor'")));
        if (IsValid(Blueprint))
        {
        

            UGridZoneEditorUtilityWidget* Editor = Cast<UGridZoneEditorUtilityWidget>(Blueprint);
            if (IsValid(Editor))
            {
                Editor->GridZone = asset;
            }

            UEditorUtilityWidgetBlueprint* EditorWidget = Cast<UEditorUtilityWidgetBlueprint>(Blueprint);
            if (IsValid(EditorWidget))
            {
                UEditorUtilitySubsystem* EditorUtilitySubsystem = GEditor->GetEditorSubsystem<UEditorUtilitySubsystem>();
                EditorUtilitySubsystem->SpawnAndRegisterTab(EditorWidget);
            }
        }
    }
}
