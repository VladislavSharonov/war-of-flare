// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Math/Vector2D.h"

#include "GridZone.generated.h"

UCLASS( BlueprintType )
class GRIDZONEEDITOR_API UGridZone : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "GridZone")
	FVector2D Size = FVector2D(3.0f , 3.0f);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "GridZone")
	float CellSize = 100.0f;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "GridZone")
	TSet<FVector2D> OccupiedCells;
};
