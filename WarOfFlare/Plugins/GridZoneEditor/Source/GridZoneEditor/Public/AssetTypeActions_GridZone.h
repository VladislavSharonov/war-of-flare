// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "EditorUtilityWidgetBlueprint.h"
#include "EditorUtilitySubsystem.h"
#include "EditorAssetLibrary.h"

#include "AssetTypeActions_Base.h"
#include "GridZoneEditorUtilityWidget.h"

#include "GridZone.h"

class GRIDZONEEDITOR_API FAssetTypeActions_GridZone : public FAssetTypeActions_Base
{
public:
    FAssetTypeActions_GridZone(EAssetTypeCategories::Type InAssetCategory);

    // IAssetTypeActions Implementation
    virtual FText GetName() const override { return NSLOCTEXT("AssetTypeActions", "FAssetTypeActions_GridZone", "Grid Zone"); }
    virtual FColor GetTypeColor() const override { return FColor(0, 0, 255); }
    virtual UClass* GetSupportedClass() const override { return UGridZone::StaticClass(); }
    virtual uint32 GetCategories() override { return AssetCategory; }
    virtual void OpenAssetEditor(
        const TArray<UObject*>& InObjects, 
        TSharedPtr<class IToolkitHost> EditWithinLevelEditor = TSharedPtr<IToolkitHost>()) override;

private:
    EAssetTypeCategories::Type AssetCategory;
};
