// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"

#include "GridZone.h"

#include "GridZoneFactory.generated.h"

UCLASS()
class GRIDZONEEDITOR_API UGridZoneFactory : public UFactory
{
	GENERATED_BODY()
	
public:
	UGridZoneFactory(const FObjectInitializer& ObjectInitializer);

	virtual UObject* FactoryCreateNew(
		UClass* InClass,
		UObject* InParent,
		FName InName,
		EObjectFlags Flags,
		UObject* Context,
		FFeedbackContext* Warn,
		FName CallingContext) override;

	/*virtual UObject* FactoryCreateFile(
		UClass* InClass, 
		UObject* InParent, 
		FName InName, 
		EObjectFlags Flags, 
		const FString& Filename, 
		const TCHAR* Parms, 
		FFeedbackContext* Warn, 
		bool& bOutOperationCanceled) override;

	virtual UObject* FactoryCreateBinary(
		UClass* InClass,
		UObject* InParent,
		FName InName,
		EObjectFlags Flags,
		UObject* Context,
		const TCHAR* Type,
		const uint8*& Buffer,
		const uint8* BufferEnd,
		FFeedbackContext* Warn) override;*/

	// Return true to make the asset show up in the Content Browser context menu.
	virtual bool ShouldShowInNewMenu() const override { return true; }

	virtual bool CanCreateNew() const override { return true; }

private:
	static const FString fileExtention;
};