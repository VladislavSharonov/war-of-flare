// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "EditorUtilityWidget.h"

#include "GridZone.h"

#include "GridZoneEditorUtilityWidget.generated.h"

UCLASS()
class GRIDZONEEDITOR_API UGridZoneEditorUtilityWidget : public UEditorUtilityWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "GridZoneEditor")
	TObjectPtr<UGridZone> GridZone;
};
