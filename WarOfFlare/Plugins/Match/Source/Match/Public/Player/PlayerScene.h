﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "PlayerScene.generated.h"

UCLASS()
class MATCH_API APlayerScene : public AActor
{
	GENERATED_BODY()
	
public:
	APlayerScene();

	virtual void SetOwner(AActor* NewOwner) override;

	UFUNCTION(BlueprintPure)
	static APlayerScene* GetPlayerSceneForPlayer(APlayerController* InPlayer);

	UFUNCTION(BlueprintPure)
	void GetAllActorsOfClass(TSubclassOf<AActor> Class, TArray<AActor*>& Actors) const;

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TObjectPtr<USceneComponent> DefaultRootComponent;
};