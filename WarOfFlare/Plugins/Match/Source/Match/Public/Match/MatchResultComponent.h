﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MatchResultComponent.generated.h"

UENUM()
enum class EMatchResult : uint8
{
	Unknown UMETA(DisplayName = "Unknown"),
	Win		UMETA(DisplayName = "Win"),
	Lose	UMETA(DisplayName = "Lose"),
	Draw	UMETA(DisplayName = "Draw")
};

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class MATCH_API UMatchResultComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UMatchResultComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void OnRep_MatchResult(const EMatchResult OldMatchResult);

	UFUNCTION(BlueprintGetter)
	EMatchResult GetMatchResult() const { return MatchResult; }
	
	UFUNCTION(BlueprintSetter)
	void SetMatchResult(EMatchResult InMatchResult = EMatchResult::Unknown);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MatchResult")
	void HandleOnWin();
	virtual void HandleOnWin_Implementation() {}
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MatchResult")
	void HandleOnLose();
	virtual void HandleOnLose_Implementation() {}
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MatchResult")
	void HandleOnDraw();
	virtual void HandleOnDraw_Implementation() {}

protected:
	UPROPERTY(BlueprintGetter=GetMatchResult, BlueprintSetter=SetMatchResult, ReplicatedUsing=OnRep_MatchResult)
	EMatchResult MatchResult = EMatchResult::Unknown;
};
