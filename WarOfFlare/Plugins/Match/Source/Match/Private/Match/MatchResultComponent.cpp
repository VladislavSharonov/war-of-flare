﻿// DemoDreams. All rights reserved.

#include "Match/MatchResultComponent.h"

#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

UMatchResultComponent::UMatchResultComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UMatchResultComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(UMatchResultComponent, MatchResult);
}

void UMatchResultComponent::OnRep_MatchResult(const EMatchResult OldMatchResult)
{
	if (!IsValid(this) || !IsValid(GetOwner()))
		return;
	
	switch (MatchResult) {
	case EMatchResult::Unknown:
		break;
	case EMatchResult::Win:
		HandleOnWin();
		break;
	case EMatchResult::Lose:
		HandleOnLose();
		break;
	case EMatchResult::Draw:
		HandleOnDraw();
		break;
	}
}

void UMatchResultComponent::SetMatchResult(const EMatchResult InMatchResult)
{
	const EMatchResult OldMatchResult = MatchResult;
	MatchResult = InMatchResult;
	
	// Validate for listen server
	if (GetOwnerRole() == ROLE_Authority && GetNetMode() == ENetMode::NM_ListenServer && GetOwner()->HasLocalNetOwner())
	{
		OnRep_MatchResult(OldMatchResult);
	}
}
