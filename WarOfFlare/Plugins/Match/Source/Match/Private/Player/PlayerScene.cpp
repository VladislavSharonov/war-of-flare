﻿// DemoDreams. All rights reserved.

#include "Player/PlayerScene.h"

#include <Net/UnrealNetwork.h>
#include <Kismet/GameplayStatics.h>
#include "EngineUtils.h"

APlayerScene::APlayerScene()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(DefaultRootComponent);
	DefaultRootComponent->SetMobility(EComponentMobility::Static);

	bReplicates = true;
}

void APlayerScene::SetOwner(AActor* NewOwner)
{
	Super::SetOwner(NewOwner);
	TArray<AActor*> Actors;
	GetAttachedActors(Actors, false, true);

	for (const auto& Actor : Actors)
		Actor->SetOwner(NewOwner);
}

APlayerScene* APlayerScene::GetPlayerSceneForPlayer(APlayerController* InPlayer)
{
	for (TActorIterator<APlayerScene> It(InPlayer->GetWorld()); It; ++It)
	{
		APlayerScene* PlayerScene = *It;
		if (PlayerScene->IsOwnedBy(InPlayer))
			return PlayerScene;
	}
	return nullptr;
}

void APlayerScene::GetAllActorsOfClass(const TSubclassOf<AActor> Class, TArray<AActor*>& Actors) const
{
	TArray<AActor*> ChildrenActors;
	GetAttachedActors(ChildrenActors, false, true);

	for (auto& Child : ChildrenActors)
	{
		if (Child->GetClass()->IsChildOf(Class))
			Actors.Add(Child);
	}
}
