// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Match/Public/Player/PlayerScene.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerScene() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	MATCH_API UClass* Z_Construct_UClass_APlayerScene();
	MATCH_API UClass* Z_Construct_UClass_APlayerScene_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Match();
// End Cross Module References
	DEFINE_FUNCTION(APlayerScene::execGetAllActorsOfClass)
	{
		P_GET_OBJECT(UClass,Z_Param_Class);
		P_GET_TARRAY_REF(AActor*,Z_Param_Out_Actors);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAllActorsOfClass(Z_Param_Class,Z_Param_Out_Actors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APlayerScene::execGetPlayerSceneForPlayer)
	{
		P_GET_OBJECT(APlayerController,Z_Param_InPlayer);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(APlayerScene**)Z_Param__Result=APlayerScene::GetPlayerSceneForPlayer(Z_Param_InPlayer);
		P_NATIVE_END;
	}
	void APlayerScene::StaticRegisterNativesAPlayerScene()
	{
		UClass* Class = APlayerScene::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAllActorsOfClass", &APlayerScene::execGetAllActorsOfClass },
			{ "GetPlayerSceneForPlayer", &APlayerScene::execGetPlayerSceneForPlayer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics
	{
		struct PlayerScene_eventGetAllActorsOfClass_Parms
		{
			TSubclassOf<AActor>  Class;
			TArray<AActor*> Actors;
		};
		static const UECodeGen_Private::FClassPropertyParams NewProp_Class;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Actors_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0014000000000080, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerScene_eventGetAllActorsOfClass_Parms, Class), Z_Construct_UClass_UClass, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerScene_eventGetAllActorsOfClass_Parms, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::NewProp_Class,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::NewProp_Actors_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::NewProp_Actors,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Player/PlayerScene.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerScene, nullptr, "GetAllActorsOfClass", nullptr, nullptr, sizeof(Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::PlayerScene_eventGetAllActorsOfClass_Parms), Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics
	{
		struct PlayerScene_eventGetPlayerSceneForPlayer_Parms
		{
			APlayerController* InPlayer;
			APlayerScene* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_InPlayer;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::NewProp_InPlayer = { "InPlayer", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerScene_eventGetPlayerSceneForPlayer_Parms, InPlayer), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerScene_eventGetPlayerSceneForPlayer_Parms, ReturnValue), Z_Construct_UClass_APlayerScene_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::NewProp_InPlayer,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Player/PlayerScene.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerScene, nullptr, "GetPlayerSceneForPlayer", nullptr, nullptr, sizeof(Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::PlayerScene_eventGetPlayerSceneForPlayer_Parms), Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(APlayerScene);
	UClass* Z_Construct_UClass_APlayerScene_NoRegister()
	{
		return APlayerScene::StaticClass();
	}
	struct Z_Construct_UClass_APlayerScene_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DefaultRootComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_DefaultRootComponent;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlayerScene_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Match,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlayerScene_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlayerScene_GetAllActorsOfClass, "GetAllActorsOfClass" }, // 2049283483
		{ &Z_Construct_UFunction_APlayerScene_GetPlayerSceneForPlayer, "GetPlayerSceneForPlayer" }, // 870631579
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerScene_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Player/PlayerScene.h" },
		{ "ModuleRelativePath", "Public/Player/PlayerScene.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerScene_Statics::NewProp_DefaultRootComponent_MetaData[] = {
		{ "Category", "PlayerScene" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Player/PlayerScene.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_APlayerScene_Statics::NewProp_DefaultRootComponent = { "DefaultRootComponent", nullptr, (EPropertyFlags)0x00240800000a001d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(APlayerScene, DefaultRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayerScene_Statics::NewProp_DefaultRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerScene_Statics::NewProp_DefaultRootComponent_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlayerScene_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerScene_Statics::NewProp_DefaultRootComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlayerScene_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlayerScene>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_APlayerScene_Statics::ClassParams = {
		&APlayerScene::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APlayerScene_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APlayerScene_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APlayerScene_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APlayerScene_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlayerScene()
	{
		if (!Z_Registration_Info_UClass_APlayerScene.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_APlayerScene.OuterSingleton, Z_Construct_UClass_APlayerScene_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_APlayerScene.OuterSingleton;
	}
	template<> MATCH_API UClass* StaticClass<APlayerScene>()
	{
		return APlayerScene::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerScene);
	APlayerScene::~APlayerScene() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_APlayerScene, APlayerScene::StaticClass, TEXT("APlayerScene"), &Z_Registration_Info_UClass_APlayerScene, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(APlayerScene), 1788143820U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_4077914566(TEXT("/Script/Match"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
