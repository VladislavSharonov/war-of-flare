// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Player/PlayerScene.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class APlayerController;
class APlayerScene;
#ifdef MATCH_PlayerScene_generated_h
#error "PlayerScene.generated.h already included, missing '#pragma once' in PlayerScene.h"
#endif
#define MATCH_PlayerScene_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAllActorsOfClass); \
	DECLARE_FUNCTION(execGetPlayerSceneForPlayer);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAllActorsOfClass); \
	DECLARE_FUNCTION(execGetPlayerSceneForPlayer);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerScene(); \
	friend struct Z_Construct_UClass_APlayerScene_Statics; \
public: \
	DECLARE_CLASS(APlayerScene, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Match"), NO_API) \
	DECLARE_SERIALIZER(APlayerScene)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerScene(); \
	friend struct Z_Construct_UClass_APlayerScene_Statics; \
public: \
	DECLARE_CLASS(APlayerScene, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Match"), NO_API) \
	DECLARE_SERIALIZER(APlayerScene)


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerScene(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerScene) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerScene); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerScene); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerScene(APlayerScene&&); \
	NO_API APlayerScene(const APlayerScene&); \
public: \
	NO_API virtual ~APlayerScene();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerScene(APlayerScene&&); \
	NO_API APlayerScene(const APlayerScene&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerScene); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerScene); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerScene) \
	NO_API virtual ~APlayerScene();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_10_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MATCH_API UClass* StaticClass<class APlayerScene>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Player_PlayerScene_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
