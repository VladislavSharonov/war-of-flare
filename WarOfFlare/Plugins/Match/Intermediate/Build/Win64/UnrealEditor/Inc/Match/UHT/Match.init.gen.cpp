// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatch_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_Match;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_Match()
	{
		if (!Z_Registration_Info_UPackage__Script_Match.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/Match",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xFF54A5C8,
				0x0A57820A,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_Match.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_Match.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_Match(Z_Construct_UPackage__Script_Match, TEXT("/Script/Match"), Z_Registration_Info_UPackage__Script_Match, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xFF54A5C8, 0x0A57820A));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
