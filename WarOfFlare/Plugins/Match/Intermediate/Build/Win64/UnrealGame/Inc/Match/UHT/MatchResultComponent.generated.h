// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Match/MatchResultComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMatchResult : uint8;
#ifdef MATCH_MatchResultComponent_generated_h
#error "MatchResultComponent.generated.h already included, missing '#pragma once' in MatchResultComponent.h"
#endif
#define MATCH_MatchResultComponent_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_RPC_WRAPPERS \
	virtual void HandleOnDraw_Implementation(); \
	virtual void HandleOnLose_Implementation(); \
	virtual void HandleOnWin_Implementation(); \
 \
	DECLARE_FUNCTION(execHandleOnDraw); \
	DECLARE_FUNCTION(execHandleOnLose); \
	DECLARE_FUNCTION(execHandleOnWin); \
	DECLARE_FUNCTION(execSetMatchResult); \
	DECLARE_FUNCTION(execGetMatchResult); \
	DECLARE_FUNCTION(execOnRep_MatchResult);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandleOnDraw); \
	DECLARE_FUNCTION(execHandleOnLose); \
	DECLARE_FUNCTION(execHandleOnWin); \
	DECLARE_FUNCTION(execSetMatchResult); \
	DECLARE_FUNCTION(execGetMatchResult); \
	DECLARE_FUNCTION(execOnRep_MatchResult);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMatchResultComponent(); \
	friend struct Z_Construct_UClass_UMatchResultComponent_Statics; \
public: \
	DECLARE_CLASS(UMatchResultComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Match"), NO_API) \
	DECLARE_SERIALIZER(UMatchResultComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		MatchResult=NETFIELD_REP_START, \
		NETFIELD_REP_END=MatchResult	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUMatchResultComponent(); \
	friend struct Z_Construct_UClass_UMatchResultComponent_Statics; \
public: \
	DECLARE_CLASS(UMatchResultComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Match"), NO_API) \
	DECLARE_SERIALIZER(UMatchResultComponent) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		MatchResult=NETFIELD_REP_START, \
		NETFIELD_REP_END=MatchResult	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMatchResultComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMatchResultComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchResultComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchResultComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchResultComponent(UMatchResultComponent&&); \
	NO_API UMatchResultComponent(const UMatchResultComponent&); \
public: \
	NO_API virtual ~UMatchResultComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMatchResultComponent(UMatchResultComponent&&); \
	NO_API UMatchResultComponent(const UMatchResultComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMatchResultComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMatchResultComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMatchResultComponent) \
	NO_API virtual ~UMatchResultComponent();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_18_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MATCH_API UClass* StaticClass<class UMatchResultComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h


#define FOREACH_ENUM_EMATCHRESULT(op) \
	op(EMatchResult::Unknown) \
	op(EMatchResult::Win) \
	op(EMatchResult::Lose) \
	op(EMatchResult::Draw) 

enum class EMatchResult : uint8;
template<> struct TIsUEnumClass<EMatchResult> { enum { Value = true }; };
template<> MATCH_API UEnum* StaticEnum<EMatchResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
