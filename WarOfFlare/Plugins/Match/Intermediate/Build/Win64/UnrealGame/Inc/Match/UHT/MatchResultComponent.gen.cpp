// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Match/Public/Match/MatchResultComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMatchResultComponent() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	MATCH_API UClass* Z_Construct_UClass_UMatchResultComponent();
	MATCH_API UClass* Z_Construct_UClass_UMatchResultComponent_NoRegister();
	MATCH_API UEnum* Z_Construct_UEnum_Match_EMatchResult();
	UPackage* Z_Construct_UPackage__Script_Match();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EMatchResult;
	static UEnum* EMatchResult_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EMatchResult.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EMatchResult.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_Match_EMatchResult, (UObject*)Z_Construct_UPackage__Script_Match(), TEXT("EMatchResult"));
		}
		return Z_Registration_Info_UEnum_EMatchResult.OuterSingleton;
	}
	template<> MATCH_API UEnum* StaticEnum<EMatchResult>()
	{
		return EMatchResult_StaticEnum();
	}
	struct Z_Construct_UEnum_Match_EMatchResult_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_Match_EMatchResult_Statics::Enumerators[] = {
		{ "EMatchResult::Unknown", (int64)EMatchResult::Unknown },
		{ "EMatchResult::Win", (int64)EMatchResult::Win },
		{ "EMatchResult::Lose", (int64)EMatchResult::Lose },
		{ "EMatchResult::Draw", (int64)EMatchResult::Draw },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_Match_EMatchResult_Statics::Enum_MetaDataParams[] = {
		{ "Draw.DisplayName", "Draw" },
		{ "Draw.Name", "EMatchResult::Draw" },
		{ "Lose.DisplayName", "Lose" },
		{ "Lose.Name", "EMatchResult::Lose" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
		{ "Unknown.DisplayName", "Unknown" },
		{ "Unknown.Name", "EMatchResult::Unknown" },
		{ "Win.DisplayName", "Win" },
		{ "Win.Name", "EMatchResult::Win" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_Match_EMatchResult_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_Match,
		nullptr,
		"EMatchResult",
		"EMatchResult",
		Z_Construct_UEnum_Match_EMatchResult_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_Match_EMatchResult_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_Match_EMatchResult_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_Match_EMatchResult_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_Match_EMatchResult()
	{
		if (!Z_Registration_Info_UEnum_EMatchResult.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EMatchResult.InnerSingleton, Z_Construct_UEnum_Match_EMatchResult_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EMatchResult.InnerSingleton;
	}
	DEFINE_FUNCTION(UMatchResultComponent::execHandleOnDraw)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleOnDraw_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchResultComponent::execHandleOnLose)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleOnLose_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchResultComponent::execHandleOnWin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleOnWin_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchResultComponent::execSetMatchResult)
	{
		P_GET_ENUM(EMatchResult,Z_Param_InMatchResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMatchResult(EMatchResult(Z_Param_InMatchResult));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchResultComponent::execGetMatchResult)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EMatchResult*)Z_Param__Result=P_THIS->GetMatchResult();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMatchResultComponent::execOnRep_MatchResult)
	{
		P_GET_ENUM(EMatchResult,Z_Param_OldMatchResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_MatchResult(EMatchResult(Z_Param_OldMatchResult));
		P_NATIVE_END;
	}
	static FName NAME_UMatchResultComponent_HandleOnDraw = FName(TEXT("HandleOnDraw"));
	void UMatchResultComponent::HandleOnDraw()
	{
		ProcessEvent(FindFunctionChecked(NAME_UMatchResultComponent_HandleOnDraw),NULL);
	}
	static FName NAME_UMatchResultComponent_HandleOnLose = FName(TEXT("HandleOnLose"));
	void UMatchResultComponent::HandleOnLose()
	{
		ProcessEvent(FindFunctionChecked(NAME_UMatchResultComponent_HandleOnLose),NULL);
	}
	static FName NAME_UMatchResultComponent_HandleOnWin = FName(TEXT("HandleOnWin"));
	void UMatchResultComponent::HandleOnWin()
	{
		ProcessEvent(FindFunctionChecked(NAME_UMatchResultComponent_HandleOnWin),NULL);
	}
	void UMatchResultComponent::StaticRegisterNativesUMatchResultComponent()
	{
		UClass* Class = UMatchResultComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetMatchResult", &UMatchResultComponent::execGetMatchResult },
			{ "HandleOnDraw", &UMatchResultComponent::execHandleOnDraw },
			{ "HandleOnLose", &UMatchResultComponent::execHandleOnLose },
			{ "HandleOnWin", &UMatchResultComponent::execHandleOnWin },
			{ "OnRep_MatchResult", &UMatchResultComponent::execOnRep_MatchResult },
			{ "SetMatchResult", &UMatchResultComponent::execSetMatchResult },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics
	{
		struct MatchResultComponent_eventGetMatchResult_Parms
		{
			EMatchResult ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MatchResultComponent_eventGetMatchResult_Parms, ReturnValue), Z_Construct_UEnum_Match_EMatchResult, METADATA_PARAMS(nullptr, 0) }; // 651067106
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchResultComponent, nullptr, "GetMatchResult", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::MatchResultComponent_eventGetMatchResult_Parms), Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchResultComponent_GetMatchResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMatchResultComponent_GetMatchResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw_Statics::Function_MetaDataParams[] = {
		{ "Category", "MatchResult" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchResultComponent, nullptr, "HandleOnDraw", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchResultComponent_HandleOnLose_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_HandleOnLose_Statics::Function_MetaDataParams[] = {
		{ "Category", "MatchResult" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchResultComponent_HandleOnLose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchResultComponent, nullptr, "HandleOnLose", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_HandleOnLose_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_HandleOnLose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchResultComponent_HandleOnLose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMatchResultComponent_HandleOnLose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchResultComponent_HandleOnWin_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_HandleOnWin_Statics::Function_MetaDataParams[] = {
		{ "Category", "MatchResult" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchResultComponent_HandleOnWin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchResultComponent, nullptr, "HandleOnWin", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_HandleOnWin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_HandleOnWin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchResultComponent_HandleOnWin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMatchResultComponent_HandleOnWin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics
	{
		struct MatchResultComponent_eventOnRep_MatchResult_Parms
		{
			EMatchResult OldMatchResult;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_OldMatchResult_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OldMatchResult_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_OldMatchResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult = { "OldMatchResult", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MatchResultComponent_eventOnRep_MatchResult_Parms, OldMatchResult), Z_Construct_UEnum_Match_EMatchResult, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult_MetaData)) }; // 651067106
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::NewProp_OldMatchResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchResultComponent, nullptr, "OnRep_MatchResult", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::MatchResultComponent_eventOnRep_MatchResult_Parms), Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics
	{
		struct MatchResultComponent_eventSetMatchResult_Parms
		{
			EMatchResult InMatchResult;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InMatchResult_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InMatchResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::NewProp_InMatchResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::NewProp_InMatchResult = { "InMatchResult", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(MatchResultComponent_eventSetMatchResult_Parms, InMatchResult), Z_Construct_UEnum_Match_EMatchResult, METADATA_PARAMS(nullptr, 0) }; // 651067106
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::NewProp_InMatchResult_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::NewProp_InMatchResult,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "CPP_Default_InMatchResult", "Unknown" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMatchResultComponent, nullptr, "SetMatchResult", nullptr, nullptr, sizeof(Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::MatchResultComponent_eventSetMatchResult_Parms), Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMatchResultComponent_SetMatchResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UMatchResultComponent_SetMatchResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UMatchResultComponent);
	UClass* Z_Construct_UClass_UMatchResultComponent_NoRegister()
	{
		return UMatchResultComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMatchResultComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_MatchResult_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MatchResult_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_MatchResult;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMatchResultComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Match,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMatchResultComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMatchResultComponent_GetMatchResult, "GetMatchResult" }, // 564009276
		{ &Z_Construct_UFunction_UMatchResultComponent_HandleOnDraw, "HandleOnDraw" }, // 2714606088
		{ &Z_Construct_UFunction_UMatchResultComponent_HandleOnLose, "HandleOnLose" }, // 1592231010
		{ &Z_Construct_UFunction_UMatchResultComponent_HandleOnWin, "HandleOnWin" }, // 1131782469
		{ &Z_Construct_UFunction_UMatchResultComponent_OnRep_MatchResult, "OnRep_MatchResult" }, // 2040852781
		{ &Z_Construct_UFunction_UMatchResultComponent_SetMatchResult, "SetMatchResult" }, // 3771369037
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchResultComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Match/MatchResultComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult_MetaData[] = {
		{ "BlueprintGetter", "GetMatchResult" },
		{ "BlueprintSetter", "SetMatchResult" },
		{ "Category", "MatchResultComponent" },
		{ "ModuleRelativePath", "Public/Match/MatchResultComponent.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult = { "MatchResult", "OnRep_MatchResult", (EPropertyFlags)0x0020080100000024, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UMatchResultComponent, MatchResult), Z_Construct_UEnum_Match_EMatchResult, METADATA_PARAMS(Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult_MetaData)) }; // 651067106
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMatchResultComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMatchResultComponent_Statics::NewProp_MatchResult,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMatchResultComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMatchResultComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UMatchResultComponent_Statics::ClassParams = {
		&UMatchResultComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMatchResultComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMatchResultComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMatchResultComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMatchResultComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMatchResultComponent()
	{
		if (!Z_Registration_Info_UClass_UMatchResultComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UMatchResultComponent.OuterSingleton, Z_Construct_UClass_UMatchResultComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UMatchResultComponent.OuterSingleton;
	}
	template<> MATCH_API UClass* StaticClass<UMatchResultComponent>()
	{
		return UMatchResultComponent::StaticClass();
	}

	void UMatchResultComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_MatchResult(TEXT("MatchResult"));

		const bool bIsValid = true
			&& Name_MatchResult == ClassReps[(int32)ENetFields_Private::MatchResult].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UMatchResultComponent"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMatchResultComponent);
	UMatchResultComponent::~UMatchResultComponent() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics::EnumInfo[] = {
		{ EMatchResult_StaticEnum, TEXT("EMatchResult"), &Z_Registration_Info_UEnum_EMatchResult, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 651067106U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UMatchResultComponent, UMatchResultComponent::StaticClass, TEXT("UMatchResultComponent"), &Z_Registration_Info_UClass_UMatchResultComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UMatchResultComponent), 3474683413U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_3156962990(TEXT("/Script/Match"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics::ClassInfo),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Match_Source_Match_Public_Match_MatchResultComponent_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
