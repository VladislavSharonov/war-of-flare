﻿// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Minions : ModuleRules
{
	public Minions(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"ElementalDamage",
				"Health",
				"Towers",
				"ObjectHovering"
			}
			);
			
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
			}
			);
	}
}
