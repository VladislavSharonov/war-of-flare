﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Armor.generated.h"

UENUM(BlueprintType)
enum class EArmor : uint8
{
    NoArmor         UMETA(DisplayName = "NoArmor"),
    LightArmor      UMETA(DisplayName = "LightArmor"),
    MediumArmor     UMETA(DisplayName = "MediumArmor"),
    HeavyArmor      UMETA(DisplayName = "HeavyArmor"),
};
