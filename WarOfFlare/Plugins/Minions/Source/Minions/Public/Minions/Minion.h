﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "ElementalDamage/ElementalDamageType.h"
#include "GameFramework/Character.h"

#include "Health/HealthComponent.h"

#include "Minions/MinionClass.h"
#include "Minions/MinionType.h"
#include "Minions/ResistanceForArmor.h"
#include "ObjectHovering/IHoverable.h"
#include "Tower/TowerTargetComponent.h"

#include "Minion.generated.h"

class AMinion;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMinionDiedSignature, EMinionType, MinionType, AActor*, MinionOwner);

UCLASS()
class MINIONS_API AMinion : public ACharacter, public IHoverable
{
	GENERATED_BODY()

public:
	AMinion();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void FellOutOfWorld(const class UDamageType& DamageType) override;
	
	UFUNCTION(BlueprintCallable, BlueprintSetter, Category = "Minion|Damage")
	void SetEnemy(APlayerController* Player);
	
protected:
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable, Category = "Minion|Damage")
	float GetResist(const EElementalDamageType InElementalType);

	UFUNCTION(BlueprintCallable, Category = "Minion|Damage")
	void Die();
	
#pragma region IHoverable
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHoverBegin();
	virtual void OnHoverBegin_Implementation() override { }
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnHoverEnd();
	virtual void OnHoverEnd_Implementation() override { }
#pragma endregion

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Replicated, Category = "Minion|Components")
	TObjectPtr<UHealthComponent> HealthComponent;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Minion|Components")
	TObjectPtr<UTowerTargetComponent> TowerTargetComponent;
	
	UPROPERTY(EditDefaultsOnly, Category = "Minion")
	EMinionType MinionType;

	UPROPERTY(EditDefaultsOnly, Category = "Minion")
	EMinionClass MinionClass;

	UPROPERTY(EditDefaultsOnly, Category = "Minion")
	EArmor Armor = EArmor::NoArmor;
	
	UPROPERTY(EditDefaultsOnly, Category = "Minion", meta=(RowType="ResistanceForArmor"))
	UDataTable* MinionClassToResist;

	UPROPERTY(BlueprintAssignable, Category = "Minion|Damage")
	FOnMinionDiedSignature OnMinionDied;

protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, BlueprintSetter = SetEnemy, Category = "Minion")
	APlayerController* Enemy = nullptr;
};