﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Resistance.generated.h"

UENUM(BlueprintType)
enum class EResistance : uint8
{
    None        UMETA(DisplayName = "None"),
    Vulnerable  UMETA(DisplayName = "Vulnerable"),
    Resistant   UMETA(DisplayName = "Resistant"),
};