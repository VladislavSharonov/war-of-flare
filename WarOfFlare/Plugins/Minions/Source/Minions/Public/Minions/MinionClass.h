﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "MinionClass.generated.h"

UENUM(BlueprintType)
enum class EMinionClass : uint8
{
    Infantry    UMETA(DisplayName = "Infantry"),
    Vehicle     UMETA(DisplayName = "Vehicle"),
    Mutants  UMETA(DisplayName = "Mutants"),
    Robots  UMETA(DisplayName = "Robots"),
};