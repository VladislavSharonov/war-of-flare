﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "Armor.h"
#include "Resistance.h"

#include "ResistanceForArmor.generated.h"

USTRUCT(BlueprintType)
struct FResistanceForArmor : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<EElementalDamageType, EResistance> NoArmor;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<EElementalDamageType, EResistance> LightArmor;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<EElementalDamageType, EResistance> MediumArmor;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<EElementalDamageType, EResistance> HeavyArmor;
	
	TMap<EElementalDamageType, EResistance> operator[](const EArmor Armor) const { return GetResistsForArmor(Armor); }

	const TMap<EElementalDamageType, EResistance>& GetResistsForArmor(const EArmor Armor) const
	{
		switch (Armor)
		{
		case EArmor::NoArmor:
			return NoArmor;
		case EArmor::LightArmor:
			return LightArmor;
		case EArmor::MediumArmor:
			return MediumArmor;
		case EArmor::HeavyArmor:
			return HeavyArmor;
		default:
			return NoArmor;
		}
	}

	EResistance GetResistance(const EArmor Armor, const EElementalDamageType ElementalDamageType) const
	{
		const EResistance* Resists = GetResistsForArmor(Armor).Find(ElementalDamageType);
		if(Resists != nullptr)
			return *Resists;

		return EResistance::None;
	}
};