﻿// DemoDreams. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "NativeGameplayTags.h"

#include "MinionType.generated.h"

UENUM(BlueprintType)
enum class EMinionType : uint8 
{
    None            UMETA(Hidden, DisplayName = "NoType"),
    Stormtrooper    UMETA(DisplayName = "Stormtrooper"),
    Landspeeder     UMETA(DisplayName = "Landspeeder"),
    Mechanoid       UMETA(DisplayName = "Mechanoid"),
};
