﻿// DemoDreams. All rights reserved.

#include "Minions/Minion.h"

#include "GameFramework/Controller.h"

#include "ElementalDamage/DamageFunctionLibrary.h"
#include "Engine/DamageEvents.h"
#include "Minions/Resistance.h"
#include "Minions/ResistanceForArmor.h"
#include "Net/UnrealNetwork.h"

AMinion::AMinion()
{
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->SetMaxHealth(100);
	HealthComponent->SetIsReplicated(true);
	
	TowerTargetComponent = CreateDefaultSubobject<UTowerTargetComponent>(TEXT("TowerTargetComponent"));
	TowerTargetComponent->SetupAttachment(GetRootComponent());
	
	bReplicates = true;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void AMinion::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AMinion, HealthComponent);
}

void AMinion::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnHealthSpent.AddDynamic(this, &AMinion::Die);
}

void AMinion::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	HealthComponent->OnHealthSpent.RemoveDynamic(this, &AMinion::Die);

	Super::EndPlay(EndPlayReason);
}

float AMinion::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (!IsValid(HealthComponent) || HealthComponent->GetHealth() <= 0)
		return 0.0f;

	const float Damage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	const float DamageFraction = GetResist(UDamageFunctionLibrary::GetDamageElement(DamageEvent.DamageTypeClass.Get()));

	HealthComponent->SubtractHealth(DamageFraction * Damage);
	return 0.0f;
}

float AMinion::GetResist(const EElementalDamageType InElementalType)
{
	const FName MinionClassName = FName(StaticEnum<EMinionClass>()->GetNameStringByValue(static_cast<int64>(MinionClass)));

	const FResistanceForArmor* ResistanceForArmor = MinionClassToResist->FindRow<FResistanceForArmor>(MinionClassName, "");
	EResistance Resistance = EResistance::None;
	if (ResistanceForArmor != nullptr)
		Resistance = ResistanceForArmor->GetResistance(Armor, InElementalType);
	
	switch (Resistance)
	{
		case EResistance::Resistant:
			return 0.5f;
		case EResistance::Vulnerable:
			return 1.5f;
		default:
			return 1.0f;
	}
}

void AMinion::FellOutOfWorld(const UDamageType& DamageType)
{
	Die();
}

void AMinion::Die()
{
	OnMinionDied.Broadcast(MinionType, Enemy);
	Destroy(true);
}

void AMinion::SetEnemy(APlayerController* Player)
{
	Enemy = Player;
}
