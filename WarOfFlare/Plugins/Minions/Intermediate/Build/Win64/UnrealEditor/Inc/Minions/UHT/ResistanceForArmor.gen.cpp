// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Minions/Public/Minions/ResistanceForArmor.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeResistanceForArmor() {}
// Cross Module References
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EResistance();
	MINIONS_API UScriptStruct* Z_Construct_UScriptStruct_FResistanceForArmor();
	UPackage* Z_Construct_UPackage__Script_Minions();
// End Cross Module References

static_assert(std::is_polymorphic<FResistanceForArmor>() == std::is_polymorphic<FTableRowBase>(), "USTRUCT FResistanceForArmor cannot be polymorphic unless super FTableRowBase is polymorphic");

	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_ResistanceForArmor;
class UScriptStruct* FResistanceForArmor::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_ResistanceForArmor.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_ResistanceForArmor.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FResistanceForArmor, (UObject*)Z_Construct_UPackage__Script_Minions(), TEXT("ResistanceForArmor"));
	}
	return Z_Registration_Info_UScriptStruct_ResistanceForArmor.OuterSingleton;
}
template<> MINIONS_API UScriptStruct* StaticStruct<FResistanceForArmor>()
{
	return FResistanceForArmor::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FResistanceForArmor_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UECodeGen_Private::FBytePropertyParams NewProp_NoArmor_ValueProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_NoArmor_ValueProp;
		static const UECodeGen_Private::FBytePropertyParams NewProp_NoArmor_Key_KeyProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_NoArmor_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_NoArmor_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_NoArmor;
		static const UECodeGen_Private::FBytePropertyParams NewProp_LightArmor_ValueProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_LightArmor_ValueProp;
		static const UECodeGen_Private::FBytePropertyParams NewProp_LightArmor_Key_KeyProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_LightArmor_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_LightArmor_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_LightArmor;
		static const UECodeGen_Private::FBytePropertyParams NewProp_MediumArmor_ValueProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_MediumArmor_ValueProp;
		static const UECodeGen_Private::FBytePropertyParams NewProp_MediumArmor_Key_KeyProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_MediumArmor_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MediumArmor_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_MediumArmor;
		static const UECodeGen_Private::FBytePropertyParams NewProp_HeavyArmor_ValueProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_HeavyArmor_ValueProp;
		static const UECodeGen_Private::FBytePropertyParams NewProp_HeavyArmor_Key_KeyProp_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_HeavyArmor_Key_KeyProp;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HeavyArmor_MetaData[];
#endif
		static const UECodeGen_Private::FMapPropertyParams NewProp_HeavyArmor;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FResistanceForArmor_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Minions/ResistanceForArmor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FResistanceForArmor>();
	}
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_ValueProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_ValueProp = { "NoArmor", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UEnum_Minions_EResistance, METADATA_PARAMS(nullptr, 0) }; // 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_Key_KeyProp = { "NoArmor_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_MetaData[] = {
		{ "Category", "ResistanceForArmor" },
		{ "ModuleRelativePath", "Public/Minions/ResistanceForArmor.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor = { "NoArmor", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FResistanceForArmor, NoArmor), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_MetaData)) }; // 1545068981 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_ValueProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_ValueProp = { "LightArmor", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UEnum_Minions_EResistance, METADATA_PARAMS(nullptr, 0) }; // 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_Key_KeyProp = { "LightArmor_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_MetaData[] = {
		{ "Category", "ResistanceForArmor" },
		{ "ModuleRelativePath", "Public/Minions/ResistanceForArmor.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor = { "LightArmor", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FResistanceForArmor, LightArmor), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_MetaData)) }; // 1545068981 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_ValueProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_ValueProp = { "MediumArmor", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UEnum_Minions_EResistance, METADATA_PARAMS(nullptr, 0) }; // 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_Key_KeyProp = { "MediumArmor_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_MetaData[] = {
		{ "Category", "ResistanceForArmor" },
		{ "ModuleRelativePath", "Public/Minions/ResistanceForArmor.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor = { "MediumArmor", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FResistanceForArmor, MediumArmor), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_MetaData)) }; // 1545068981 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_ValueProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_ValueProp = { "HeavyArmor", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 1, Z_Construct_UEnum_Minions_EResistance, METADATA_PARAMS(nullptr, 0) }; // 333490117
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_Key_KeyProp_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_Key_KeyProp = { "HeavyArmor_Key", nullptr, (EPropertyFlags)0x0000000000000001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(nullptr, 0) }; // 1545068981
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_MetaData[] = {
		{ "Category", "ResistanceForArmor" },
		{ "ModuleRelativePath", "Public/Minions/ResistanceForArmor.h" },
	};
#endif
	const UECodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor = { "HeavyArmor", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FResistanceForArmor, HeavyArmor), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_MetaData)) }; // 1545068981 333490117
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FResistanceForArmor_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_ValueProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_Key_KeyProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_NoArmor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_ValueProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_Key_KeyProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_LightArmor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_ValueProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_Key_KeyProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_MediumArmor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_ValueProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_ValueProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_Key_KeyProp_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor_Key_KeyProp,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewProp_HeavyArmor,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FResistanceForArmor_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Minions,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"ResistanceForArmor",
		sizeof(FResistanceForArmor),
		alignof(FResistanceForArmor),
		Z_Construct_UScriptStruct_FResistanceForArmor_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FResistanceForArmor_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FResistanceForArmor()
	{
		if (!Z_Registration_Info_UScriptStruct_ResistanceForArmor.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_ResistanceForArmor.InnerSingleton, Z_Construct_UScriptStruct_FResistanceForArmor_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_ResistanceForArmor.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h_Statics
	{
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h_Statics::ScriptStructInfo[] = {
		{ FResistanceForArmor::StaticStruct, Z_Construct_UScriptStruct_FResistanceForArmor_Statics::NewStructOps, TEXT("ResistanceForArmor"), &Z_Registration_Info_UScriptStruct_ResistanceForArmor, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FResistanceForArmor), 3004783838U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h_3615672937(TEXT("/Script/Minions"),
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h_Statics::ScriptStructInfo),
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
