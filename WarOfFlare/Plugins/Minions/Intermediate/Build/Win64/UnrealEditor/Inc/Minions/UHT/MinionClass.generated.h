// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Minions/MinionClass.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONS_MinionClass_generated_h
#error "MinionClass.generated.h already included, missing '#pragma once' in MinionClass.h"
#endif
#define MINIONS_MinionClass_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionClass_h


#define FOREACH_ENUM_EMINIONCLASS(op) \
	op(EMinionClass::Infantry) \
	op(EMinionClass::Vehicle) \
	op(EMinionClass::Mutants) \
	op(EMinionClass::Robots) 

enum class EMinionClass : uint8;
template<> struct TIsUEnumClass<EMinionClass> { enum { Value = true }; };
template<> MINIONS_API UEnum* StaticEnum<EMinionClass>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
