// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Minions/Public/Minions/MinionType.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionType() {}
// Cross Module References
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EMinionType();
	UPackage* Z_Construct_UPackage__Script_Minions();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EMinionType;
	static UEnum* EMinionType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EMinionType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EMinionType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_Minions_EMinionType, (UObject*)Z_Construct_UPackage__Script_Minions(), TEXT("EMinionType"));
		}
		return Z_Registration_Info_UEnum_EMinionType.OuterSingleton;
	}
	template<> MINIONS_API UEnum* StaticEnum<EMinionType>()
	{
		return EMinionType_StaticEnum();
	}
	struct Z_Construct_UEnum_Minions_EMinionType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_Minions_EMinionType_Statics::Enumerators[] = {
		{ "EMinionType::None", (int64)EMinionType::None },
		{ "EMinionType::Stormtrooper", (int64)EMinionType::Stormtrooper },
		{ "EMinionType::Landspeeder", (int64)EMinionType::Landspeeder },
		{ "EMinionType::Mechanoid", (int64)EMinionType::Mechanoid },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_Minions_EMinionType_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Landspeeder.DisplayName", "Landspeeder" },
		{ "Landspeeder.Name", "EMinionType::Landspeeder" },
		{ "Mechanoid.DisplayName", "Mechanoid" },
		{ "Mechanoid.Name", "EMinionType::Mechanoid" },
		{ "ModuleRelativePath", "Public/Minions/MinionType.h" },
		{ "None.DisplayName", "NoType" },
		{ "None.Hidden", "" },
		{ "None.Name", "EMinionType::None" },
		{ "Stormtrooper.DisplayName", "Stormtrooper" },
		{ "Stormtrooper.Name", "EMinionType::Stormtrooper" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_Minions_EMinionType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_Minions,
		nullptr,
		"EMinionType",
		"EMinionType",
		Z_Construct_UEnum_Minions_EMinionType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_Minions_EMinionType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_Minions_EMinionType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_Minions_EMinionType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_Minions_EMinionType()
	{
		if (!Z_Registration_Info_UEnum_EMinionType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EMinionType.InnerSingleton, Z_Construct_UEnum_Minions_EMinionType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EMinionType.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionType_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionType_h_Statics::EnumInfo[] = {
		{ EMinionType_StaticEnum, TEXT("EMinionType"), &Z_Registration_Info_UEnum_EMinionType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 3979814491U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionType_h_3195422872(TEXT("/Script/Minions"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionType_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionType_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
