// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Minions/Public/Minions/Minion.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinion() {}
// Cross Module References
	ELEMENTALDAMAGE_API UEnum* Z_Construct_UEnum_ElementalDamage_EElementalDamageType();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	HEALTH_API UClass* Z_Construct_UClass_UHealthComponent_NoRegister();
	MINIONS_API UClass* Z_Construct_UClass_AMinion();
	MINIONS_API UClass* Z_Construct_UClass_AMinion_NoRegister();
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EArmor();
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EMinionClass();
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EMinionType();
	MINIONS_API UFunction* Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature();
	OBJECTHOVERING_API UClass* Z_Construct_UClass_UHoverable_NoRegister();
	TOWERS_API UClass* Z_Construct_UClass_UTowerTargetComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Minions();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics
	{
		struct _Script_Minions_eventOnMinionDiedSignature_Parms
		{
			EMinionType MinionType;
			AActor* MinionOwner;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_MinionType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_MinionType;
		static const UECodeGen_Private::FObjectPropertyParams NewProp_MinionOwner;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::NewProp_MinionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::NewProp_MinionType = { "MinionType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Minions_eventOnMinionDiedSignature_Parms, MinionType), Z_Construct_UEnum_Minions_EMinionType, METADATA_PARAMS(nullptr, 0) }; // 3979814491
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::NewProp_MinionOwner = { "MinionOwner", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(_Script_Minions_eventOnMinionDiedSignature_Parms, MinionOwner), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::NewProp_MinionType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::NewProp_MinionType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::NewProp_MinionOwner,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Minions, nullptr, "OnMinionDiedSignature__DelegateSignature", nullptr, nullptr, sizeof(Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::_Script_Minions_eventOnMinionDiedSignature_Parms), Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
void FOnMinionDiedSignature_DelegateWrapper(const FMulticastScriptDelegate& OnMinionDiedSignature, EMinionType MinionType, AActor* MinionOwner)
{
	struct _Script_Minions_eventOnMinionDiedSignature_Parms
	{
		EMinionType MinionType;
		AActor* MinionOwner;
	};
	_Script_Minions_eventOnMinionDiedSignature_Parms Parms;
	Parms.MinionType=MinionType;
	Parms.MinionOwner=MinionOwner;
	OnMinionDiedSignature.ProcessMulticastDelegate<UObject>(&Parms);
}
	DEFINE_FUNCTION(AMinion::execOnHoverEnd)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHoverEnd_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMinion::execOnHoverBegin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnHoverBegin_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMinion::execDie)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Die();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMinion::execGetResist)
	{
		P_GET_ENUM(EElementalDamageType,Z_Param_InElementalType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetResist(EElementalDamageType(Z_Param_InElementalType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMinion::execSetEnemy)
	{
		P_GET_OBJECT(APlayerController,Z_Param_Player);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnemy(Z_Param_Player);
		P_NATIVE_END;
	}
	static FName NAME_AMinion_OnHoverBegin = FName(TEXT("OnHoverBegin"));
	void AMinion::OnHoverBegin()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMinion_OnHoverBegin),NULL);
	}
	static FName NAME_AMinion_OnHoverEnd = FName(TEXT("OnHoverEnd"));
	void AMinion::OnHoverEnd()
	{
		ProcessEvent(FindFunctionChecked(NAME_AMinion_OnHoverEnd),NULL);
	}
	void AMinion::StaticRegisterNativesAMinion()
	{
		UClass* Class = AMinion::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Die", &AMinion::execDie },
			{ "GetResist", &AMinion::execGetResist },
			{ "OnHoverBegin", &AMinion::execOnHoverBegin },
			{ "OnHoverEnd", &AMinion::execOnHoverEnd },
			{ "SetEnemy", &AMinion::execSetEnemy },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMinion_Die_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMinion_Die_Statics::Function_MetaDataParams[] = {
		{ "Category", "Minion|Damage" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMinion_Die_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMinion, nullptr, "Die", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMinion_Die_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_Die_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMinion_Die()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMinion_Die_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMinion_GetResist_Statics
	{
		struct Minion_eventGetResist_Parms
		{
			EElementalDamageType InElementalType;
			float ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_InElementalType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InElementalType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_InElementalType;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType = { "InElementalType", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Minion_eventGetResist_Parms, InElementalType), Z_Construct_UEnum_ElementalDamage_EElementalDamageType, METADATA_PARAMS(Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType_MetaData)) }; // 1545068981
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Minion_eventGetResist_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMinion_GetResist_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_InElementalType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMinion_GetResist_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMinion_GetResist_Statics::Function_MetaDataParams[] = {
		{ "Category", "Minion|Damage" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMinion_GetResist_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMinion, nullptr, "GetResist", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMinion_GetResist_Statics::Minion_eventGetResist_Parms), Z_Construct_UFunction_AMinion_GetResist_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_GetResist_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMinion_GetResist_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_GetResist_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMinion_GetResist()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMinion_GetResist_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMinion_OnHoverBegin_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMinion_OnHoverBegin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMinion_OnHoverBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMinion, nullptr, "OnHoverBegin", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMinion_OnHoverBegin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_OnHoverBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMinion_OnHoverBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMinion_OnHoverBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMinion_OnHoverEnd_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMinion_OnHoverEnd_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMinion_OnHoverEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMinion, nullptr, "OnHoverEnd", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMinion_OnHoverEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_OnHoverEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMinion_OnHoverEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMinion_OnHoverEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMinion_SetEnemy_Statics
	{
		struct Minion_eventSetEnemy_Parms
		{
			APlayerController* Player;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Player;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMinion_SetEnemy_Statics::NewProp_Player = { "Player", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(Minion_eventSetEnemy_Parms, Player), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMinion_SetEnemy_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMinion_SetEnemy_Statics::NewProp_Player,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMinion_SetEnemy_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Category", "Minion|Damage" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AMinion_SetEnemy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMinion, nullptr, "SetEnemy", nullptr, nullptr, sizeof(Z_Construct_UFunction_AMinion_SetEnemy_Statics::Minion_eventSetEnemy_Parms), Z_Construct_UFunction_AMinion_SetEnemy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_SetEnemy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMinion_SetEnemy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMinion_SetEnemy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMinion_SetEnemy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AMinion_SetEnemy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AMinion);
	UClass* Z_Construct_UClass_AMinion_NoRegister()
	{
		return AMinion::StaticClass();
	}
	struct Z_Construct_UClass_AMinion_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_HealthComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_HealthComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TowerTargetComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_TowerTargetComponent;
		static const UECodeGen_Private::FBytePropertyParams NewProp_MinionType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_MinionType;
		static const UECodeGen_Private::FBytePropertyParams NewProp_MinionClass_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionClass_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_MinionClass;
		static const UECodeGen_Private::FBytePropertyParams NewProp_Armor_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Armor_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_Armor;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MinionClassToResist_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_MinionClassToResist;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnMinionDied_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMinionDied;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Enemy_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Enemy;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMinion_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Minions,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMinion_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMinion_Die, "Die" }, // 3137872191
		{ &Z_Construct_UFunction_AMinion_GetResist, "GetResist" }, // 1864964612
		{ &Z_Construct_UFunction_AMinion_OnHoverBegin, "OnHoverBegin" }, // 1376990295
		{ &Z_Construct_UFunction_AMinion_OnHoverEnd, "OnHoverEnd" }, // 2792189467
		{ &Z_Construct_UFunction_AMinion_SetEnemy, "SetEnemy" }, // 3154689054
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Minions/Minion.h" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_HealthComponent_MetaData[] = {
		{ "Category", "Minion|Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_HealthComponent = { "HealthComponent", nullptr, (EPropertyFlags)0x001400000009003d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, HealthComponent), Z_Construct_UClass_UHealthComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_HealthComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_HealthComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_TowerTargetComponent_MetaData[] = {
		{ "Category", "Minion|Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_TowerTargetComponent = { "TowerTargetComponent", nullptr, (EPropertyFlags)0x001400000009001d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, TowerTargetComponent), Z_Construct_UClass_UTowerTargetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_TowerTargetComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_TowerTargetComponent_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_MinionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_MinionType_MetaData[] = {
		{ "Category", "Minion" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_MinionType = { "MinionType", nullptr, (EPropertyFlags)0x0010000000010001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, MinionType), Z_Construct_UEnum_Minions_EMinionType, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_MinionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_MinionType_MetaData)) }; // 3979814491
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass_MetaData[] = {
		{ "Category", "Minion" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass = { "MinionClass", nullptr, (EPropertyFlags)0x0010000000010001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, MinionClass), Z_Construct_UEnum_Minions_EMinionClass, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass_MetaData)) }; // 814396269
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_Armor_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_Armor_MetaData[] = {
		{ "Category", "Minion" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_Armor = { "Armor", nullptr, (EPropertyFlags)0x0010000000010001, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, Armor), Z_Construct_UEnum_Minions_EArmor, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_Armor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_Armor_MetaData)) }; // 2443793846
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_MinionClassToResist_MetaData[] = {
		{ "Category", "Minion" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
		{ "RowType", "ResistanceForArmor" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_MinionClassToResist = { "MinionClassToResist", nullptr, (EPropertyFlags)0x0010000000010001, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, MinionClassToResist), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_MinionClassToResist_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_MinionClassToResist_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_OnMinionDied_MetaData[] = {
		{ "Category", "Minion|Damage" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_OnMinionDied = { "OnMinionDied", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, OnMinionDied), Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_OnMinionDied_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_OnMinionDied_MetaData)) }; // 1326915486
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinion_Statics::NewProp_Enemy_MetaData[] = {
		{ "BlueprintSetter", "SetEnemy" },
		{ "Category", "Minion" },
		{ "ModuleRelativePath", "Public/Minions/Minion.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMinion_Statics::NewProp_Enemy = { "Enemy", nullptr, (EPropertyFlags)0x0020080000020005, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(AMinion, Enemy), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::NewProp_Enemy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::NewProp_Enemy_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMinion_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_HealthComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_TowerTargetComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_MinionType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_MinionType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_MinionClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_Armor_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_Armor,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_MinionClassToResist,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_OnMinionDied,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinion_Statics::NewProp_Enemy,
	};
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AMinion_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UHoverable_NoRegister, (int32)VTABLE_OFFSET(AMinion, IHoverable), false },  // 4226604227
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMinion_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMinion>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AMinion_Statics::ClassParams = {
		&AMinion::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMinion_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMinion_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMinion_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMinion()
	{
		if (!Z_Registration_Info_UClass_AMinion.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AMinion.OuterSingleton, Z_Construct_UClass_AMinion_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AMinion.OuterSingleton;
	}
	template<> MINIONS_API UClass* StaticClass<AMinion>()
	{
		return AMinion::StaticClass();
	}

	void AMinion::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_HealthComponent(TEXT("HealthComponent"));

		const bool bIsValid = true
			&& Name_HealthComponent == ClassReps[(int32)ENetFields_Private::HealthComponent].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in AMinion"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMinion);
	AMinion::~AMinion() {}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AMinion, AMinion::StaticClass, TEXT("AMinion"), &Z_Registration_Info_UClass_AMinion, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AMinion), 2251609006U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_558493716(TEXT("/Script/Minions"),
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
