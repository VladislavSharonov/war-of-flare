// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Minions/MinionType.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONS_MinionType_generated_h
#error "MinionType.generated.h already included, missing '#pragma once' in MinionType.h"
#endif
#define MINIONS_MinionType_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionType_h


#define FOREACH_ENUM_EMINIONTYPE(op) \
	op(EMinionType::None) \
	op(EMinionType::Stormtrooper) \
	op(EMinionType::Landspeeder) \
	op(EMinionType::Mechanoid) 

enum class EMinionType : uint8;
template<> struct TIsUEnumClass<EMinionType> { enum { Value = true }; };
template<> MINIONS_API UEnum* StaticEnum<EMinionType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
