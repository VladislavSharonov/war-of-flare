// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Minions/ResistanceForArmor.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONS_ResistanceForArmor_generated_h
#error "ResistanceForArmor.generated.h already included, missing '#pragma once' in ResistanceForArmor.h"
#endif
#define MINIONS_ResistanceForArmor_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FResistanceForArmor_Statics; \
	MINIONS_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> MINIONS_API UScriptStruct* StaticStruct<struct FResistanceForArmor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_ResistanceForArmor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
