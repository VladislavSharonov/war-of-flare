// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Minions/Minion.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class APlayerController;
enum class EElementalDamageType : uint8;
enum class EMinionType : uint8;
#ifdef MINIONS_Minion_generated_h
#error "Minion.generated.h already included, missing '#pragma once' in Minion.h"
#endif
#define MINIONS_Minion_generated_h

#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_21_DELEGATE \
MINIONS_API void FOnMinionDiedSignature_DelegateWrapper(const FMulticastScriptDelegate& OnMinionDiedSignature, EMinionType MinionType, AActor* MinionOwner);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_SPARSE_DATA
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_RPC_WRAPPERS \
	virtual void OnHoverEnd_Implementation(); \
	virtual void OnHoverBegin_Implementation(); \
 \
	DECLARE_FUNCTION(execOnHoverEnd); \
	DECLARE_FUNCTION(execOnHoverBegin); \
	DECLARE_FUNCTION(execDie); \
	DECLARE_FUNCTION(execGetResist); \
	DECLARE_FUNCTION(execSetEnemy);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHoverEnd); \
	DECLARE_FUNCTION(execOnHoverBegin); \
	DECLARE_FUNCTION(execDie); \
	DECLARE_FUNCTION(execGetResist); \
	DECLARE_FUNCTION(execSetEnemy);


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_ACCESSORS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_CALLBACK_WRAPPERS
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMinion(); \
	friend struct Z_Construct_UClass_AMinion_Statics; \
public: \
	DECLARE_CLASS(AMinion, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Minions"), NO_API) \
	DECLARE_SERIALIZER(AMinion) \
	virtual UObject* _getUObject() const override { return const_cast<AMinion*>(this); } \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		HealthComponent=NETFIELD_REP_START, \
		NETFIELD_REP_END=HealthComponent	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_INCLASS \
private: \
	static void StaticRegisterNativesAMinion(); \
	friend struct Z_Construct_UClass_AMinion_Statics; \
public: \
	DECLARE_CLASS(AMinion, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Minions"), NO_API) \
	DECLARE_SERIALIZER(AMinion) \
	virtual UObject* _getUObject() const override { return const_cast<AMinion*>(this); } \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		HealthComponent=NETFIELD_REP_START, \
		NETFIELD_REP_END=HealthComponent	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMinion(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMinion) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMinion); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMinion); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMinion(AMinion&&); \
	NO_API AMinion(const AMinion&); \
public: \
	NO_API virtual ~AMinion();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMinion(AMinion&&); \
	NO_API AMinion(const AMinion&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMinion); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMinion); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMinion) \
	NO_API virtual ~AMinion();


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_23_PROLOG
#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_RPC_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_INCLASS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_SPARSE_DATA \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_ACCESSORS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_CALLBACK_WRAPPERS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_INCLASS_NO_PURE_DECLS \
	FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MINIONS_API UClass* StaticClass<class AMinion>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Minion_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
