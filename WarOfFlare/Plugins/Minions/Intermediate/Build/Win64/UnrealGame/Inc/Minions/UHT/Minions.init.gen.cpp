// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinions_init() {}
	MINIONS_API UFunction* Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_Minions;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_Minions()
	{
		if (!Z_Registration_Info_UPackage__Script_Minions.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_Minions_OnMinionDiedSignature__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/Minions",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x22F07E11,
				0x734E398D,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_Minions.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_Minions.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_Minions(Z_Construct_UPackage__Script_Minions, TEXT("/Script/Minions"), Z_Registration_Info_UPackage__Script_Minions, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x22F07E11, 0x734E398D));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
