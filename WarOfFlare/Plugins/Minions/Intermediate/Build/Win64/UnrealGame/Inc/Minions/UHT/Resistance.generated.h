// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Minions/Resistance.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONS_Resistance_generated_h
#error "Resistance.generated.h already included, missing '#pragma once' in Resistance.h"
#endif
#define MINIONS_Resistance_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Resistance_h


#define FOREACH_ENUM_ERESISTANCE(op) \
	op(EResistance::None) \
	op(EResistance::Vulnerable) \
	op(EResistance::Resistant) 

enum class EResistance : uint8;
template<> struct TIsUEnumClass<EResistance> { enum { Value = true }; };
template<> MINIONS_API UEnum* StaticEnum<EResistance>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
