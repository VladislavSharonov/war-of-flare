// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "Minions/Armor.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MINIONS_Armor_generated_h
#error "Armor.generated.h already included, missing '#pragma once' in Armor.h"
#endif
#define MINIONS_Armor_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Armor_h


#define FOREACH_ENUM_EARMOR(op) \
	op(EArmor::NoArmor) \
	op(EArmor::LightArmor) \
	op(EArmor::MediumArmor) \
	op(EArmor::HeavyArmor) 

enum class EArmor : uint8;
template<> struct TIsUEnumClass<EArmor> { enum { Value = true }; };
template<> MINIONS_API UEnum* StaticEnum<EArmor>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
