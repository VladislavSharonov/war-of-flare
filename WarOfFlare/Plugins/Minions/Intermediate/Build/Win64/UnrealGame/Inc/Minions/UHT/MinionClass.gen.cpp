// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Minions/Public/Minions/MinionClass.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionClass() {}
// Cross Module References
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EMinionClass();
	UPackage* Z_Construct_UPackage__Script_Minions();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EMinionClass;
	static UEnum* EMinionClass_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EMinionClass.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EMinionClass.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_Minions_EMinionClass, (UObject*)Z_Construct_UPackage__Script_Minions(), TEXT("EMinionClass"));
		}
		return Z_Registration_Info_UEnum_EMinionClass.OuterSingleton;
	}
	template<> MINIONS_API UEnum* StaticEnum<EMinionClass>()
	{
		return EMinionClass_StaticEnum();
	}
	struct Z_Construct_UEnum_Minions_EMinionClass_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_Minions_EMinionClass_Statics::Enumerators[] = {
		{ "EMinionClass::Infantry", (int64)EMinionClass::Infantry },
		{ "EMinionClass::Vehicle", (int64)EMinionClass::Vehicle },
		{ "EMinionClass::Mutants", (int64)EMinionClass::Mutants },
		{ "EMinionClass::Robots", (int64)EMinionClass::Robots },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_Minions_EMinionClass_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Infantry.DisplayName", "Infantry" },
		{ "Infantry.Name", "EMinionClass::Infantry" },
		{ "ModuleRelativePath", "Public/Minions/MinionClass.h" },
		{ "Mutants.DisplayName", "Mutants" },
		{ "Mutants.Name", "EMinionClass::Mutants" },
		{ "Robots.DisplayName", "Robots" },
		{ "Robots.Name", "EMinionClass::Robots" },
		{ "Vehicle.DisplayName", "Vehicle" },
		{ "Vehicle.Name", "EMinionClass::Vehicle" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_Minions_EMinionClass_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_Minions,
		nullptr,
		"EMinionClass",
		"EMinionClass",
		Z_Construct_UEnum_Minions_EMinionClass_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_Minions_EMinionClass_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_Minions_EMinionClass_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_Minions_EMinionClass_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_Minions_EMinionClass()
	{
		if (!Z_Registration_Info_UEnum_EMinionClass.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EMinionClass.InnerSingleton, Z_Construct_UEnum_Minions_EMinionClass_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EMinionClass.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionClass_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionClass_h_Statics::EnumInfo[] = {
		{ EMinionClass_StaticEnum, TEXT("EMinionClass"), &Z_Registration_Info_UEnum_EMinionClass, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 814396269U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionClass_h_2165383223(TEXT("/Script/Minions"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionClass_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_MinionClass_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
