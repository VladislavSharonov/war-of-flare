// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Minions/Public/Minions/Armor.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArmor() {}
// Cross Module References
	MINIONS_API UEnum* Z_Construct_UEnum_Minions_EArmor();
	UPackage* Z_Construct_UPackage__Script_Minions();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EArmor;
	static UEnum* EArmor_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EArmor.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EArmor.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_Minions_EArmor, (UObject*)Z_Construct_UPackage__Script_Minions(), TEXT("EArmor"));
		}
		return Z_Registration_Info_UEnum_EArmor.OuterSingleton;
	}
	template<> MINIONS_API UEnum* StaticEnum<EArmor>()
	{
		return EArmor_StaticEnum();
	}
	struct Z_Construct_UEnum_Minions_EArmor_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_Minions_EArmor_Statics::Enumerators[] = {
		{ "EArmor::NoArmor", (int64)EArmor::NoArmor },
		{ "EArmor::LightArmor", (int64)EArmor::LightArmor },
		{ "EArmor::MediumArmor", (int64)EArmor::MediumArmor },
		{ "EArmor::HeavyArmor", (int64)EArmor::HeavyArmor },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_Minions_EArmor_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HeavyArmor.DisplayName", "HeavyArmor" },
		{ "HeavyArmor.Name", "EArmor::HeavyArmor" },
		{ "LightArmor.DisplayName", "LightArmor" },
		{ "LightArmor.Name", "EArmor::LightArmor" },
		{ "MediumArmor.DisplayName", "MediumArmor" },
		{ "MediumArmor.Name", "EArmor::MediumArmor" },
		{ "ModuleRelativePath", "Public/Minions/Armor.h" },
		{ "NoArmor.DisplayName", "NoArmor" },
		{ "NoArmor.Name", "EArmor::NoArmor" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_Minions_EArmor_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_Minions,
		nullptr,
		"EArmor",
		"EArmor",
		Z_Construct_UEnum_Minions_EArmor_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_Minions_EArmor_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_Minions_EArmor_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_Minions_EArmor_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_Minions_EArmor()
	{
		if (!Z_Registration_Info_UEnum_EArmor.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EArmor.InnerSingleton, Z_Construct_UEnum_Minions_EArmor_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EArmor.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Armor_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Armor_h_Statics::EnumInfo[] = {
		{ EArmor_StaticEnum, TEXT("EArmor"), &Z_Registration_Info_UEnum_EArmor, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 2443793846U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Armor_h_246304810(TEXT("/Script/Minions"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Armor_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Projects_WarOfFlare_WarOfFlare_Plugins_Minions_Source_Minions_Public_Minions_Armor_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
